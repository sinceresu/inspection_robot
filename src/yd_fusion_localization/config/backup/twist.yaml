map_frame: map
robot_frame: lidar_pose

frontend:
  update_interval_time: 0.1
  update_delay_time: 1.5
  max_vel: 2
  max_gyro: 2
  max_std_deviation: [0.5e6, 0.5e6, 0.5e6, 0.2e6, 0.2e6, 0.2e6]
  inlier_check: true

  initialpose:
    topic: /initialpose
    buffer_size: 5
    buffer_time: 0.5
    z: -134.5

  historypose:
    interval_time: 10
    interval_distance: 0.2
    interval_angle: 0.1
    save_num: 2

  diagnostic_publisher:
    topic: /localization/heartbeat
    hardware_id: 2
    node_id: 21
    buffer_size: 10

  pose_publisher:
    type: Odometry # Odometry Pose
    topic: /robot_pose
    buffer_size: 100

  predictors:
    number: 1
    no_1:
      type: Twist
      topic: /yida/wheel/odometer
      sensor_to_robot: [0, 0, 0, 0, 0, 0]
      sensor_frame: lidar_pose
      buffer_size: 100
      buffer_time: 150
      noise: [0.2, 0.2, 0.2, 0.5, 0.5, 0.5]
      vel_coef: 0.99
      gyro_coef: 0.973
      max_vel: 2
      max_gyro: 2

backend:
  interval_time: 21
  interval_distance: 10
  interval_angle: 30
  interval_predict_time: 1e6
  interval_predict_distance: 1e6
  interval_predict_angle: 1e6
  init_interval_time: 0.2
  init_delay_ltime: 3
  init_wait_ptime: 1
  predictor_delay_time: 1.5
  measurer_delay_time: 1.5
  predictor_wait_time: 0.5
  measurer_wait_time: 0.5
  state_buffer_time: 15
  save: false
  history_loop: true
  loop_time_thresh: 10.0
  localizer_thresh: [120e6, 0.5e6, 1e6]
  std_deviation_thresh: [0.5e6, 0.5e6, 0.5e6, 0.2e6, 0.2e6, 0.2e6]

  measurers:
    number: 1
    no_1:
      subscriber:
        type: Cloud
        topic: /velodyne_points
        sensor_to_robot: [0, 0, 0, 0, 0, 0]
        sensor_frame: laser_frame
        buffer_size: 100
        buffer_time: 15
        noise: [0.2e6, 0.2e6, 0.2e6, 0.1e6, 0.1e6, 0.1e6]
        cloud_fields: XYZT
      localizer:
        # local_to_map: [0, 0, 0, 0, 0, 0]
        local_frame: map
        global_map:
          map_path: map/demo2.0_0.05.pcd
          filter:
            method: voxel_filter
            leaf_size: [0.1, 0.1, 0.1]
        local_map:
          use_box: false
          # box_filter:
          #   size: [-70.0, 130.0, -100.0, 100.0, -10.0, 100.0]
          #   origin: [140, 100, 2914.7]
          # edge_distance: [50, 50, 5]
        frame:
          filter:
            method: approximate_voxel_filter
            leaf_size: [0.2, 0.2, 0.2]
          distance_threshold: [1.0, 150]
          scan_number: 16
          scan_period: 0.1
        registration:
          method: NDTOMP # NDTOMP HTTP
          # url: http://192.168.1.74:8088/
          res: 1
          step_size: 0.1
          trans_eps: 0.01
          max_iter: 10
          neighborhood_search_method: DIRECT7
          num_threads: 8
          match_threshold: 2e5
          init_match_threshold: 2e5
          # invalid_ranges: [1, 2, 3, 4, 5, 6]
          distance_threshold: 5e6
          angle_threshold: 0.5e6
          undistort: true
        scan_context:
          sc_loop: false
          sc_save: false
          sc_replace_old: 2
          sc_dis_thresh: 0.1
          sc_angle_thresh: 0.2
          loop_key_size: 10
          sc_loop_path: loop
          sc_save_path: save
          sc_registration:
            method: NDTOMP # NDTOMP HTTP
            res: 1
            step_size: 0.1
            trans_eps: 0.01
            max_iter: 10
            neighborhood_search_method: DIRECT7
            num_threads: 8
            match_threshold: 0.5
            match_distance_threshold: 2
            match_angle_threshold: 0.3
          params:
            LIDAR_HEIGHT: 0.6
            PC_NUM_RING: 20
            PC_NUM_SECTOR: 60
            PC_MAX_RADIUS: 80.0
            NUM_EXCLUDE_RECENT: 50
            NUM_CANDIDATES_FROM_TREE: 10
            SEARCH_RATIO: 0.1
            SC_DIST_THRES: 0.18
            TREE_MAKING_PERIOD: 10
      publisher:
        global_map:
          type: Cloud
          topic: /global_map
          frame_id: map
          buffer_size: 1
          latch: true
        local_map:
          type: Cloud
          topic: /local_map
          frame_id: map
          buffer_size: 1
          latch: true
        current_scan:
          type: Cloud
          topic: /current_scan
          buffer_size: 5
          frame_id: map

  fusion:
    type: fusion_graph
    graph:
      graph_optimizer_type: g2o
      g2o:
        state_type: Basic
        window_size: 10
        max_state_num: 100
        max_iterations_num: 100
        check_chi2: 10
        check_predictor: [0.4, 0.4, 0.4, 0.2, 0.2, 0.2]
        check_localizer: [2e6, 2e6, 2e6, 0.2e6, 0.2e6, 0.2e6]
        predictor_outlier_chi2: 1
        localizer_outlier_chi2: 10
        init_window_size: 2
        marge_info: false
        compute_marginals: true
        solver_type: lm_var_cholmod #lm_var_cholmod lm_var
        robust_kernel_name: Huber
        robust_kernel_delta: 0.5