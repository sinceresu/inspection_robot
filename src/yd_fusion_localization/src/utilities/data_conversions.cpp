/**
 * @file data_conversions.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-19
 * @brief data conversions
 */

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    double ClampRotation(double rotation)
    {
        while (rotation > PI)
        {
            rotation -= TAU;
        }

        while (rotation < -PI)
        {
            rotation += TAU;
        }

        return rotation;
    }

    Sophus::SE3d Interpolate(double stamp, double start_stamp, double end_stamp,
                             const Sophus::SE3d &start_pose, const Sophus::SE3d &end_pose)
    {
        assert(end_stamp > start_stamp);
        double u = (stamp - start_stamp) / (end_stamp - start_stamp);
        if (u < 0)
        {
            return start_pose;
        }
        else if (u > 1)
        {
            return end_pose;
        }
        else
        {
            return Sophus::interpolate(start_pose, end_pose, u);
        }
    }

    Eigen::Vector3d Interpolate(double stamp, double start_stamp, double end_stamp,
                                const Eigen::Vector3d &start_p, const Eigen::Vector3d &end_p)
    {
        assert(end_stamp > start_stamp);
        double u = (stamp - start_stamp) / (end_stamp - start_stamp);
        if (u < 0)
        {
            return start_p;
        }
        else if (u > 1)
        {
            return end_p;
        }
        else
        {
            return (1.0 - u) * start_p + u * end_p;
        }
    }

    Eigen::Quaterniond Interpolate(double stamp, double start_stamp, double end_stamp,
                                   const Eigen::Quaterniond &start_q, const Eigen::Quaterniond &end_q)
    {
        assert(end_stamp > start_stamp);
        double u = (stamp - start_stamp) / (end_stamp - start_stamp);
        if (u < 0)
        {
            return start_q;
        }
        else if (u > 1)
        {
            return end_q;
        }
        else
        {
            return start_q.slerp(u, end_q);
        }
    }

    State Interpolate(double stamp, const State &start_state, const State &end_state)
    {
        assert(end_state.stamp > start_state.stamp);
        double u = (stamp - start_state.stamp) / (end_state.stamp - start_state.stamp);
        if (u < 0)
        {
            return start_state;
        }
        else if (u > 1)
        {
            return end_state;
        }
        else
        {
            State state;
            state.stamp = stamp;
            state.p = (1.0 - u) * start_state.p + u * end_state.p;
            state.q = start_state.q.slerp(u, end_state.q);
            state.v = (1.0 - u) * start_state.v + u * end_state.v;
            state.ba = (1.0 - u) * start_state.ba + u * end_state.ba;
            state.bg = (1.0 - u) * start_state.bg + u * end_state.bg;
            return state;
        }
    }

    void CopyCovariance(const Eigen::Matrix<double, 6, 6> &covariance, double *arr)
    {
        for (size_t i = 0; i < 6; i++)
        {
            for (size_t j = 0; j < 6; j++)
            {
                arr[6 * i + j] = covariance(i, j);
            }
        }
    }

    bool GetTransform(const YAML::Node &yaml_node, Sophus::SE3d &transform)
    {
        if (!yaml_node.IsDefined())
        {
            transform = Sophus::SE3d();
            return false;
        }
        if (!yaml_node.IsSequence())
        {
            transform = Sophus::SE3d();
            return false;
        }
        std::vector<double> pose = yaml_node.as<std::vector<double>>();
        return GetTransform(pose, transform);
    }

    bool GetTransform(const std::vector<double> &pose, Sophus::SE3d &transform)
    {
        if (pose.size() == 6)
        {
            Eigen::Vector3d translation(pose[0], pose[1], pose[2]);
            Eigen::AngleAxisd roll(pose[3], Eigen::Vector3d::UnitX());
            Eigen::AngleAxisd pitch(pose[4], Eigen::Vector3d::UnitY());
            Eigen::AngleAxisd yaw(pose[5], Eigen::Vector3d::UnitZ());
            transform.translation() = translation;
            transform.setQuaternion(Eigen::Quaterniond(yaw * pitch * roll));
        }
        else if (pose.size() == 7)
        {
            Eigen::Vector3d translation(pose[0], pose[1], pose[2]);
            Eigen::Quaterniond q(pose[3], pose[4], pose[5], pose[6]);
            if (q.norm() < 0.1)
            {
                q = Eigen::Quaterniond(1, 0, 0, 0);
            }
            q.normalize();
            transform.translation() = translation;
            transform.setQuaternion(q);
        }
        else if (pose.size() == 16)
        {
            Eigen::Matrix4d mat;
            mat << pose[0], pose[1], pose[2], pose[3],
                pose[4], pose[5], pose[6], pose[7],
                pose[8], pose[9], pose[10], pose[11],
                pose[12], pose[13], pose[14], pose[15];
            transform = Sophus::SE3d(mat);
        }
        else
        {
            LOG(ERROR) << "transform size is " << pose.size() << "! Only support 6, 7, or 16!";
            return false;
        }
        return true;
    }

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, Sophus::SE3d &data)
    {
        data = local_to_map * data * sensor_to_robot.inverse();
    }

    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, Sophus::SE3d &data)
    {
        data = local_to_map.inverse() * data * sensor_to_robot;
    }

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, MState &data)
    {
        Sophus::SE3d temp = sensor_to_robot.inverse();
        data.pose = local_to_map * data.pose * temp;
        Eigen::Vector3d t = temp.translation();
        Eigen::Quaterniond q = temp.unit_quaternion().inverse();
        data.linear_velocity = q * (data.angular_velocity.cross(t) + data.linear_velocity);
        data.angular_velocity = q * data.angular_velocity;
    }
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, MState &data)
    {
        data.pose = local_to_map.inverse() * data.pose * sensor_to_robot;
        Eigen::Vector3d t = sensor_to_robot.translation();
        Eigen::Quaterniond q = sensor_to_robot.unit_quaternion().inverse();
        data.linear_velocity = q * (data.angular_velocity.cross(t) + data.linear_velocity);
        data.angular_velocity = q * data.angular_velocity;
    }

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, BState &data)
    {
        Sophus::SE3d temp = sensor_to_robot.inverse();
        Eigen::Vector3f t = temp.translation().cast<float>();
        Eigen::Quaternionf q = temp.unit_quaternion().inverse().cast<float>();
        data.linear_velocity = q * (data.angular_velocity.cross(t) + data.linear_velocity);
        data.angular_velocity = q * data.angular_velocity;
    }
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, BState &data)
    {
        Eigen::Vector3f t = sensor_to_robot.translation().cast<float>();
        Eigen::Quaternionf q = sensor_to_robot.unit_quaternion().inverse().cast<float>();
        data.linear_velocity = q * (data.angular_velocity.cross(t) + data.linear_velocity);
        data.angular_velocity = q * data.angular_velocity;
    }

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, IState &data)
    {
        Sophus::SE3d temp = sensor_to_robot.inverse();
        data.pose = local_to_map * data.pose * temp;
    }
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, IState &data)
    {
        data.pose = local_to_map.inverse() * data.pose * sensor_to_robot;
    }

    void TransformDataType(const geometry_msgs::Point &m, Eigen::Vector3d &s)
    {
        tf::pointMsgToEigen(m, s);
    }

    void TransformDataType(const Eigen::Vector3d &s, geometry_msgs::Point &m)
    {
        tf::pointEigenToMsg(s, m);
    }

    void TransformDataType(const geometry_msgs::Vector3 &m, Eigen::Vector3d &s)
    {
        tf::vectorMsgToEigen(m, s);
    }

    void TransformDataType(const Eigen::Vector3d &s, geometry_msgs::Vector3 &m)
    {
        tf::vectorEigenToMsg(s, m);
    }

    void TransformDataType(const geometry_msgs::Quaternion &m, Eigen::Quaterniond &s)
    {
        tf::quaternionMsgToEigen(m, s);
    }
    void TransformDataType(const Eigen::Quaterniond &s, geometry_msgs::Quaternion &m)
    {
        tf::quaternionEigenToMsg(s, m);
    }

    void TransformDataType(const geometry_msgs::Pose &m, Sophus::SE3d &s)
    {
        tf::pointMsgToEigen(m.position, s.translation());
        Eigen::Quaterniond e;
        tf::quaternionMsgToEigen(m.orientation, e);
        if (e.norm() < 0.1)
        {
            e = Eigen::Quaterniond(1, 0, 0, 0);
        }
        e.normalize();
        s.setQuaternion(e);
    }

    void TransformDataType(const Sophus::SE3d &s, geometry_msgs::Pose &m)
    {
        tf::pointEigenToMsg(s.translation(), m.position);
        tf::quaternionEigenToMsg(s.unit_quaternion(), m.orientation);
    }

    void TransformDataType(const tf::Transform &m, Sophus::SE3d &s)
    {
        Eigen::Isometry3d e;
        tf::transformTFToEigen(m, e);
        s = Sophus::SE3d(e.matrix());
    }

    void TransformDataType(const Sophus::SE3d &s, tf::Transform &m)
    {
        tf::transformEigenToTF(Eigen::Isometry3d(s.matrix()), m);
    }

    void TransformDataType(const Eigen::Matrix4d &e, Sophus::SE3d &s)
    {
        s.translation() = e.block<3, 1>(0, 3);
        Eigen::Quaterniond q(e.block<3, 3>(0, 0));
        if (q.norm() < 0.1)
        {
            q = Eigen::Quaterniond(1, 0, 0, 0);
        }
        q.normalize();
        s.setQuaternion(q);
    }
    void TransformDataType(const Sophus::SE3d &s, Eigen::Matrix4d &e)
    {
        e = s.matrix();
    }

    bool CopyPredictData(const PredictDataPtr &src, PredictDataPtr &dest)
    {
        if (src->GetType() == dest->GetType())
        {
            if (src->GetType() == PredictType::Imu)
            {
                ImuDataPtr src_tmp = std::dynamic_pointer_cast<ImuData>(src);
                ImuDataPtr dest_tmp = std::dynamic_pointer_cast<ImuData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
            else if (src->GetType() == PredictType::Twist)
            {
                TwistDataPtr src_tmp = std::dynamic_pointer_cast<TwistData>(src);
                TwistDataPtr dest_tmp = std::dynamic_pointer_cast<TwistData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
            else if (src->GetType() == PredictType::Odometry)
            {
                OdometryDataPtr src_tmp = std::dynamic_pointer_cast<OdometryData>(src);
                OdometryDataPtr dest_tmp = std::dynamic_pointer_cast<OdometryData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
        }
        return false;
    }

    bool CopyMeasureData(const MeasureDataPtr &src, MeasureDataPtr &dest)
    {
        if (src->GetType() == dest->GetType())
        {
            if (src->GetType() == MeasureType::CloudM)
            {
                CloudDataPtr src_tmp = std::dynamic_pointer_cast<CloudData>(src);
                CloudDataPtr dest_tmp = std::dynamic_pointer_cast<CloudData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
            else if (src->GetType() == MeasureType::GNSSM)
            {
                GNSSDataPtr src_tmp = std::dynamic_pointer_cast<GNSSData>(src);
                GNSSDataPtr dest_tmp = std::dynamic_pointer_cast<GNSSData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
            else if (src->GetType() == MeasureType::ImageM)
            {
                ImageDataPtr src_tmp = std::dynamic_pointer_cast<ImageData>(src);
                ImageDataPtr dest_tmp = std::dynamic_pointer_cast<ImageData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
            else if (src->GetType() == MeasureType::PoseM)
            {
                PoseDataPtr src_tmp = std::dynamic_pointer_cast<PoseData>(src);
                PoseDataPtr dest_tmp = std::dynamic_pointer_cast<PoseData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
        }
        return false;
    }
    bool CopyLocalizerData(const LocalizerDataPtr &src, LocalizerDataPtr &dest)
    {
        if (src->GetType() == dest->GetType())
        {
            if (src->GetType() == LocalizerType::PL)
            {
                PLocalizerDataPtr src_tmp = std::dynamic_pointer_cast<PLocalizerData>(src);
                PLocalizerDataPtr dest_tmp = std::dynamic_pointer_cast<PLocalizerData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
            else if (src->GetType() == LocalizerType::PoseL)
            {
                PoseLocalizerDataPtr src_tmp = std::dynamic_pointer_cast<PoseLocalizerData>(src);
                PoseLocalizerDataPtr dest_tmp = std::dynamic_pointer_cast<PoseLocalizerData>(dest);
                *dest_tmp = *src_tmp;
                return true;
            }
        }
        return false;
    }

    void getTransformFromSe3(const Eigen::Matrix<double, 6, 1> &se3, Eigen::Quaterniond &q, Eigen::Vector3d &t)
    {
        Eigen::Vector3d omega(se3.data());
        Eigen::Vector3d upsilon(se3.data() + 3);
        Eigen::Matrix3d Omega = skewSymmetric(omega);

        double theta = omega.norm();
        double half_theta = 0.5 * theta;

        double imag_factor;
        double real_factor = cos(half_theta);
        if (theta < 1e-10)
        {
            double theta_sq = theta * theta;
            double theta_po4 = theta_sq * theta_sq;
            imag_factor = 0.5 - 0.0208333 * theta_sq + 0.000260417 * theta_po4;
        }
        else
        {
            double sin_half_theta = sin(half_theta);
            imag_factor = sin_half_theta / theta;
        }

        q = Eigen::Quaterniond(real_factor, imag_factor * omega.x(), imag_factor * omega.y(), imag_factor * omega.z());

        Eigen::Matrix3d J;
        if (theta < 1e-10)
        {
            J = q.matrix();
        }
        else
        {
            Eigen::Matrix3d Omega2 = Omega * Omega;
            J = (Eigen::Matrix3d::Identity() + (1 - cos(theta)) / (theta * theta) * Omega + (theta - sin(theta)) / (pow(theta, 3)) * Omega2);
        }

        t = J * upsilon;
    }

    int SymbolToInt(unsigned char c, int j)
    {
        return ((c - 'A') << 26) + j;
    }
    namespace symbol_shorthand
    {
        int A(int j) { return SymbolToInt('A', j); }
        int B(int j) { return SymbolToInt('B', j); }
        int C(int j) { return SymbolToInt('C', j); }
        int D(int j) { return SymbolToInt('D', j); }
        int E(int j) { return SymbolToInt('E', j); }
        int F(int j) { return SymbolToInt('F', j); }
        int G(int j) { return SymbolToInt('G', j); }
        int H(int j) { return SymbolToInt('H', j); }
        int I(int j) { return SymbolToInt('I', j); }
        int J(int j) { return SymbolToInt('J', j); }
        int K(int j) { return SymbolToInt('K', j); }
        int L(int j) { return SymbolToInt('L', j); }
        int M(int j) { return SymbolToInt('M', j); }
        int N(int j) { return SymbolToInt('N', j); }
        int O(int j) { return SymbolToInt('O', j); }
        int P(int j) { return SymbolToInt('P', j); }
        int Q(int j) { return SymbolToInt('Q', j); }
        int R(int j) { return SymbolToInt('R', j); }
        int S(int j) { return SymbolToInt('S', j); }
        int T(int j) { return SymbolToInt('T', j); }
        int U(int j) { return SymbolToInt('U', j); }
        int V(int j) { return SymbolToInt('V', j); }
        int W(int j) { return SymbolToInt('W', j); }
        int X(int j) { return SymbolToInt('X', j); }
        int Y(int j) { return SymbolToInt('Y', j); }
        int Z(int j) { return SymbolToInt('Z', j); }
    }
} // namespace yd_fusion_localization
