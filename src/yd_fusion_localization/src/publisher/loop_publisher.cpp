/**
 * @file loop_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-7-22
 * @brief loop publisher
 */

#include "yd_fusion_localization/publisher/loop_publisher.hpp"

namespace yd_fusion_localization
{
    LoopPublisher::LoopPublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<visualization_msgs::MarkerArray>(topic_name, buff_size, latch);
    }

    void LoopPublisher::Publish(const PublishDataPtr &data)
    {
        LoopPublishDataPtr loop_data = std::dynamic_pointer_cast<LoopPublishData>(data);

        visualization_msgs::Marker markerNode;
        markerNode.header.frame_id = frame_id_;
        markerNode.header.stamp = ros::Time(loop_data->stamp);
        markerNode.action = visualization_msgs::Marker::ADD;
        markerNode.type = visualization_msgs::Marker::SPHERE_LIST;
        markerNode.ns = "loop_nodes";
        markerNode.id = 0;
        markerNode.pose.orientation.w = 1;
        markerNode.scale.x = 0.3; markerNode.scale.y = 0.3; markerNode.scale.z = 0.3; 
        markerNode.color.r = 0; markerNode.color.g = 0.8; markerNode.color.b = 1;
        markerNode.color.a = 1;

        visualization_msgs::Marker markerEdge;
        markerEdge.header.frame_id = frame_id_;
        markerEdge.header.stamp = ros::Time(loop_data->stamp);
        markerEdge.action = visualization_msgs::Marker::ADD;
        markerEdge.type = visualization_msgs::Marker::LINE_LIST;
        markerEdge.ns = "loop_edges";
        markerEdge.id = 1;
        markerEdge.pose.orientation.w = 1;
        markerEdge.scale.x = 0.1;
        markerEdge.color.r = 0.9; markerEdge.color.g = 0.9; markerEdge.color.b = 0;
        markerEdge.color.a = 1;

        geometry_msgs::Point p;
        p.x = loop_data->key_pose_prev.translation().x();
        p.y = loop_data->key_pose_prev.translation().y();
        p.z = loop_data->key_pose_prev.translation().z();
        markerNode.points.push_back(p);
        markerEdge.points.push_back(p);
        p.x = loop_data->key_pose_next.translation().x();
        p.y = loop_data->key_pose_next.translation().y();
        p.z = loop_data->key_pose_next.translation().z();
        markerNode.points.push_back(p);
        markerEdge.points.push_back(p);

        visualization_msgs::MarkerArray markerArray;

        markerArray.markers.push_back(markerNode);
        markerArray.markers.push_back(markerEdge);
        publisher_.publish(markerArray);
    }
} // namespace yd_fusion_localization