/**
 * @file pose_publisher.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-21
 * @brief pose publisher
 */

#include "yd_fusion_localization/publisher/pose_publisher.hpp"

namespace yd_fusion_localization
{
    PosePublisher::PosePublisher(ros::NodeHandle &nh,
                                 const std::string &topic_name,
                                 const std::string &frame_id,
                                 int buff_size,
                                 bool latch) : Publisher(frame_id)
    {
        publisher_ = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>(topic_name, buff_size, latch);
    }

    void PosePublisher::Publish(const PublishDataPtr &data)
    {
        PosePublishDataPtr pose_data = std::dynamic_pointer_cast<PosePublishData>(data);
        geometry_msgs::PoseWithCovarianceStamped pose_publish;

        pose_publish.header.stamp = ros::Time(pose_data->stamp);
        pose_publish.header.frame_id = frame_id_;

        TransformDataType(pose_data->pose, pose_publish.pose.pose);

        CopyCovariance(pose_data->covariance, &(pose_publish.pose.covariance[0]));

        publisher_.publish(pose_publish);
    }
} // namespace yd_fusion_localization