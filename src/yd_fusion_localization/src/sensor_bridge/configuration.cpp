#include "yd_fusion_localization/sensor_bridge/configuration.hpp"

namespace yd_fusion_localization
{
    bool ConfigurePublishers(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &frame_id, std::unordered_map<std::string, std::pair<std::shared_ptr<Publisher>, PublishDataPtr>> &publishers)
    {
        publishers.clear();
        if (!yaml_node.IsDefined())
        {
            return false;
        }
        if (yaml_node["global_map"])
        {
            std::pair<std::shared_ptr<Publisher>, PublishDataPtr> publisher;
            if (ConfigurePublisher(nh, yaml_node["global_map"], frame_id, publisher))
            {
                publishers["global_map"] = publisher;
            }
        }
        if (yaml_node["local_map"])
        {
            std::pair<std::shared_ptr<Publisher>, PublishDataPtr> publisher;
            if (ConfigurePublisher(nh, yaml_node["local_map"], frame_id, publisher))
            {
                publishers["local_map"] = publisher;
            }
        }
        if (yaml_node["current_scan"])
        {
            std::pair<std::shared_ptr<Publisher>, PublishDataPtr> publisher;
            if (ConfigurePublisher(nh, yaml_node["current_scan"], frame_id, publisher))
            {
                publishers["current_scan"] = publisher;
            }
        }

        if (publishers.empty())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool ConfigurePublisher(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &frame_id, std::pair<std::shared_ptr<Publisher>, PublishDataPtr> &publisher)
    {
        if (!yaml_node.IsDefined())
        {
            publisher.first = nullptr;
            LOG(INFO) << "Publisher yaml node not defined!";
            return false;
        }
        std::string type = yaml_node["type"].as<std::string>();
        std::string topic = yaml_node["topic"].as<std::string>();

        int buffer_size = yaml_node["buffer_size"].as<int>();
        bool latch = false;
        if (yaml_node["latch"].IsDefined())
        {
            latch = yaml_node["latch"].as<bool>();
        }

        if (type == "Pose")
        {
            publisher.first = std::make_shared<PosePublisher>(nh, topic, frame_id, buffer_size, latch);
            publisher.second = std::make_shared<PosePublishData>();
            return true;
        }
        else if (type == "Odometry")
        {
            publisher.first = std::make_shared<OdometryPublisher>(nh, topic, frame_id, buffer_size, latch);
            publisher.second = std::make_shared<OdometryPublishData>();
            return true;
        }
        else if (type == "Cloud")
        {
            publisher.first = std::make_shared<CloudPublisher>(nh, topic, frame_id, buffer_size, latch);
            publisher.second = std::make_shared<CloudPublishData>();
            return true;
        }
        else if (type == "CloudXYZI")
        {
            publisher.first = std::make_shared<CloudxyziPublisher>(nh, topic, frame_id, buffer_size, latch);
            publisher.second = std::make_shared<CloudXYZIPublishData>();
            return true;
        }
        else if (type == "Path")
        {
            publisher.first = std::make_shared<PathPublisher>(nh, topic, frame_id, buffer_size, latch);
            publisher.second = std::make_shared<PathPublishData>();
            return true;
        }
        else if (type == "Loop")
        {
            publisher.first = std::make_shared<LoopPublisher>(nh, topic, frame_id, buffer_size, latch);
            publisher.second = std::make_shared<LoopPublishData>();
            return true;
        }
        else
        {
            LOG(ERROR) << "Publisher type error: " << type << "! Only support Cloud, Odometry or Pose!";
        }
        publisher.first = nullptr;
        publisher.second = nullptr;
        return false;
    }
} // namespace yd_fusion_localization