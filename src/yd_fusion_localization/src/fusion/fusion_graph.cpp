/**
 * @file fusion_graph.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-6
 * @brief fusion graph
 */

#include "yd_fusion_localization/fusion/fusion_graph.hpp"

namespace yd_fusion_localization
{
    FusionGraph::FusionGraph(const YAML::Node &yaml_node)
    {
        std::string graph_optimizer_type = yaml_node["graph_optimizer_type"].as<std::string>();
        if (graph_optimizer_type == "g2o")
        {
            graph_optimizer_ptr_ = std::make_shared<GraphOptimizerG2o>(yaml_node["g2o"]);
        }
        else
        {
            LOG(ERROR) << "没有找到与 " << graph_optimizer_type << " 对应的图优化模式，请检查配置文件";
            return;
        }
        LOG(INFO) << "图优化选择的优化器为：" << graph_optimizer_type << std::endl;
    }

    void FusionGraph::Reset()
    {
        graph_optimizer_ptr_->Reset();
    }

    bool FusionGraph::AddState(std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data)
    {
        return graph_optimizer_ptr_->AddState(predict_data, localizer_data);
    }
    bool FusionGraph::AddState(const State &state, std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data)
    {
        return graph_optimizer_ptr_->AddState(state, predict_data, localizer_data);
    }
    bool FusionGraph::Optimize(std::vector<int> &predict_inlier, std::vector<int> &localizer_inlier, bool key_frame, State &result_state)
    {
        return graph_optimizer_ptr_->Optimize(predict_inlier, localizer_inlier, key_frame, result_state);
    }
    bool FusionGraph::WindowInited() const
    {
        return graph_optimizer_ptr_->WindowInited();
    }
    int FusionGraph::GetStatesNum() const
    {
        return graph_optimizer_ptr_->GetStatesNum();
    }
} // namespace yd_fusion_localization