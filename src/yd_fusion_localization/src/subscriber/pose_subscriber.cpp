/**
 * @file pose_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe pose data
 */

#include "yd_fusion_localization/subscriber/pose_subscriber.hpp"

namespace yd_fusion_localization
{
    PoseSubscriber::PoseSubscriber(ros::NodeHandle &nh,
                                   const YAML::Node &yaml_node)
        : MeasureSubscriber(yaml_node)
    {
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<geometry_msgs::PoseWithCovarianceStampedConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(topic_, buffer_size_, [this](const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool PoseSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.pose_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void PoseSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void PoseSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double PoseSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool PoseSubscriber::HasData(double start_stamp, double end_stamp, double &stamp)
    {
        return MeasureSubscriber::HasData(start_stamp, end_stamp, stamp, data_buffer_);
    }

    bool PoseSubscriber::ValidData(double stamp, MeasureDataPtr &data)
    {
        return MeasureSubscriber::ValidData<geometry_msgs::PoseWithCovarianceStampedConstPtr>(stamp, data, data_buffer_, boost::bind(&PoseSubscriber::ParseData, this, _1));
    }

    bool PoseSubscriber::GetLatestData(MeasureDataPtr &data)
    {
        return MeasureSubscriber::GetLatestData<geometry_msgs::PoseWithCovarianceStampedConstPtr>(data, data_buffer_, boost::bind(&PoseSubscriber::ParseData, this, _1));
    }

    bool PoseSubscriber::GetAllData(std::deque<MeasureDataPtr> &data)
    {
        return MeasureSubscriber::GetAllData<geometry_msgs::PoseWithCovarianceStampedConstPtr>(data, data_buffer_, boost::bind(&PoseSubscriber::ParseData, this, _1));
    }

    void PoseSubscriber::ParseData(MeasureDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<PoseData>();
        }
        PoseDataPtr pose_data = std::dynamic_pointer_cast<PoseData>(data);
        pose_data->sensor_to_robot = sensor_to_robot_;
        pose_data->noise = noise_;
        pose_data->stamp = stamp_;
        TransformDataType(data_buffer_[index_]->pose.pose, pose_data->pose);
    }
} // namespace yd_fusion_localization