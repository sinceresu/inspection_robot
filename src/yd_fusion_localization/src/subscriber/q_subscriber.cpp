/**
 * @file q_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-22
 * @brief subscribe q data
 */

#include "yd_fusion_localization/subscriber/q_subscriber.hpp"

namespace yd_fusion_localization
{
    QSubscriber::QSubscriber(ros::NodeHandle &nh,
                             const YAML::Node &yaml_node)
        : MeasureSubscriber(yaml_node)
    {
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<sensor_msgs::Imu>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<sensor_msgs::ImuConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<sensor_msgs::Imu>(topic_, buffer_size_, [this](const sensor_msgs::ImuConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool QSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.q_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void QSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void QSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double QSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool QSubscriber::HasData(double start_stamp, double end_stamp, double &stamp)
    {
        return MeasureSubscriber::HasData(start_stamp, end_stamp, stamp, data_buffer_);
    }

    bool QSubscriber::ValidData(double stamp, MeasureDataPtr &data)
    {
        return MeasureSubscriber::ValidData2<sensor_msgs::ImuConstPtr>(stamp, data, data_buffer_, boost::bind(&QSubscriber::ParseData, this, _1));
    }

    bool QSubscriber::GetLatestData(MeasureDataPtr &data)
    {
        return MeasureSubscriber::GetLatestData<sensor_msgs::ImuConstPtr>(data, data_buffer_, boost::bind(&QSubscriber::ParseData, this, _1));
    }

    bool QSubscriber::GetAllData(std::deque<MeasureDataPtr> &data)
    {
        return MeasureSubscriber::GetAllData<sensor_msgs::ImuConstPtr>(data, data_buffer_, boost::bind(&QSubscriber::ParseData, this, _1));
    }

    void QSubscriber::ParseData(MeasureDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<QData>();
        }
        QDataPtr q_data = std::dynamic_pointer_cast<QData>(data);
        q_data->sensor_to_robot = sensor_to_robot_;
        q_data->noise = noise_;
        q_data->stamp = stamp_;
        if (data_buffer_.size() - index_ == 1)
        {
            TransformDataType(data_buffer_[index_]->orientation, q_data->q);
        }
        else
        {
            Eigen::Quaterniond start_q;
            Eigen::Quaterniond end_q;
            TransformDataType(data_buffer_[index_]->orientation, start_q);
            TransformDataType(data_buffer_[index_ + 1]->orientation, end_q);

            q_data->q = Interpolate(stamp_, data_buffer_[index_]->header.stamp.toSec(), data_buffer_[index_ + 1]->header.stamp.toSec(), start_q, end_q);
        }
        q_data->q.normalize();
    }
} // namespace yd_fusion_localization