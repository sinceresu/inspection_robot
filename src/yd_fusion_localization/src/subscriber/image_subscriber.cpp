/**
 * @file image_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-10-15
 * @brief subscribe image data
 */

#include "yd_fusion_localization/subscriber/image_subscriber.hpp"

namespace yd_fusion_localization
{
    ImageSubscriber::ImageSubscriber(ros::NodeHandle &nh,
                                     const YAML::Node &yaml_node)
        : MeasureSubscriber(yaml_node)
    {
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<sensor_msgs::Image>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<sensor_msgs::ImageConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<sensor_msgs::Image>(topic_, buffer_size_, [this](const sensor_msgs::Image::ConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool ImageSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.image_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void ImageSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void ImageSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double ImageSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool ImageSubscriber::HasData(double start_stamp, double end_stamp, double &stamp)
    {
        return MeasureSubscriber::HasData(start_stamp, end_stamp, stamp, data_buffer_);
    }

    bool ImageSubscriber::ValidData(double stamp, MeasureDataPtr &data)
    {
        return MeasureSubscriber::ValidData<sensor_msgs::Image::ConstPtr>(stamp, data, data_buffer_, boost::bind(&ImageSubscriber::ParseData, this, _1));
    }

    bool ImageSubscriber::GetLatestData(MeasureDataPtr &data)
    {
        return MeasureSubscriber::GetLatestData<sensor_msgs::Image::ConstPtr>(data, data_buffer_, boost::bind(&ImageSubscriber::ParseData, this, _1));
    }

    bool ImageSubscriber::GetAllData(std::deque<MeasureDataPtr> &data)
    {
        return MeasureSubscriber::GetAllData<sensor_msgs::Image::ConstPtr>(data, data_buffer_, boost::bind(&ImageSubscriber::ParseData, this, _1));
    }

    void ImageSubscriber::ParseData(MeasureDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<ImageData>();
        }
        ImageDataPtr img_data = std::dynamic_pointer_cast<ImageData>(data);
        img_data->sensor_to_robot = sensor_to_robot_;
        img_data->noise = noise_;
        img_data->stamp = stamp_;
        img_data->image_ptr = cv_bridge::toCvShare(data_buffer_[index_]);
    }
} // namespace yd_fusion_localization