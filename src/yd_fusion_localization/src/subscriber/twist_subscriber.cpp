/**
 * @file twist_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe twist data
 */

#include "yd_fusion_localization/subscriber/twist_subscriber.hpp"

namespace yd_fusion_localization
{
    TwistSubscriber::TwistSubscriber(ros::NodeHandle &nh,
                                     const YAML::Node &yaml_node)
        : PredictSubscriber(yaml_node)
    {
        vel_coef_ = yaml_node["vel_coef"].as<double>();
        gyro_coef_ = yaml_node["gyro_coef"].as<double>();
        max_vel_ = yaml_node["max_vel"].as<double>();
        max_gyro_ = yaml_node["max_gyro"].as<double>();
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<geometry_msgs::TwistStamped>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<geometry_msgs::TwistStampedConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<geometry_msgs::TwistStamped>(topic_, buffer_size_, [this](const geometry_msgs::TwistStampedConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool TwistSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.twist_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void TwistSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void TwistSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double TwistSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool TwistSubscriber::ValidData(double start_stamp, double end_stamp, PredictDataPtr &data)
    {
        return PredictSubscriber::ValidData<geometry_msgs::TwistStampedConstPtr>(start_stamp, end_stamp, data, data_buffer_, boost::bind(&TwistSubscriber::ParseData, this, _1));
    }

    double TwistSubscriber::GetLatestData(double start_stamp, PredictDataPtr &data)
    {
        return PredictSubscriber::GetLatestData<geometry_msgs::TwistStampedConstPtr>(start_stamp, data, data_buffer_, boost::bind(&TwistSubscriber::ValidData, this, _1, _2, _3));
    }

    bool TwistSubscriber::CheckData(const PredictDataPtr &data) const
    {
        assert(data->stamps.size() > 1);
        TwistDataPtr twist_data = std::dynamic_pointer_cast<TwistData>(data);
        for (int i = 0; i < twist_data->stamps.size(); ++i)
        {
            if (twist_data->vel[i].norm() > max_vel_)
            {
                return false;
            }
            if (twist_data->gyro[i].norm() > max_gyro_)
            {
                return false;
            }
        }
        return true;
    }

    bool TwistSubscriber::CheckDRData(const PredictDataPtr &data) const
    {
        return true;
    }

    void TwistSubscriber::ParseData(PredictDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<TwistData>();
        }
        assert(data_buffer_.size() > 1);
        TwistDataPtr twist_data = std::dynamic_pointer_cast<TwistData>(data);
        twist_data->Clear();
        twist_data->sensor_to_robot = sensor_to_robot_;
        twist_data->noise = noise_;
        twist_data->stamps.push_back(start_stamp_);
        Eigen::Vector3d start_v;
        Eigen::Vector3d end_v;
        TransformDataType(data_buffer_[start_index_]->twist.linear, start_v);
        TransformDataType(data_buffer_[start_index_ + 1]->twist.linear, end_v);
        twist_data->vel.push_back(Interpolate(start_stamp_, data_buffer_[start_index_]->header.stamp.toSec(),
                                              data_buffer_[start_index_ + 1]->header.stamp.toSec(),
                                              start_v, end_v));
        TransformDataType(data_buffer_[start_index_]->twist.angular, start_v);
        TransformDataType(data_buffer_[start_index_ + 1]->twist.angular, end_v);
        twist_data->gyro.push_back(Interpolate(start_stamp_, data_buffer_[start_index_]->header.stamp.toSec(),
                                               data_buffer_[start_index_ + 1]->header.stamp.toSec(),
                                               start_v, end_v));
        double prev_stamp = start_stamp_;
        for (int i = start_index_ + 1; i < end_index_; ++i)
        {
            double stamp = data_buffer_[i]->header.stamp.toSec();
            if (stamp - prev_stamp > 1e-3)
            {
                Eigen::Vector3d temp_v;
                twist_data->stamps.push_back(stamp);
                TransformDataType(data_buffer_[i]->twist.linear, temp_v);
                twist_data->vel.push_back(temp_v);
                TransformDataType(data_buffer_[i]->twist.angular, temp_v);
                twist_data->gyro.push_back(temp_v);
                prev_stamp = stamp;
            }
        }
        if (end_stamp_ - prev_stamp < 1e-3 && twist_data->stamps.size() > 1)
        {
            twist_data->stamps.pop_back();
            twist_data->vel.pop_back();
            twist_data->gyro.pop_back();
        }
        twist_data->stamps.push_back(end_stamp_);
        TransformDataType(data_buffer_[end_index_ - 1]->twist.linear, start_v);
        TransformDataType(data_buffer_[end_index_]->twist.linear, end_v);
        twist_data->vel.push_back(Interpolate(start_stamp_, data_buffer_[end_index_ - 1]->header.stamp.toSec(),
                                              data_buffer_[end_index_]->header.stamp.toSec(),
                                              start_v, end_v));
        TransformDataType(data_buffer_[end_index_ - 1]->twist.angular, start_v);
        TransformDataType(data_buffer_[end_index_]->twist.angular, end_v);
        twist_data->gyro.push_back(Interpolate(start_stamp_, data_buffer_[end_index_ - 1]->header.stamp.toSec(),
                                               data_buffer_[end_index_]->header.stamp.toSec(),
                                               start_v, end_v));

        for (int i = 0; i < twist_data->stamps.size(); ++i)
        {
            twist_data->vel[i] *= vel_coef_;
            twist_data->gyro[i] *= gyro_coef_;
        }
    }
} // namespace yd_fusion_localization