/**
 * @file p_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-22
 * @brief subscribe p data
 */

#include "yd_fusion_localization/subscriber/p_subscriber.hpp"

namespace yd_fusion_localization
{
    PSubscriber::PSubscriber(ros::NodeHandle &nh,
                             const YAML::Node &yaml_node)
        : MeasureSubscriber(yaml_node)
    {
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<geometry_msgs::PoseStamped>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<geometry_msgs::PoseStampedConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<geometry_msgs::PoseStamped>(topic_, buffer_size_, [this](const geometry_msgs::PoseStampedConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool PSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.p_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void PSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void PSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double PSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool PSubscriber::HasData(double start_stamp, double end_stamp, double &stamp)
    {
        return MeasureSubscriber::HasData(start_stamp, end_stamp, stamp, data_buffer_);
    }

    bool PSubscriber::ValidData(double stamp, MeasureDataPtr &data)
    {
        return MeasureSubscriber::ValidData<geometry_msgs::PoseStampedConstPtr>(stamp, data, data_buffer_, boost::bind(&PSubscriber::ParseData, this, _1));
    }

    bool PSubscriber::GetLatestData(MeasureDataPtr &data)
    {
        return MeasureSubscriber::GetLatestData<geometry_msgs::PoseStampedConstPtr>(data, data_buffer_, boost::bind(&PSubscriber::ParseData, this, _1));
    }

    bool PSubscriber::GetAllData(std::deque<MeasureDataPtr> &data)
    {
        return MeasureSubscriber::GetAllData<geometry_msgs::PoseStampedConstPtr>(data, data_buffer_, boost::bind(&PSubscriber::ParseData, this, _1));
    }

    void PSubscriber::ParseData(MeasureDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<PData>();
        }
        PDataPtr p_data = std::dynamic_pointer_cast<PData>(data);
        p_data->sensor_to_robot = sensor_to_robot_;
        p_data->noise = noise_;
        p_data->stamp = stamp_;
        TransformDataType(data_buffer_[index_]->pose.position, p_data->p);
    }
} // namespace yd_fusion_localization