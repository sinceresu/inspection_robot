/**
 * @file initialpose_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe initialpose data
 */

#include "yd_fusion_localization/subscriber/initialpose_subscriber.hpp"

namespace yd_fusion_localization
{
    InitialposeSubscriber::InitialposeSubscriber(ros::NodeHandle &nh,
                                                 const YAML::Node &yaml_node)
        : Subscriber(yaml_node)
    {
        data_buffer_.clear();
        if (yaml_node["z"].IsDefined())
        {
            z_ = yaml_node["z"].as<double>();
        }
        else
        {
            z_ = -1e20;
        }
        // subscriber_ = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<geometry_msgs::PoseWithCovarianceStampedConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(topic_, buffer_size_, &InitialposeSubscriber::msg_callback, this);
    }

    bool InitialposeSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            msg_callback(msg_ptr.pose_ptr);
            return true;
        }
        return false;
    }

    void InitialposeSubscriber::ClearData()
    {
    }

    void InitialposeSubscriber::ClearAllData()
    {
    }

    double InitialposeSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool InitialposeSubscriber::ValidData(Sophus::SE3d &data)
    {
        buffer_mutex_.lock();
        if (!data_buffer_.empty())
        {
            ParseData(data);
            data_buffer_.clear();
            buffer_mutex_.unlock();
            return true;
        }
        else
        {
            buffer_mutex_.unlock();
            return false;
        }
    }

    void InitialposeSubscriber::ParseData(Sophus::SE3d &data)
    {
        assert(!data_buffer_.empty());
        TransformDataType(data_buffer_.back()->pose.pose, data);
        if (z_ > -1e20 + 1)
        {
            data.translation()[2] = z_;
        }
    }

    void InitialposeSubscriber::msg_callback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg_ptr)
    {
        buffer_mutex_.lock();
        data_buffer_.push_back(msg_ptr);
        buffer_mutex_.unlock();
    }
} // namespace yd_fusion_localization