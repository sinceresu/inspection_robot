/**
 * @file imu_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe imu data
 */

#include "yd_fusion_localization/subscriber/imu_subscriber.hpp"

namespace yd_fusion_localization
{
    ImuSubscriber::ImuSubscriber(ros::NodeHandle &nh,
                                 const YAML::Node &yaml_node)
        : PredictSubscriber(yaml_node)
    {
        max_time_ = yaml_node["max_time"].as<double>();
        max_gyro_ = yaml_node["max_gyro"].as<double>();
        max_acc_ = yaml_node["max_acc"].as<double>();
        std::vector<double> gravity = yaml_node["gravity"].as<std::vector<double>>();
        for (unsigned int i = 0; i < 3; ++i)
        {
            gravity_[i] = gravity[i];
        }
        data_buffer_.clear();
        // subscriber_ = nh.subscribe<sensor_msgs::Imu>(topic_, buffer_size_, boost::bind(&Subscriber::msg_callback<sensor_msgs::ImuConstPtr>, this, _1, data_buffer_));
        subscriber_ = nh.subscribe<sensor_msgs::Imu>(topic_, buffer_size_, [this](const sensor_msgs::ImuConstPtr &msg_ptr) { Subscriber::msg_callback(msg_ptr, data_buffer_); });
    }

    bool ImuSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (topic == topic_)
        {
            Subscriber::msg_callback(msg_ptr.imu_ptr, data_buffer_);
            return true;
        }
        return false;
    }

    void ImuSubscriber::ClearData()
    {
        Subscriber::ClearData(data_buffer_);
    }

    void ImuSubscriber::ClearAllData()
    {
        Subscriber::ClearAllData(data_buffer_);
    }

    double ImuSubscriber::GetEarliestStamp()
    {
        return Subscriber::GetEarliestStamp(data_buffer_);
    }

    bool ImuSubscriber::ValidData(double start_stamp, double end_stamp, PredictDataPtr &data)
    {
        return PredictSubscriber::ValidData<sensor_msgs::ImuConstPtr>(start_stamp, end_stamp, data, data_buffer_, boost::bind(&ImuSubscriber::ParseData, this, _1));
    }

    double ImuSubscriber::GetLatestData(double start_stamp, PredictDataPtr &data)
    {
        return PredictSubscriber::GetLatestData<sensor_msgs::ImuConstPtr>(start_stamp, data, data_buffer_, boost::bind(&ImuSubscriber::ValidData, this, _1, _2, _3));
    }

    bool ImuSubscriber::CheckData(const PredictDataPtr &data) const
    {
        assert(data->stamps.size() > 1);
        ImuDataPtr imu_data = std::dynamic_pointer_cast<ImuData>(data);
        for (int i = 0; i < imu_data->stamps.size(); ++i)
        {
            if (imu_data->gyro[i].norm() > max_gyro_)
            {
                return false;
            }
            if (imu_data->acc[i].norm() > max_acc_)
            {
                return false;
            }
        }
        return true;
    }

    bool ImuSubscriber::CheckDRData(const PredictDataPtr &data) const
    {
        assert(data->stamps.size() > 1);
        if (data->stamps.back() - data->stamps.front() > max_time_)
        {
            return false;
        }
        return true;
    }

    void ImuSubscriber::ParseData(PredictDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<ImuData>();
        }
        assert(data_buffer_.size() > 1);
        ImuDataPtr imu_data = std::dynamic_pointer_cast<ImuData>(data);
        imu_data->Clear();
        imu_data->sensor_to_robot = sensor_to_robot_;
        imu_data->noise = noise_;
        imu_data->gravity = gravity_;
        imu_data->stamps.push_back(start_stamp_);
        Eigen::Vector3d start_v;
        Eigen::Vector3d end_v;
        TransformDataType(data_buffer_[0]->angular_velocity, start_v);
        TransformDataType(data_buffer_[1]->angular_velocity, end_v);
        imu_data->gyro.push_back(Interpolate(start_stamp_, data_buffer_[0]->header.stamp.toSec(),
                                             data_buffer_[1]->header.stamp.toSec(),
                                             start_v, end_v));
        TransformDataType(data_buffer_[0]->linear_acceleration, start_v);
        TransformDataType(data_buffer_[1]->linear_acceleration, end_v);
        imu_data->acc.push_back(Interpolate(start_stamp_, data_buffer_[0]->header.stamp.toSec(),
                                            data_buffer_[1]->header.stamp.toSec(),
                                            start_v, end_v));
        double prev_stamp = start_stamp_;
        for (int i = 1; i < end_index_; ++i)
        {
            double stamp = data_buffer_[i]->header.stamp.toSec();
            if (stamp - prev_stamp > 1e-3)
            {
                Eigen::Vector3d temp_v;
                imu_data->stamps.push_back(stamp);
                TransformDataType(data_buffer_[i]->angular_velocity, temp_v);
                imu_data->gyro.push_back(temp_v);
                TransformDataType(data_buffer_[i]->linear_acceleration, temp_v);
                imu_data->acc.push_back(temp_v);
                prev_stamp = stamp;
            }
        }
        if (end_stamp_ - prev_stamp < 1e-3 && imu_data->stamps.size() > 1)
        {
            imu_data->stamps.pop_back();
            imu_data->gyro.pop_back();
            imu_data->acc.pop_back();
        }
        imu_data->stamps.push_back(end_stamp_);
        TransformDataType(data_buffer_[end_index_ - 1]->angular_velocity, start_v);
        TransformDataType(data_buffer_[end_index_]->angular_velocity, end_v);
        imu_data->gyro.push_back(Interpolate(start_stamp_, data_buffer_[end_index_ - 1]->header.stamp.toSec(),
                                             data_buffer_[end_index_]->header.stamp.toSec(),
                                             start_v, end_v));
        TransformDataType(data_buffer_[end_index_ - 1]->linear_acceleration, start_v);
        TransformDataType(data_buffer_[end_index_]->linear_acceleration, end_v);
        imu_data->acc.push_back(Interpolate(start_stamp_, data_buffer_[end_index_ - 1]->header.stamp.toSec(),
                                            data_buffer_[end_index_]->header.stamp.toSec(),
                                            start_v, end_v));
    }
} // namespace yd_fusion_localization