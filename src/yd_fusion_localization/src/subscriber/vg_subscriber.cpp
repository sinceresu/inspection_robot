/**
 * @file vg_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-6
 * @brief subscribe twist and imu data
 */

#include "yd_fusion_localization/subscriber/vg_subscriber.hpp"

namespace yd_fusion_localization
{
    VgSubscriber::VgSubscriber(ros::NodeHandle &nh,
                               const YAML::Node &yaml_node) : PredictSubscriber(yaml_node["twist"])
    {
        max_vel_ = yaml_node["twist"]["max_vel"].as<double>();
        max_gyro_ = yaml_node["imu"]["max_gyro"].as<double>();
        twist_subscriber_ = std::make_shared<TwistSubscriber>(nh, yaml_node["twist"]);
        imu_subscriber_ = std::make_shared<ImuSubscriber>(nh, yaml_node["imu"]);
        twist_data_ = std::make_shared<TwistData>();
        imu_data_ = std::make_shared<ImuData>();
        sensor_to_robot_ = twist_subscriber_->sensor_to_robot_;
        sensor_frame_ = twist_subscriber_->sensor_frame_;
        topic_ = twist_subscriber_->topic_;
        sensor_to_robot_gyro_ = imu_subscriber_->sensor_to_robot_;
        sensor_frame_gyro_ = imu_subscriber_->sensor_frame_;
        topic_gyro_ = imu_subscriber_->topic_;
    }

    bool VgSubscriber::HandleMessage(const MessagePointer &msg_ptr, const std::string &topic)
    {
        if (twist_subscriber_->HandleMessage(msg_ptr, topic))
        {
            return true;
        }
        if (imu_subscriber_->HandleMessage(msg_ptr, topic))
        {
            return true;
        }
        return false;
    }

    void VgSubscriber::ClearData()
    {
        twist_subscriber_->ClearData();
        imu_subscriber_->ClearData();
    }

    void VgSubscriber::ClearAllData()
    {
        twist_subscriber_->ClearAllData();
        imu_subscriber_->ClearAllData();
    }

    double VgSubscriber::GetEarliestStamp()
    {
        double stamp1 = twist_subscriber_->GetEarliestStamp();
        double stamp2 = imu_subscriber_->GetEarliestStamp();
        return std::max(stamp1, stamp2);
    }

    bool VgSubscriber::ValidData(double start_stamp, double end_stamp, PredictDataPtr &data)
    {
        buffer_mutex2_.lock();
        PredictDataPtr tdata = std::dynamic_pointer_cast<PredictData>(twist_data_);
        PredictDataPtr idata = std::dynamic_pointer_cast<PredictData>(imu_data_);
        if (!twist_subscriber_->ValidData(start_stamp, end_stamp, tdata))
        {
            buffer_mutex2_.unlock();
            return false;
        }
        if (!imu_subscriber_->ValidData(start_stamp, end_stamp, idata))
        {
            buffer_mutex2_.unlock();
            return false;
        }
        ParseData(data);
        buffer_mutex2_.unlock();
        return true;
    }

    double VgSubscriber::GetLatestData(double start_stamp, PredictDataPtr &data)
    {
        buffer_mutex2_.lock();
        PredictDataPtr tdata = std::dynamic_pointer_cast<PredictData>(twist_data_);
        PredictDataPtr idata = std::dynamic_pointer_cast<PredictData>(imu_data_);
        double stamp = twist_subscriber_->GetLatestData(start_stamp, tdata);
        if (stamp > start_stamp)
        {
            double stamp2 = imu_subscriber_->GetLatestData(start_stamp, idata);
            if (stamp2 > stamp)
            {
                if (imu_subscriber_->ValidData(start_stamp, stamp, idata))
                {
                    ParseData(data);
                    buffer_mutex2_.unlock();
                    return stamp;
                }
            }
            else if (stamp2 > start_stamp)
            {
                if (twist_subscriber_->ValidData(start_stamp, stamp2, tdata))
                {
                    ParseData(data);
                    buffer_mutex2_.unlock();
                    return stamp2;
                }
            }
        }
        buffer_mutex2_.unlock();
        return -1.0;
    }

    bool VgSubscriber::CheckData(const PredictDataPtr &data) const
    {
        assert(data->stamps.size() > 1);
        VgDataPtr vg_data = std::dynamic_pointer_cast<VgData>(data);
        for (int i = 0; i < vg_data->stamps.size(); ++i)
        {
            if (vg_data->vel[i].norm() > max_vel_)
            {
                return false;
            }
            if (vg_data->gyro[i].norm() > max_gyro_)
            {
                return false;
            }
        }
        return true;
    }

    bool VgSubscriber::CheckDRData(const PredictDataPtr &data) const
    {
        return true;
    }

    void VgSubscriber::ParseData(const TwistDataPtr &twist_data, const ImuDataPtr &imu_data, VgDataPtr &vg_data)
    {
        vg_data->Clear();
        vg_data->sensor_to_robot = twist_data->sensor_to_robot;
        vg_data->sensor_to_robot_gyro = imu_data->sensor_to_robot;
        vg_data->noise.resize(9);
        vg_data->noise.head(3) = twist_data->noise.head(3);
        vg_data->noise.segment(3, 3) = imu_data->noise.segment(3, 3);
        vg_data->noise.tail(3) = imu_data->noise.tail(3);

        double stamp = twist_data->stamps.front();
        vg_data->stamps.push_back(stamp);
        vg_data->vel.push_back(twist_data->vel.front());
        vg_data->gyro.push_back(imu_data->gyro.front());
        int index1 = 1;
        int index2 = 1;
        while (index1 + 1 < twist_data->stamps.size() || index2 + 1 < imu_data->stamps.size())
        {
            assert(index1 < twist_data->stamps.size());
            assert(index2 < imu_data->stamps.size());
            if (twist_data->stamps[index1] < imu_data->stamps[index2])
            {
                stamp = twist_data->stamps[index1];
                if (stamp - vg_data->stamps.back() < 1e-5)
                {
                    index1++;
                    continue;
                }
                vg_data->stamps.push_back(stamp);
                vg_data->vel.push_back(twist_data->vel[index1]);
                vg_data->gyro.push_back(Interpolate(stamp, imu_data->stamps[index2 - 1], imu_data->stamps[index2], imu_data->gyro[index2 - 1], imu_data->gyro[index2]));
                index1++;
            }
            else
            {
                stamp = imu_data->stamps[index2];
                if (stamp - vg_data->stamps.back() < 1e-5)
                {
                    index2++;
                    continue;
                }
                vg_data->stamps.push_back(stamp);
                vg_data->gyro.push_back(imu_data->gyro[index2]);
                vg_data->vel.push_back(Interpolate(stamp, twist_data->stamps[index1 - 1], twist_data->stamps[index1], twist_data->vel[index1 - 1], twist_data->vel[index1]));
                index2++;
            }
        }
        stamp = twist_data->stamps.back();
        if (stamp - vg_data->stamps.back() < 1e-5)
        {
            vg_data->stamps.pop_back();
            vg_data->vel.pop_back();
            vg_data->gyro.pop_back();
        }
        vg_data->stamps.push_back(stamp);
        vg_data->vel.push_back(twist_data->vel.back());
        vg_data->gyro.push_back(imu_data->gyro.back());
    }

    void VgSubscriber::ParseData(PredictDataPtr &data)
    {
        if (data == nullptr)
        {
            data = std::make_shared<VgData>();
        }
        VgDataPtr vg_data = std::dynamic_pointer_cast<VgData>(data);
        VgSubscriber::ParseData(twist_data_, imu_data_, vg_data);
    }
} // namespace yd_fusion_localization