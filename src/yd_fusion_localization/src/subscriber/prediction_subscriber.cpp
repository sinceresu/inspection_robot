/**
 * @file predict_subscriber.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-6-19
 * @brief subscribe predict data
 */

#include "yd_fusion_localization/subscriber/predict_subscriber.hpp"

namespace yd_fusion_localization
{
    PredictSubscriber::PredictSubscriber(const YAML::Node &yaml_node)
        : Subscriber(yaml_node),
          start_stamp_(-1.0),
          end_stamp_(-1.0),
          start_index_(-1),
          end_index_(-1)
    {
    }
} // namespace yd_fusion_localization