/**
 * @file preintegration_vg_ex.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-3
 * @brief preintegration vg ex
 */

#include "yd_fusion_localization/models/preintegration/preintegration_vg_ex.hpp"

namespace yd_fusion_localization
{
    PreIntegrationVgEx::PreIntegrationVgEx(const VgData &vg_data, const Eigen::Vector3d &linearized_bg)
        : data_(vg_data),
          s2r1_(vg_data.sensor_to_robot),
          s2r2_(vg_data.sensor_to_robot_gyro)
    {
        sumdt_ = data_.stamps.back() - data_.stamps.front();
        assert(sumdt_ > 0);
        noise_.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise_(i, i) = data_.noise(i) * data_.noise(i);
            noise_(i + 3, i + 3) = noise_(i, i);

            noise_(i + 6, i + 6) = data_.noise(i + 3) * data_.noise(i + 3);
            noise_(i + 9, i + 9) = noise_(i + 6, i + 6);

            noise_(i + 12, i + 12) = data_.noise(i + 6) * data_.noise(i + 6);
        }
        linearized_bg_ = linearized_bg;
        alpha_.setZero();
        gamma_.setIdentity();
        jacobian_ = Eigen::Matrix<double, 9, 9>::Identity();
        covariance_.setZero();
        Propagate();
    }
    Eigen::Matrix<double, 9, 1> PreIntegrationVgEx::Error(const Eigen::Vector3d &Pi,
                                                          const Eigen::Quaterniond &Qi,
                                                          const Eigen::Vector3d &Bgi,
                                                          const Eigen::Vector3d &Pj,
                                                          const Eigen::Quaterniond &Qj,
                                                          const Eigen::Vector3d &Bgj)
    {
        Eigen::Matrix<double, 9, 1> residuals;
        Eigen::Vector3d dbg = Bgi - linearized_bg_;

        Eigen::Matrix3d dalpha_dbg = jacobian_.block<3, 3>(0, 6);
        Eigen::Matrix3d dtheta_dbg = jacobian_.block<3, 3>(3, 6);

        Eigen::Vector3d corrected_alpha = alpha_ + dalpha_dbg * dbg;
        Eigen::Quaterniond corrected_gamma = gamma_ * deltaQ(dtheta_dbg * dbg);

        residuals.block<3, 1>(0, 0) = Qi.inverse() * (Pj - Pi) - corrected_alpha;
        residuals.block<3, 1>(3, 0) = 2 * (corrected_gamma.inverse() * (Qi.inverse() * Qj)).vec();
        residuals.block<3, 1>(6, 0) = Bgj - Bgi;
        return residuals;
    }
    Eigen::Matrix<double, 9, 18> PreIntegrationVgEx::Jacobian(const Eigen::Vector3d &Pi,
                                                              const Eigen::Quaterniond &Qi,
                                                              const Eigen::Vector3d &Bgi,
                                                              const Eigen::Vector3d &Pj,
                                                              const Eigen::Quaterniond &Qj,
                                                              const Eigen::Vector3d &Bgj)
    {
        Eigen::Matrix<double, 9, 18> jacobians;
        jacobians.setZero();
        Eigen::Vector3d dbg = Bgi - linearized_bg_;
        Eigen::Matrix3d dalpha_dbg = jacobian_.block<3, 3>(0, 6);
        Eigen::Matrix3d dtheta_dbg = jacobian_.block<3, 3>(3, 6);
        // Eigen::Vector3d corrected_alpha = alpha_ + dalpha_dbg * dbg;
        Eigen::Quaterniond corrected_gamma = gamma_ * deltaQ(dtheta_dbg * dbg);

        jacobians.block<3, 3>(0, 0) = -Qi.inverse().toRotationMatrix();
        jacobians.block<3, 3>(0, 3) = skewSymmetric(Qi.inverse() * (Pj - Pi));
        jacobians.block<3, 3>(0, 6) = -dalpha_dbg;

        jacobians.block<3, 3>(0, 9) = Qi.inverse().toRotationMatrix();

        jacobians.block<3, 3>(3, 3) = -(Qleft(Qj.inverse() * Qi) * Qright(corrected_gamma)).bottomRightCorner<3, 3>();
        jacobians.block<3, 3>(3, 6) = -(Qleft(Qj.inverse() * Qi * gamma_)).bottomRightCorner<3, 3>() * dtheta_dbg;
        jacobians.block<3, 3>(3, 12) = (Qleft(corrected_gamma.inverse() * Qi.inverse() * Qj)).bottomRightCorner<3, 3>();

        jacobians.block<3, 3>(6, 6) = -Eigen::Matrix3d::Identity();

        jacobians.block<3, 3>(6, 15) = Eigen::Matrix3d::Identity();

        return jacobians;
    }
    State PreIntegrationVgEx::DeadReckoning(const State &state,
                                            const VgData &vg_data)
    {
        Sophus::SE3d s2r1(vg_data.sensor_to_robot);
        Sophus::SE3d s2r2(vg_data.sensor_to_robot_gyro);
        Eigen::Matrix<double, 15, 15> noise;
        noise.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise(i, i) = vg_data.noise(i) * vg_data.noise(i);
            noise(i + 3, i + 3) = noise(i, i);

            noise(i + 6, i + 6) = vg_data.noise(i + 3) * vg_data.noise(i + 3);
            noise(i + 9, i + 9) = noise(i + 6, i + 6);

            noise(i + 12, i + 12) = vg_data.noise(i + 6) * vg_data.noise(i + 6);
        }
        Eigen::Vector3d p(state.p);
        Eigen::Quaterniond q(state.q);
        Eigen::Vector3d linearized_bg(state.bg);
        Eigen::Matrix<double, 9, 9> covariance;
        if (state.type == StateType::Bg)
        {
            covariance = state.covariance.block(0, 0, 9, 9);
        }
        else if (state.type == StateType::VBaBg)
        {
            covariance.block<6, 6>(0, 0) = state.covariance.block(0, 0, 6, 6);
            covariance.bottomRightCorner<3, 3>() = state.covariance.bottomRightCorner<3, 3>();
            covariance.block<6, 3>(0, 6) = state.covariance.block(0, 12, 6, 3);
            covariance.block<3, 6>(6, 0) = covariance.block<6, 3>(0, 6).transpose();
        }
        else
        {
            LOG(ERROR) << "PreIntegrationVgEx State type error!!!";
            return state;
        }

        Eigen::Matrix<double, 9, 9> jacobian;
        for (int i = 1; i < static_cast<int>(vg_data.stamps.size()); ++i)
        {
            double dt = vg_data.stamps[i] - vg_data.stamps[i - 1];
            Eigen::Vector3d vel0 = vg_data.vel[i - 1];
            Eigen::Vector3d vel1 = vg_data.vel[i];
            Eigen::Vector3d gyr0 = vg_data.gyro[i - 1];
            Eigen::Vector3d gyr1 = vg_data.gyro[i];

            Eigen::Vector3d result_p;
            Eigen::Quaterniond result_q;
            Eigen::Matrix<double, 9, 9> result_covariance;
            MidPointIntegration(dt, s2r1, s2r2, noise, vel0, vel1, gyr0, gyr1, p, q, linearized_bg, jacobian, covariance, result_p, result_q, jacobian, result_covariance, false);

            p = result_p;
            q = result_q;
            covariance = result_covariance;
        }
        State result_state;
        result_state.stamp = vg_data.stamps.back();
        result_state.index = -1;
        result_state.type = StateType::Bg;
        result_state.p = p;
        result_state.q = q;
        result_state.bg = linearized_bg;
        result_state.covariance = covariance;
        return result_state;
    }
    void PreIntegrationVgEx::MidPointIntegration(double dt,
                                                 const Sophus::SE3d &s2r1,
                                                 const Sophus::SE3d &s2r2,
                                                 const Eigen::Matrix<double, 15, 15> noise,
                                                 const Eigen::Vector3d &vel0,
                                                 const Eigen::Vector3d &vel1,
                                                 const Eigen::Vector3d &gyr0,
                                                 const Eigen::Vector3d &gyr1,
                                                 const Eigen::Vector3d &p,
                                                 const Eigen::Quaterniond &q,
                                                 const Eigen::Vector3d &linearized_bg,
                                                 const Eigen::Matrix<double, 9, 9> &jacobian,
                                                 const Eigen::Matrix<double, 9, 9> &covariance,
                                                 Eigen::Vector3d &result_p,
                                                 Eigen::Quaterniond &result_q,
                                                 Eigen::Matrix<double, 9, 9> &result_jacobian,
                                                 Eigen::Matrix<double, 9, 9> &result_covariance,
                                                 bool update_jacobian)
    {
        assert(dt > 0);
        Eigen::Quaterniond qex1(s2r1.unit_quaternion());
        Eigen::Vector3d pex1(s2r1.translation());
        Eigen::Matrix3d Rex1 = qex1.toRotationMatrix();
        Eigen::Quaterniond qex2(s2r2.unit_quaternion());
        // Eigen::Vector3d pex2(s2r2.translation());
        Eigen::Matrix3d Rex2 = qex2.toRotationMatrix();

        Eigen::Vector3d gyro_av = 0.5 * (gyr0 + gyr1) - linearized_bg;
        gyro_av = Rex2 * gyro_av;
        Eigen::Quaterniond temp = Eigen::Quaterniond(1, gyro_av(0) * dt / 2, gyro_av(1) * dt / 2, gyro_av(2) * dt / 2);
        temp.normalize();
        result_q = q * temp;
        result_q.normalize();

        Eigen::Vector3d v0 = Rex1 * vel0;
        Eigen::Vector3d v1 = Rex1 * vel1;
        Eigen::Vector3d omega0 = gyr0 - linearized_bg;
        omega0 = Rex2 * omega0;
        Eigen::Vector3d omega1 = gyr1 - linearized_bg;
        omega1 = Rex2 * omega1;

        v0 = v0 - omega0.cross(pex1);
        v1 = v1 - omega1.cross(pex1);

        Eigen::Vector3d deltap = 0.5 * dt * (q * v0 + result_q * v1);
        result_p = p + deltap;

        Eigen::Matrix3d R_w_x, R_v0_x, R_v1_x, R_pex1_x;

        R_w_x << 0, -gyro_av(2), gyro_av(1),
            gyro_av(2), 0, -gyro_av(0),
            -gyro_av(1), gyro_av(0), 0;
        R_v0_x << 0, -v0(2), v0(1),
            v0(2), 0, -v0(0),
            -v0(1), v0(0), 0;
        R_v1_x << 0, -v1(2), v1(1),
            v1(2), 0, -v1(0),
            -v1(1), v1(0), 0;
        R_pex1_x << 0, -pex1(2), pex1(1),
            pex1(2), 0, -pex1(0),
            -pex1(1), pex1(0), 0;

        Eigen::Matrix<double, 9, 9> F = Eigen::Matrix<double, 9, 9>::Zero();

        F.block<3, 3>(6, 6) = Eigen::Matrix3d::Identity();

        F.block<3, 3>(3, 3) = Eigen::Matrix3d::Identity() - R_w_x * dt;
        F.block<3, 3>(3, 6) = -1.0 * dt * Rex2;

        F.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(0, 3) = -0.5 * dt * (q.toRotationMatrix() * R_v0_x + result_q.toRotationMatrix() * R_v1_x * (Eigen::Matrix3d::Identity() - R_w_x * dt));
        F.block<3, 3>(0, 6) = 0.5 * dt * dt * (result_q.toRotationMatrix() * R_v1_x * Rex2) - 0.5 * dt * ((q.toRotationMatrix() + result_q.toRotationMatrix()) * R_pex1_x * Rex2);

        Eigen::Matrix<double, 9, 15> G = Eigen::Matrix<double, 9, 15>::Zero();

        G.block<3, 3>(6, 12) = Eigen::Matrix3d::Identity() * dt;

        G.block<3, 3>(3, 6) = -0.5 * dt * Rex2;
        G.block<3, 3>(3, 9) = G.block<3, 3>(3, 6);
        G.block<3, 3>(3, 12) = dt * G.block<3, 3>(3, 6);

        G.block<3, 3>(0, 0) = -0.5 * dt * (q.toRotationMatrix() * Rex1);
        G.block<3, 3>(0, 3) = -0.5 * dt * (result_q.toRotationMatrix() * Rex1);
        G.block<3, 3>(0, 6) = 0.25 * dt * dt * (result_q.toRotationMatrix() * R_v1_x * Rex2);
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 6);
        G.block<3, 3>(0, 6) = G.block<3, 3>(0, 6) - 0.5 * dt * (q.toRotationMatrix() * R_pex1_x * Rex2);
        G.block<3, 3>(0, 9) = G.block<3, 3>(0, 9) - 0.5 * dt * (result_q.toRotationMatrix() * R_pex1_x * Rex2);
        G.block<3, 3>(0, 12) = dt * G.block<3, 3>(0, 9);

        result_covariance = F * covariance * F.transpose() + G * noise * G.transpose();
        if (update_jacobian)
        {
            result_jacobian = F * jacobian;
        }
    }

    bool PreIntegrationVgEx::RePropagate(const Eigen::Vector3d &Bgi)
    {
        if ((Bgi - linearized_bg_).norm() < 0.01)
        {
            return false;
        }
        linearized_bg_ = Bgi;
        alpha_.setZero();
        gamma_.setIdentity();
        jacobian_ = Eigen::Matrix<double, 9, 9>::Identity();
        covariance_.setZero();
        Propagate();
        return true;
    }

    void PreIntegrationVgEx::Propagate()
    {
        for (int i = 1; i < static_cast<int>(data_.stamps.size()); ++i)
        {
            double dt = data_.stamps[i] - data_.stamps[i - 1];
            Eigen::Vector3d vel0 = data_.vel[i - 1];
            Eigen::Vector3d vel1 = data_.vel[i];
            Eigen::Vector3d gyr0 = data_.gyro[i - 1];
            Eigen::Vector3d gyr1 = data_.gyro[i];

            Eigen::Vector3d result_alpha;
            Eigen::Quaterniond result_gamma;
            Eigen::Matrix<double, 9, 9> result_jacobian;
            Eigen::Matrix<double, 9, 9> result_covariance;
            MidPointIntegration(dt, s2r1_, s2r2_, noise_, vel0, vel1, gyr0, gyr1, alpha_, gamma_, linearized_bg_, jacobian_, covariance_, result_alpha, result_gamma, result_jacobian, result_covariance, true);

            alpha_ = result_alpha;
            gamma_ = result_gamma;
            jacobian_ = result_jacobian;
            covariance_ = result_covariance;
        }
        information_ = covariance_.inverse();
        assert(!std::isnan(information_(0, 0)));
        assert(!std::isinf(information_(0, 0)));
    }
} // namespace yd_fusion_localization