/**
 * @file preintegration_odom_ex.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-6
 * @brief preintegration odom ex
 */

#include "yd_fusion_localization/models/preintegration/preintegration_odom_ex.hpp"

namespace yd_fusion_localization
{
    PreIntegrationOdomEx::PreIntegrationOdomEx(const OdometryData &odom_data)
        : data_(odom_data),
          s2r_(odom_data.sensor_to_robot)
    {
        sumdt_ = data_.stamps.back() - data_.stamps.front();
        assert(sumdt_ > 0);
        noise_.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise_(i, i) = data_.noise(i) * data_.noise(i);
            noise_(i + 3, i + 3) = data_.noise(i + 3) * data_.noise(i + 3);
        }
        Propagate();
    }
    Eigen::Matrix<double, 6, 1> PreIntegrationOdomEx::Error(const Eigen::Vector3d &Pi,
                                                            const Eigen::Quaterniond &Qi,
                                                            const Eigen::Vector3d &Pj,
                                                            const Eigen::Quaterniond &Qj)
    {
        Eigen::Matrix<double, 6, 1> residuals;
        residuals.head(3) = Qi.inverse() * (Pj - Pi) - alpha_;
        residuals.tail(3) = 2 * (gamma_.inverse() * (Qi.inverse() * Qj)).vec();
        return residuals;
    }
    Eigen::Matrix<double, 6, 12> PreIntegrationOdomEx::Jacobian(const Eigen::Vector3d &Pi,
                                                                const Eigen::Quaterniond &Qi,
                                                                const Eigen::Vector3d &Pj,
                                                                const Eigen::Quaterniond &Qj)
    {
        Eigen::Matrix<double, 6, 12> jacobians;
        jacobians.setZero();
        jacobians.block<3, 3>(0, 0) = -Qi.inverse().toRotationMatrix();
        jacobians.block<3, 3>(0, 3) = skewSymmetric(Qi.inverse() * (Pj - Pi));
        jacobians.block<3, 3>(0, 6) = Qi.inverse().toRotationMatrix();

        jacobians.block<3, 3>(3, 3) = -(Qleft(Qj.inverse() * Qi) * Qright(gamma_)).bottomRightCorner<3, 3>();
        jacobians.block<3, 3>(3, 9) = (Qleft(gamma_.inverse() * Qi.inverse() * Qj)).bottomRightCorner<3, 3>();
        return jacobians;
    }
    State PreIntegrationOdomEx::DeadReckoning(const State &state,
                                              const OdometryData &odom_data)
    {
        Sophus::SE3d s2r(odom_data.sensor_to_robot);
        Eigen::Matrix<double, 6, 6> noise;
        noise.setZero();
        for (int i = 0; i < 3; ++i)
        {
            noise(i, i) = odom_data.noise(i) * odom_data.noise(i);
            noise(i + 3, i + 3) = odom_data.noise(i + 3) * odom_data.noise(i + 3);
        }
        Eigen::Vector3d p(state.p);
        Eigen::Quaterniond q(state.q);
        Eigen::Matrix<double, 6, 6> covariance(state.covariance.block(0, 0, 6, 6));
        Sophus::SE3d tij = odom_data.pose.front().inverse() * odom_data.pose.back();
        Sophus::SE3d rpose = s2r * tij * s2r.inverse();
        Eigen::Vector3d result_p = p + q * rpose.translation();
        Eigen::Quaterniond result_q = q * rpose.unit_quaternion();
        // double sumdt = data_.stamps.back() - data_.stamps.front();
        // noise = sumdt * sumdt * noise;

        Eigen::Matrix<double, 6, 6> F = Eigen::Matrix<double, 6, 6>::Zero();
        F.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
        F.block<3, 3>(0, 3) = -1.0 * q.toRotationMatrix() * skewSymmetric(rpose.translation());
        F.block<3, 3>(3, 3) = rpose.unit_quaternion().inverse().toRotationMatrix();

        Eigen::Matrix<double, 6, 6> G = Eigen::Matrix<double, 6, 6>::Zero();
        G.block<3, 3>(0, 0) = (q * s2r.unit_quaternion()).toRotationMatrix();
        G.block<3, 3>(0, 3) = (q * s2r.unit_quaternion() * tij.unit_quaternion()).toRotationMatrix() * skewSymmetric(s2r.unit_quaternion().inverse() * s2r.translation());
        G.block<3, 3>(3, 3) = s2r.unit_quaternion().toRotationMatrix();

        covariance = F * covariance * F.transpose() + G * noise * G.transpose();

        State result_state;
        result_state.stamp = odom_data.stamps.back();
        result_state.index = -1;
        result_state.type = StateType::Basic;
        result_state.p = result_p;
        result_state.q = result_q;
        result_state.covariance = covariance;
        return result_state;
    }

    void PreIntegrationOdomEx::Propagate()
    {
        Sophus::SE3d tij = data_.pose.front().inverse() * data_.pose.back();
        Sophus::SE3d rpose = s2r_ * tij * s2r_.inverse();
        alpha_ = rpose.translation();
        gamma_ = rpose.unit_quaternion();
        // covariance_ = sumdt_ * sumdt_ * noise_;
        Eigen::Matrix<double, 6, 6> G = Eigen::Matrix<double, 6, 6>::Zero();
        G.block<3, 3>(0, 0) = s2r_.unit_quaternion().toRotationMatrix();
        G.block<3, 3>(0, 3) = (s2r_.unit_quaternion() * tij.unit_quaternion()).toRotationMatrix() * skewSymmetric(s2r_.unit_quaternion().inverse() * s2r_.translation());
        G.block<3, 3>(3, 3) = s2r_.unit_quaternion().toRotationMatrix();
        covariance_ = G * noise_ * G.transpose();
        information_ = covariance_.inverse();
        assert(!std::isnan(information_(0, 0)));
        assert(!std::isinf(information_(0, 0)));
    }
} // namespace yd_fusion_localization