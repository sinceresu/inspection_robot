/**
 * @file ndt_registration.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief ndt registration
 */

#include "yd_fusion_localization/models/registration/ndt_registration.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    NDTRegistration<PointType>::NDTRegistration(const YAML::Node &node)
        : ndt_ptr_(new pcl::NormalDistributionsTransform<PointType, PointType>())
    {

        float res = node["res"].as<float>();
        float step_size = node["step_size"].as<float>();
        float trans_eps = node["trans_eps"].as<float>();
        int max_iter = node["max_iter"].as<int>();

        SetRegistrationParam(res, step_size, trans_eps, max_iter);
    }

    template<typename PointType>
    NDTRegistration<PointType>::NDTRegistration(float res, float step_size, float trans_eps, int max_iter)
        : ndt_ptr_(new pcl::NormalDistributionsTransform<PointType, PointType>())
    {

        SetRegistrationParam(res, step_size, trans_eps, max_iter);
    }

    template<typename PointType>
    bool NDTRegistration<PointType>::SetRegistrationParam(float res, float step_size, float trans_eps, int max_iter)
    {
        ndt_ptr_->setResolution(res);
        ndt_ptr_->setStepSize(step_size);
        ndt_ptr_->setTransformationEpsilon(trans_eps);
        ndt_ptr_->setMaximumIterations(max_iter);

        std::cout << "NDT 的匹配参数为：" << std::endl
                  << "res: " << res << ", "
                  << "step_size: " << step_size << ", "
                  << "trans_eps: " << trans_eps << ", "
                  << "max_iter: " << max_iter
                  << std::endl
                  << std::endl;

        return true;
    }

    template<typename PointType>
    bool NDTRegistration<PointType>::SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target)
    {
        ndt_ptr_->setInputTarget(input_target);

        return true;
    }

    template<typename PointType>
    bool NDTRegistration<PointType>::ScanMatch(double stamp,
                                    const typename pcl::PointCloud<PointType>::Ptr &input_source,
                                    typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                                    const Eigen::Matrix4f &predict_pose,
                                    Eigen::Matrix4f &result_pose)
    {
        ndt_ptr_->setInputSource(input_source);

        ndt_ptr_->align(*result_cloud_ptr, predict_pose);
        result_pose = ndt_ptr_->getFinalTransformation();

        return true;
    }

    template<typename PointType>
    float NDTRegistration<PointType>::GetFitnessScore()
    {
        return ndt_ptr_->getFitnessScore();
    }
    template class NDTRegistration<pcl::PointXYZ>;
    template class NDTRegistration<pcl::PointXYZI>;
} // namespace yd_fusion_localization