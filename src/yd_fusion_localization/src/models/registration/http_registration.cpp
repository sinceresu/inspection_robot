/**
 * @file http_registration.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief http registration
 */

#include "yd_fusion_localization/models/registration/http_registration.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    HttpRegistration<PointType>::HttpRegistration(const YAML::Node &node)
    {
        url_ = node["url"].as<std::string>();
        fitness_score_ = 1e6;
    }

    template<typename PointType>
    HttpRegistration<PointType>::~HttpRegistration() {}

    template<typename PointType>
    bool HttpRegistration<PointType>::SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target)
    {
        return true;
    }

    template<typename PointType>
    bool HttpRegistration<PointType>::ScanMatch(double stamp,
                                     const typename pcl::PointCloud<PointType>::Ptr &input_source,
                                     typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                                     const Eigen::Matrix4f &predict_pose,
                                     Eigen::Matrix4f &result_pose)
    {
        result_pose = predict_pose;
        CURL *curl_handle = curl_easy_init();
        if (curl_handle)
        {
            std::string url(url_ + "?key=slam_pose&value=");
            std::stringstream ss;
            ss << std::setprecision(15) << stamp;
            url += ss.str();
            for (int i = 0; i < predict_pose.rows() - 1; ++i)
            {
                for (int j = 0; j < predict_pose.cols(); ++j)
                {
                    std::stringstream sss;
                    sss << std::setprecision(9) << "_" << predict_pose(i, j);
                    url += sss.str();
                }
            }
            std::string userdata;
            curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
            // curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, false);
            // curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, false);
            // curl_easy_setopt(curl_handle, CURLOPT_POST, true);
            // curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, request.c_str());
            curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, HttpRegistration::write_data);
            curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)(&userdata));
            CURLcode res = curl_easy_perform(curl_handle);
            curl_easy_cleanup(curl_handle);
            if (res == CURLE_OK)
            {
                return ParseData(userdata, result_pose);
            }
            else
            {
                LOG(WARNING) << "http registration fail!!! CURLcode: " << res;
                fitness_score_ = 1e6;
                return false;
            }
        }
        else
        {
            LOG(WARNING) << "http registration curl_easy_init fail!!!";
            fitness_score_ = 1e6;
            return false;
        }
    }

    template<typename PointType>
    float HttpRegistration<PointType>::GetFitnessScore()
    {
        return fitness_score_;
    }

    template<typename PointType>
    size_t HttpRegistration<PointType>::write_data(void *ptr, size_t size, size_t nmemb, void *userdata)
    {
        std::string *str = (std::string *)userdata;
        str->append((char *)ptr, size * nmemb);
        return size * nmemb;
    }

    template<typename PointType>
    bool HttpRegistration<PointType>::ParseData(const std::string userdata, Eigen::Matrix4f &result_pose)
    {
        std::string ss("");
        bool received = false;
        int count = 0;
        for (int i = 0; i < userdata.size(); ++i)
        {
            if (received)
            {
                if (userdata[i] == 'f')
                {
                    LOG(WARNING) << "http registration failed!!!";
                    fitness_score_ = 1e6;
                    return false;
                }
                else if (userdata[i] == '_' || i + 1 == userdata.size())
                {
                    int index = count - 1;
                    if (index / 4 > 2)
                    {
                        LOG(WARNING) << "http registration result pose index error!!!";
                        fitness_score_ = 1e6;
                        return false;
                    }
                    if (count == 0)
                    {
                        fitness_score_ = std::stof(ss);
                    }
                    else
                    {
                        result_pose(index / 4, index % 4) = std::stof(ss);
                    }
                    ss = "";
                    count++;
                }
                else
                {
                    ss += userdata[i];
                }
            }
            else
            {
                if (userdata[i] == '=')
                {
                    received = true;
                    ss = "";
                }
            }
        }
        if (count == 13)
        {
            return true;
        }
        else
        {
            LOG(WARNING) << "http registration result pose index error!!!";
            fitness_score_ = 1e6;
            return false;
        }
    }
    template class HttpRegistration<pcl::PointXYZ>;
    template class HttpRegistration<pcl::PointXYZI>;
} // namespace yd_fusion_localization