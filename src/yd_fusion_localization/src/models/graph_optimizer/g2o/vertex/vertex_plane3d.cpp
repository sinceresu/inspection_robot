#include "yd_fusion_localization/models/graph_optimizer/g2o/vertex/vertex_plane3d.hpp"

namespace g2o
{
    VertexPlane3d::VertexPlane3d() : BaseVertex<3, yd_fusion_localization::Plane3D>()
    {
        setToOriginImpl();
    }
    bool VertexPlane3d::read(std::istream &is)
    {
        Eigen::Vector4d lv;
        for (int i = 0; i < 4; i++)
            is >> lv[i];
        setEstimate(yd_fusion_localization::Plane3D(lv));
        return true;
    }

    bool VertexPlane3d::write(std::ostream &os) const
    {
        Eigen::Vector4d lv = estimate().coeffs();
        for (int i = 0; i < 4; i++)
        {
            os << lv[i] << " ";
        }
        return os.good();
    }
    void VertexPlane3d::setToOriginImpl()
    {
        _estimate = yd_fusion_localization::Plane3D();
    }

    void VertexPlane3d::oplusImpl(const double *update_)
    {
        Eigen::Map<const Eigen::Vector3d> update(update_);
        _estimate.oplus(update);
    }
} // namespace g2o