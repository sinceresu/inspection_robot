#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_imu.hpp"

namespace g2o
{
    EdgeImu::EdgeImu() : BaseMultiEdge<15, std::shared_ptr<yd_fusion_localization::PreIntegrationImuEx>>()
    {
        resize(10);
    }
    void EdgeImu::computeError()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Vector3d Vi = (dynamic_cast<VertexVec *>(_vertices[2]))->estimate();
        Eigen::Vector3d Bai = (dynamic_cast<VertexVec *>(_vertices[3]))->estimate();
        Eigen::Vector3d Bgi = (dynamic_cast<VertexVec *>(_vertices[4]))->estimate();
        Eigen::Vector3d Pj = (dynamic_cast<VertexVec *>(_vertices[5]))->estimate();
        Eigen::Quaterniond Qj = (dynamic_cast<VertexQ *>(_vertices[6]))->estimate();
        Eigen::Vector3d Vj = (dynamic_cast<VertexVec *>(_vertices[7]))->estimate();
        Eigen::Vector3d Baj = (dynamic_cast<VertexVec *>(_vertices[8]))->estimate();
        Eigen::Vector3d Bgj = (dynamic_cast<VertexVec *>(_vertices[9]))->estimate();
        if (_measurement->RePropagate(Bai, Bgi))
        {
            _information = _measurement->information_;
        }
        _error = _measurement->Error(Pi, Qi, Vi, Bai, Bgi, Pj, Qj, Vj, Baj, Bgj);
    }
    void EdgeImu::linearizeOplus()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Vector3d Vi = (dynamic_cast<VertexVec *>(_vertices[2]))->estimate();
        Eigen::Vector3d Bai = (dynamic_cast<VertexVec *>(_vertices[3]))->estimate();
        Eigen::Vector3d Bgi = (dynamic_cast<VertexVec *>(_vertices[4]))->estimate();
        Eigen::Vector3d Pj = (dynamic_cast<VertexVec *>(_vertices[5]))->estimate();
        Eigen::Quaterniond Qj = (dynamic_cast<VertexQ *>(_vertices[6]))->estimate();
        Eigen::Vector3d Vj = (dynamic_cast<VertexVec *>(_vertices[7]))->estimate();
        Eigen::Vector3d Baj = (dynamic_cast<VertexVec *>(_vertices[8]))->estimate();
        Eigen::Vector3d Bgj = (dynamic_cast<VertexVec *>(_vertices[9]))->estimate();
        // if (_measurement->RePropagate(Bai, Bgi))
        // {
        //     _information = _measurement->information_;
        // }
        Eigen::Matrix<double, 15, 30> jacs = _measurement->Jacobian(Pi, Qi, Vi, Bai, Bgi, Pj, Qj, Vj, Baj, Bgj);
        for (unsigned int i = 0; i < _vertices.size(); i++)
        {
            _jacobianOplus[i] = jacs.block<15, 3>(0, 3 * i);
        }
    }
    void EdgeImu::setMeasurement(const std::shared_ptr<yd_fusion_localization::PreIntegrationImuEx> &m)
    {
        _measurement = m;
        _information = _measurement->information_;
    }
} // namespace g2o