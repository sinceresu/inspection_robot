#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_pl.hpp"

namespace g2o
{
    EdgePL::EdgePL() : BaseMultiEdge<3, yd_fusion_localization::PLocalizerData>()
    {
        resize(2);
    }

    void EdgePL::computeError()
    {
        Eigen::Vector3d Pi = (dynamic_cast<const VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Sophus::SE3d t1 = _measurement.local_to_map.inverse();
        _error = t1.translation() + t1.unit_quaternion() * Pi + t1.unit_quaternion() * Qi * _measurement.sensor_to_robot.translation() - _measurement.p;
    }

    void EdgePL::linearizeOplus()
    {
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Quaterniond q1 = _measurement.local_to_map.unit_quaternion().inverse();
        _jacobianOplus[0] = q1.toRotationMatrix();
        _jacobianOplus[1] = -(q1 * Qi).toRotationMatrix() * yd_fusion_localization::skewSymmetric(_measurement.sensor_to_robot.translation());
    }

    void EdgePL::setMeasurement(const yd_fusion_localization::PLocalizerData &m)
    {
        _measurement = m;
        _information = Eigen::Matrix3d::Zero();
        for (int i = 0; i < 3; i++)
        {
            _information(i, i) = 1.0 / _measurement.noise(i) / _measurement.noise(i);
            assert(!std::isnan(_information(i, i)));
            assert(!std::isinf(_information(i, i)));
        }
    }
} // namespace g2o