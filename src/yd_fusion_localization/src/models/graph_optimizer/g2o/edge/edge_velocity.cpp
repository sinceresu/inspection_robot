#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_velocity.hpp"

namespace g2o
{
    EdgeVelocity::EdgeVelocity(const yd_fusion_localization::TwistData &data) : BaseMultiEdge<3, Eigen::Vector3d>()
    {
        resize(2);

        Eigen::Matrix<double, 3, 6> jacs = Eigen::Matrix<double, 3, 6>::Zero();
        jacs.block<3, 3>(0, 0) = data.sensor_to_robot.unit_quaternion().toRotationMatrix();
        jacs.block<3, 3>(0, 3) = yd_fusion_localization::skewSymmetric(data.sensor_to_robot.translation()) * jacs.block<3, 3>(0, 0);
        Eigen::Matrix<double, 6, 1> x;
        x.head(3) = data.vel.back();
        x.tail(3) = data.gyro.back();
        setMeasurement(jacs * x);

        Eigen::Matrix<double, 6, 6> cov = Eigen::Matrix<double, 6, 6>::Zero();
        for (int i = 0; i < 6; ++i)
        {
            cov(i, i) = data.noise[i] * data.noise[i];
        }
        Eigen::Matrix3d covariance = jacs * cov * jacs.transpose();
        setInformation(covariance.inverse());
    }
    void EdgeVelocity::computeError()
    {
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[0]))->estimate();
        Eigen::Vector3d Vi = (dynamic_cast<VertexVec *>(_vertices[1]))->estimate();

        _error = Vi - Qi * _measurement;
    }
    void EdgeVelocity::linearizeOplus()
    {
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[0]))->estimate();
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[1]))->estimate();

        _jacobianOplus[0] = Qi.toRotationMatrix() * yd_fusion_localization::skewSymmetric(_measurement);
        _jacobianOplus[1] = Eigen::Matrix3d::Identity();
    }
    void EdgeVelocity::setMeasurement(const Eigen::Vector3d &m)
    {
        _measurement = m;
    }
} // namespace g2o