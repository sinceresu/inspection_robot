#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_twist.hpp"

namespace g2o
{
    EdgeTwist::EdgeTwist() : BaseMultiEdge<6, std::shared_ptr<yd_fusion_localization::PreIntegrationTwistEx>>()
    {
        resize(4);
    }
    void EdgeTwist::computeError()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Vector3d Pj = (dynamic_cast<VertexVec *>(_vertices[2]))->estimate();
        Eigen::Quaterniond Qj = (dynamic_cast<VertexQ *>(_vertices[3]))->estimate();
        
        _error = _measurement->Error(Pi, Qi, Pj, Qj);
    }
    void EdgeTwist::linearizeOplus()
    {
        Eigen::Vector3d Pi = (dynamic_cast<VertexVec *>(_vertices[0]))->estimate();
        Eigen::Quaterniond Qi = (dynamic_cast<VertexQ *>(_vertices[1]))->estimate();
        Eigen::Vector3d Pj = (dynamic_cast<VertexVec *>(_vertices[2]))->estimate();
        Eigen::Quaterniond Qj = (dynamic_cast<VertexQ *>(_vertices[3]))->estimate();

        Eigen::Matrix<double, 6, 12> jacs = _measurement->Jacobian(Pi, Qi, Pj, Qj);

        for (unsigned int i = 0; i < _vertices.size(); i++)
        {
            _jacobianOplus[i] = jacs.block<6, 3>(0, 3 * i);
        }
    }
    void EdgeTwist::setMeasurement(const std::shared_ptr<yd_fusion_localization::PreIntegrationTwistEx> &m)
    {
        _measurement = m;
        _information = _measurement->information_;
    }
} // namespace g2o