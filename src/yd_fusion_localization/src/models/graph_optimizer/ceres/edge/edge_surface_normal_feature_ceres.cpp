#include "yd_fusion_localization/models/graph_optimizer/ceres/edge/edge_surface_normal_feature_ceres.hpp"

namespace ceres
{
    EdgeSurfNormalFeatureCeres::EdgeSurfNormalFeatureCeres(const yd_fusion_localization::SurfaceNormalFeatureDistance &surface_normal_feature)
        : surface_normal_feature_(surface_normal_feature)
    {
    }

    bool EdgeSurfNormalFeatureCeres::Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
    {
        return surface_normal_feature_.Evaluate(parameters, residuals, jacobians);
    }
}