#include "yd_fusion_localization/models/lidar_feature/plane3d.hpp"

namespace yd_fusion_localization
{
    Plane3D::Plane3D()
    {
        Eigen::Vector4d v;
        v << 0, 0, 1, 0;
        fromVector(v);
    }

    Plane3D::Plane3D(const Eigen::Vector4d &v)
    {
        fromVector(v);
    }

    Eigen::Vector4d Plane3D::toVector() const
    {
        return coeffs_;
    }

    const Eigen::Vector4d &Plane3D::coeffs() const
    {
        return coeffs_;
    }

    void Plane3D::fromVector(const Eigen::Vector4d &coeffs)
    {
        coeffs_ = coeffs;
        normalize(coeffs_);
    }

    double Plane3D::distance() const
    {
        return -coeffs_(3);
    }

    Eigen::Vector3d Plane3D::normal() const
    {
        return coeffs_.head<3>();
    }

    void Plane3D::oplus(const Eigen::Vector3d &v)
    {
        //construct a normal from azimuth and evelation;
        double az = v[0];
        double ele = v[1];
        double s = std::sin(ele), c = std::cos(ele);
        Eigen::Vector3d n(c * std::cos(az), c * std::sin(az), s);

        // rotate the normal
        Eigen::Matrix3d R = rotation(normal());
        double d = distance() + v[2];
        coeffs_.head<3>() = R * n;
        coeffs_(3) = -d;
        normalize(coeffs_);
    }

    Eigen::Vector3d Plane3D::ominus(const Plane3D &plane)
    {
        //construct the rotation that would bring the plane normal in (1 0 0)
        Eigen::Matrix3d R = rotation(normal()).transpose();
        Eigen::Vector3d n = R * plane.normal();
        double d = distance() - plane.distance();
        return Eigen::Vector3d(azimuth(n), elevation(n), d);
    }

    double Plane3D::azimuth(const Eigen::Vector3d &v)
    {
        return std::atan2(v(1), v(0));
    }

    double Plane3D::elevation(const Eigen::Vector3d &v)
    {
        return std::atan2(v(2), v.head<2>().norm());
    }
    Eigen::Matrix3d Plane3D::rotation(const Eigen::Vector3d &v)
    {
        double az = azimuth(v);
        double ele = elevation(v);
        return (Eigen::AngleAxisd(az, Eigen::Vector3d::UnitZ()) * Eigen::AngleAxisd(-ele, Eigen::Vector3d::UnitY())).toRotationMatrix();
    }
    void Plane3D::normalize(Eigen::Vector4d &coeffs)
    {
        double n = coeffs.head<3>().norm();
        coeffs = coeffs * (1. / n);
    }

    Plane3D operator*(const Sophus::SE3d &t, const Plane3D &plane)
    {
        Eigen::Vector4d v = plane.coeffs();
        Eigen::Vector4d v2;
        v2.head<3>() = t.unit_quaternion() * v.head<3>();
        v2(3) = v(3) - t.translation().dot(v2.head<3>());
        return Plane3D(v2);
    };
}