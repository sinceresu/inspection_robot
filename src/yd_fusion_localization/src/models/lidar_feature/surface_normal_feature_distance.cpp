#include "yd_fusion_localization/models/lidar_feature/surface_normal_feature_distance.hpp"

namespace yd_fusion_localization
{
    SurfaceNormalFeatureDistance::SurfaceNormalFeatureDistance(const Eigen::Vector3d &curr_point, const Eigen::Vector3d &plane_unit_norm, double negative_OA_dot_norm)
        : curr_point_(curr_point), plane_unit_norm_(plane_unit_norm), negative_OA_dot_norm_(negative_OA_dot_norm)
    {
    }

    double SurfaceNormalFeatureDistance::Error(const Eigen::Quaterniond &q_w_curr, const Eigen::Vector3d &t_w_curr) const
    {
        Eigen::Vector3d point_w = q_w_curr * curr_point_ + t_w_curr;
        return plane_unit_norm_.dot(point_w) + negative_OA_dot_norm_;
    }

    bool SurfaceNormalFeatureDistance::Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
    {
        Eigen::Map<const Eigen::Quaterniond> q_w_curr(parameters[0]);
        Eigen::Map<const Eigen::Vector3d> t_w_curr(parameters[0] + 4);
        Eigen::Vector3d point_w = q_w_curr * curr_point_ + t_w_curr;
        residuals[0] = plane_unit_norm_.dot(point_w) + negative_OA_dot_norm_;

        if (jacobians != NULL)
        {
            if (jacobians[0] != NULL)
            {
                Eigen::Matrix3d skew_point_w = skewSymmetric(point_w);
                Eigen::Matrix<double, 3, 6> dp_by_se3;
                dp_by_se3.block<3, 3>(0, 0) = -skew_point_w;
                (dp_by_se3.block<3, 3>(0, 3)).setIdentity();
                Eigen::Map<Eigen::Matrix<double, 1, 7, Eigen::RowMajor>> J_se3(jacobians[0]);
                J_se3.setZero();
                J_se3.block<1, 6>(0, 0) = plane_unit_norm_.transpose() * dp_by_se3;
            }
        }
        return true;
    }
}