/**
 * @file no_filter.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief no filter
 */

#include "yd_fusion_localization/models/cloud_filter/no_filter.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    NoFilter<PointType>::NoFilter()
    {
    }

    template<typename PointType>
    bool NoFilter<PointType>::Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr)
    {
        filtered_cloud_ptr.reset(new pcl::PointCloud<PointType>(*input_cloud_ptr));
        return true;
    }
    template class NoFilter<pcl::PointXYZ>;
    template class NoFilter<pcl::PointXYZI>;
} // namespace yd_fusion_localization