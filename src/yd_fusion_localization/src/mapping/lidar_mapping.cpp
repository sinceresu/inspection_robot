/**
 * @file lidar_mapping.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-21
 * @brief lidar_mapping
 */

#include "yd_fusion_localization/models/cloud_filter/voxel_filter.hpp"
#include "yd_fusion_localization/global_defination/global_defination.h"

#include "yd_fusion_localization/mapping/lidar_mapping.hpp"
#include <pcl/registration/icp.h>
#include "yd_fusion_localization/utilities/file_manager.hpp"
#include "yd_fusion_localization/mapping/utility.hpp"

namespace yd_fusion_localization
{
    using namespace symbol_shorthand;
    LidarMapping::LidarMapping(const YAML::Node &yaml_node) : Mapping(yaml_node)
    {
        outlier_chi2_ = yaml_node["outlier_chi2"].as<double>();
        map_path_ = std::string(std::getenv("HOME"))  + "/"  + yaml_node["map_path"].as<std::string>() + "/";
        if (!FileManager::CreateDirectory(map_path_))
        {
            LOG(ERROR) << "map save directory false!!！";
        }
        full_filter_ = std::make_shared<VoxelFilter<pcl::PointXYZI>>(yaml_node["full_filter"]);

        robust_kernel_factory_ = g2o::RobustKernelFactory::instance();
        robust_kernel_name_ = yaml_node["robust_kernel_name"].as<std::string>();
        robust_kernel_delta_ = yaml_node["robust_kernel_delta"].as<double>();

        graph_ptr_.reset(new g2o::SparseOptimizer());
        std::string solver_type = yaml_node["solver_type"].as<std::string>();
        g2o::OptimizationAlgorithmFactory *solver_factory = g2o::OptimizationAlgorithmFactory::instance();
        g2o::OptimizationAlgorithmProperty solver_property;
        g2o::OptimizationAlgorithm *solver = solver_factory->construct(solver_type, solver_property);
        graph_ptr_->setAlgorithm(solver);
        if (!graph_ptr_->solver())
        {
            LOG(ERROR) << "g2o solver fail！";
            solver_factory->listSolvers(std::cout);
        }
        loop_filter_ = std::make_shared<VoxelFilter<pcl::PointXYZI>>(yaml_node["loop_filter"]);
        loop_near_key_num_ = yaml_node["loop_near_key_num"].as<int>();
        loop_search_radius_ = yaml_node["loop_search_radius"].as<double>();
        loop_fitness_score_ = yaml_node["loop_fitness_score"].as<double>();
        Reset();
    }

    LidarMapping::~LidarMapping()
    {
    }
    void LidarMapping::Reset()
    {
        graph_ptr_->clear();
        key_frame_.clear();
        optimized_pose_.clear();
        optimized_pose_time_.clear();
        edge_id_ = 0;
        robust_edges_.clear();
        odom2map_ = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());
    }
    bool LidarMapping::Initialize(const std::deque<LocalizerDataPtr> &localizer_data, FrameDataPtr &frame)
    {
        Reset();
        if (frame->GetType() != FrameDataType::Lidar)
        {
            return false;
        }

        LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(frame);
        Sophus::SE3d pose = lidar_frame->odometry * lidar_frame->sensor_to_robot.inverse();
        g2o::VertexVec *vpa(new g2o::VertexVec());
        vpa->setId(A(0));
        vpa->setEstimate(pose.translation());
        vpa->setFixed(true);
        graph_ptr_->addVertex(vpa);
        g2o::VertexQ *vqa(new g2o::VertexQ());
        vqa->setId(A(1));
        vqa->setEstimate(pose.unit_quaternion());
        vqa->setFixed(true);
        graph_ptr_->addVertex(vqa);

        g2o::VertexVec *vp0(new g2o::VertexVec());
        vp0->setId(P(lidar_frame->index));
        vp0->setEstimate(pose.translation());
        graph_ptr_->addVertex(vp0);
        g2o::VertexQ *vq0(new g2o::VertexQ());
        vq0->setId(Q(lidar_frame->index));
        vq0->setEstimate(pose.unit_quaternion());
        graph_ptr_->addVertex(vq0);

        g2o::EdgeOdom *edge(new g2o::EdgeOdom());

        OdometryData odom_data;
        odom_data.noise = Eigen::Matrix<double, 6, 1>::Ones() * 1e5;
        odom_data.sensor_to_robot = lidar_frame->sensor_to_robot;
        odom_data.stamps.resize(2);
        odom_data.stamps[0] = lidar_frame->stamp - 0.1;
        odom_data.stamps[1] = lidar_frame->stamp;
        odom_data.pose.resize(2);
        odom_data.pose[0] = lidar_frame->odometry;
        odom_data.pose[1] = lidar_frame->odometry;

        std::shared_ptr<PreIntegrationOdomEx> pre = std::make_shared<PreIntegrationOdomEx>(odom_data);
        edge->setMeasurement(pre);
        edge->vertices()[0] = vpa;
        edge->vertices()[1] = vqa;
        edge->vertices()[2] = vp0;
        edge->vertices()[3] = vq0;

        edge->setId(edge_id_);
        edge_id_ = edge_id_ + 1;
        graph_ptr_->addEdge(edge);

        if (!lidar_frame->plane.empty())
        {
            for (int i = 0; i < lidar_frame->plane.size(); ++i)
            {
                int index = L(lidar_frame->plane[i]->index);
                if (graph_ptr_->vertex(index) == nullptr)
                {
                    g2o::VertexPlane3d *vpp(new g2o::VertexPlane3d());
                    vpp->setId(index);
                    Plane3D plane = lidar_frame->odometry * Plane3D(lidar_frame->plane[i]->coeff);
                    vpp->setEstimate(plane);
                    graph_ptr_->addVertex(vpp);
                    if (lidar_frame->plane[i]->index == 0)
                    {
                        vpp->setEstimate(Plane3D(Eigen::Vector4d(0, 0, 1, 0)));
                        vpp->setFixed(true);
                    }
                }
                PlaneLandmarkData landmark;
                landmark.index = lidar_frame->plane[i]->index;
                landmark.frame_index = lidar_frame->index;
                landmark.stamp = lidar_frame->stamp;
                landmark.sensor_to_robot = lidar_frame->sensor_to_robot;
                landmark.noise = lidar_frame->plane[i]->noise;
                landmark.coeff = lidar_frame->plane[i]->coeff;
                g2o::EdgePlane3d *edgep(new g2o::EdgePlane3d());
                edgep->setMeasurement(landmark);
                edgep->vertices()[0] = graph_ptr_->vertex(index);
                edgep->vertices()[1] = vp0;
                edgep->vertices()[2] = vq0;

                edgep->setId(edge_id_);
                edge_id_ = edge_id_ + 1;
                graph_ptr_->addEdge(edgep);
                AddRobustKernel(edge);
            }
        }

        for (int i = 0; i < localizer_data.size(); ++i)
        {
            AddEdgeLocalizer(lidar_frame->index, localizer_data[i]);
        }
        key_frame_.emplace_back(frame);
        optimized_pose_.emplace_back(std::make_pair(false, odom2map_ * lidar_frame->odometry * lidar_frame->sensor_to_robot.inverse()));
        optimized_pose_time_.emplace_back(frame->stamp);
        return true;
    }

    bool LidarMapping::Handle(const std::deque<LocalizerDataPtr> &localizer_data, FrameDataPtr &frame)
    {
        if (frame->GetType() != FrameDataType::Lidar)
        {
            return false;
        }
        if (frame->index < 0)
        {
            return true;
        }

        LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(frame);
        Sophus::SE3d pose = lidar_frame->odometry * lidar_frame->sensor_to_robot.inverse();
        g2o::VertexVec *vp(new g2o::VertexVec());
        vp->setId(P(lidar_frame->index));
        vp->setEstimate(pose.translation());
        graph_ptr_->addVertex(vp);
        g2o::VertexQ *vq(new g2o::VertexQ());
        vq->setId(Q(lidar_frame->index));
        vq->setEstimate(pose.unit_quaternion());
        graph_ptr_->addVertex(vq);

        LOG(INFO) << "index " << frame->index << "; pose " << pose.translation().transpose() << ", " << pose.unit_quaternion().coeffs().transpose();

        g2o::EdgeOdom *edge(new g2o::EdgeOdom());

        OdometryData odom_data;
        odom_data.noise = lidar_frame->noise;
        odom_data.sensor_to_robot = lidar_frame->sensor_to_robot;
        odom_data.stamps.resize(2);
        odom_data.stamps[0] = key_frame_.back()->stamp;
        odom_data.stamps[1] = lidar_frame->stamp;
        odom_data.pose.resize(2);
        odom_data.pose[0] = key_frame_.back()->odometry;
        odom_data.pose[1] = lidar_frame->odometry;

        std::shared_ptr<PreIntegrationOdomEx> pre = std::make_shared<PreIntegrationOdomEx>(odom_data);
        edge->setMeasurement(pre);
        int prev_index = key_frame_.back()->index;
        edge->vertices()[0] = graph_ptr_->vertex(P(prev_index));
        edge->vertices()[1] = graph_ptr_->vertex(Q(prev_index));
        edge->vertices()[2] = vp;
        edge->vertices()[3] = vq;

        edge->setId(edge_id_);
        edge_id_ = edge_id_ + 1;
        graph_ptr_->addEdge(edge);
        // AddRobustKernel(edge);

        if (!lidar_frame->plane.empty())
        {
            for (int i = 0; i < lidar_frame->plane.size(); ++i)
            {
                int index = L(lidar_frame->plane[i]->index);
                if (graph_ptr_->vertex(index) == nullptr)
                {
                    g2o::VertexPlane3d *vpp(new g2o::VertexPlane3d());
                    vpp->setId(index);
                    Plane3D plane = lidar_frame->odometry * Plane3D(lidar_frame->plane[i]->coeff);
                    vpp->setEstimate(plane);
                    graph_ptr_->addVertex(vpp);
                    if (lidar_frame->plane[i]->index == 0)
                    {
                        vpp->setEstimate(Plane3D(Eigen::Vector4d(0, 0, 1, 0)));
                        vpp->setFixed(true);
                    }
                }
                PlaneLandmarkData landmark;
                landmark.index = lidar_frame->plane[i]->index;
                landmark.frame_index = lidar_frame->index;
                landmark.stamp = lidar_frame->stamp;
                landmark.sensor_to_robot = lidar_frame->sensor_to_robot;
                landmark.noise = lidar_frame->plane[i]->noise;
                landmark.coeff = lidar_frame->plane[i]->coeff;
                g2o::EdgePlane3d *edgep(new g2o::EdgePlane3d());
                edgep->setMeasurement(landmark);
                edgep->vertices()[0] = graph_ptr_->vertex(index);
                edgep->vertices()[1] = vp;
                edgep->vertices()[2] = vq;

                edgep->setId(edge_id_);
                edge_id_ = edge_id_ + 1;
                graph_ptr_->addEdge(edgep);
                AddRobustKernel(edge);
            }
        }

        for (int i = 0; i < localizer_data.size(); ++i)
        {
            AddEdgeLocalizer(lidar_frame->index, localizer_data[i]);
        }
        key_frame_.emplace_back(frame);
        optimized_pose_.emplace_back(std::make_pair(false, odom2map_ * lidar_frame->odometry * lidar_frame->sensor_to_robot.inverse()));
        optimized_pose_time_.emplace_back(frame->stamp);
        return true;
    }

    bool LidarMapping::Handle(const LoopDataPtr &loop_detect, LoopDataPtr &loop_result)
    {
        if (!LoopMatching(loop_detect, loop_result))
        {
            return false;
        }
        LOG(INFO) << "Loop index " << loop_result->index_prev << ", " << loop_result->index_next << "; rpose " << loop_result->relative_pose.translation().transpose() << ", " << loop_result->relative_pose.unit_quaternion().coeffs().transpose();

        g2o::EdgeOdom *edge(new g2o::EdgeOdom());

        OdometryData odom_data;
        odom_data.noise = loop_result->noise;
        odom_data.sensor_to_robot = loop_result->sensor_to_robot;
        odom_data.stamps.resize(2);
        odom_data.stamps[0] = loop_result->stamp_next - 0.1;
        odom_data.stamps[1] = loop_result->stamp_next;
        odom_data.pose.resize(2);
        odom_data.pose[0] = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());
        odom_data.pose[1] = loop_result->relative_pose;

        std::shared_ptr<PreIntegrationOdomEx> pre = std::make_shared<PreIntegrationOdomEx>(odom_data);
        edge->setMeasurement(pre);
        int index = loop_result->index_prev;
        edge->vertices()[0] = graph_ptr_->vertex(P(index));
        edge->vertices()[1] = graph_ptr_->vertex(Q(index));
        index = loop_result->index_next;
        edge->vertices()[2] = graph_ptr_->vertex(P(index));
        edge->vertices()[3] = graph_ptr_->vertex(Q(index));

        edge->setId(edge_id_);
        edge_id_ = edge_id_ + 1;
        graph_ptr_->addEdge(edge);
        AddRobustKernel(edge);
        return true;
    }

    bool LidarMapping::Optimize()
    {
        if (graph_ptr_->edges().size() < 1)
        {
            return false;
        }

        graph_ptr_->initializeOptimization();
        graph_ptr_->computeInitialGuess();
        graph_ptr_->computeActiveErrors();
        graph_ptr_->setVerbose(false);

        double chi2 = graph_ptr_->chi2();

        graph_ptr_->optimize(max_iterations_num_);

        double chi22 = graph_ptr_->chi2();
        // for (auto &it : graph_ptr_->edges())
        // {
        //     std::cout << dynamic_cast<g2o::OptimizableGraph::Edge *>(it)->chi2() << std::endl;
        // }

        LOG(INFO) << std::endl
                  << "------ LidarMapping::Optimize -------" << std::endl
                  << "Vertices: " << graph_ptr_->vertices().size() << ", Edges: " << graph_ptr_->edges().size() << std::endl
                  << ". Chi2: " << chi2 << "--->" << chi22 << std::endl;

        RemoveEdgeOutlier();
        RemoveEdgeRobustKernel();

        graph_ptr_->initializeOptimization();
        graph_ptr_->computeInitialGuess();
        graph_ptr_->computeActiveErrors();
        graph_ptr_->setVerbose(false);

        chi2 = graph_ptr_->chi2();

        graph_ptr_->optimize(max_iterations_num_);

        chi22 = graph_ptr_->chi2();

        LOG(INFO) << std::endl
                  << "------ LidarMapping::Optimize2 -------" << std::endl
                  << "Vertices: " << graph_ptr_->vertices().size() << ", Edges: " << graph_ptr_->edges().size() << std::endl
                  << ". Chi2: " << chi2 << "--->" << chi22 << std::endl;

        for (int i = 0; i < key_frame_.size(); ++i)
        {
            int index = key_frame_[i]->index;
            auto v1 = graph_ptr_->vertex(P(index));
            auto v2 = graph_ptr_->vertex(Q(index));
            if (v1 == nullptr || v2 == nullptr)
            {
                optimized_pose_[i].first = false;
            }
            else
            {
                g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(v1);
                g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(v2);
                optimized_pose_[i].first = true;
                optimized_pose_[i].second = Sophus::SE3d(vq->estimate(), vp->estimate());
                odom2map_ = optimized_pose_[i].second * key_frame_[i]->sensor_to_robot * key_frame_[i]->odometry.inverse();
            }
        }
        LOG(INFO) << "exit LidarMapping::Optimize()" << std::endl;

        return true;
    }

    bool LidarMapping::SaveMap(const std::string& map_directory)
    {
        LOG(INFO) << "enter LidarMapping::SaveMap()"<<  std::endl;
        pcl::PointCloud<pcl::PointXYZI>::Ptr filtered = GetGlobalMap();
        pcl::io::savePCDFileBinary(map_directory + "/full.pcd", *filtered);

        pcl::PointCloud<PointTypePose>::Ptr transformedPointTypePose(new pcl::PointCloud<PointTypePose>());

        // pcl::PointCloud<pcl::PointXYZ>::Ptr path(new pcl::PointCloud<pcl::PointXYZ>());
        for (int i = 0; i < optimized_pose_.size(); ++i)
        {
            // Eigen::Vector3d p = optimized_pose_[i].second.translation();
            // path->push_back(pcl::PointXYZ(p.x(), p.y(), p.z()));
            Eigen::Affine3d t(optimized_pose_[i].second.matrix());
            double x, y, z, roll, pitch, yaw;
            pcl::getTranslationAndEulerAngles(t, x, y, z, roll, pitch, yaw);
            PointTypePose transformed_pose;
            transformed_pose.x = x;
            transformed_pose.y = y;
            transformed_pose.z = z;
            transformed_pose.roll = roll;
            transformed_pose.pitch = pitch;
            transformed_pose.yaw = yaw;
            transformed_pose.time = optimized_pose_time_[i];
            transformedPointTypePose->push_back(transformed_pose);

        }
        pcl::io::savePCDFileBinary(map_directory + "/path.pcd", *transformedPointTypePose);

        LOG(INFO) << "exit LidarMapping::SaveMap()" << std::endl;

        return true;
    }

    std::deque<std::pair<bool, Sophus::SE3d>> LidarMapping::GetOptimizedPose()
    {
        return optimized_pose_;
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr LidarMapping::GetGlobalMap()
    {
        pcl::PointCloud<pcl::PointXYZI>::Ptr map(new pcl::PointCloud<pcl::PointXYZI>());
        for (int i = 0; i < optimized_pose_.size(); ++i)
        {
            LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(key_frame_[i]);
            pcl::PointCloud<pcl::PointXYZI>::Ptr full(new pcl::PointCloud<pcl::PointXYZI>());
            Sophus::SE3d pose = optimized_pose_[i].second * lidar_frame->sensor_to_robot;
            pcl::transformPointCloud(*(lidar_frame->xyzi_ptr), *full, pose.matrix().cast<float>());
            *map += *full;
        }
        pcl::PointCloud<pcl::PointXYZI>::Ptr filtered(new pcl::PointCloud<pcl::PointXYZI>());
        full_filter_->Filter(map, filtered);
        return filtered;
    }

    Sophus::SE3d LidarMapping::GetOdom2Map() const
    {
        return odom2map_;
    }

    bool LidarMapping::GetOptimizedPose(int index, Sophus::SE3d &op) const
    {
        if (index >= 0 && index < optimized_pose_.size())
        {
            op = optimized_pose_[index].second;
            return true;
        }
        return false;
    }

    std::deque<FrameDataPtr> LidarMapping::GetKeyFrames()
    {
        return key_frame_;
    }

    bool LidarMapping::LoopMatching(const LoopDataPtr &loop_detect, LoopDataPtr &loop_result)
    {
        if (loop_result == nullptr)
        {
            loop_result = std::make_shared<LoopData>();
        }
        *loop_result = *loop_detect;
        if (loop_result->index_next == -1 || loop_result->index_prev == -1)
        {
            if (!LoopFindKey(loop_result))
            {
                return false;
            }
        }
        // extract cloud
        pcl::PointCloud<pcl::PointXYZI>::Ptr prevKeyframeCloud = LoopFindNearKeyframes(loop_result->index_prev, loop_near_key_num_);
        pcl::PointCloud<pcl::PointXYZI>::Ptr nextKeyframeCloud = LoopFindNearKeyframes(loop_result->index_next, 0);
        if (prevKeyframeCloud->size() < 1000 || nextKeyframeCloud->size() < 300)
        {
            return false;
        }

        // ICP Settings
        static pcl::IterativeClosestPoint<pcl::PointXYZI, pcl::PointXYZI> icp;
        icp.setMaxCorrespondenceDistance(loop_search_radius_ * 2);
        icp.setMaximumIterations(100);
        icp.setTransformationEpsilon(1e-6);
        icp.setEuclideanFitnessEpsilon(1e-6);
        icp.setRANSACIterations(0);

        // Align clouds
        icp.setInputSource(nextKeyframeCloud);
        icp.setInputTarget(prevKeyframeCloud);
        pcl::PointCloud<pcl::PointXYZI>::Ptr unused_result(new pcl::PointCloud<pcl::PointXYZI>());
        icp.align(*unused_result, loop_result->relative_pose.matrix().cast<float>());

        if (icp.hasConverged() == false || icp.getFitnessScore() > loop_fitness_score_)
        {
            return false;
        }
        Eigen::Matrix4d mat = icp.getFinalTransformation().cast<double>();

        loop_result->relative_pose = Sophus::SE3d(Eigen::Quaterniond(mat.block<3, 3>(0, 0)).normalized(), mat.block<3, 1>(0, 3));
        loop_result->noise = loop_result->noise * icp.getFitnessScore();
        return true;
    }

    bool LidarMapping::LoopFindKey(LoopDataPtr &loop_result)
    {
        if (loop_result->index_next == -1)
        {
            for (int i = key_frame_.size() - 1; i >= 0; --i)
            {
                if (key_frame_[i]->stamp + 0.001 > loop_result->stamp_next)
                {
                    loop_result->index_next = key_frame_[i]->index;
                    loop_result->stamp_next = key_frame_[i]->stamp;
                }
                else
                {
                    break;
                }
            }
        }
        if (loop_result->index_next == -1)
        {
            return false;
        }
        if (loop_result->index_prev == -1)
        {
            for (int i = 0; i < key_frame_.size(); --i)
            {
                if (key_frame_[i]->stamp - 0.001 < loop_result->stamp_next)
                {
                    loop_result->index_prev = key_frame_[i]->index;
                    loop_result->stamp_prev = key_frame_[i]->stamp;
                }
                else
                {
                    break;
                }
            }
        }
        if (loop_result->index_prev == -1)
        {
            return false;
        }
        return true;
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr LidarMapping::LoopFindNearKeyframes(const int &key, const int &searchNum)
    {
        // extract near keyframes
        int cloudSize = key_frame_.size();
        pcl::PointCloud<pcl::PointXYZI>::Ptr near_temp(new pcl::PointCloud<pcl::PointXYZI>());
        pcl::PointCloud<pcl::PointXYZI>::Ptr nearKeyframes(new pcl::PointCloud<pcl::PointXYZI>());
        Sophus::SE3d pose_origin_inv = (optimized_pose_[key].second * key_frame_[key]->sensor_to_robot).inverse();
        for (int i = -searchNum; i <= searchNum; ++i)
        {
            int key_near = key + i;
            if (key_near < 0 || key_near >= cloudSize)
                continue;
            LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(key_frame_[key_near]);
            Sophus::SE3d pose = pose_origin_inv * (optimized_pose_[key_near].second * lidar_frame->sensor_to_robot);
            pcl::PointCloud<pcl::PointXYZI>::Ptr corner(new pcl::PointCloud<pcl::PointXYZI>());
            pcl::transformPointCloud(*(lidar_frame->corner_ptr), *corner, pose.matrix().cast<float>());
            *near_temp += *corner;
            pcl::PointCloud<pcl::PointXYZI>::Ptr surf(new pcl::PointCloud<pcl::PointXYZI>());
            pcl::transformPointCloud(*(lidar_frame->surface_ptr), *surf, pose.matrix().cast<float>());
            *near_temp += *surf;
        }

        if (!near_temp->empty())
        {
            loop_filter_->Filter(near_temp, nearKeyframes);
        }
        return nearKeyframes;
    }

    void LidarMapping::AddRobustKernel(g2o::OptimizableGraph::Edge *edge)
    {
        if (robust_kernel_name_ == "NONE")
        {
            return;
        }
        g2o::RobustKernel *kernel = robust_kernel_factory_->construct(robust_kernel_name_);
        if (kernel == nullptr)
        {
            LOG(WARNING) << "warning : invalid robust kernel type: " << robust_kernel_name_ << std::endl;
            return;
        }
        kernel->setDelta(robust_kernel_delta_);
        edge->setRobustKernel(kernel);
        robust_edges_.push_back(edge);
    }

    void LidarMapping::AddEdgeLocalizer(int frame_index, const LocalizerDataPtr &data)
    {
        if (data->GetType() == LocalizerType::PL)
        {
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(frame_index)));
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(frame_index)));

            g2o::EdgePL *edge(new g2o::EdgePL());
            PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(data);
            edge->setMeasurement(*pl_data);
            edge->vertices()[0] = vp;
            edge->vertices()[1] = vq;

            edge->setId(edge_id_);
            edge_id_ = edge_id_ + 1;
            graph_ptr_->addEdge(edge);
            AddRobustKernel(edge);
        }
        else if (data->GetType() == LocalizerType::QL)
        {
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(frame_index)));

            g2o::EdgeQL *edge(new g2o::EdgeQL());
            QLocalizerDataPtr ql_data = std::dynamic_pointer_cast<QLocalizerData>(data);
            edge->setMeasurement(*ql_data);
            edge->vertices()[0] = vq;

            edge->setId(edge_id_);
            edge_id_ = edge_id_ + 1;
            graph_ptr_->addEdge(edge);
            AddRobustKernel(edge);
        }
        else if (data->GetType() == LocalizerType::PoseL)
        {
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(frame_index)));
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(frame_index)));
            g2o::EdgePoseL *edge(new g2o::EdgePoseL());
            PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(data);
            edge->setMeasurement(*posel_data);
            edge->vertices()[0] = vp;
            edge->vertices()[1] = vq;

            edge->setId(edge_id_);
            edge_id_ = edge_id_ + 1;
            graph_ptr_->addEdge(edge);
            AddRobustKernel(edge);
        }
        else
        {
            LOG(ERROR) << "AddEdgeLocalizer fail! Wrong LocalizerType " << data->GetType();
            return;
        }
    }

    void LidarMapping::RemoveEdgeOutlier()
    {
        for (int i = 0; i < robust_edges_.size(); ++i)
        {
            if (robust_edges_[i])
            {
                robust_edges_[i]->computeError();
                double chi2 = robust_edges_[i]->chi2();
                if (chi2 > outlier_chi2_)
                {
                    graph_ptr_->removeEdge(dynamic_cast<g2o::HyperGraph::Edge *>(robust_edges_[i]));
                }
                robust_edges_[i] = nullptr;
            }
        }
    }

    void LidarMapping::RemoveEdgeRobustKernel()
    {
        for (int i = 0; i < robust_edges_.size(); ++i)
        {
            if (robust_edges_[i])
            {
                robust_edges_[i]->setRobustKernel(nullptr);
            }
        }
        robust_edges_.clear();
    }
}
