/**
 * @file q_localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-19
 * @brief q localizer
 */

#include "yd_fusion_localization/localizer/q_localizer.hpp"

namespace yd_fusion_localization
{
    QLocalizer::QLocalizer(const YAML::Node &yaml_node) : Localizer(yaml_node)
    {
        if (yaml_node["invalid_ranges"].IsDefined())
        {
            std::vector<double> inr = yaml_node["invalid_ranges"].as<std::vector<double>>();
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = inr[i];
            }
        }
        else
        {
            for (int i = 0; i < 6; ++i)
            {
                invalid_ranges_(i) = 0;
            }
        }
        angle_threshold_ = yaml_node["angle_threshold"].as<double>();
    }

    bool QLocalizer::CheckPosition(const Sophus::SE3d &pose)
    {
        Eigen::Vector3d p = pose.translation();
        if (p[0] > invalid_ranges_[0] && p[0] < invalid_ranges_[1] &&
            p[1] > invalid_ranges_[2] && p[1] < invalid_ranges_[3] &&
            p[2] > invalid_ranges_[4] && p[2] < invalid_ranges_[5])
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool QLocalizer::LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose)
    {
        Eigen::Quaterniond result_q;
        if (!GetPose(data, result_q))
        {
            return false;
        }
        pose.push_back(Sophus::SE3d(result_q, Eigen::Vector3d::Zero()));
        return true;
    }

    bool QLocalizer::Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data)
    {
        return false;
    }

    bool QLocalizer::GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data)
    {
        Eigen::Quaterniond result_q;
        if (!GetPose(data, result_q))
        {
            return false;
        }

        if (std::acos(std::fabs((guess.pose.unit_quaternion().inverse() * result_q).w())) * 2 > angle_threshold_)
        {
            return false;
        }

        QLocalizerDataPtr ql_data = std::dynamic_pointer_cast<QLocalizerData>(localizer_data);
        ql_data->sensor_to_robot = data->sensor_to_robot;
        ql_data->local_to_map = local_to_map_;
        ql_data->noise = data->noise;
        ql_data->stamp = data->stamp;
        ql_data->q = result_q;
        return true;
    }
    bool QLocalizer::GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data)
    {
        Eigen::Quaterniond result_q;
        if (!GetPose(data, result_q))
        {
            return false;
        }

        QLocalizerDataPtr ql_data = std::dynamic_pointer_cast<QLocalizerData>(localizer_data);
        ql_data->sensor_to_robot = data->sensor_to_robot;
        ql_data->local_to_map = local_to_map_;
        ql_data->noise = data->noise;
        ql_data->stamp = data->stamp;
        ql_data->q = result_q;
        return true;
    }
    bool QLocalizer::Save(const MState &mstate, const MeasureDataPtr &data)
    {
        return false;
    }
    bool QLocalizer::Save()
    {
        return false;
    }
    bool QLocalizer::GetPose(const MeasureDataPtr &data, Eigen::Quaterniond &result_q)
    {
        QDataPtr q_data = std::dynamic_pointer_cast<QData>(data);
        result_q = q_data->q;
        return true;
    }
} // namespace yd_fusion_localization