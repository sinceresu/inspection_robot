/**
 * @file imagelandmark_localizer.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-10-15
 * @brief imagelandmark localizer
 */

#include "yd_fusion_localization/localizer/imagelandmark_localizer.hpp"
#include "tag36h11.h"
#include "apriltag_pose.h"

namespace yd_fusion_localization
{
    ImageLandmarkLocalizer::ImageLandmarkLocalizer(const YAML::Node &yaml_node) : Localizer(yaml_node)
    {
        tag_size_ = yaml_node["tag_size"].as<double>();
        intrinsic_mat_ << yaml_node["intrinsic_matrix"][0].as<float>(),
            yaml_node["intrinsic_matrix"][1].as<float>(),
            yaml_node["intrinsic_matrix"][2].as<float>(),
            yaml_node["intrinsic_matrix"][3].as<float>(),
            yaml_node["intrinsic_matrix"][4].as<float>(),
            yaml_node["intrinsic_matrix"][5].as<float>(),
            yaml_node["intrinsic_matrix"][6].as<float>(),
            yaml_node["intrinsic_matrix"][7].as<float>(),
            yaml_node["intrinsic_matrix"][8].as<float>();
        if (yaml_node["valid_ranges"].IsDefined())
        {
            std::vector<double> inr = yaml_node["valid_ranges"].as<std::vector<double>>();
            for (int i = 0; i < 6; ++i)
            {
                valid_ranges_(i) = inr[i];
            }
        }
        else
        {
            for (int i = 0; i < 6; ++i)
            {
                valid_ranges_(i) = 0;
            }
        }
        distance_threshold_ = yaml_node["distance_threshold"].as<double>();
        angle_threshold_ = yaml_node["angle_threshold"].as<double>();
    }

    bool ImageLandmarkLocalizer::CheckPosition(const Sophus::SE3d &pose)
    {
        Eigen::Vector3d p = pose.translation();
        if (p[0] > valid_ranges_[0] && p[0] < valid_ranges_[1] &&
            p[1] > valid_ranges_[2] && p[1] < valid_ranges_[3] &&
            p[2] > valid_ranges_[4] && p[2] < valid_ranges_[5])
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ImageLandmarkLocalizer::LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose)
    {
        Sophus::SE3d result_pose;
        if (!GetPose(data, result_pose))
        {
            return false;
        }
        pose.push_back(result_pose);
        return true;
    }

    bool ImageLandmarkLocalizer::Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data)
    {
        return GetPose(data, mstate, localizer_data);
    }

    bool ImageLandmarkLocalizer::GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data)
    {
        Sophus::SE3d result_pose;
        if (!GetPose(data, result_pose))
        {
            return false;
        }

        if ((guess.pose.translation() - result_pose.translation()).norm() > distance_threshold_ || std::acos(std::fabs((guess.pose.unit_quaternion().inverse() * result_pose.unit_quaternion()).w())) * 2 > angle_threshold_)
        {
            return false;
        }

        PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(localizer_data);
        posel_data->sensor_to_robot = data->sensor_to_robot;
        posel_data->local_to_map = local_to_map_;
        posel_data->noise = data->noise;
        posel_data->stamp = data->stamp;
        posel_data->pose = result_pose;
        return true;
    }
    bool ImageLandmarkLocalizer::GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data)
    {
        return false;
    }
    bool ImageLandmarkLocalizer::Save(const MState &mstate, const MeasureDataPtr &data)
    {
        return false;
    }
    bool ImageLandmarkLocalizer::Save()
    {
        return false;
    }
    bool ImageLandmarkLocalizer::GetPose(const MeasureDataPtr &data, Sophus::SE3d &result_pose)
    {
        ImageDataPtr img_data = std::dynamic_pointer_cast<ImageData>(data);

        apriltag_detection_info_t info;
        info.tagsize = tag_size_;
        info.fx = intrinsic_mat_(0, 0);
        info.fy = intrinsic_mat_(1, 1);
        info.cx = intrinsic_mat_(0, 2);
        info.cy = intrinsic_mat_(1, 2);
        cv::Mat gray;
        cvtColor(img_data->image_ptr->image, gray, cv::COLOR_BGR2GRAY);
        apriltag_family_t *tf = tag36h11_create();
        apriltag_detector_t *td = apriltag_detector_create();
        apriltag_detector_add_family(td, tf);
        image_u8_t im = {.width = gray.cols,
                         .height = gray.rows,
                         .stride = gray.cols,
                         .buf = gray.data};
        zarray_t *detections = apriltag_detector_detect(td, &im);
        if (zarray_size(detections) == 0)
        {
            apriltag_detections_destroy(detections);
            apriltag_detector_destroy(td);
            tag36h11_destroy(tf);
            return false;
        }
        apriltag_detection_t *det;
        zarray_get(detections, 0, &det);
        info.det = det;
        apriltag_pose_t pose;
        double err = estimate_tag_pose(&info, &pose);
        Eigen::Isometry3d T;
        Eigen::Matrix3d R;
        Eigen::Vector3d transpose;
        R << pose.R->data[0], pose.R->data[1], pose.R->data[2],
            pose.R->data[3], pose.R->data[4], pose.R->data[5],
            pose.R->data[6], pose.R->data[7], pose.R->data[8];
        transpose << pose.t->data[0], pose.t->data[1], pose.t->data[2];
        T.linear() = R;
        T.translation() = transpose;
        Eigen::Isometry3d T_inv = T.inverse();
        apriltag_detections_destroy(detections);
        apriltag_detector_destroy(td);
        tag36h11_destroy(tf);
        if (T_inv.linear().determinant() < 0.95 || T_inv.linear().determinant() > 1.05)
        {
            return false;
        }
        result_pose = Sophus::SE3d(T_inv.linear(), T_inv.translation());
        return true;
    }
} // namespace yd_fusion_localization