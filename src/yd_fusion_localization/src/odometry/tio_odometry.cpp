/**
 * @file tio_odometry.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-29
 * @brief tio_odometry
 */

#include "yd_fusion_localization/odometry/tio_odometry.hpp"

namespace yd_fusion_localization
{
    using namespace symbol_shorthand;
    TioOdometry::TioOdometry(const YAML::Node &yaml_node) : Odometry(yaml_node)
    {
        std::string state_type = yaml_node["state_type"].as<std::string>();
        if (state_type == "Basic")
        {
            state_type_ = StateType::Basic;
        }
        else if (state_type == "Bg")
        {
            state_type_ = StateType::Bg;
        }
        else if (state_type == "VBaBg")
        {
            state_type_ = StateType::VBaBg;
        }
        else
        {
            LOG(ERROR) << "TioOdometry state_type " << state_type << " wrong!";
            return;
        }

        window_size_ = yaml_node["window_size"].as<int>();
        max_vel_ = yaml_node["max_vel"].as<double>();
        max_gyro_ = yaml_node["max_gyro"].as<double>();
        max_iteration_num_ = yaml_node["max_iteration_num"].as<int>();

        graph_ptr_.reset(new g2o::SparseOptimizer());
        std::string solver_type = yaml_node["solver_type"].as<std::string>();
        g2o::OptimizationAlgorithmFactory *solver_factory = g2o::OptimizationAlgorithmFactory::instance();
        g2o::OptimizationAlgorithmProperty solver_property;
        g2o::OptimizationAlgorithm *solver = solver_factory->construct(solver_type, solver_property);
        graph_ptr_->setAlgorithm(solver);
        if (!graph_ptr_->solver())
        {
            LOG(ERROR) << "g2o solver fail！";
            solver_factory->listSolvers(std::cout);
        }
        last_key_frame_ = std::make_shared<TIOFrameData>();
        second2last_key_frame_ = std::make_shared<TIOFrameData>();
        Reset();
    }

    TioOdometry::~TioOdometry() {}

    void TioOdometry::Reset()
    {
        graph_ptr_->clear();
        last_key_frame_->stamp = -1.0;
        last_key_frame_->index = -1;
        second2last_key_frame_->stamp = -2.0;
        second2last_key_frame_->index = -2;
        edge_id_ = 0;
        key_num_ = 0;
    }

    bool TioOdometry::Initialize(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data)
    {
        Reset();
        if (local_data.empty() || local_data.front()->GetType() != LocalizerType::PoseL)
        {
            return false;
        }
        PoseLocalizerDataPtr pose_data = std::dynamic_pointer_cast<PoseLocalizerData>(local_data.front());
        Sophus::SE3d pose = pose_data->local_to_map * pose_data->pose * pose_data->sensor_to_robot.inverse();
        g2o::VertexVec *vp(new g2o::VertexVec());
        vp->setId(P(0));
        vp->setEstimate(pose.translation());
        graph_ptr_->addVertex(vp);
        g2o::VertexQ *vq(new g2o::VertexQ());
        vq->setId(Q(0));
        vq->setEstimate(pose.unit_quaternion());
        graph_ptr_->addVertex(vq);

        if (state_type_ == StateType::Bg)
        {
            g2o::VertexVec *vbg(new g2o::VertexVec());
            vbg->setId(G(0));
            vbg->setEstimate(Eigen::Vector3d::Zero());
            graph_ptr_->addVertex(vbg);
        }
        else if (state_type_ == StateType::VBaBg)
        {
            g2o::VertexVec *vv(new g2o::VertexVec());
            vv->setId(V(0));
            vv->setEstimate(Eigen::Vector3d::Zero());
            graph_ptr_->addVertex(vv);
            g2o::VertexVec *vba(new g2o::VertexVec());
            vba->setId(A(0));
            vba->setEstimate(Eigen::Vector3d::Zero());
            graph_ptr_->addVertex(vba);
            g2o::VertexVec *vbg(new g2o::VertexVec());
            vbg->setId(G(0));
            vbg->setEstimate(Eigen::Vector3d::Zero());
            graph_ptr_->addVertex(vbg);
        }
        key_num_++;
        AddEdgeLocalizer(0, pose_data);

        last_key_frame_->sensor_to_robot = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());
        last_key_frame_->stamp = pose_data->stamp;
        last_key_frame_->odometry = pose;
        last_key_frame_->index = 0;
        last_key_frame_->v = Eigen::Vector3d::Zero();
        last_key_frame_->ba = Eigen::Vector3d::Zero();
        last_key_frame_->bg = Eigen::Vector3d::Zero();
        last_key_frame_->type = state_type_;
        if (result_data == nullptr)
        {
            result_data = std::make_shared<TIOFrameData>();
        }
        TIOFrameDataPtr tio_frame = std::dynamic_pointer_cast<TIOFrameData>(result_data);
        *tio_frame = *last_key_frame_;

        return true;
    }

    bool TioOdometry::Handle(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data)
    {
        if (!IfInited())
        {
            return false;
        }
        if (local_data.empty() || local_data.front()->GetType() != LocalizerType::PoseL)
        {
            return false;
        }
        PoseLocalizerDataPtr pose_data = std::dynamic_pointer_cast<PoseLocalizerData>(local_data.front());
        int index = (last_key_frame_->index + 1) % window_size_;
        SlideWindow(index);
        Sophus::SE3d pose = pose_data->local_to_map * pose_data->pose * pose_data->sensor_to_robot.inverse();
        g2o::VertexVec *vp(new g2o::VertexVec());
        vp->setId(P(index));
        vp->setEstimate(pose.translation());
        graph_ptr_->addVertex(vp);
        g2o::VertexQ *vq(new g2o::VertexQ());
        vq->setId(Q(index));
        vq->setEstimate(pose.unit_quaternion());
        graph_ptr_->addVertex(vq);

        if (state_type_ == StateType::Bg)
        {
            g2o::VertexVec *vbg(new g2o::VertexVec());
            vbg->setId(G(index));
            vbg->setEstimate(last_key_frame_->bg);
            graph_ptr_->addVertex(vbg);
        }
        else if (state_type_ == StateType::VBaBg)
        {
            g2o::VertexVec *vv(new g2o::VertexVec());
            vv->setId(V(index));
            vv->setEstimate(last_key_frame_->v);
            graph_ptr_->addVertex(vv);
            g2o::VertexVec *vba(new g2o::VertexVec());
            vba->setId(A(index));
            vba->setEstimate(last_key_frame_->ba);
            graph_ptr_->addVertex(vba);
            g2o::VertexVec *vbg(new g2o::VertexVec());
            vbg->setId(G(index));
            vbg->setEstimate(last_key_frame_->bg);
            graph_ptr_->addVertex(vbg);
        }
        key_num_++;

        for (int i = 0; i < predict_data.size(); ++i)
        {
            AddEdgePredict(last_key_frame_->index, index, predict_data[i]);
        }
        AddEdgeLocalizer(index, pose_data);
        Optimize();
        SaveKeyFrame(index, pose_data->stamp);
        if (result_data == nullptr)
        {
            result_data = std::make_shared<TIOFrameData>();
        }
        TIOFrameDataPtr tio_frame = std::dynamic_pointer_cast<TIOFrameData>(result_data);
        *tio_frame = *last_key_frame_;
        return true;
    }

    bool TioOdometry::PredictMState(const std::deque<PredictDataPtr> &predict_data, MState &mstate)
    {
        State result_state;
        bool res1 = DeadReckoning(predict_data, result_state);
        if (!res1)
        {
            LOG(INFO) << "!res1!!!";
            mstate.pose = last_key_frame_->odometry;
        }
        else
        {
            mstate.pose = Sophus::SE3d(result_state.q, result_state.p);
            mstate.pose.translation() = last_key_frame_->odometry.translation();
        }

        bool res2 = GetVelocity(predict_data, mstate.linear_velocity, mstate.angular_velocity);
        if (!res2)
        {
            LOG(INFO) << "!res2!!!";
        }
        if (!res1 || !res2)
        {
            if (last_key_frame_->stamp - second2last_key_frame_->stamp < 0.2)
            {
                double dt = last_key_frame_->stamp - second2last_key_frame_->stamp;
                double dt2 = mstate.stamp - last_key_frame_->stamp;
                Eigen::Matrix<double, 6, 1> twist = (second2last_key_frame_->odometry.inverse() * last_key_frame_->odometry).log();
                if (!res1)
                {
                    mstate.pose = last_key_frame_->odometry * Sophus::SE3d::exp(dt2 / dt * twist);
                    res1 = true;
                }
                if (!res2)
                {
                    mstate.linear_velocity = twist.head<3>() / dt;
                    mstate.angular_velocity = twist.tail<3>() / dt;
                    res2 = true;
                }
            }
        }
        return res1 && res2;
    }

    double TioOdometry::GetCurrentStamp() const
    {
        return last_key_frame_->stamp;
    }

    void TioOdometry::SlideWindow(int index)
    {
        auto vp0 = graph_ptr_->vertex(P(index));
        if (vp0 == nullptr)
        {
            return;
        }
        graph_ptr_->removeVertex(graph_ptr_->vertex(P(index)));
        graph_ptr_->removeVertex(graph_ptr_->vertex(Q(index)));
        if (state_type_ == StateType::VBaBg)
        {
            graph_ptr_->removeVertex(graph_ptr_->vertex(V(index)));
            graph_ptr_->removeVertex(graph_ptr_->vertex(A(index)));
            graph_ptr_->removeVertex(graph_ptr_->vertex(G(index)));
        }
        else if (state_type_ == StateType::Bg)
        {
            graph_ptr_->removeVertex(graph_ptr_->vertex(G(index)));
        }
        key_num_--;
    }

    void TioOdometry::AddEdgePredict(int index1, int index2, const PredictDataPtr &data)
    {
        g2o::VertexVec *vp1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(index1)));
        g2o::VertexQ *vq1 = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(index1)));

        g2o::VertexVec *vp2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(index2)));
        g2o::VertexQ *vq2 = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(index2)));

        if (data->GetType() == PredictType::Odometry)
        {
            g2o::EdgeOdom *edge(new g2o::EdgeOdom());
            OdometryDataPtr odom_data = std::dynamic_pointer_cast<OdometryData>(data);
            std::shared_ptr<PreIntegrationOdomEx> pre = std::make_shared<PreIntegrationOdomEx>(*odom_data);
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vp2;
            edge->vertices()[3] = vq2;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);
        }
        else if (data->GetType() == PredictType::Imu)
        {
            g2o::VertexVec *vv1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(V(index1)));
            g2o::VertexVec *vba1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(A(index1)));
            g2o::VertexVec *vbg1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(G(index1)));

            g2o::VertexVec *vv2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(V(index2)));
            g2o::VertexVec *vba2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(A(index2)));
            g2o::VertexVec *vbg2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(G(index2)));

            g2o::EdgeImu *edge(new g2o::EdgeImu());
            ImuDataPtr imu_data = std::dynamic_pointer_cast<ImuData>(data);
            std::shared_ptr<PreIntegrationImuEx> pre = std::make_shared<PreIntegrationImuEx>(*imu_data, vba1->estimate(), vbg1->estimate());
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vv1;
            edge->vertices()[3] = vba1;
            edge->vertices()[4] = vbg1;
            edge->vertices()[5] = vp2;
            edge->vertices()[6] = vq2;
            edge->vertices()[7] = vv2;
            edge->vertices()[8] = vba2;
            edge->vertices()[9] = vbg2;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);
        }
        else if (data->GetType() == PredictType::Twist)
        {
            g2o::EdgeTwist *edge(new g2o::EdgeTwist());
            TwistDataPtr twist_data = std::dynamic_pointer_cast<TwistData>(data);
            std::shared_ptr<PreIntegrationTwistEx> pre = std::make_shared<PreIntegrationTwistEx>(*twist_data);
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vp2;
            edge->vertices()[3] = vq2;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);

            if (state_type_ == StateType::VBaBg)
            {
                g2o::VertexVec *vv2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(V(index2)));
                g2o::EdgeVelocity *edge2(new g2o::EdgeVelocity(*twist_data));
                edge2->vertices()[0] = vq2;
                edge2->vertices()[1] = vv2;

                edge2->setId(edge_id_);
                edge_id_++;
                graph_ptr_->addEdge(edge2);
            }
        }
        else if (data->GetType() == PredictType::Vg)
        {
            g2o::VertexVec *vbg1 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(G(index1)));
            g2o::VertexVec *vbg2 = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(G(index2)));

            g2o::EdgeVg *edge(new g2o::EdgeVg());
            VgDataPtr vg_data = std::dynamic_pointer_cast<VgData>(data);
            std::shared_ptr<PreIntegrationVgEx> pre = std::make_shared<PreIntegrationVgEx>(*vg_data, vbg1->estimate());
            edge->setMeasurement(pre);
            edge->vertices()[0] = vp1;
            edge->vertices()[1] = vq1;
            edge->vertices()[2] = vbg1;
            edge->vertices()[3] = vp2;
            edge->vertices()[4] = vq2;
            edge->vertices()[5] = vbg2;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);
        }
    }

    void TioOdometry::AddEdgeLocalizer(int index, const LocalizerDataPtr &data)
    {
        if (data->GetType() == LocalizerType::PL)
        {
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(index)));
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(index)));

            g2o::EdgePL *edge(new g2o::EdgePL());
            PLocalizerDataPtr pl_data = std::dynamic_pointer_cast<PLocalizerData>(data);
            edge->setMeasurement(*pl_data);
            edge->vertices()[0] = vp;
            edge->vertices()[1] = vq;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);
        }
        else if (data->GetType() == LocalizerType::QL)
        {
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(index)));

            g2o::EdgeQL *edge(new g2o::EdgeQL());
            QLocalizerDataPtr ql_data = std::dynamic_pointer_cast<QLocalizerData>(data);
            edge->setMeasurement(*ql_data);
            edge->vertices()[0] = vq;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);
        }
        else if (data->GetType() == LocalizerType::PoseL)
        {
            g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(index)));
            g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(index)));
            g2o::EdgePoseL *edge(new g2o::EdgePoseL());
            PoseLocalizerDataPtr posel_data = std::dynamic_pointer_cast<PoseLocalizerData>(data);
            edge->setMeasurement(*posel_data);
            edge->vertices()[0] = vp;
            edge->vertices()[1] = vq;

            edge->setId(edge_id_);
            edge_id_++;
            graph_ptr_->addEdge(edge);
        }
    }

    void TioOdometry::Optimize()
    {
        if (graph_ptr_->edges().size() < 1)
        {
            return;
        }

        graph_ptr_->initializeOptimization();
        graph_ptr_->computeInitialGuess();
        graph_ptr_->computeActiveErrors();
        graph_ptr_->setVerbose(false);

        double chi2 = graph_ptr_->chi2();

        graph_ptr_->optimize(max_iteration_num_);

        double chi22 = graph_ptr_->chi2();
        // for (auto &it : graph_ptr_->edges())
        // {
        //     std::cout << dynamic_cast<g2o::OptimizableGraph::Edge *>(it)->chi2() << std::endl;
        // }

        // LOG(INFO) << std::endl
        //           << "------ TioOdometry::Optimize -------" << std::endl
        //           << "Vertices: " << graph_ptr_->vertices().size() << ", Edges: " << graph_ptr_->edges().size() << std::endl
        //           << ". Chi2: " << chi2 << "--->" << chi22 << std::endl;
    }

    void TioOdometry::SaveKeyFrame(int index, double stamp)
    {
        *second2last_key_frame_ = *last_key_frame_;
        last_key_frame_->index = index;
        last_key_frame_->stamp = stamp;
        g2o::VertexVec *vp = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(P(index)));
        g2o::VertexQ *vq = dynamic_cast<g2o::VertexQ *>(graph_ptr_->vertex(Q(index)));
        last_key_frame_->odometry = Sophus::SE3d(vq->estimate().normalized(), vp->estimate());
        if (state_type_ == StateType::VBaBg)
        {
            g2o::VertexVec *vv = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(V(index)));
            g2o::VertexVec *vba = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(A(index)));
            g2o::VertexVec *vbg = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(G(index)));
            last_key_frame_->v = vv->estimate();
            last_key_frame_->ba = vba->estimate();
            last_key_frame_->bg = vbg->estimate();
            if (vv->hessianIndex() != -1 && vba->hessianIndex() != -1 && vbg->hessianIndex() != -1)
            {
                last_key_frame_->type = StateType::VBaBg;
            }
            else if (vbg->hessianIndex() != -1)
            {
                last_key_frame_->type = StateType::Bg;
            }
            else
            {
                last_key_frame_->type = StateType::Basic;
            }
        }
        else if (state_type_ == StateType::Bg)
        {
            g2o::VertexVec *vbg = dynamic_cast<g2o::VertexVec *>(graph_ptr_->vertex(G(index)));
            last_key_frame_->bg = vbg->estimate();
            if (vbg->hessianIndex() != -1)
            {
                last_key_frame_->type = StateType::Bg;
            }
            else
            {
                last_key_frame_->type = StateType::Basic;
            }
        }
        else
        {
            last_key_frame_->type = StateType::Basic;
        }
    }

    bool TioOdometry::DeadReckoning(const std::deque<PredictDataPtr> &predict_data, State &result_state)
    {
        if (last_key_frame_->type == StateType::VBaBg || last_key_frame_->type == StateType::Bg)
        {
            int index_twist = -1;
            int index_imu = -1;
            for (int i = 0; i < predict_data.size(); ++i)
            {
                if (predict_data[i]->GetType() == PredictType::Twist)
                {
                    index_twist = i;
                }
                else if (predict_data[i]->GetType() == PredictType::Imu)
                {
                    index_imu = i;
                }
                if (index_twist != -1 && index_imu != -1)
                {
                    break;
                }
            }
            if (index_twist != -1 && index_imu != -1)
            {
                TwistDataPtr twist_data = std::dynamic_pointer_cast<TwistData>(predict_data[index_twist]);
                ImuDataPtr imu_data = std::dynamic_pointer_cast<ImuData>(predict_data[index_imu]);
                VgDataPtr vg_data = std::make_shared<VgData>();
                ParseVgData(twist_data, imu_data, vg_data);
                if (DeadReckoning(std::dynamic_pointer_cast<PredictData>(vg_data), result_state))
                {
                    return true;
                }
            }
        }
        int index = -1;
        for (int i = 0; i < predict_data.size(); ++i)
        {
            if (last_key_frame_->type != StateType::VBaBg && predict_data[i]->GetType() == PredictType::Imu)
            {
                continue;
            }
            else if ((last_key_frame_->type != StateType::VBaBg || last_key_frame_->type != StateType::Bg) && predict_data[i]->GetType() == PredictType::Vg)
            {
                continue;
            }

            if (DeadReckoning(predict_data[i], result_state))
            {
                return true;
            }
            else
            {
                continue;
            }
        }
        return false;
    }

    bool TioOdometry::DeadReckoning(const PredictDataPtr &predict_data, State &result_state)
    {
        State state;
        state.stamp = last_key_frame_->stamp;
        state.type = last_key_frame_->type;
        state.p = last_key_frame_->odometry.translation();
        state.q = last_key_frame_->odometry.unit_quaternion();
        state.v = last_key_frame_->v;
        state.ba = last_key_frame_->ba;
        state.bg = last_key_frame_->bg;
        state.covariance = Eigen::Matrix<double, 15, 15>::Zero();
        if (predict_data->GetType() == PredictType::Vg)
        {
            result_state = PreIntegrationVgEx::DeadReckoning(state, *(std::dynamic_pointer_cast<VgData>(predict_data)));
        }
        else if (predict_data->GetType() == PredictType::Twist)
        {
            result_state = PreIntegrationTwistEx::DeadReckoning(state, *(std::dynamic_pointer_cast<TwistData>(predict_data)));
        }
        else if (predict_data->GetType() == PredictType::Imu)
        {
            result_state = PreIntegrationImuEx::DeadReckoning(state, *(std::dynamic_pointer_cast<ImuData>(predict_data)));
        }
        else if (predict_data->GetType() == PredictType::Odometry)
        {
            result_state = PreIntegrationOdomEx::DeadReckoning(state, *(std::dynamic_pointer_cast<OdometryData>(predict_data)));
        }
        else
        {
            return false;
        }
        result_state.q.normalize();
        double dt = result_state.stamp - state.stamp;
        double dp = (state.p - result_state.p).norm();
        double dr = std::acos(std::fabs((state.q.inverse() * result_state.q).w())) * 2;
        if (dp < max_vel_ * dt && dr < max_gyro_ * dt)
        {
            return true;
        }

        return false;
    }

    void TioOdometry::ParseVgData(const TwistDataPtr &twist_data, const ImuDataPtr &imu_data, VgDataPtr &vg_data)
    {
        vg_data->Clear();
        vg_data->sensor_to_robot = twist_data->sensor_to_robot;
        vg_data->sensor_to_robot_gyro = imu_data->sensor_to_robot;
        vg_data->noise.resize(9);
        vg_data->noise.head(3) = twist_data->noise.head(3);
        vg_data->noise.segment(3, 3) = imu_data->noise.segment(3, 3);
        vg_data->noise.tail(3) = imu_data->noise.tail(3);

        double stamp = twist_data->stamps.front();
        vg_data->stamps.push_back(stamp);
        vg_data->vel.push_back(twist_data->vel.front());
        vg_data->gyro.push_back(imu_data->gyro.front());
        int index1 = 1;
        int index2 = 1;
        while (index1 + 1 < twist_data->stamps.size() || index2 + 1 < imu_data->stamps.size())
        {
            assert(index1 < twist_data->stamps.size());
            assert(index2 < imu_data->stamps.size());
            if (twist_data->stamps[index1] < imu_data->stamps[index2])
            {
                stamp = twist_data->stamps[index1];
                if (stamp - vg_data->stamps.back() < 1e-5)
                {
                    index1++;
                    continue;
                }
                vg_data->stamps.push_back(stamp);
                vg_data->vel.push_back(twist_data->vel[index1]);
                vg_data->gyro.push_back(Interpolate(stamp, imu_data->stamps[index2 - 1], imu_data->stamps[index2], imu_data->gyro[index2 - 1], imu_data->gyro[index2]));
                index1++;
            }
            else
            {
                stamp = imu_data->stamps[index2];
                if (stamp - vg_data->stamps.back() < 1e-5)
                {
                    index2++;
                    continue;
                }
                vg_data->stamps.push_back(stamp);
                vg_data->gyro.push_back(imu_data->gyro[index2]);
                vg_data->vel.push_back(Interpolate(stamp, twist_data->stamps[index1 - 1], twist_data->stamps[index1], twist_data->vel[index1 - 1], twist_data->vel[index1]));
                index2++;
            }
        }
        stamp = twist_data->stamps.back();
        if (stamp - vg_data->stamps.back() < 1e-5)
        {
            vg_data->stamps.pop_back();
            vg_data->vel.pop_back();
            vg_data->gyro.pop_back();
        }
        vg_data->stamps.push_back(stamp);
        vg_data->vel.push_back(twist_data->vel.back());
        vg_data->gyro.push_back(imu_data->gyro.back());
    }

    bool TioOdometry::GetVelocity(const std::deque<PredictDataPtr> &predict_data, Eigen::Vector3d &linear_velocity, Eigen::Vector3d &angular_velocity)
    {
        linear_velocity = Eigen::Vector3d::Zero();
        angular_velocity = Eigen::Vector3d::Zero();
        bool first_angular = false;
        bool first_linear = false;
        double sumdt;
        auto fi = std::find_if(predict_data.begin(), predict_data.end(), [this](const PredictDataPtr &it) { return it->GetType() == PredictType::Imu; });
        if (fi != predict_data.end())
        {
            ImuDataPtr imudata = std::dynamic_pointer_cast<ImuData>(*fi);
            for (int i = 1; i < imudata->stamps.size(); ++i)
            {
                if (imudata->stamps[i] > imudata->stamps.back() - 0.1)
                {
                    if (!first_angular)
                    {
                        sumdt = imudata->stamps.back() - imudata->stamps[i - 1];
                        first_angular = true;
                    }
                    double half_dt = 0.5 * (imudata->stamps[i] - imudata->stamps[i - 1]);
                    angular_velocity += half_dt * (imudata->gyro[i - 1] + imudata->gyro[i]);
                }
            }
            angular_velocity = angular_velocity / sumdt;
            angular_velocity = angular_velocity - last_key_frame_->bg;
            angular_velocity = imudata->sensor_to_robot.unit_quaternion() * angular_velocity;
        }

        fi = std::find_if(predict_data.begin(), predict_data.end(), [this](const PredictDataPtr &it) { return it->GetType() == PredictType::Vg; });
        if (fi != predict_data.end())
        {
            VgDataPtr vgdata = std::dynamic_pointer_cast<VgData>(*fi);
            for (int i = 1; i < vgdata->stamps.size(); ++i)
            {
                if (vgdata->stamps[i] > vgdata->stamps.back() - 0.1)
                {
                    if (!first_linear)
                    {
                        sumdt = vgdata->stamps.back() - vgdata->stamps[i - 1];
                        first_linear = true;
                    }
                    double half_dt = 0.5 * (vgdata->stamps[i] - vgdata->stamps[i - 1]);
                    linear_velocity += half_dt * (vgdata->vel[i - 1] + vgdata->vel[i]);
                    if (!first_angular)
                    {
                        angular_velocity += half_dt * (vgdata->gyro[i - 1] + vgdata->gyro[i]);
                    }
                }
            }
            double sumdt = vgdata->stamps.back() - vgdata->stamps.front();
            if (!first_angular)
            {
                angular_velocity = angular_velocity / sumdt;
                angular_velocity = angular_velocity - last_key_frame_->bg;
                angular_velocity = vgdata->sensor_to_robot_gyro.unit_quaternion() * angular_velocity;
                first_angular = true;
            }
            linear_velocity = linear_velocity / sumdt;
            linear_velocity = vgdata->sensor_to_robot.unit_quaternion() * linear_velocity;
            linear_velocity = linear_velocity - angular_velocity.cross(vgdata->sensor_to_robot.translation());
            return true;
        }
        fi = std::find_if(predict_data.begin(), predict_data.end(), [this](const PredictDataPtr &it) { return it->GetType() == PredictType::Twist; });
        if (fi != predict_data.end())
        {
            TwistDataPtr twistdata = std::dynamic_pointer_cast<TwistData>(*fi);
            for (int i = 1; i < twistdata->stamps.size(); ++i)
            {
                if (twistdata->stamps[i] > twistdata->stamps.back() - 0.1)
                {
                    if (!first_linear)
                    {
                        sumdt = twistdata->stamps.back() - twistdata->stamps[i - 1];
                        first_linear = true;
                    }
                }
                double half_dt = 0.5 * (twistdata->stamps[i] - twistdata->stamps[i - 1]);
                linear_velocity += half_dt * (twistdata->vel[i - 1] + twistdata->vel[i]);
                if (!first_angular)
                {
                    angular_velocity += half_dt * (twistdata->gyro[i - 1] + twistdata->gyro[i]);
                }
            }
            double sumdt = twistdata->stamps.back() - twistdata->stamps.front();
            if (!first_angular)
            {
                angular_velocity = angular_velocity / sumdt;
                angular_velocity = twistdata->sensor_to_robot.unit_quaternion() * angular_velocity;
                first_angular = true;
            }
            linear_velocity = linear_velocity / sumdt;
            linear_velocity = twistdata->sensor_to_robot.unit_quaternion() * linear_velocity;
            linear_velocity = linear_velocity - angular_velocity.cross(twistdata->sensor_to_robot.translation());
            return true;
        }
        fi = std::find_if(predict_data.begin(), predict_data.end(), [this](const PredictDataPtr &it) { return it->GetType() == PredictType::Odometry; });
        if (fi != predict_data.end())
        {
            OdometryDataPtr odomdata = std::dynamic_pointer_cast<OdometryData>(*fi);
            for (int i = 1; i < odomdata->stamps.size(); ++i)
            {
                if (odomdata->stamps[i] > odomdata->stamps.back() - 0.1)
                {
                    if (!first_linear)
                    {
                        sumdt = odomdata->stamps.back() - odomdata->stamps[i - 1];
                        first_linear = true;
                        Sophus::SE3d rpose = odomdata->pose[i - 1].inverse() * odomdata->pose.back();
                        if (!first_angular)
                        {
                            angular_velocity = rpose.so3().log() / sumdt;
                            angular_velocity = odomdata->sensor_to_robot.unit_quaternion() * angular_velocity;
                            first_angular = true;
                        }
                        linear_velocity = rpose.translation() / sumdt;
                        linear_velocity = odomdata->sensor_to_robot.unit_quaternion() * linear_velocity;
                        linear_velocity = linear_velocity - angular_velocity.cross(odomdata->sensor_to_robot.translation());
                        return true;
                    }
                }
            }
        }
        return false;
    }
}