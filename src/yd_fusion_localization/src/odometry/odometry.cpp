/**
 * @file odometry.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-8
 * @brief odometry
 */

#include "yd_fusion_localization/odometry/odometry.hpp"

namespace yd_fusion_localization
{
    Odometry::Odometry(const YAML::Node &yaml_node)
        : inited_(false),
          window_inited_(false)
    {
    }

    bool Odometry::IfInited() const
    {
        return inited_;
    }

    void Odometry::SetInited(bool inited)
    {
        inited_ = inited;
    }

    bool Odometry::IfWindowInited() const
    {
        return window_inited_;
    }

    void Odometry::SetWindowInited(bool window_inited)
    {
        window_inited_ = window_inited;
    }
}