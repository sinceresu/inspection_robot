/**
 * @file lidar_odometry.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-10
 * @brief lidar odometry
 */

#include "yd_fusion_localization/odometry/lidar_odometry.hpp"

namespace yd_fusion_localization
{
    LidarOdometry::LidarOdometry(const YAML::Node &yaml_node) : Odometry(yaml_node)
    {
        key_distance_thresh_ = yaml_node["key_distance_thresh"].as<double>();
        key_angle_thresh_ = yaml_node["key_angle_thresh"].as<double>();
        local_map_range_ = yaml_node["local_map_range"].as<double>();
        edge_min_num_thresh_ = yaml_node["edge_min_num_thresh"].as<int>();
        surf_min_num_thresh_ = yaml_node["surf_min_num_thresh"].as<int>();
        odom_trans_thresh_ = yaml_node["odom_trans_thresh"].as<double>();
        odom_rot_thresh_ = yaml_node["odom_rot_thresh"].as<double>();
        window_size_ = yaml_node["window_size"].as<int>();
        translation_iter_thresh_ = yaml_node["translation_iter_thresh"].as<double>();
        rotation_iter_thresh_ = yaml_node["rotation_iter_thresh"].as<double>();

        corner_filter_ = std::make_shared<VoxelFilter<pcl::PointXYZI>>(yaml_node["corner_filter"]);
        surface_filter_ = std::make_shared<VoxelFilter<pcl::PointXYZI>>(yaml_node["surface_filter"]);
        map_filter_ = std::make_shared<VoxelFilter<pcl::PointXYZI>>(yaml_node["map_filter"]);
        AllocateMemory();
        local_map_threads_num_ = yaml_node["local_map_threads_num"].as<int>();
        iteration_num_ = yaml_node["iteration_num"].as<int>();
    }

    LidarOdometry::~LidarOdometry() {}

    void LidarOdometry::Reset()
    {
        key_frame_.clear();
    }

    bool LidarOdometry::Initialize(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data)
    {
        if (!CheckData(predict_data, local_data, pre_data))
        {
            return false;
        }
        QLocalizerDataPtr qdata = nullptr;
        if (!local_data.empty())
        {
            qdata = std::dynamic_pointer_cast<QLocalizerData>(local_data.front());
        }
        CloudFeatureDataPtr data = std::dynamic_pointer_cast<CloudFeatureData>(pre_data.front());
        if (result_data == nullptr)
        {
            result_data = std::make_shared<LidarFrameData>();
        }
        LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(result_data);

        IState new_istate;
        new_istate.stamp = data->stamp;
        if (qdata)
        {
            new_istate.pose = Sophus::SE3d(qdata->q * qdata->sensor_to_robot.unit_quaternion().inverse(), Eigen::Vector3d::Zero());
        }
        else
        {
            new_istate.pose = Sophus::SE3d(Eigen::Quaterniond::Identity(), Eigen::Vector3d::Zero());
        }

        new_istate.pose = new_istate.pose * data->sensor_to_robot;

        ParseData(new_istate, data, lidar_frame);
        ScanMatching(lidar_frame);
        RegisterOnLocalMap(lidar_frame);

        return true;
    }

    bool LidarOdometry::Handle(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data)
    {
        if (!IfInited())
        {
            return false;
        }
        if (!CheckData(predict_data, local_data, pre_data))
        {
            return false;
        }

        CloudFeatureDataPtr data = std::dynamic_pointer_cast<CloudFeatureData>(pre_data.front());
        if (result_data == nullptr)
        {
            result_data = std::make_shared<LidarFrameData>();
        }
        LidarFrameDataPtr lidar_frame = std::dynamic_pointer_cast<LidarFrameData>(result_data);

        IState new_istate = istate;
        new_istate.pose = new_istate.pose * data->sensor_to_robot;
        if (!local_data.empty() && local_data.front()->GetType() == LocalizerType::QL)
        {
            QLocalizerDataPtr qdata = std::dynamic_pointer_cast<QLocalizerData>(local_data.front());
            new_istate.pose.setQuaternion(qdata->q * qdata->sensor_to_robot.unit_quaternion().inverse() * data->sensor_to_robot.unit_quaternion());
        }

        ParseData(new_istate, data, lidar_frame);
        ScanMatching(lidar_frame);
        if (!local_data.empty() && local_data.front()->GetType() == LocalizerType::QL)
        {
            Eigen::Quaterniond qm = lidar_frame->odometry.unit_quaternion().slerp(0.01, new_istate.pose.unit_quaternion());
            lidar_frame->odometry.setQuaternion(qm);
        }
        RegisterOnLocalMap(lidar_frame);
        if (key_frame_.size() == window_size_)
        {
            SetWindowInited(true);
        }
        return true;
    }

    bool LidarOdometry::PredictMState(const std::deque<PredictDataPtr> &predict_data, MState &mstate)
    {
        return false;
    }

    double LidarOdometry::GetCurrentStamp() const
    {
        if (key_frame_.empty())
        {
            return -1.0;
        }
        return key_frame_.back()->stamp;
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr LidarOdometry::GetCurrentScan() const
    {
        return key_frame_.back()->xyzi_ptr;
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr LidarOdometry::GetCornerLocalMap() const
    {
        return corner_local_map_;
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr LidarOdometry::GetSurfaceLocalMap() const
    {
        return surface_local_map_;
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr LidarOdometry::GetLocalMap()
    {
        local_map_tmp_->clear();
        // #pragma omp parallel for num_threads(local_map_threads_num_)
        for (int i = 0; i < (int)key_frame_.size(); ++i)
        {
            *local_map_tmp_ += *key_frame_[i]->xyzi_ptr;
        }
        map_filter_->Filter(local_map_tmp_, local_map_);
        return local_map_;
    }

    void LidarOdometry::AllocateMemory()
    {
        key_frame_.clear();
        corner_local_map_tmp_.reset(new pcl::PointCloud<pcl::PointXYZI>());
        surface_local_map_tmp_.reset(new pcl::PointCloud<pcl::PointXYZI>());
        local_map_tmp_.reset(new pcl::PointCloud<pcl::PointXYZI>());
        corner_local_map_.reset(new pcl::PointCloud<pcl::PointXYZI>());
        surface_local_map_.reset(new pcl::PointCloud<pcl::PointXYZI>());
        local_map_.reset(new pcl::PointCloud<pcl::PointXYZI>());
        kdtree_corner_local_map_.reset(new pcl::KdTreeFLANN<pcl::PointXYZI>());
        kdtree_surface_local_map_.reset(new pcl::KdTreeFLANN<pcl::PointXYZI>());
    }

    bool LidarOdometry::CheckData(const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data)
    {
        if (pre_data.empty())
        {
            return false;
        }
        if (pre_data.front()->GetType() != PreprocessedType::CloudFeature)
        {
            return false;
        }
        return true;
    }

    void LidarOdometry::ParseData(const IState &istate, const CloudFeatureDataPtr &data, LidarFrameDataPtr &lidar_frame)
    {
        q_w_curr_ = istate.pose.unit_quaternion();
        t_w_curr_ = istate.pose.translation();
        lidar_frame->stamp = data->stamp;
        lidar_frame->index = -1;
        lidar_frame->sensor_to_robot = data->sensor_to_robot;
        lidar_frame->noise = data->noise;
        lidar_frame->odometry = istate.pose;
        lidar_frame->xyzi_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        lidar_frame->corner_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        lidar_frame->surface_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
        pcl::copyPointCloud(*data->xyzi_ptr, *lidar_frame->xyzi_ptr);
        corner_filter_->Filter(data->corner_ptr, lidar_frame->corner_ptr);
        surface_filter_->Filter(data->surface_ptr, lidar_frame->surface_ptr);
    }

    bool LidarOdometry::ScanMatching(LidarFrameDataPtr &lidar_frame)
    {
        if (key_frame_.empty())
        {
            return true;
        }
        if (lidar_frame->corner_ptr->size() <= edge_min_num_thresh_ || lidar_frame->surface_ptr->size() <= surf_min_num_thresh_)
        {
            LOG(WARNING) << "LidarOdometry::ScanMatching Not enough features! Only " << lidar_frame->corner_ptr->size() << " edge and " << lidar_frame->surface_ptr->size() << " planar features available.";
            // return false;
        }
        Eigen::Vector3d last_twc = t_w_curr_;
        Eigen::Quaterniond last_qwc = q_w_curr_;
        for (int iterCount = 0; iterCount < iteration_num_; iterCount++)
        {
            ceres::LossFunction *loss_function = new ceres::HuberLoss(0.1);
            ceres::Problem::Options problem_options;
            ceres::Problem problem(problem_options);

            problem.AddParameterBlock(parameters_, 7, new ceres::VertexSE3Ceres());

            int corner_num = AddEdgeCorner(lidar_frame->corner_ptr, corner_local_map_, problem, loss_function);
            int surf_num = AddEdgeSurfaceNormal(lidar_frame->surface_ptr, surface_local_map_, problem, loss_function);
            if (iterCount == 0 && corner_num + surf_num < 50)
            {
                LOG(WARNING) << "LidarOdometry::ScanMatching Not enough matching features! Only " << corner_num << " edge and " << surf_num << " planar features available.";
                // return false;
            }

            ceres::Solver::Options options;
            options.linear_solver_type = ceres::DENSE_QR;
            options.max_num_iterations = 4;
            options.minimizer_progress_to_stdout = false;
            options.check_gradients = false;
            options.gradient_check_relative_precision = 1e-4;
            ceres::Solver::Summary summary;

            ceres::Solve(options, &problem, &summary);

            if (iterCount == 0)
            {
                ceres::Covariance::Options covariance_options;
                ceres::Covariance covariance(covariance_options);

                std::vector<std::pair<const double *, const double *>> covariance_blocks;
                covariance_blocks.push_back(std::make_pair(parameters_, parameters_));

                if (!covariance.Compute(covariance_blocks, &problem))
                {
                    LOG(WARNING) << "LidarOdometry::ScanMatching !covariance.Compute";
                    // return false;
                }
                double covariance_xx[6 * 6];
                covariance.GetCovarianceBlockInTangentSpace(parameters_, parameters_, covariance_xx);
                for (int i = 0; i < 6; ++i)
                {
                    if (covariance_xx[i * 6 + i] > 0.01)
                    {
                        LOG(WARNING) << "LidarOdometry::ScanMatching !covariance_xx";
                        // return false;
                    }
                }
            }
            Eigen::Vector3d delp = t_w_curr_ - last_twc;
            Eigen::Quaterniond delq = last_qwc.inverse() * q_w_curr_;
            delq.normalize();
            double dpn = delp.norm();
            double dqn = std::acos(std::fabs(delq.w())) * 2;
            if (dpn < translation_iter_thresh_ && dqn < rotation_iter_thresh_)
            {
                break;
            }
            last_twc = t_w_curr_;
            last_qwc = q_w_curr_;
        }
        Eigen::Vector3d dp = lidar_frame->odometry.translation() - t_w_curr_;
        double pdis = dp.norm();
        if (pdis > odom_trans_thresh_)
        {
            LOG(WARNING) << "LidarOdometry::ScanMatching pdis " << pdis << " > odom_trans_thresh_ " << odom_trans_thresh_;
            return false;
        }
        Eigen::Quaterniond dq = lidar_frame->odometry.unit_quaternion().inverse() * q_w_curr_;
        dq.normalize();
        double qdis = std::acos(std::fabs(dq.w())) * 2;
        if (qdis > odom_rot_thresh_)
        {
            LOG(WARNING) << "LidarOdometry::ScanMatching qdis " << qdis << " > odom_rot_thresh_ " << odom_rot_thresh_;
            return false;
        }
        lidar_frame->odometry.setQuaternion(q_w_curr_);
        lidar_frame->odometry.translation() = t_w_curr_;
        return true;
    }

    void LidarOdometry::RegisterOnLocalMap(LidarFrameDataPtr &lidar_frame)
    {
        if (IfKeyFrame(lidar_frame))
        {
            lidar_frame->index = key_frame_.empty() ? 0 : key_frame_.back()->index + 1;
            LidarFrameDataPtr new_frame = std::make_shared<LidarFrameData>();
            new_frame->index = lidar_frame->index;
            new_frame->stamp = lidar_frame->stamp;
            new_frame->sensor_to_robot = lidar_frame->sensor_to_robot;
            new_frame->noise = lidar_frame->noise;
            new_frame->odometry = lidar_frame->odometry;
            new_frame->corner_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
            new_frame->surface_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
            new_frame->xyzi_ptr.reset(new pcl::PointCloud<pcl::PointXYZI>());
            Eigen::Matrix4f trans = lidar_frame->odometry.matrix().cast<float>();
            pcl::transformPointCloud(*lidar_frame->corner_ptr, *new_frame->corner_ptr, trans);
            pcl::transformPointCloud(*lidar_frame->surface_ptr, *new_frame->surface_ptr, trans);
            pcl::transformPointCloud(*lidar_frame->xyzi_ptr, *new_frame->xyzi_ptr, trans);
            key_frame_.emplace_back(new_frame);
            SlideWindow();
        }
    }

    bool LidarOdometry::IfKeyFrame(const LidarFrameDataPtr &lidar_frame)
    {
        if (key_frame_.empty())
        {
            return true;
        }
        if (FramePdistance(key_frame_.back(), lidar_frame) < key_distance_thresh_ && FramePdistance(key_frame_.back(), lidar_frame) < key_angle_thresh_)
        {
            return false;
        }
        return true;
    }

    void LidarOdometry::SlideWindow()
    {
        while (!key_frame_.empty() && FramePdistance(key_frame_.front(), key_frame_.back()) > local_map_range_)
        {
            key_frame_.pop_front();
        }
        // fuse the map
        corner_local_map_tmp_->clear();
        surface_local_map_tmp_->clear();
        // #pragma omp parallel for num_threads(local_map_threads_num_)
        for (int i = 0; i < (int)key_frame_.size(); ++i)
        {
            *corner_local_map_tmp_ += *key_frame_[i]->corner_ptr;
            *surface_local_map_tmp_ += *key_frame_[i]->surface_ptr;
        }
        // Downsample the surrounding corner and surface key frames (or map)
        corner_filter_->Filter(corner_local_map_tmp_, corner_local_map_);
        surface_filter_->Filter(surface_local_map_tmp_, surface_local_map_);
        kdtree_corner_local_map_->setInputCloud(corner_local_map_);
        kdtree_surface_local_map_->setInputCloud(surface_local_map_);
    }

    double LidarOdometry::FramePdistance(const LidarFrameDataPtr &a, const LidarFrameDataPtr &b)
    {
        Eigen::Vector3d dis = a->odometry.translation() - b->odometry.translation();
        return dis.norm();
    }

    double LidarOdometry::FrameQdistance(const LidarFrameDataPtr &a, const LidarFrameDataPtr &b)
    {
        Eigen::Quaterniond dis = a->odometry.unit_quaternion().inverse() * b->odometry.unit_quaternion();
        dis.normalize();
        return std::acos(std::fabs(dis.w())) * 2;
    }

    int LidarOdometry::AddEdgeCorner(const pcl::PointCloud<pcl::PointXYZI>::Ptr &pc_in, const pcl::PointCloud<pcl::PointXYZI>::Ptr &map_in, ceres::Problem &problem, ceres::LossFunction *loss_function)
    {
        int corner_num = 0;
#pragma omp parallel for num_threads(local_map_threads_num_)
        for (int i = 0; i < (int)pc_in->points.size(); i++)
        {
            pcl::PointXYZI point_temp;
            PointAssociateToMap(&(pc_in->points[i]), &point_temp);

            std::vector<int> pointSearchInd;
            std::vector<float> pointSearchSqDis;
            kdtree_corner_local_map_->nearestKSearch(point_temp, 5, pointSearchInd, pointSearchSqDis);
            if (pointSearchSqDis[4] < 1.0)
            {
                std::vector<Eigen::Vector3d> nearCorners;
                Eigen::Vector3d center(0, 0, 0);
                for (int j = 0; j < 5; j++)
                {
                    Eigen::Vector3d tmp(map_in->points[pointSearchInd[j]].x,
                                        map_in->points[pointSearchInd[j]].y,
                                        map_in->points[pointSearchInd[j]].z);
                    center = center + tmp;
                    nearCorners.push_back(tmp);
                }
                center = center / 5.0;

                Eigen::Matrix3d covMat = Eigen::Matrix3d::Zero();
                for (int j = 0; j < 5; j++)
                {
                    Eigen::Matrix<double, 3, 1> tmpZeroMean = nearCorners[j] - center;
                    covMat = covMat + tmpZeroMean * tmpZeroMean.transpose();
                }

                Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> saes(covMat);

                Eigen::Vector3d unit_direction = saes.eigenvectors().col(2);
                Eigen::Vector3d curr_point(pc_in->points[i].x, pc_in->points[i].y, pc_in->points[i].z);
                if (saes.eigenvalues()[2] > 3 * saes.eigenvalues()[1])
                {
                    Eigen::Vector3d point_on_line = center;
                    Eigen::Vector3d point_a, point_b;
                    point_a = 0.1 * unit_direction + point_on_line;
                    point_b = -0.1 * unit_direction + point_on_line;

                    CornerFeatureDistance corner_feature(curr_point, point_a, point_b);
                    if (std::fabs(corner_feature.Error(q_w_curr_, t_w_curr_)) < 1.0)
                    {
                        ceres::CostFunction *cost_function = new ceres::EdgeCornerFeatureCeres(std::move(corner_feature));
#pragma omp critical
                        {
                            problem.AddResidualBlock(cost_function, loss_function, parameters_);
                            corner_num++;
                        }
                    }
                }
            }
        }
        if (corner_num < 20)
        {
            LOG(WARNING) << "lidar odometry not enough corner points: " << corner_num;
        }
        return corner_num;
    }
    int LidarOdometry::AddEdgeSurfaceNormal(const pcl::PointCloud<pcl::PointXYZI>::Ptr &pc_in, const pcl::PointCloud<pcl::PointXYZI>::Ptr &map_in, ceres::Problem &problem, ceres::LossFunction *loss_function)
    {
        int surf_num = 0;
#pragma omp parallel for num_threads(local_map_threads_num_)
        for (int i = 0; i < (int)pc_in->points.size(); i++)
        {
            pcl::PointXYZI point_temp;
            PointAssociateToMap(&(pc_in->points[i]), &point_temp);
            std::vector<int> pointSearchInd;
            std::vector<float> pointSearchSqDis;
            kdtree_surface_local_map_->nearestKSearch(point_temp, 5, pointSearchInd, pointSearchSqDis);

            Eigen::Matrix<double, 5, 3> matA0;
            Eigen::Matrix<double, 5, 1> matB0 = -1 * Eigen::Matrix<double, 5, 1>::Ones();
            if (pointSearchSqDis[4] < 1.0)
            {
                for (int j = 0; j < 5; j++)
                {
                    matA0(j, 0) = map_in->points[pointSearchInd[j]].x;
                    matA0(j, 1) = map_in->points[pointSearchInd[j]].y;
                    matA0(j, 2) = map_in->points[pointSearchInd[j]].z;
                }
                // find the norm of plane
                Eigen::Vector3d norm = matA0.colPivHouseholderQr().solve(matB0);
                double negative_OA_dot_norm = 1 / norm.norm();
                norm.normalize();

                bool planeValid = true;
                for (int j = 0; j < 5; j++)
                {
                    // if OX * n > 0.2, then plane is not fit well
                    if (fabs(norm(0) * map_in->points[pointSearchInd[j]].x +
                             norm(1) * map_in->points[pointSearchInd[j]].y +
                             norm(2) * map_in->points[pointSearchInd[j]].z + negative_OA_dot_norm) > 0.2)
                    {
                        planeValid = false;
                        break;
                    }
                }
                Eigen::Vector3d curr_point(pc_in->points[i].x, pc_in->points[i].y, pc_in->points[i].z);
                if (planeValid)
                {
                    SurfaceNormalFeatureDistance surface_normal_feature(curr_point, norm, negative_OA_dot_norm);
                    if (std::fabs(surface_normal_feature.Error(q_w_curr_, t_w_curr_)) < 1.0)
                    {
                        ceres::CostFunction *cost_function = new ceres::EdgeSurfNormalFeatureCeres(std::move(surface_normal_feature));
#pragma omp critical
                        {
                            problem.AddResidualBlock(cost_function, loss_function, parameters_);
                            surf_num++;
                        }
                    }
                }
            }
        }
        if (surf_num < 20)
        {
            LOG(WARNING) << "not enough surface points: " << surf_num;
        }
        return surf_num;
    }

    void LidarOdometry::PointAssociateToMap(pcl::PointXYZI const *const pi, pcl::PointXYZI *const po)
    {
        Eigen::Vector3d point_curr(pi->x, pi->y, pi->z);
        Eigen::Vector3d point_w = q_w_curr_ * point_curr + t_w_curr_;
        po->x = point_w.x();
        po->y = point_w.y();
        po->z = point_w.z();
        po->intensity = pi->intensity;
        //po->intensity = 1.0;
    }
}