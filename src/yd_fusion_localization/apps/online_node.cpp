/**
 * @file online_node.cpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-21
 * @brief online node
 */

#include "yd_fusion_localization/localization/localization_flow.hpp"
#include <rosbag/view.h>
#include <rosgraph_msgs/Clock.h>

namespace yd_fusion_localization {
std::string WORK_SPACE_PATH;
}
using namespace yd_fusion_localization;

int main(int argc, char *argv[])
{
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir =  "/home/Log";
    FLAGS_stop_logging_if_full_disk = true;
    FLAGS_logtostderr = true;
    yd_fusion_localization::WORK_SPACE_PATH = ros::package::getPath("yd_fusion_localization");
    if (argc > 1)
    {
        int tostderr = std::atoi(argv[1]);
        if (tostderr == 1)
        {
            FLAGS_logtostderr = false;
        }
        else if (tostderr == 2)
        {
            FLAGS_logtostderr = false;
            FLAGS_alsologtostderr = true;
        }
    }

    ros::init(argc, argv, "online_node");

    ros::NodeHandle nh;
    std::shared_ptr<LocalizationFlow> localization_flow_ptr = std::make_shared<LocalizationFlow>(nh);

    ros::Rate rate(20);
    while (true)
    {
        if (!ros::ok())
        {
            localization_flow_ptr->Finish();
            break;
        }
        ros::spinOnce();
        localization_flow_ptr->Run(false);

        rate.sleep();
    }

    return 0;
}