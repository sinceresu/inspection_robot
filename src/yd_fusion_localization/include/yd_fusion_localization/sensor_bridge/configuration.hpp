/**
 * @file configuration.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-14
 * @brief configuration
 */

#ifndef YD_FUSION_LOCALIZATION_CONFIGURATION_HPP_
#define YD_FUSION_LOCALIZATION_CONFIGURATION_HPP_

/// publisher
#include "yd_fusion_localization/publisher/pose_publisher.hpp"
#include "yd_fusion_localization/publisher/cloud_publisher.hpp"
#include "yd_fusion_localization/publisher/odometry_publisher.hpp"
#include "yd_fusion_localization/publisher/diagnostic_publisher.hpp"
#include "yd_fusion_localization/publisher/path_publisher.hpp"
#include "yd_fusion_localization/publisher/cloudxyzi_publisher.hpp"
#include "yd_fusion_localization/publisher/loop_publisher.hpp"

namespace yd_fusion_localization
{
    bool ConfigurePublishers(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &frame_id, std::unordered_map<std::string, std::pair<std::shared_ptr<Publisher>, PublishDataPtr>> &publishers);
    bool ConfigurePublisher(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &frame_id, std::pair<std::shared_ptr<Publisher>, PublishDataPtr> &publisher);
} // namespace yd_fusion_localization
#endif