/**
 * @file historypose.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-14
 * @brief historypose
 */

#ifndef YD_FUSION_LOCALIZATION_HISTORYPOSE_HPP_
#define YD_FUSION_LOCALIZATION_HISTORYPOSE_HPP_

/// localizer
#include "yd_fusion_localization/localizer/historypose_localizer.hpp"

namespace yd_fusion_localization
{
    class Historypose
    {
    public:
        Historypose(const YAML::Node &yaml_node);
        ~Historypose() {}
        Historypose(const Historypose &other) = delete;
        Historypose &operator=(const Historypose &other) = delete;

        void LoopDetection(std::deque<Sophus::SE3d> &poses);
        void Save(const State &state);

    private:
        bool Condition(const State &state);

        std::shared_ptr<HistoryposeLocalizer> localizer_;
        double interval_time_;
        double interval_distance_;
        double interval_angle_;
        State save_state_;
    };
} // namespace yd_fusion_localization

#endif