/**
 * @file odometry_publisher.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-9-2
 * @brief odometry publisher
 */

#ifndef YD_FUSION_LOCALIZATION_ODOMETRY_PUBLISHER_HPP_
#define YD_FUSION_LOCALIZATION_ODOMETRY_PUBLISHER_HPP_

#include "yd_fusion_localization/publisher/publisher.hpp"

namespace yd_fusion_localization
{
    class OdometryPublisher : public Publisher
    {
    public:
        OdometryPublisher(ros::NodeHandle &nh,
                      const std::string &topic_name,
                      const std::string &frame_id,
                      int buff_size,
                      bool latch = false);

        void Publish(const PublishDataPtr &data) override;
    };
} // namespace yd_fusion_localization

#endif