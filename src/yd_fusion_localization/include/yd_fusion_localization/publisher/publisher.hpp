/**
 * @file publisher.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-21
 * @brief publisher
 */

#ifndef YD_FUSION_LOCALIZATION_PUBLISHER_HPP_
#define YD_FUSION_LOCALIZATION_PUBLISHER_HPP_

#include <ros/ros.h>

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Publisher
    {
    public:
        Publisher(const std::string& frame_id) : frame_id_(frame_id) {}
        ~Publisher() {}

        virtual void Publish(const PublishDataPtr &data)  = 0;

        virtual bool HasSubscribers() { return publisher_.getNumSubscribers() != 0; }

    protected:
        std::string frame_id_;
        ros::Publisher publisher_;
    };
} // namespace yd_fusion_localization

#endif