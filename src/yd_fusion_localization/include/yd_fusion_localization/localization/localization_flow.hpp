/**
 * @file localization_flow.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-7
 * @brief localization flow
 */

#ifndef YD_FUSION_LOCALIZATION_LOCALIZATION_FLOW_HPP_
#define YD_FUSION_LOCALIZATION_LOCALIZATION_FLOW_HPP_

#include "yd_fusion_localization/localization/front_end.hpp"
#include "yd_fusion_localization/localization/back_end.hpp"

namespace yd_fusion_localization
{
    class LocalizationFlow
    {
    public:
        LocalizationFlow(ros::NodeHandle &nh);
        ~LocalizationFlow() {}
        LocalizationFlow(const LocalizationFlow &rhs) = delete;
        LocalizationFlow &operator=(const LocalizationFlow &rhs) = delete;
        
        void HandleMessage(const MessagePointer &msg_ptr, const std::string &topic);
        void Run(bool single_thread);
        void Finish();

    private:
        void ClearData();
        void Configure(ros::NodeHandle &nh);
        void RunBackEndThread();

        std::shared_ptr<FrontEnd> frontend_;
        std::shared_ptr<BackEnd> backend_;
    };
} // namespace yd_fusion_localization

#endif