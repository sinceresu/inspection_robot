/**
 * @file back_end.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-18
 * @brief back end
 */

#ifndef YD_FUSION_LOCALIZATION_BACKEND_HPP_
#define YD_FUSION_LOCALIZATION_BACKEND_HPP_

#include "yd_fusion_localization/localization/front_end.hpp"
#include "yd_fusion_localization/sensor_bridge/measurer.hpp"
#include "yd_fusion_localization/fusion/fusion_graph.hpp"

namespace yd_fusion_localization
{
    class BackEnd
    {
    public:
        BackEnd(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame, const std::shared_ptr<FrontEnd> &frontend);
        ~BackEnd() {}
        BackEnd(const BackEnd &backend) = delete;
        BackEnd &operator=(const BackEnd &backend) = delete;

        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic);
        void ClearData();
        void Reset();
        void Notify(const State &state);
        void Finish();
        void Wait();
        bool IfInited();
        void SetUninited();
        bool Init(bool choice);
        bool InitWindow();
        void Run();

    private:
        bool IfFinish();
        bool LoopDetection(std::deque<Sophus::SE3d> &poses, std::string &index);
        bool Initialize(const std::deque<Sophus::SE3d> &poses);
        void SetInited(bool inited);
        bool CheckLocalizerThresh(const State &nstate, Eigen::Vector3d &dis);
        bool CheckCovariance(const Eigen::MatrixXd &covariance);
        bool IfMeasure(const State &nstate);
        bool IfPredict(const State &nstate);
        bool GetMeasureState(State &nstate);
        bool GetPredictState(State &nstate);
        int Interp(double stamp, State &state);
        void AddState(const State &state);
        void SetPState(const State &state);
        bool GetMStamp(const Sophus::SE3d &pose, double start_stamp, double &end_stamp);
        bool HasLData();
        bool GetNState(double stamp, State &nstate);
        bool GetMState(const State &nstate);
        void SetMState(const State &nstate);
        void Publish(const State &state);
        void Save(const State &state);
        void ResetPdata();
        void ResetLdata();
        void ConfigureMeasurers(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame);
        void ConfigureFusion(const YAML::Node &yaml_node);
        void CheckSensorDataEmpty();

        double interval_time_;
        double interval_distance_;
        double interval_angle_;
        double interval_predict_time_;
        double interval_predict_distance_;
        double interval_predict_angle_;
        double init_interval_time_;
        double init_delay_ltime_;
        double init_wait_ptime_;
        double predictor_delay_time_;
        double measurer_delay_time_;
        double predictor_wait_time_;
        double measurer_wait_time_;
        double state_buffer_time_;
        bool save_;
        bool history_loop_;
        double loop_time_thresh_;
        std::vector<double> localizer_thresh_;
        std::vector<double> covariance_thresh_;

        std::mutex mutex_backend_;
        std::condition_variable cv_backend_;

        std::shared_ptr<FrontEnd> frontend_;
        std::deque<std::shared_ptr<Measurer>> measurers_;
        std::shared_ptr<FusionInterface> fusion_;

        State localizer_state_;
        State lstate_;
        State pstate_;
        MState mstate_;
        std::deque<std::pair<bool, PredictDataPtr>> pdata_;
        std::deque<std::pair<bool, LocalizerDataPtr>> ldata_;
        int init_index_;

        std::deque<State> state_buffer_;
        std::mutex mutex_state_buffer_;

        bool inited_;
        std::mutex mutex_inited_;

        bool finish_;
        bool finished_;
        std::mutex mutex_finish_;
        std::mutex mutex_finished_;

        std::unordered_map<std::string, double> valid_sensor_time;
        double limit_time_;

    };
} // namespace yd_fusion_localization

#endif