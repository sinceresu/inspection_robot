/**
 * @file fusion_interface.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-1-6
 * @brief fusion interface
 */

#ifndef YD_FUSION_LOCALIZATION_FUSION_INTERFACE_HPP_
#define YD_FUSION_LOCALIZATION_FUSION_INTERFACE_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class FusionInterface
    {
    public:
        virtual ~FusionInterface() {}
        virtual void Reset() = 0;
        virtual bool AddState(std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) = 0;
        virtual bool AddState(const State &state, std::deque<std::pair<bool, PredictDataPtr>> &predict_data, std::deque<std::pair<bool, LocalizerDataPtr>> &localizer_data) = 0;
        virtual bool Optimize(std::vector<int> &predict_inlier, std::vector<int> &localizer_inlier, bool key_frame, State &result_state) = 0;
        virtual bool WindowInited() const = 0;
        virtual int GetStatesNum() const = 0;
    };
} // namespace yd_fusion_localization

#endif