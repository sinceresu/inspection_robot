/**
 * @file tio_odometry.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-29
 * @brief tio_odometry
 */

#ifndef YD_FUSION_LOCALIZATION_TIOODOMETRY_HPP_
#define YD_FUSION_LOCALIZATION_TIOODOMETRY_HPP_

#include <g2o/stuff/macros.h>
#include <g2o/core/factory.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/linear_solver.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/solvers/pcg/linear_solver_pcg.h>
#include <g2o/solvers/eigen/linear_solver_eigen.h>
#include <g2o/core/optimization_algorithm_levenberg.h>

#include "yd_fusion_localization/odometry/odometry.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_twist.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_imu.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_odom.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_velocity.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_vg.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_pl.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_ql.hpp"
#include "yd_fusion_localization/models/graph_optimizer/g2o/edge/edge_posel.hpp"

namespace yd_fusion_localization
{
    class TioOdometry : public Odometry
    {
    public:
        TioOdometry(const YAML::Node &yaml_node);
        ~TioOdometry();
        void Reset() override;
        bool Initialize(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data) override;
        bool Handle(const IState &istate, const std::deque<PredictDataPtr> &predict_data, const std::deque<LocalizerDataPtr> &local_data, const std::deque<PreprocessedDataPtr> &pre_data, FrameDataPtr &result_data) override;
        bool PredictMState(const std::deque<PredictDataPtr> &predict_data, MState &mstate) override;
        double GetCurrentStamp() const override;

    private:
        void SlideWindow(int index);
        void AddEdgePredict(int index1, int index2, const PredictDataPtr &data);
        void AddEdgeLocalizer(int index, const LocalizerDataPtr &data);
        void Optimize();
        void SaveKeyFrame(int index, double stamp);
        bool DeadReckoning(const std::deque<PredictDataPtr> &predict_data, State &result_state);
        bool DeadReckoning(const PredictDataPtr &predict_data, State &result_state);
        void ParseVgData(const TwistDataPtr &twist_data, const ImuDataPtr &imu_data, VgDataPtr &vg_data);
        bool GetVelocity(const std::deque<PredictDataPtr> &predict_data, Eigen::Vector3d &linear_velocity, Eigen::Vector3d &angular_velocity);

        StateType state_type_;
        int window_size_;
        double max_vel_;
        double max_gyro_;
        int max_iteration_num_;
        std::unique_ptr<g2o::SparseOptimizer> graph_ptr_;
        int edge_id_;
        int key_num_;
        TIOFrameDataPtr last_key_frame_;
        TIOFrameDataPtr second2last_key_frame_;
    };
} // namespace yd_fusion_localization

#endif