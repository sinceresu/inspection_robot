/**
 * @file imagelandmark_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-10-15
 * @brief imagelandmark localizer
 */

#ifndef YD_FUSION_LOCALIZATION_IMAGELANDMARK_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_IMAGELANDMARK_LOCALIZER_HPP_

#include "yd_fusion_localization/localizer/localizer.hpp"

namespace yd_fusion_localization
{
    class ImageLandmarkLocalizer : public Localizer
    {
    public:
        ImageLandmarkLocalizer(const YAML::Node &yaml_node);
        ~ImageLandmarkLocalizer() {}
        bool CheckPosition(const Sophus::SE3d &pose) override;
        bool LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose) override;
        bool Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data) override;
        bool Save(const MState &mstate, const MeasureDataPtr &data) override;
        bool Save() override;

    private:
        bool GetPose(const MeasureDataPtr &data, Sophus::SE3d &result_pose);

        double tag_size_;
        Eigen::Matrix<float, 3, 3> intrinsic_mat_;
        Eigen::Matrix<double, 6, 1> valid_ranges_;
        double distance_threshold_;
        double angle_threshold_;
    };
} // namespace yd_fusion_localization

#endif