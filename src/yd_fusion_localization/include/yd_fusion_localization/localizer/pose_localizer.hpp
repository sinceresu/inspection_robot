/**
 * @file pose_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-21
 * @brief pose localizer
 */

#ifndef YD_FUSION_LOCALIZATION_POSE_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_POSE_LOCALIZER_HPP_

#include "yd_fusion_localization/localizer/localizer.hpp"

namespace yd_fusion_localization
{
    class PoseLocalizer : public Localizer
    {
    public:
        PoseLocalizer(const YAML::Node &yaml_node);
        ~PoseLocalizer() {}
        bool CheckPosition(const Sophus::SE3d &pose) override;
        bool LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose) override;
        bool Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data) override;
        bool Save(const MState &mstate, const MeasureDataPtr &data) override;
        bool Save() override;

    private:
        bool GetPose(const MeasureDataPtr &data, Sophus::SE3d &result_pose);

        Eigen::Matrix<double, 6, 1> invalid_ranges_;
        double distance_threshold_;
        double angle_threshold_;
    };
} // namespace yd_fusion_localization

#endif