/**
 * @file historypose_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-26
 * @brief historypose localizer
 */

#ifndef YD_FUSION_LOCALIZATION_HISTORYPOSE_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_HISTORYPOSE_LOCALIZER_HPP_

#include "yd_fusion_localization/localizer/localizer.hpp"

namespace yd_fusion_localization
{
    class HistoryposeLocalizer
    {
    public:
        HistoryposeLocalizer(const YAML::Node &yaml_node);
        ~HistoryposeLocalizer();
        bool LoopDetection(std::deque<Sophus::SE3d> &data);
        void SavePose(const Sophus::SE3d &data);

    private:
        std::deque<std::string> path_;
        int num_;
        int count_;
    };
} // namespace yd_fusion_localization

#endif