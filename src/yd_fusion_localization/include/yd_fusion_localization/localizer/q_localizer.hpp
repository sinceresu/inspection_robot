/**
 * @file q_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-9
 * @brief q localizer
 */

#ifndef YD_FUSION_LOCALIZATION_Q_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_Q_LOCALIZER_HPP_

#include "yd_fusion_localization/localizer/localizer.hpp"

namespace yd_fusion_localization
{
    class QLocalizer : public Localizer
    {
    public:
        QLocalizer(const YAML::Node &yaml_node);
        ~QLocalizer() {}
        bool CheckPosition(const Sophus::SE3d &pose) override;
        bool LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose) override;
        bool Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data) override;
        bool Save(const MState &mstate, const MeasureDataPtr &data) override;
        bool Save() override;

    private:
        bool GetPose(const MeasureDataPtr &data, Eigen::Quaterniond &result_q);

        Eigen::Matrix<double, 6, 1> invalid_ranges_;
        double angle_threshold_;
    };
} // namespace yd_fusion_localization

#endif