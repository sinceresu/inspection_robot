/**
 * @file cloud_localizer.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-21
 * @brief cloud localizer
 */

#ifndef YD_FUSION_LOCALIZATION_CLOUD_LOCALIZER_HPP_
#define YD_FUSION_LOCALIZATION_CLOUD_LOCALIZER_HPP_

#include "yd_fusion_localization/localizer/localizer.hpp"

#include "yd_fusion_localization/models/registration/registration.hpp"
#include "yd_fusion_localization/models/cloud_filter/cloud_filter.hpp"
#include "yd_fusion_localization/models/cloud_filter/box_filter.hpp"
#include "yd_fusion_localization/models/scan_context/scan_context.h"

namespace yd_fusion_localization
{
    class CloudLocalizer : public Localizer
    {
    public:
        CloudLocalizer(const YAML::Node &yaml_node);
        ~CloudLocalizer() {}
        bool CheckPosition(const Sophus::SE3d &pose) override;
        bool LoopDetection(const MeasureDataPtr &data, std::deque<Sophus::SE3d> &pose) override;
        bool Initialize(const MeasureDataPtr &data, const MState &mstate, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, const MState &guess, LocalizerDataPtr &localizer_data) override;
        bool GetPose(const MeasureDataPtr &data, LocalizerDataPtr &localizer_data) override;
        bool Save(const MState &mstate, const MeasureDataPtr &data) override;
        bool Save() override;

        pcl::PointCloud<pcl::PointXYZ>::Ptr &GetGlobalMap();
        pcl::PointCloud<pcl::PointXYZ>::Ptr &GetLocalMap();
        pcl::PointCloud<pcl::PointXYZ>::Ptr &GetCurrentScan();

    private:
        bool GetPose(const MeasureDataPtr &data, const MState &guess, Sophus::SE3d &result_pose);
        bool InitWithConfig(const YAML::Node &yaml_node);
        bool InitGlobalMap(const YAML::Node &yaml_node);
        bool InitLocalMap(const YAML::Node &yaml_node);
        bool InitFrame(const YAML::Node &yaml_node);
        bool InitRegistration(const YAML::Node &yaml_node);
        bool InitDataPath(const YAML::Node &yaml_node);
        bool InitFilter(std::string filter_user, std::shared_ptr<CloudFilter<pcl::PointXYZ>> &filter_ptr, const YAML::Node &yaml_node);
        bool InitSC(const YAML::Node &yaml_node);
        bool InitSCRegistration(const YAML::Node &yaml_node);
        bool ResetLocalMap(float x, float y, float z);
        template <typename PointT>
        bool RemovePointCloud(const pcl::PointCloud<PointT> &cloud_in,
                              pcl::PointCloud<PointT> &cloud_out, float lower_bound, float upper_bound);
        void AddTimeFieldtoPointCloud(const pcl::PointCloud<pcl::PointXYZ> &cloud_in,
                                      pcl::PointCloud<PointXYZT> &cloud_out);
        void Undistort(const pcl::PointCloud<PointXYZT> &cloud_in,
                       pcl::PointCloud<pcl::PointXYZ> &cloud_out,
                       const Eigen::Vector3f &linear_velocity,
                       const Eigen::Vector3f &angular_velocity);
        void Undistort(PointXYZT const *const pi, pcl::PointXYZ *const po, const Eigen::Vector3f &linear_velocity, const Eigen::Vector3f &angular_velocity);
        void DeleteOldKeys();
        void ReadKeys(const YAML::Node &yaml_node);

        std::string map_path_ = "";
        std::shared_ptr<CloudFilter<pcl::PointXYZ>> global_map_filter_ptr_;
        pcl::PointCloud<pcl::PointXYZ>::Ptr global_map_ptr_;

        std::shared_ptr<CloudFilter<pcl::PointXYZ>> local_map_filter_ptr_;
        pcl::PointCloud<pcl::PointXYZ>::Ptr local_map_ptr_;
        bool local_map_use_box_;
        std::vector<float> local_map_edge_distance_;

        std::shared_ptr<CloudFilter<pcl::PointXYZ>> frame_filter_ptr_;
        pcl::PointCloud<pcl::PointXYZ>::Ptr current_scan_ptr_;
        std::vector<float> frame_distance_threshold_;
        int scan_number_;
        double scan_period_;

        std::shared_ptr<Registration<pcl::PointXYZ>> registration_ptr_;
        float match_threshold_;
        float match_score_;
        float init_match_threshold_;
        Eigen::Matrix<double, 6, 1> invalid_ranges_;
        double distance_threshold_;
        double angle_threshold_;
        bool undistort_;

        bool sc_loop_;
        bool sc_save_;
        int sc_replace_old_;
        double sc_dis_thresh_;
        double sc_angle_thresh_;
        std::string sc_loop_path_;
        std::string sc_save_path_;
        std::shared_ptr<Registration<pcl::PointXYZ>> sc_registration_ptr_;
        double sc_match_threshold_;
        double sc_match_distance_threshold_;
        double sc_match_angle_threshold_;
        int loop_key_size_;
        std::vector<State> sc_state_;
        std::shared_ptr<SCManager> scmanager_;
        int old_key_size_;
        std::vector<int> sc_old_to_delete_;
    };
} // namespace yd_fusion_localization

#endif