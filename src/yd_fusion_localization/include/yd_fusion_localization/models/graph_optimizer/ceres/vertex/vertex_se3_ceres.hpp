#pragma once
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace ceres
{
    class VertexSE3Ceres : public ceres::LocalParameterization
    {
    public:
        VertexSE3Ceres() {}
        virtual ~VertexSE3Ceres() {}
        virtual bool Plus(const double *x, const double *delta, double *x_plus_delta) const;
        virtual bool ComputeJacobian(const double *x, double *jacobian) const;
        virtual int GlobalSize() const { return 7; }
        virtual int LocalSize() const { return 6; }
    };
}