#pragma once
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include "yd_fusion_localization/models/lidar_feature/corner_feature_distance.hpp"

namespace ceres
{
    class EdgeCornerFeatureCeres : public ceres::SizedCostFunction<1, 7>
    {
    public:
        EdgeCornerFeatureCeres(const yd_fusion_localization::CornerFeatureDistance &corner_feature);
        virtual ~EdgeCornerFeatureCeres() {}
        virtual bool Evaluate(double const *const *parameters, double *residuals, double **jacobians) const;

        yd_fusion_localization::CornerFeatureDistance corner_feature_;
    };
}