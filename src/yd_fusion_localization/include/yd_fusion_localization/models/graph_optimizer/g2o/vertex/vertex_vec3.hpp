#pragma once
#include "g2o/core/base_vertex.h"

namespace g2o
{
    class VertexVec3 : public g2o::BaseVertex<9, Eigen::Matrix<double, 9, 1>>
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
        VertexVec3() : BaseVertex<9, Eigen::Matrix<double, 9, 1>>() { setToOriginImpl(); }
        virtual bool read(std::istream &is) { return false; }

        virtual bool write(std::ostream &os) const { return false; }

        virtual void setToOriginImpl() { _estimate.fill(0.); }

        virtual void oplusImpl(const double *update_)
        {
            Eigen::Map<const Eigen::Matrix<double, 9, 1>> update(update_);
            _estimate += update;
        }

        virtual bool setEstimateDataImpl(const double *est)
        {
            Eigen::Map<const Eigen::Matrix<double, 9, 1>> _est(est);
            _estimate = _est;
            return true;
        }

        virtual bool getEstimateData(double *est) const
        {
            Eigen::Map<Eigen::Matrix<double, 9, 1>> _est(est);
            _est = _estimate;
            return true;
        }

        virtual int estimateDimension() const
        {
            return 3;
        }

        virtual bool setMinimalEstimateDataImpl(const double *est)
        {
            _estimate = Eigen::Map<const Eigen::Matrix<double, 9, 1>>(est);
            return true;
        }

        virtual bool getMinimalEstimateData(double *est) const
        {
            Eigen::Map<Eigen::Matrix<double, 9, 1>> v(est);
            v = _estimate;
            return true;
        }

        virtual int minimalEstimateDimension() const
        {
            return 9;
        }
    };
} // namespace g2o