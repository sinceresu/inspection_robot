/**
 * @file http_registration.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-9-3
 * @brief http registration
 */

#ifndef YD_FUSION_LOCALIZATION_HTTPREGISTRATION_HPP_
#define YD_FUSION_LOCALIZATION_HTTPREGISTRATION_HPP_

#include "yd_fusion_localization/models/registration/registration.hpp"
#include <curl/curl.h>

namespace yd_fusion_localization
{
    template<typename PointType>
    class HttpRegistration : public Registration<PointType>
    {
    public:
        HttpRegistration(const YAML::Node &node);
        ~HttpRegistration();
        bool SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target) override;
        bool ScanMatch(double stamp,
                       const typename pcl::PointCloud<PointType>::Ptr &input_source,
                       typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                       const Eigen::Matrix4f &predict_pose,
                       Eigen::Matrix4f &result_pose) override;
        float GetFitnessScore() override;

    private:
        static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream);
        bool ParseData(const std::string userdata, Eigen::Matrix4f &result_pose);

    private:
        std::string url_;
        float fitness_score_;
    };
} // namespace yd_fusion_localization

#endif