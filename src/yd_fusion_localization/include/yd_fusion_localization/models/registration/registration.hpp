/**
 * @file registration.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief registration
 */


#ifndef YD_FUSION_LOCALIZATION_REGISTRATION_HPP_
#define YD_FUSION_LOCALIZATION_REGISTRATION_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    class Registration
    {
    public:
        // Registration() {}
        // ~Registration() {}
        virtual bool SetInputTarget(const typename pcl::PointCloud<PointType>::Ptr &input_target) = 0;
        virtual bool ScanMatch(double stamp,
                               const typename pcl::PointCloud<PointType>::Ptr &input_source,
                               typename pcl::PointCloud<PointType>::Ptr &result_cloud_ptr,
                               const Eigen::Matrix4f &predict_pose,
                               Eigen::Matrix4f &result_pose) = 0;
        virtual float GetFitnessScore() = 0;
    };
} // namespace yd_fusion_localization

#endif