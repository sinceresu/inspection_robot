#pragma once
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class CornerFeatureDistance
    {
    public:
        CornerFeatureDistance(const Eigen::Vector3d &curr_point, const Eigen::Vector3d &last_point_a, const Eigen::Vector3d &last_point_b);
        virtual ~CornerFeatureDistance() {}
        virtual double Error(const Eigen::Quaterniond &q_last_curr, const Eigen::Vector3d &t_last_curr) const;
        virtual Eigen::Matrix<double, 1, 6> Jacobian(const Eigen::Quaterniond &q_last_curr, const Eigen::Vector3d &t_last_curr) const;
        virtual bool Evaluate(double const *const *parameters, double *residuals, double **jacobians) const;

        Eigen::Vector3d curr_point_;
        Eigen::Vector3d last_point_a_;
        Eigen::Vector3d last_point_b_;
    };
}