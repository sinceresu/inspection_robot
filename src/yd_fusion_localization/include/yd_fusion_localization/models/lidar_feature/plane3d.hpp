#pragma once
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Plane3D
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

        Plane3D();
        Plane3D(const Eigen::Vector4d &v);
        Eigen::Vector4d toVector() const;
        const Eigen::Vector4d &coeffs() const;
        void fromVector(const Eigen::Vector4d &coeffs);
        double distance() const;
        Eigen::Vector3d normal() const;
        void oplus(const Eigen::Vector3d &v);
        Eigen::Vector3d ominus(const Plane3D &plane);
        static double azimuth(const Eigen::Vector3d &v);
        static double elevation(const Eigen::Vector3d &v);
        static Eigen::Matrix3d rotation(const Eigen::Vector3d &v);
        static void normalize(Eigen::Vector4d &coeffs);

    private:
        Eigen::Vector4d coeffs_;
    };

    Plane3D operator*(const Sophus::SE3d &t, const Plane3D &plane);
}