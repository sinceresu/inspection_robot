/**
 * @file preintegration_imu_ex.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-3
 * @brief preintegration imu ex
 */

#pragma once
#include <Eigen/Core>
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class PreIntegrationImuEx
    {
    public:
        PreIntegrationImuEx() = delete;
        PreIntegrationImuEx &operator=(const PreIntegrationImuEx &rhs) = delete;
        ~PreIntegrationImuEx() {}
        PreIntegrationImuEx(const ImuData &imu_data, const Eigen::Vector3d &linearized_ba, const Eigen::Vector3d &linearized_bg);
        Eigen::Matrix<double, 15, 1> Error(const Eigen::Vector3d &Pi,
                                           const Eigen::Quaterniond &Qi,
                                           const Eigen::Vector3d &Vi,
                                           const Eigen::Vector3d &Bai,
                                           const Eigen::Vector3d &Bgi,
                                           const Eigen::Vector3d &Pj,
                                           const Eigen::Quaterniond &Qj,
                                           const Eigen::Vector3d &Vj,
                                           const Eigen::Vector3d &Baj,
                                           const Eigen::Vector3d &Bgj);
        Eigen::Matrix<double, 15, 30> Jacobian(const Eigen::Vector3d &Pi,
                                               const Eigen::Quaterniond &Qi,
                                               const Eigen::Vector3d &Vi,
                                               const Eigen::Vector3d &Bai,
                                               const Eigen::Vector3d &Bgi,
                                               const Eigen::Vector3d &Pj,
                                               const Eigen::Quaterniond &Qj,
                                               const Eigen::Vector3d &Vj,
                                               const Eigen::Vector3d &Baj,
                                               const Eigen::Vector3d &Bgj);
        static State DeadReckoning(const State &state,
                                   const ImuData &imu_data);
        static void MidPointIntegration(double dt,
                                        const Sophus::SE3d &s2r,
                                        const Eigen::Vector3d &gravity,
                                        const Eigen::Matrix<double, 18, 18> noise,
                                        const Eigen::Vector3d &acc0,
                                        const Eigen::Vector3d &acc1,
                                        const Eigen::Vector3d &gyr0,
                                        const Eigen::Vector3d &gyr1,
                                        const Eigen::Vector3d &p,
                                        const Eigen::Quaterniond &q,
                                        const Eigen::Vector3d &v,
                                        const Eigen::Vector3d &linearized_ba,
                                        const Eigen::Vector3d &linearized_bg,
                                        const Eigen::Matrix<double, 15, 15> &jacobian,
                                        const Eigen::Matrix<double, 15, 15> &covariance,
                                        Eigen::Vector3d &result_p,
                                        Eigen::Quaterniond &result_q,
                                        Eigen::Vector3d &result_v,
                                        Eigen::Matrix<double, 15, 15> &result_jacobian,
                                        Eigen::Matrix<double, 15, 15> &result_covariance,
                                        bool gravity_included,
                                        bool update_jacobian);
        bool RePropagate(const Eigen::Vector3d &Bai, const Eigen::Vector3d &Bgi);

        Eigen::Matrix<double, 15, 15> information_;

    private:
        void Propagate();

        ImuData data_;
        Sophus::SE3d s2r_;
        double sumdt_;
        Eigen::Vector3d gravity_;
        Eigen::Matrix<double, 18, 18> noise_;

        Eigen::Vector3d linearized_ba_;
        Eigen::Vector3d linearized_bg_;
        Eigen::Vector3d alpha_;
        Eigen::Quaterniond gamma_;
        Eigen::Vector3d beta_;
        Eigen::Matrix<double, 15, 15> jacobian_;
        Eigen::Matrix<double, 15, 15> covariance_;
    };
} // namespace yd_fusion_localization