/**
 * @file preintegration_twist_ex.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-3
 * @brief preintegration twist ex
 */

#pragma once
#include <Eigen/Core>
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class PreIntegrationTwistEx
    {
    public:
        PreIntegrationTwistEx() = delete;
        PreIntegrationTwistEx &operator=(const PreIntegrationTwistEx &rhs) = delete;
        ~PreIntegrationTwistEx() {}
        PreIntegrationTwistEx(const TwistData &twist_data);
        Eigen::Matrix<double, 6, 1> Error(const Eigen::Vector3d &Pi,
                                          const Eigen::Quaterniond &Qi,
                                          const Eigen::Vector3d &Pj,
                                          const Eigen::Quaterniond &Qj);
        Eigen::Matrix<double, 6, 12> Jacobian(const Eigen::Vector3d &Pi,
                                              const Eigen::Quaterniond &Qi,
                                              const Eigen::Vector3d &Pj,
                                              const Eigen::Quaterniond &Qj);
        static State DeadReckoning(const State &state,
                                   const TwistData &twist_data);
        static void MidPointIntegration(double dt,
                                        const Sophus::SE3d &s2r,
                                        const Eigen::Matrix<double, 12, 12> noise,
                                        const Eigen::Vector3d &vel0,
                                        const Eigen::Vector3d &vel1,
                                        const Eigen::Vector3d &gyr0,
                                        const Eigen::Vector3d &gyr1,
                                        const Eigen::Vector3d &p,
                                        const Eigen::Quaterniond &q,
                                        const Eigen::Matrix<double, 6, 6> &covariance,
                                        Eigen::Vector3d &result_p,
                                        Eigen::Quaterniond &result_q,
                                        Eigen::Matrix<double, 6, 6> &result_covariance);

        Eigen::Matrix<double, 6, 6> information_;

    private:
        void Propagate();

        TwistData data_;
        Sophus::SE3d s2r_;
        double sumdt_;
        Eigen::Matrix<double, 12, 12> noise_;

        Eigen::Vector3d alpha_;
        Eigen::Quaterniond gamma_;
        Eigen::Matrix<double, 6, 6> covariance_;
    };
} // namespace yd_fusion_localization