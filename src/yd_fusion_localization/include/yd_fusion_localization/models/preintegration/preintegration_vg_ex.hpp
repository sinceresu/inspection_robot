/**
 * @file preintegration_vg_ex.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-3
 * @brief preintegration vg ex
 */

#pragma once
#include <Eigen/Core>
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class PreIntegrationVgEx
    {
    public:
        PreIntegrationVgEx() = delete;
        PreIntegrationVgEx &operator=(const PreIntegrationVgEx &rhs) = delete;
        ~PreIntegrationVgEx() {}
        PreIntegrationVgEx(const VgData &vg_data, const Eigen::Vector3d &linearized_bg);
        Eigen::Matrix<double, 9, 1> Error(const Eigen::Vector3d &Pi,
                                          const Eigen::Quaterniond &Qi,
                                          const Eigen::Vector3d &Bgi,
                                          const Eigen::Vector3d &Pj,
                                          const Eigen::Quaterniond &Qj,
                                          const Eigen::Vector3d &Bgj);
        Eigen::Matrix<double, 9, 18> Jacobian(const Eigen::Vector3d &Pi,
                                              const Eigen::Quaterniond &Qi,
                                              const Eigen::Vector3d &Bgi,
                                              const Eigen::Vector3d &Pj,
                                              const Eigen::Quaterniond &Qj,
                                              const Eigen::Vector3d &Bgj);
        static State DeadReckoning(const State &state,
                                   const VgData &vg_data);
        static void MidPointIntegration(double dt,
                                        const Sophus::SE3d &s2r1,
                                        const Sophus::SE3d &s2r2,
                                        const Eigen::Matrix<double, 15, 15> noise,
                                        const Eigen::Vector3d &vel0,
                                        const Eigen::Vector3d &vel1,
                                        const Eigen::Vector3d &gyr0,
                                        const Eigen::Vector3d &gyr1,
                                        const Eigen::Vector3d &p,
                                        const Eigen::Quaterniond &q,
                                        const Eigen::Vector3d &linearized_bg,
                                        const Eigen::Matrix<double, 9, 9> &jacobian,
                                        const Eigen::Matrix<double, 9, 9> &covariance,
                                        Eigen::Vector3d &result_p,
                                        Eigen::Quaterniond &result_q,
                                        Eigen::Matrix<double, 9, 9> &result_jacobian,
                                        Eigen::Matrix<double, 9, 9> &result_covariance,
                                        bool update_jacobian);
        bool RePropagate(const Eigen::Vector3d &Bgi);

        Eigen::Matrix<double, 9, 9> information_;

    private:
        void Propagate();

        VgData data_;
        Sophus::SE3d s2r1_;
        Sophus::SE3d s2r2_;
        double sumdt_;
        Eigen::Matrix<double, 15, 15> noise_;

        Eigen::Vector3d linearized_bg_;
        Eigen::Vector3d alpha_;
        Eigen::Quaterniond gamma_;
        Eigen::Matrix<double, 9, 9> jacobian_;
        Eigen::Matrix<double, 9, 9> covariance_;
    };
} // namespace yd_fusion_localization