/**
 * @file preintegration_odom_ex.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-2-6
 * @brief preintegration odom ex
 */

#pragma once
#include <Eigen/Core>
#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class PreIntegrationOdomEx
    {
    public:
        PreIntegrationOdomEx() = delete;
        PreIntegrationOdomEx &operator=(const PreIntegrationOdomEx &rhs) = delete;
        ~PreIntegrationOdomEx() {}
        PreIntegrationOdomEx(const OdometryData &odom_data);
        Eigen::Matrix<double, 6, 1> Error(const Eigen::Vector3d &Pi,
                                          const Eigen::Quaterniond &Qi,
                                          const Eigen::Vector3d &Pj,
                                          const Eigen::Quaterniond &Qj);
        Eigen::Matrix<double, 6, 12> Jacobian(const Eigen::Vector3d &Pi,
                                              const Eigen::Quaterniond &Qi,
                                              const Eigen::Vector3d &Pj,
                                              const Eigen::Quaterniond &Qj);
        static State DeadReckoning(const State &state,
                                   const OdometryData &odom_data);

        Eigen::Matrix<double, 6, 6> information_;

    private:
        void Propagate();

        OdometryData data_;
        Sophus::SE3d s2r_;
        double sumdt_;
        Eigen::Matrix<double, 6, 6> noise_;

        Eigen::Vector3d alpha_;
        Eigen::Quaterniond gamma_;
        Eigen::Matrix<double, 6, 6> covariance_;
    };
} // namespace yd_fusion_localization