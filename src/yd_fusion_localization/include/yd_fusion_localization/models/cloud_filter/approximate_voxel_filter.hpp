/**
 * @file approximate_voxel_filter.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-6-4
 * @brief approximate voxel filter
 */

#ifndef YD_FUSION_LOCALIZATION_APPROXIMATE_VOXEL_FILTER_HPP_
#define YD_FUSION_LOCALIZATION_APPROXIMATE_VOXEL_FILTER_HPP_

#include <pcl/filters/approximate_voxel_grid.h>
#include "yd_fusion_localization/models/cloud_filter/cloud_filter.hpp"

namespace yd_fusion_localization
{
    template<typename PointType>
    class ApproximateVoxelFilter : public CloudFilter<PointType>
    {
    public:
        ApproximateVoxelFilter(const YAML::Node &node);
        ApproximateVoxelFilter(float leaf_size_x, float leaf_size_y, float leaf_size_z);

        bool Filter(const typename pcl::PointCloud<PointType>::Ptr &input_cloud_ptr, typename pcl::PointCloud<PointType>::Ptr &filtered_cloud_ptr) override;

    private:
        bool SetFilterParam(float leaf_size_x, float leaf_size_y, float leaf_size_z);

    private:
        pcl::ApproximateVoxelGrid<PointType> approximate_voxel_filter_;
    };
} // namespace yd_fusion_localization
#endif