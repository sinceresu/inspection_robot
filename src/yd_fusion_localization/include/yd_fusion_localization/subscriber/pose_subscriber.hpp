/**
 * @file pose_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-22
 * @brief subscribe pose data
 */

#ifndef YD_FUSION_LOCALIZATION_POSE_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_POSE_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/measure_subscriber.hpp"

namespace yd_fusion_localization
{
    class PoseSubscriber : public MeasureSubscriber
    {
    public:
        PoseSubscriber(ros::NodeHandle &nh,
                       const YAML::Node &yaml_node);
        ~PoseSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        MeasureType GetType() const { return MeasureType::PoseM; }
        bool HasData(double start_stamp, double end_stamp, double &stamp) override;
        bool ValidData(double stamp, MeasureDataPtr &data) override;
        bool GetLatestData(MeasureDataPtr &data) override;
        bool GetAllData(std::deque<MeasureDataPtr> &data) override;

    private:
        void ParseData(MeasureDataPtr &data) override;

        std::deque<geometry_msgs::PoseWithCovarianceStampedConstPtr> data_buffer_;
    };
} // namespace yd_fusion_localization

#endif