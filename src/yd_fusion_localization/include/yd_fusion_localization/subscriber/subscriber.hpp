/**
 * @file subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-18
 * @brief subscribe data
 */

#ifndef YD_FUSION_LOCALIZATION_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_SUBSCRIBER_HPP_

#include <mutex>
#include <thread>

#include <ros/ros.h>

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Subscriber
    {
    public:
        Subscriber(const YAML::Node &yaml_node);
        virtual ~Subscriber() {}
        virtual bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) = 0;
        virtual void ClearData() = 0;
        virtual void ClearAllData() = 0;
        virtual double GetEarliestStamp() = 0;

        Sophus::SE3d sensor_to_robot_;
        std::string sensor_frame_;
        std::string topic_;

    protected:
        Eigen::VectorXd noise_;
        int buffer_size_;
        double buffer_time_;
        std::mutex buffer_mutex_;
        ros::Subscriber subscriber_;

        template <typename ptrtype>
        void ClearData(std::deque<ptrtype> &data_buffer)
        {
            buffer_mutex_.lock();
            double ros_time = ros::Time::now().toSec();
            while (!data_buffer.empty())
            {
                if (ros_time - data_buffer.front()->header.stamp.toSec() > buffer_time_)
                {
                    data_buffer.pop_front();
                }
                else
                {
                    break;
                }
                
            }
            buffer_mutex_.unlock();
        }

        template <typename ptrtype>
        void ClearAllData(std::deque<ptrtype> &data_buffer)
        {
            buffer_mutex_.lock();
            data_buffer.clear();
            buffer_mutex_.unlock();
        }

        template <typename ptrtype>
        void msg_callback(const ptrtype &msg_ptr, std::deque<ptrtype> &data_buffer)
        {
            if (msg_ptr.get() == nullptr)
            {
                LOG(ERROR) << "msg sub error";
                return;
            }
            buffer_mutex_.lock();
            if (!data_buffer.empty())
            {
                if (msg_ptr->header.stamp.toSec() - data_buffer.back()->header.stamp.toSec() > 1e-5)
                {
                    data_buffer.push_back(msg_ptr);
                }
                else if (msg_ptr->header.stamp.toSec() - data_buffer.back()->header.stamp.toSec() < -1e-5)
                {
                    data_buffer.clear();
                    data_buffer.push_back(msg_ptr);
                }
            }
            else
            {
                data_buffer.push_back(msg_ptr);
            }
            buffer_mutex_.unlock();
        }

        template <typename ptrtype>
        double GetEarliestStamp(std::deque<ptrtype> &data_buffer)
        {
            double stamp = -1;
            buffer_mutex_.lock();
            if (!data_buffer.empty())
            {
                stamp = data_buffer.front()->header.stamp.toSec();
            }
            buffer_mutex_.unlock();
            return stamp;
        }
    };
} // namespace yd_fusion_localization

#endif