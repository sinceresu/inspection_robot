/**
 * @file q_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-3-22
 * @brief subscribe q data
 */

#ifndef YD_FUSION_LOCALIZATION_Q_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_Q_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/measure_subscriber.hpp"

namespace yd_fusion_localization
{
    class QSubscriber : public MeasureSubscriber
    {
    public:
        QSubscriber(ros::NodeHandle &nh,
                       const YAML::Node &yaml_node);
        ~QSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        MeasureType GetType() const { return MeasureType::QM; }
        bool HasData(double start_stamp, double end_stamp, double &stamp) override;
        bool ValidData(double stamp, MeasureDataPtr &data) override;
        bool GetLatestData(MeasureDataPtr &data) override;
        bool GetAllData(std::deque<MeasureDataPtr> &data) override;

    private:
        void ParseData(MeasureDataPtr &data) override;

        std::deque<sensor_msgs::ImuConstPtr> data_buffer_;
    };
} // namespace yd_fusion_localization

#endif