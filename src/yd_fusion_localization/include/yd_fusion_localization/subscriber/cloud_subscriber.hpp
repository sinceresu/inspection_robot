/**
 * @file cloud_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe cloud data
 */

#ifndef YD_FUSION_LOCALIZATION_CLOUD_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_CLOUD_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/measure_subscriber.hpp"

namespace yd_fusion_localization
{
    class CloudSubscriber : public MeasureSubscriber
    {
    public:
        CloudSubscriber(ros::NodeHandle &nh,
                        const YAML::Node &yaml_node);
        ~CloudSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        void ClearData() override;
        void ClearAllData() override;
        double GetEarliestStamp() override;
        MeasureType GetType() const { return MeasureType::CloudM; }
        bool HasData(double start_stamp, double end_stamp, double &stamp) override;
        bool ValidData(double stamp, MeasureDataPtr &data) override;
        bool GetLatestData(MeasureDataPtr &data) override;
        bool GetAllData(std::deque<MeasureDataPtr> &data) override;

    private:
        void ParseData(MeasureDataPtr &data) override;
        bool PointCloud2HasField(const sensor_msgs::PointCloud2 &pc2,
                                 const std::string &field_name);

        std::deque<sensor_msgs::PointCloud2::ConstPtr> data_buffer_;
        std::string cloud_fields_;
        Sophus::SE3d lidar_map_transform_;

    };
} // namespace yd_fusion_localization

#endif