/**
 * @file measure_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe measure data
 */

#ifndef YD_FUSION_LOCALIZATION_MEASURE_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_MEASURE_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/subscriber.hpp"

namespace yd_fusion_localization
{
    class MeasureSubscriber : public Subscriber
    {
    public:
        MeasureSubscriber(const YAML::Node &yaml_node);
        virtual ~MeasureSubscriber() {}
        virtual bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) = 0;
        virtual void ClearData() = 0;
        virtual void ClearAllData() = 0;
        virtual double GetEarliestStamp() = 0;
        virtual MeasureType GetType() const = 0;
        virtual bool HasData(double start_stamp, double end_stamp, double &stamp) = 0;
        virtual bool ValidData(double stamp, MeasureDataPtr &data) = 0;
        virtual bool GetLatestData(MeasureDataPtr &data) = 0;
        virtual bool GetAllData(std::deque<MeasureDataPtr> &data) = 0;

    protected:
        virtual void ParseData(MeasureDataPtr &data) = 0;

        double stamp_;
        int index_;

        template <typename ptrtype>
        bool HasData(double start_stamp, double end_stamp, double &stamp, std::deque<ptrtype> &data_buffer)
        {
            buffer_mutex_.lock();
            double first_stamp = std::min(start_stamp + 0.02, 0.8 * start_stamp + 0.2 * end_stamp);
            double second_stamp = end_stamp;
            int index = -1;
            for (int i = 0; i < data_buffer.size(); ++i)
            {
                if (data_buffer[i]->header.stamp.toSec() > first_stamp && data_buffer[i]->header.stamp.toSec() < second_stamp)
                {
                    if (index >= 0)
                    {
                        if (std::fabs(data_buffer[i]->header.stamp.toSec() - end_stamp) < std::fabs(data_buffer[index]->header.stamp.toSec() - end_stamp))
                        {
                            index = i;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        index = i;
                    }
                }
            }
            if (index >= 0)
            {
                stamp = data_buffer[index]->header.stamp.toSec();
                buffer_mutex_.unlock();
                return true;
            }
            else
            {
                buffer_mutex_.unlock();
                return false;
            }
        }

        template <typename ptrtype>
        bool ValidData(double stamp, MeasureDataPtr &data, std::deque<ptrtype> &data_buffer, const boost::function<void(MeasureDataPtr &)> &parse)
        {
            buffer_mutex_.lock();
            stamp_ = stamp;
            index_ = 0;
            while (data_buffer.size() > index_)
            {
                if (data_buffer[index_]->header.stamp.toSec() < stamp_ - 1e-5)
                {
                    index_++;
                }
                else
                {
                    if (data_buffer[index_]->header.stamp.toSec() >= stamp_ - 1e-5 && data_buffer[index_]->header.stamp.toSec() < stamp_ + 1e-5)
                    {
                        parse(data);
                        buffer_mutex_.unlock();
                        return true;
                    }
                    else
                    {
                        buffer_mutex_.unlock();
                        return false;
                    }
                }
            }
            buffer_mutex_.unlock();
            return false;
        }

        template <typename ptrtype>
        bool ValidData2(double stamp, MeasureDataPtr &data, std::deque<ptrtype> &data_buffer, const boost::function<void(MeasureDataPtr &)> &parse)
        {
            buffer_mutex_.lock();
            stamp_ = stamp;
            index_ = 1;
            while (data_buffer.size() > index_)
            {
                if (data_buffer[index_]->header.stamp.toSec() < stamp_ + 1e-5)
                {
                    index_++;
                }
                else
                {
                    break;
                }
            }
            index_--;

            if (!data_buffer.empty())
            {
                if (data_buffer[index_]->header.stamp.toSec() < stamp_ + 1e-5 && data_buffer.back()->header.stamp.toSec() > stamp_ - 1e-5)
                {
                    if (data_buffer.size() > index_ + 1)
                    {
                        if ((data_buffer[index_ + 1]->header.stamp.toSec() - data_buffer[index_]->header.stamp.toSec()) > 5e-1)
                        {
                            buffer_mutex_.unlock();
                            return false;
                        }
                    }
                    parse(data);
                    buffer_mutex_.unlock();
                    return true;
                }
                else
                {
                    buffer_mutex_.unlock();
                    return false;
                }
            }
            else
            {
                buffer_mutex_.unlock();
                return false;
            }
        }

        template <typename ptrtype>
        bool GetLatestData(MeasureDataPtr &data, std::deque<ptrtype> &data_buffer, const boost::function<void(MeasureDataPtr &)> &parse)
        {
            buffer_mutex_.lock();
            if (data_buffer.empty())
            {
                buffer_mutex_.unlock();
                return false;
            }
            stamp_ = data_buffer.back()->header.stamp.toSec();
            index_ = data_buffer.size() - 1;
            parse(data);
            buffer_mutex_.unlock();
            return true;
        }

        template <typename ptrtype>
        bool GetAllData(std::deque<MeasureDataPtr> &data, std::deque<ptrtype> &data_buffer, const boost::function<void(MeasureDataPtr &)> &parse)
        {
            buffer_mutex_.lock();
            if (data_buffer.empty())
            {
                buffer_mutex_.unlock();
                return false;
            }
            while (!data_buffer.empty())
            {
                double temp = data_buffer.front()->header.stamp.toSec();
                index_ = 0;
                if (temp > stamp_ + 1e-5)
                {
                    stamp_ = temp;
                    MeasureDataPtr new_data = nullptr;
                    parse(new_data);
                    data.push_back(new_data);
                }
                data_buffer.pop_front();
            }
            buffer_mutex_.unlock();
            return true;
        }
    };
} // namespace yd_fusion_localization

#endif