/**
 * @file twist_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe twist data
 */

#ifndef YD_FUSION_LOCALIZATION_TWIST_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_TWIST_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/predict_subscriber.hpp"

namespace yd_fusion_localization
{
    class TwistSubscriber : public PredictSubscriber
    {
    public:
        TwistSubscriber(ros::NodeHandle &nh,
                        const YAML::Node &yaml_node);
        ~TwistSubscriber() {}
        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) override;
        double GetEarliestStamp() override;
        void ClearData() override;
        void ClearAllData() override;
        PredictType GetType() const { return PredictType::Twist; }
        bool ValidData(double start_stamp, double end_stamp, PredictDataPtr &data) override;
        double GetLatestData(double start_stamp, PredictDataPtr &data) override;
        bool CheckData(const PredictDataPtr &data) const override;
        bool CheckDRData(const PredictDataPtr &data) const override;

    private:
        void ParseData(PredictDataPtr &data) override;

        double vel_coef_;
        double gyro_coef_;
        double max_vel_;
        double max_gyro_;
        std::deque<geometry_msgs::TwistStampedConstPtr> data_buffer_;
    };
} // namespace yd_fusion_localization

#endif