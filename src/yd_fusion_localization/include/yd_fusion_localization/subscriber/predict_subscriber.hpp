/**
 * @file predict_subscriber.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-15
 * @brief subscribe predict data
 */

#ifndef YD_FUSION_LOCALIZATION_PREDICT_SUBSCRIBER_HPP_
#define YD_FUSION_LOCALIZATION_PREDICT_SUBSCRIBER_HPP_

#include "yd_fusion_localization/subscriber/subscriber.hpp"

namespace yd_fusion_localization
{
    class PredictSubscriber : public Subscriber
    {
    public:
        PredictSubscriber(const YAML::Node &yaml_node);
        virtual ~PredictSubscriber() {}
        virtual bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic) = 0;
        virtual void ClearData() = 0;
        virtual void ClearAllData() = 0;
        virtual double GetEarliestStamp() = 0;
        virtual PredictType GetType() const = 0;
        virtual bool ValidData(double start_stamp, double end_stamp, PredictDataPtr &data) = 0;
        virtual double GetLatestData(double start_stamp, PredictDataPtr &data) = 0;
        virtual bool CheckData(const PredictDataPtr &data) const = 0;
        virtual bool CheckDRData(const PredictDataPtr &data) const = 0;

    protected:
        virtual void ParseData(PredictDataPtr &data) = 0;

        double start_stamp_;
        double end_stamp_;
        int start_index_;
        int end_index_;

        template <typename ptrtype>
        bool ValidData(double start_stamp, double end_stamp, PredictDataPtr &data, std::deque<ptrtype> &data_buffer, const boost::function<void(PredictDataPtr &)> &parse)
        {
            assert(end_stamp > start_stamp);
            buffer_mutex_.lock();
            start_stamp_ = start_stamp;
            end_stamp_ = end_stamp;
            start_index_ = 1;
            bool found = false;
            while (data_buffer.size() > start_index_)
            {
                if (data_buffer[start_index_]->header.stamp.toSec() < start_stamp_ + 1e-5)
                {
                    start_index_++;
                }
                else
                {
                    found = true;
                    break;
                }
            }
            start_index_--;

            if (found)
            {
                if (data_buffer.size() > 1 && data_buffer[start_index_]->header.stamp.toSec() < start_stamp_ + 1e-5 && data_buffer.back()->header.stamp.toSec() > end_stamp_ - 1e-5)
                {
                    end_index_ = data_buffer.size() - 1;
                    for (int i = start_index_; i < data_buffer.size(); ++i)
                    {
                        if (i + 1 < data_buffer.size())
                        {
                            if ((data_buffer[i + 1]->header.stamp.toSec() - data_buffer[i]->header.stamp.toSec()) > 5e-1)
                            {
                                buffer_mutex_.unlock();
                                return false;
                            }
                        }

                        if (data_buffer[i]->header.stamp.toSec() > end_stamp_ - 1e-5)
                        {
                            end_index_ = i;
                            break;
                        }
                    }
                    parse(data);
                    buffer_mutex_.unlock();
                    return true;
                }
                else
                {
                    buffer_mutex_.unlock();
                    return false;
                }
            }
            else
            {
                buffer_mutex_.unlock();
                return false;
            }
        }

        template <typename ptrtype>
        double GetLatestData(double start_stamp, PredictDataPtr &data, std::deque<ptrtype> &data_buffer, const boost::function<bool(double, double, PredictDataPtr &)> &valid)
        {
            buffer_mutex_.lock();
            if (data_buffer.empty())
            {
                buffer_mutex_.unlock();
                return -1.0;
            }
            else
            {
                double end_stamp = data_buffer.back()->header.stamp.toSec();
                buffer_mutex_.unlock();
                if (end_stamp > start_stamp + 1e-2)
                {
                    if (valid(start_stamp, end_stamp, data))
                    {
                        return end_stamp;
                    }
                }
                return -1.0;
            }
        }
    };
} // namespace yd_fusion_localization

#endif