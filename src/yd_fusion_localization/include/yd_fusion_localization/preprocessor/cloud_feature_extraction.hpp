/**
 * @file cloud_feature_extraction.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief cloud_feature_extraction
 */

#ifndef YD_FUSION_LOCALIZATION_CLOUDFEATUREEXTRACTION_HPP_
#define YD_FUSION_LOCALIZATION_CLOUDFEATUREEXTRACTION_HPP_

#include "yd_fusion_localization/preprocessor/preprocessor.hpp"
#include "yd_fusion_localization/models/cloud_filter/voxel_filter.hpp"

namespace yd_fusion_localization
{
    class CloudFeatureExtraction : public Preprocessor
    {
    public:
        struct smoothness_t
        {
            float value;
            size_t ind;
        };

        struct by_value
        {
            bool operator()(smoothness_t const &left, smoothness_t const &right)
            {
                return left.value < right.value;
            }
        };

        CloudFeatureExtraction(const YAML::Node &yaml_node);
        ~CloudFeatureExtraction();
        bool Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data) override;
        bool Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2) override;

    private:
        void CloudImageExtraction(const CloudImageDataPtr &cloud_image, CloudFeatureDataPtr &cloud_feature);
        void CalculateSmoothness(CloudFeatureDataPtr &cloud_feature);
        void MarkOccludedPoints(CloudFeatureDataPtr &cloud_feature);
        void ExtractFeatures(CloudFeatureDataPtr &cloud_feature);

        int scan_number_;
        int horizon_scan_;
        float edge_threshold_;
        float surf_threshold_;
        float *cloud_curvature_;
        int *cloud_neighbor_picked_;
        int *cloud_label_;
        std::vector<smoothness_t> cloud_smoothness_;
        std::shared_ptr<CloudFilter<pcl::PointXYZI>> surface_filter_;
    };
} // namespace yd_fusion_localization

#endif