/**
 * @file cloud_ground_detection.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-2
 * @brief cloud_ground_detection
 */

#ifndef YD_FUSION_LOCALIZATION_CLOUDGROUNDDETECTION_HPP_
#define YD_FUSION_LOCALIZATION_CLOUDGROUNDDETECTION_HPP_

#include "yd_fusion_localization/preprocessor/preprocessor.hpp"
#include "yd_fusion_localization/models/cloud_filter/voxel_filter.hpp"
#include <pcl/features/normal_3d.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/filters/impl/plane_clipper3D.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>

namespace yd_fusion_localization
{
    class CloudGroundDetection : public Preprocessor
    {
    public:
        CloudGroundDetection(const YAML::Node &yaml_node);
        ~CloudGroundDetection();
        bool Preprocess(const MeasureDataPtr &data, const BState &bstate, PreprocessedDataPtr &preprocessed_data) override;
        bool Preprocess(PreprocessedDataPtr &data1, PreprocessedDataPtr &data2) override;

    private:
        void CloudImageExtraction(const CloudImageDataPtr &cloud_image);
        template <typename PointType>
        bool PlaneClip(const typename pcl::PointCloud<PointType>::Ptr &src_cloud, typename pcl::PointCloud<PointType>::Ptr &dst_cloud, const Eigen::Vector4f &plane, bool negative);
        template <typename PointType>
        bool NormalFilter(const typename pcl::PointCloud<PointType>::Ptr &src_cloud, typename pcl::PointCloud<PointType>::Ptr &dst_cloud);
        template <typename PointType>
        bool Ransac(const typename pcl::PointCloud<PointType>::Ptr &src_cloud, const Eigen::Vector3f reference, typename pcl::PointCloud<PointType>::Ptr &dst_cloud, Eigen::Vector4f &coeffs);

        float sensor_height_;
        float height_clip_range_;
        float normal_filter_thresh_;
        float ground_points_thresh_;
        float ground_normal_thresh_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr extracted_;
        pcl::PointCloud<pcl::PointXYZI>::Ptr extracted2_;
    };
} // namespace yd_fusion_localization

#endif