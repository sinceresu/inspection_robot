/**
 * @file loop.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-17
 * @brief loop
 */

#ifndef YD_FUSION_LOCALIZATION_LOOP_HPP_
#define YD_FUSION_LOCALIZATION_LOOP_HPP_

#include "yd_fusion_localization/utilities/data_conversions.hpp"

namespace yd_fusion_localization
{
    class Loop
    {
    public:
        Loop(const YAML::Node &yaml_node);
        virtual ~Loop() {}
        virtual void AddFrame(FrameDataPtr &frame) = 0;
        virtual bool Detect(FrameDataPtr &frame, LoopDataPtr &loop_data) = 0;
        virtual bool Detect(const std::deque<FrameDataPtr> &history, FrameDataPtr &frame, LoopDataPtr &loop_data) = 0;

    protected:
        double loop_time_interval_;
    };
} // namespace yd_fusion_localization

#endif