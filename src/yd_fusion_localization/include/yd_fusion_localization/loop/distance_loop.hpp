/**
 * @file distance_loop.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-6-17
 * @brief distance loop
 */

#ifndef YD_FUSION_LOCALIZATION_DISTANCELOOP_HPP_
#define YD_FUSION_LOCALIZATION_DISTANCELOOP_HPP_

#include "yd_fusion_localization/loop/loop.hpp"
#include <pcl/kdtree/kdtree_flann.h>

namespace yd_fusion_localization
{
    class DistanceLoop : public Loop
    {
    public:
        DistanceLoop(const YAML::Node &yaml_node);
        virtual ~DistanceLoop() {}
        virtual void AddFrame(FrameDataPtr &frame) override;
        virtual bool Detect(FrameDataPtr &frame, LoopDataPtr &loop_data) override;
        virtual bool Detect(const std::deque<FrameDataPtr> &history, FrameDataPtr &frame, LoopDataPtr &loop_data) override;

    private:
        pcl::PointCloud<PointPQIndT>::Ptr history_xyz_;
        pcl::KdTreeFLANN<PointPQIndT>::Ptr history_kdtree_;
        bool neglect_z_;
        float search_radius_;
        Eigen::VectorXd noise_;
        double time_interval_thresh_;
        int history_num_;
    };
} // namespace yd_fusion_localization

#endif