/**
 * @file lidar_mapping_flow.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2021-7-6
 * @brief lidar mapping flow
 */

#ifndef YD_FUSION_LOCALIZATION_LIDARMAPPINGFLOW_HPP_
#define YD_FUSION_LOCALIZATION_LIDARMAPPINGFLOW_HPP_

#include "yd_fusion_localization/sensor_bridge/predictor.hpp"
#include "yd_fusion_localization/sensor_bridge/measurer.hpp"
#include "yd_fusion_localization/preprocessor/cloud_image_projection.hpp"
#include "yd_fusion_localization/preprocessor/cloud_feature_extraction.hpp"
#include "yd_fusion_localization/preprocessor/cloud_ground_detection.hpp"

#include "yd_fusion_localization/odometry/tio_odometry.hpp"
#include "yd_fusion_localization/odometry/lidar_odometry.hpp"
#include "yd_fusion_localization/loop/distance_loop.hpp"
#include "yd_fusion_localization/mapping/lidar_mapping.hpp"

namespace yd_fusion_localization
{
    class LidarMappingFlow
    {
    public:
        LidarMappingFlow(ros::NodeHandle &nh, const std::string map_directory);
        virtual ~LidarMappingFlow() {}
        LidarMappingFlow(const LidarMappingFlow &rhs) = delete;
        LidarMappingFlow &operator=(const LidarMappingFlow &rhs) = delete;

        bool HandleMessage(const MessagePointer &msg_ptr, const std::string &topic);
        void Run();
        void Finish();

    private:
        void PublishLidarOdometry(const LidarFrameDataPtr &frame);
        void PublishGround(const LidarFrameDataPtr &frame, const CloudPlaneDataPtr &ground);
        void PublishLidarMapping(const LidarFrameDataPtr &frame);
        void PublishLoop(const LoopDataPtr &loop_data);
        void Reset();
        void SetInited(bool inited);
        bool IfInited();
        void ClearData();
        void SaveMap();
        void Configure(ros::NodeHandle &nh);
        void ConfigurePredictors(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &robot_frame);
        void ConfigureMeasurers(ros::NodeHandle &nh, const YAML::Node &yaml_node, const std::string &map_frame, const std::string &robot_frame);
        bool IfGroundDetection(const IState &istate);
        bool IfMeasure(const IState &istate, int index);
        bool IfLoop(const IState &istate);

        double ground_detect_distance_;
        double ground_detect_angle_;
        double ground_detect_time_;
        IState last_ground_istate_;
        std::vector<double> measure_distance_;
        std::vector<double> measure_angle_;
        std::vector<double> measure_time_;
        std::vector<IState> last_measure_istate_;
        double loop_time_;
        IState last_loop_istate_;

        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> odom_publisher_;
        std::shared_ptr<TFBroadCaster> sensor2robot_tf_;
        std::shared_ptr<TFBroadCaster> robot2odom_tf_;
        std::shared_ptr<TFBroadCaster> odom2map_tf_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> currentscan_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> ground_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> corner_localmap_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> surface_localmap_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> localmap_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> globalmap_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> keyframe_publisher_;
        std::pair<std::shared_ptr<Publisher>, PublishDataPtr> loop_publisher_;
        std::deque<std::shared_ptr<Predictor>> predictors_;
        std::deque<PredictDataPtr> pdata_;
        std::deque<std::shared_ptr<Measurer>> measurers_;
        std::deque<LocalizerDataPtr> ldata_;

        std::shared_ptr<CloudSubscriber> cloud_subscriber_;
        std::shared_ptr<CloudImageProjection> cloud_image_projection_;
        std::shared_ptr<CloudFeatureExtraction> cloud_feature_extraction_;
        std::shared_ptr<CloudGroundDetection> cloud_ground_detection_;

        std::shared_ptr<TioOdometry> tio_odometry_;
        std::shared_ptr<LidarOdometry> lidar_odometry_;
        std::shared_ptr<DistanceLoop> distance_loop_;
        std::shared_ptr<LidarMapping> lidar_mapping_;
        bool inited_;
        std::deque<MeasureDataPtr> cloud_datas_;
        double last_stamp_;
        const std::string map_directory_;
    };
}

#endif