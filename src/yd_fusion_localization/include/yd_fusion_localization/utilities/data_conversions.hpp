/**
 * @file data_conversions.hpp
 * @author Zhang Songpeng
 * @version 1.0
 * @date 2020-5-19
 * @brief data conversions
 */

#ifndef YD_FUSION_LOCALIZATION_DATA_CONVERSIONS_HPP_
#define YD_FUSION_LOCALIZATION_DATA_CONVERSIONS_HPP_

#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>

#include "yd_fusion_localization/utilities/data_types.hpp"

namespace yd_fusion_localization
{
    constexpr double PI = 3.141592653589793;
    constexpr double TAU = 6.283185307179587;
    double ClampRotation(double rotation);
    constexpr double DegToRad(double deg) { return PI * deg / 180.0; }

    Sophus::SE3d Interpolate(double stamp, double start_stamp, double end_stamp, const Sophus::SE3d &start_pose, const Sophus::SE3d &end_pose);

    Eigen::Vector3d Interpolate(double stamp, double start_stamp, double end_stamp, const Eigen::Vector3d &start_p, const Eigen::Vector3d &end_p);

    Eigen::Quaterniond Interpolate(double stamp, double start_stamp, double end_stamp, const Eigen::Quaterniond &start_q, const Eigen::Quaterniond &end_q);

    State Interpolate(double stamp, const State &start_state, const State &end_state);

    void CopyCovariance(const Eigen::Matrix<double, 6, 6> &covariance, double *arr);

    bool GetTransform(const YAML::Node &yaml_node, Sophus::SE3d &transform);
    bool GetTransform(const std::vector<double> &pose, Sophus::SE3d &transform);

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, Sophus::SE3d &data);
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, Sophus::SE3d &data);

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, MState &data);
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, MState &data);

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, BState &data);
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, BState &data);

    void TransformSL2RM(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, IState &data);
    void TransformRM2SL(const Sophus::SE3d &sensor_to_robot, const Sophus::SE3d &local_to_map, IState &data);

    void TransformDataType(const geometry_msgs::Point &m, Eigen::Vector3d &s);
    void TransformDataType(const Eigen::Vector3d &s, geometry_msgs::Point &m);

    void TransformDataType(const geometry_msgs::Vector3 &m, Eigen::Vector3d &s);
    void TransformDataType(const Eigen::Vector3d &s, geometry_msgs::Vector3 &m);

    void TransformDataType(const geometry_msgs::Quaternion &m, Eigen::Quaterniond &s);
    void TransformDataType(const Eigen::Quaterniond &s, geometry_msgs::Quaternion &m);

    void TransformDataType(const geometry_msgs::Pose &m, Sophus::SE3d &s);
    void TransformDataType(const Sophus::SE3d &s, geometry_msgs::Pose &m);

    void TransformDataType(const tf::Transform &m, Sophus::SE3d &s);
    void TransformDataType(const Sophus::SE3d &s, tf::Transform &m);

    void TransformDataType(const Eigen::Matrix4d &e, Sophus::SE3d &s);
    void TransformDataType(const Sophus::SE3d &s, Eigen::Matrix4d &e);

    bool CopyPredictData(const PredictDataPtr &src, PredictDataPtr &dest);
    bool CopyMeasureData(const MeasureDataPtr &src, MeasureDataPtr &dest);
    bool CopyLocalizerData(const LocalizerDataPtr &src, LocalizerDataPtr &dest);

    template <typename Derived>
    static Eigen::Quaternion<typename Derived::Scalar> deltaQ(const Eigen::MatrixBase<Derived> &theta)
    {
        typedef typename Derived::Scalar Scalar_t;

        Eigen::Quaternion<Scalar_t> dq;
        Eigen::Matrix<Scalar_t, 3, 1> half_theta = theta;
        half_theta /= static_cast<Scalar_t>(2.0);
        dq.w() = static_cast<Scalar_t>(1.0);
        dq.x() = half_theta.x();
        dq.y() = half_theta.y();
        dq.z() = half_theta.z();
        // dq.normalize();
        return dq;
    }
    template <typename Derived>
    static Eigen::Matrix<typename Derived::Scalar, 3, 3> skewSymmetric(const Eigen::MatrixBase<Derived> &q)
    {
        Eigen::Matrix<typename Derived::Scalar, 3, 3> ans;
        ans << typename Derived::Scalar(0), -q(2), q(1),
            q(2), typename Derived::Scalar(0), -q(0),
            -q(1), q(0), typename Derived::Scalar(0);
        return ans;
    }

    template <typename Derived>
    static Eigen::Quaternion<typename Derived::Scalar> positify(const Eigen::QuaternionBase<Derived> &q)
    {
        return q;
    }
    template <typename Derived>
    static Eigen::Matrix<typename Derived::Scalar, 4, 4> Qleft(const Eigen::QuaternionBase<Derived> &q)
    {
        Eigen::Quaternion<typename Derived::Scalar> qq = positify(q);
        Eigen::Matrix<typename Derived::Scalar, 4, 4> ans;
        ans(0, 0) = qq.w(), ans.template block<1, 3>(0, 1) = -qq.vec().transpose();
        ans.template block<3, 1>(1, 0) = qq.vec(), ans.template block<3, 3>(1, 1) = qq.w() * Eigen::Matrix<typename Derived::Scalar, 3, 3>::Identity() + skewSymmetric(qq.vec());
        return ans;
    }

    template <typename Derived>
    static Eigen::Matrix<typename Derived::Scalar, 4, 4> Qright(const Eigen::QuaternionBase<Derived> &p)
    {
        Eigen::Quaternion<typename Derived::Scalar> pp = positify(p);
        Eigen::Matrix<typename Derived::Scalar, 4, 4> ans;
        ans(0, 0) = pp.w(), ans.template block<1, 3>(0, 1) = -pp.vec().transpose();
        ans.template block<3, 1>(1, 0) = pp.vec(), ans.template block<3, 3>(1, 1) = pp.w() * Eigen::Matrix<typename Derived::Scalar, 3, 3>::Identity() - skewSymmetric(pp.vec());
        return ans;
    }

    void getTransformFromSe3(const Eigen::Matrix<double, 6, 1> &se3, Eigen::Quaterniond &q, Eigen::Vector3d &t);

    int SymbolToInt(unsigned char c, int j);

    namespace symbol_shorthand
    {
        int A(int j);
        int B(int j);
        int C(int j);
        int D(int j);
        int E(int j);
        int F(int j);
        int G(int j);
        int H(int j);
        int I(int j);
        int J(int j);
        int K(int j);
        int L(int j);
        int M(int j);
        int N(int j);
        int O(int j);
        int P(int j);
        int Q(int j);
        int R(int j);
        int S(int j);
        int T(int j);
        int U(int j);
        int V(int j);
        int W(int j);
        int X(int j);
        int Y(int j);
        int Z(int j);
    };
} // namespace yd_fusion_localization

#endif