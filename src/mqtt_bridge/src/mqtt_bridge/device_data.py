from typing import Dict, Callable


def create_device_id_extractor(params: Dict) -> Callable[[str], str]:
    device_id = params.get('device_id', '')
    def extractor():
        return device_id
    return extractor


__all__ = ['create_device_id_extractor']
