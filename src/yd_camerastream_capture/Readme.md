# yd_camerastream_capture

新的云台的红外和可见光图像均通过rtsp流获取，该package功能是将图像通过topic发送出来

## Start Run

roslaunch yd_camerastream_capture infrared_stream_capture.launch

roslaunch yd_camerastream_capture visible_stream_capture.launch

### Subscribed Topics

None

### Published Topics

1、/yida/internal/infrared/image_proc/compressed  (sensor_msgs/CompressedImage)  
2、/yida/internal/visible/image_proc/compressed  (sensor_msgs/CompressedImage)  
3、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  

### Params

**infrared_topic_name: **红外图像topic名称

**infrared_rtsp_string: **红外图像rtsp流地址

**heartbeat_topic_string: **节点心跳topic名称

**visible_topic_name: **可见光图像topic名称

**visible_rtsp_string: **可见光图像rtsp流地址



# yd_camerastream_capture

## haikang_sdk_node

可见光使用海康sdk编写的节点

## Start Run

1、下载海康sdk，将CH-HCNetSDKV6.1.4.7_build20191226_Linux64/lib下的所有文件及文件夹复制到工程目录workspace/src/yd_camerastream_capture/libs下

2、运行roslaunch yd_camerastream_capture haikang_sdk_capture.launch

### Subscribed Topics

None

### Published Topics

1、/yida/internal/visible/image_proc  (sensor_msgs/Image)  
2、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  

### Params

**haikang_topic_name: **获取图像topic名称

**sdkcom_dir:**组件库路径

**heartbeat_topic_string: **节点心跳topic名称

**device_ip: **设备ip

**device_port: **设备端口

**device_username: **设备登录用户名

**device_password: **设备登录密码

**image_width: **可见光图像宽度

**image_height: **可见光图像高度



# yd_camerastream_capture

## rtsp_to_h264_node

通过rtsp流获取h264码流的节点

## Start Run

1、.so动态库已上传至公司网盘，

链接：https://pan.baidu.com/s/1xwCDDK1vTUzJoW137nf7bQ 
提取码：qiut   

ffmpeg_lib下的所有文件及文件夹复制到工程目录workspace/src/yd_camerastream_capture/libs/ffmpeg_lib下

2、运行roslaunch yd_camerastream_capture rtsp_to_h264_capture.launch

### Subscribed Topics

None

### Published Topics

1、/yida/internal/visible/image_proc  (sensor_msgs/Image)  
2、/yida/hearbeat (diagnostic_msgs::DiagnosticArray)  

### Params

**visible_topic_name: **获取图像topic名称

**heartbeat_topic_string: **节点心跳topic名称

**visible_rtsp_string: **rtsp流地址