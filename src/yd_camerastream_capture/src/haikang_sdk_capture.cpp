#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <string>
#include <sstream>
#include <vector>

#include "haikang_sdk/HCNetSDK.h"
#include "haikang_sdk/LinuxPlayM4.h"
#include "haikang_sdk/convert.h"

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>

#include <haikang_sdk/haikang_sdk_capture.hpp>

using namespace std;

Mat g_frame;
int g_lPort = 0;
char* g_rgbBuffer = NULL;
unsigned char* g_yuvBuffer = NULL;
unsigned int g_len = 0;
std::string g_error_info_str;

long lUserID;
long lRealPlayHandle;

int flag = 0;

bool YV12ToBGR24_Table(unsigned char* pYUV,unsigned char* pBGR24,int width,int height)
{
    cout << "YV12toBGR24" << endl;
    if (width < 1 || height < 1 || pYUV == NULL || pBGR24 == NULL)
        return false;
        const long len = width * height;
         unsigned char* yData = pYUV;
         unsigned char* vData = &yData[len];
         unsigned char* uData = &vData[len >> 2];
         int bgr[3];
         int yIdx,uIdx,vIdx,idx;
         int rdif,invgdif,bdif;
         for (int i = 0;i < height;i++)
            {
                for (int j = 0;j < width;j++)
                {
                    yIdx = i * width + j;
                    vIdx = (i/2) * (width/2) + (j/2);
                    uIdx = vIdx;
                    rdif = Table_fv1[vData[vIdx]];
                    invgdif = Table_fu1[uData[uIdx]] + Table_fv2[vData[vIdx]];
                    bdif = Table_fu2[uData[uIdx]];
                    bgr[0] = yData[yIdx] + bdif;
                    bgr[1] = yData[yIdx] - invgdif;
                    bgr[2] = yData[yIdx] + rdif;
                    for (int k = 0;k < 3;k++)
                    {
                       idx = (i * width + j) * 3 + k;
                       if(bgr[k] >= 0 && bgr[k] <= 255)
                       pBGR24[idx] = bgr[k];
                       else
                       pBGR24[idx] = (bgr[k] < 0)?0:255;
                    }
                }
            }
           return true;
}

void CALLBACK DecCBFun(int nPort, char* pBuf, int  nSize, FRAME_INFO* pFrameInfo, int nReserved1, int nReserved2)
{
    cout << "DecCBFun" << endl;
    if(pFrameInfo->nType == T_YV12)
    {
    	g_yuvBuffer = (unsigned char*)pBuf;
        g_len = strlen(pBuf);
        for (int n=0;n<4;n++)
		            printf("  0x%02x\t%02x\n",g_yuvBuffer+n,g_yuvBuffer[n]);
        std::cout << "strlen(pBuf):" << strlen(pBuf) << std::endl;
		
		/*if(flag == 0)
		{
			flag = 1;
		    FILE *fp = NULL;
			fp = fopen("yuv_file.yuv", "w+");
			//fwrite(pBuf, strlen(pBuf),1,fp);
			fwrite(pBuf, 1920*1080,1,fp);
		}*/

        //YV12ToBGR24_Table((unsigned char*)pBuf,(unsigned char*)g_rgbBuffer ,pFrameInfo->nWidth, pFrameInfo->nHeight);
        //g_frame = cv::Mat(pFrameInfo->nHeight, pFrameInfo->nWidth, CV_8UC3, g_rgbBuffer);
        //char test[20] = {0};
        //sprintf(test, "Frame:---%d", pFrameInfo->dwFrameNum);
    }
}

void CALLBACK RealDataCallBack_V30(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void* dwUser)
{
	/*switch (dwDataType)
	{
		case NET_DVR_SYSHEAD: 
			if (!PlayM4_GetPort(&g_lPort))
				break;
			if (dwBufSize > 0)
			{
				if (!PlayM4_SetStreamOpenMode(g_lPort, STREAME_REALTIME))
					break;
				if (!PlayM4_OpenStream(g_lPort, pBuffer, dwBufSize, SOURCE_BUF_MAX))
					break;
				if (!PlayM4_Play(g_lPort, 0))
					break;
				cout << "set DecCBFun" << endl;
				PlayM4_SetDecCallBack(g_lPort,DecCBFun);
                //g_yuvBuffer = pBuffer;
                //for (int n=0;n<4;n++)
		        //    printf("  0x%02x\t%02x\n",g_yuvBuffer+n,g_yuvBuffer[n]);
			}
		case NET_DVR_STREAMDATA:
			if (dwBufSize > 0 && g_lPort != -1)
			{
	            //cout << "Buf size: " << dwBufSize << endl;
				if (!PlayM4_InputData(g_lPort, pBuffer, dwBufSize))
					break;
			}
	}*/
    DWORD dRet;
	haikang_sdk_capture* pCapObj = (haikang_sdk_capture*)dwUser;

	if (!pCapObj)
	{
		return ;
	}
	if (lRealHandle==0)
	{
		dRet = 0;
	}
	else if (lRealHandle==1)
	{
		dRet = 1;
	}

	pCapObj->receive_real_data(lRealHandle, dwDataType, pBuffer, dwBufSize, dwUser);
}

/*void CALLBACK StdDataCallBack(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void* pUser)
{
    haikang_sdk_capture* pCapObj = (haikang_sdk_capture*)pUser;
	int nRet = -1;
	switch (dwDataType)
	{
	case NET_DVR_SYSHEAD://coming the stream header, open stream
		{
			nRet = 1;
		}
		break;
	case NET_DVR_STD_VIDEODATA:
		{
			nRet = 4;
		}
		break;
	case NET_DVR_STD_AUDIODATA:
		{
			nRet = 5;
		}
		break;
	case NET_DVR_STREAMDATA:
		{
			nRet = 2;
 		}
		break;
	}
    //pCapObj->receive_real_data(lRealHandle, dwDataType, pBuffer, dwBufSize, pUser);
}*/

haikang_sdk_capture::haikang_sdk_capture()
{ 
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;

    private_node_handle.param<std::string>("haikang_topic_name", visible_topic_str, "/haikangsdk/image_proc");
    private_node_handle.param<std::string>("sdkcom_dir", sdkcom_dir_str, "/home/yd/workspace/src/yd_camerastream_capture/libs");
    private_node_handle.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");
    private_node_handle.param<std::string>("device_ip", m_device_ip, "192.168.1.64");
    private_node_handle.param<std::string>("device_port", m_device_port, "8000");
    private_node_handle.param<std::string>("device_username", m_device_username, "admin");
    private_node_handle.param<std::string>("device_password", m_device_password, "123qweasd");
    private_node_handle.param<std::string>("image_width", m_image_width, "1920");
    private_node_handle.param<std::string>("image_height", m_image_height, "1080");

    char* device_ip = new char[100];
    strcpy(device_ip, m_device_ip.c_str());
    char* device_username = new char[100];
    strcpy(device_username, m_device_username.c_str());
    char* device_password = new char[100];
    strcpy(device_password, m_device_password.c_str());

    //image_transport::ImageTransport it(node_handle_);
    //publisher_image_ = it.advertise(visible_topic_str, 1);
    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    buffer_pub_ = node_handle_.advertise<sensor_msgs::Image>(visible_topic_str, 1);

    lUserID = -1;
    lRealPlayHandle = -1;

    for (int i = 0; i < MAX_DEV+1; i++)
	{
		if(i < MAX_DEV)
		{
			for(int nI=0; nI < MAX_FRAME_LENTH; nI++)
			{
				memset(&m_pFrameCache[i][nI], 0x00, sizeof(CacheBuffder));
			}
			m_mFrameCacheLenth[i] = 0;
		}
	}

    NET_DVR_LOCAL_SDK_PATH struComPath = {0};
    std::string comDir = sdkcom_dir_str; // HCNetSDKCom
    sprintf(struComPath.sPath, comDir.c_str());
    NET_DVR_SetSDKInitCfg(NET_SDK_INIT_CFG_SDK_PATH, &struComPath);

    NET_DVR_Init();
    //登录参数，包括设备地址、登录用户、密码等
    NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
    struLoginInfo.bUseAsynLogin = 0;                    //同步登录方式
    strcpy(struLoginInfo.sDeviceAddress, device_ip);    //设备IP地址
    struLoginInfo.wPort = atoi(m_device_port.c_str());  //设备服务端口
    strcpy(struLoginInfo.sUserName, device_username);   //设备登录用户名
    strcpy(struLoginInfo.sPassword, device_password);   //设备登录密码
    //strcpy(struLoginInfo.sDeviceAddress, "192.168.1.62");    //设备IP地址
    //struLoginInfo.wPort = 8000;  //设备服务端口
    //strcpy(struLoginInfo.sUserName, "admin");   //设备登录用户名
    //strcpy(struLoginInfo.sPassword, "123qweasd");   //设备登录密码
    NET_DVR_DEVICEINFO_V40 struDeviceInfo = {0};

    char* sJpegPicBuffer = new char[atoi(m_image_width.c_str()) * atoi(m_image_height.c_str()) * 3];
    assert(sJpegPicBuffer);
    g_rgbBuffer = sJpegPicBuffer;

    lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfo);
    if (lUserID < 0)
    {
        printf("ip: %s---Login error,  %d\n", m_device_ip.c_str(), NET_DVR_GetLastError());
        g_error_info_str = "login error!";
        NET_DVR_Cleanup();
    }
    else
    {
        NET_DVR_PREVIEWINFO struPlayInfo = {0};
        struPlayInfo.hPlayWnd     = 0;       //需要SDK解码时句柄设为有效值，仅取流不解码时可设为空
        struPlayInfo.lChannel     = 1;       //预览通道号
        struPlayInfo.dwStreamType = 0;       //0-主码流，1-子码流，2-码流3，3-码流4，以此类推
        struPlayInfo.dwLinkMode   = 0;       //0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP
        struPlayInfo.bBlocked     = 1;       //0- 非阻塞取流，1- 阻塞取流
        lRealPlayHandle = NET_DVR_RealPlay_V40(lUserID, &struPlayInfo, RealDataCallBack_V30, this);
        if(lRealPlayHandle < 0)
        {
            printf("ip: %s---Login error,  %d\n", m_device_ip.c_str(), NET_DVR_GetLastError());
            g_error_info_str = "get data error!";
            NET_DVR_Logout_V30(lUserID);
            NET_DVR_Cleanup();
        }

        //设置标准音视频数据回调
		//BOOL bSuc = FALSE;
		//bSuc = NET_DVR_SetStandardDataCallBackEx(lRealPlayHandle, StdDataCallBack, this);
    }

}

haikang_sdk_capture::~haikang_sdk_capture()
{

}

void haikang_sdk_capture::receive_real_data(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void* dwUser)
{
    if (lRealHandle>=0)
	{
		DWORD dRet = 0;
		switch (dwDataType)
		{
            case NET_DVR_SYSHEAD:
            {
                if (!PlayM4_GetPort(&g_lPort))
                {
                    break;
                }
                if (!PlayM4_OpenStream(g_lPort,pBuffer,dwBufSize,SOURCE_BUF_MAX))
                {
                    dRet=PlayM4_GetLastError(g_lPort);
                    break;
                }
                if (!PlayM4_SetDecCallBackEx(g_lPort,DecCBFun,0,0))
                {
                    dRet=PlayM4_GetLastError(g_lPort);
                    break;
                }
                /*
                //打开视频解码
                if (!PlayM4_Play(g_lPort,0))
                {
                    dRet=PlayM4_GetLastError(g_lPort);
                    break;
                }
                //打开音频解码, 需要码流是复合流
                if (!PlayM4_PlaySound(g_lPort))
                {
                    dRet=PlayM4_GetLastError(g_lPort);
                    break;
                }*/
            }
            break;
            case NET_DVR_STREAMDATA:
            {
                //std::cout << "**************** get in streamdata ***************" << std::endl;
                bool inData=PlayM4_InputData(g_lPort,pBuffer,dwBufSize);
                while (!inData)
                {
                    PlayM4_CloseStream(g_lPort);
                    sleep(5);
                    PlayM4_OpenStream(g_lPort,pBuffer,dwBufSize,SOURCE_BUF_MAX);
                    inData=PlayM4_InputData(g_lPort,pBuffer,dwBufSize);
                    std::cout << "PlayM4_InputData failed \n" << std::endl;	
                }
                if(pBuffer != NULL)
                {
                    g_yuvBuffer = (unsigned char*)pBuffer;
                    g_len = dwBufSize;
                    //for (int n=0;n<4;n++)
                    //            printf("  0x%02x\t%02x\n",g_yuvBuffer+n,g_yuvBuffer[n]);
                    //std::cout << "strlen(pBuffer):" << dwBufSize << std::endl;

                    vector<unsigned char> vc(g_yuvBuffer, g_yuvBuffer+g_len);
                    sensor_msgs::Image msg;
                    msg.data = vc;
                    buffer_pub_.publish(msg);
                }
            }
            break;
        }
	}
}

//从PS流中取H264数据
//返回值：是否为数据包 
bool haikang_sdk_capture::get_h246_fromps(BYTE* pBuffer, int nBufLenth, BYTE** pH264, int& nH264Lenth, bool& bVideo)
{
	if (!pBuffer || nBufLenth<=0)
	{
		return false;
	}

	BYTE* pH264Buffer = NULL;
	int nHerderLen = 0;
	if( pBuffer
		&& pBuffer[0]==0x00
		&& pBuffer[1]==0x00 
		&& pBuffer[2]==0x01
		&& pBuffer[3]==0xE0) //E==视频数据(此处E0标识为视频)
	{   
		bVideo = true;
		nHerderLen = 9 + (int)pBuffer[8];//9个为固定的数据包头长度，pBuffer[8]为填充头部分的长度
		pH264Buffer = pBuffer + nHerderLen;
		if (*pH264 == NULL)
		{
			*pH264 = new BYTE[nBufLenth];
		}
		if (*pH264 && pH264Buffer && (nBufLenth-nHerderLen) > 0)
		{	
			memcpy(*pH264, pH264Buffer, (nBufLenth-nHerderLen));
		}
        if(pH264Buffer != NULL)
        {
            pH264Buffer = NULL;
        }	
		nH264Lenth = nBufLenth - nHerderLen;
		return true;
	}	
	else if(pBuffer 
		&& pBuffer[0]==0x00
		&& pBuffer[1]==0x00
		&& pBuffer[2]==0x01
		&& pBuffer[3]==0xC0) //C==音频数据？
	{
        if(*pH264 != NULL)
        {
            delete []*pH264;
            *pH264 = NULL;
        }
		nH264Lenth = 0;
		bVideo = false;
	}
	else if(pBuffer 
		&& pBuffer[0]==0x00
		&& pBuffer[1]==0x00
		&& pBuffer[2]==0x01
		&& pBuffer[3]==0xBA) //视频流数据包 包头
	{
		bVideo = true;
        if(*pH264 != NULL)
        {
            delete []*pH264;
            *pH264 = NULL;
        }
		nH264Lenth = 0;
		return false;
	}
	return false;
}

void haikang_sdk_capture::update()
{
    //if(g_frame.empty())
    if(g_len == 0)
    {
        int level = 2;
        std::string message = g_error_info_str;
        std::string hardware_id = "visible_camera";
        pub_heartbeat(level, message, hardware_id);
    }
    else
    {
        g_len = 0;
        int level = 0;
        std::string message = "visible camera is ok!";
        std::string hardware_id = "visible_camera";
        pub_heartbeat(level, message, hardware_id);
    }
}

void haikang_sdk_capture::pub_heartbeat(int level, string message, string hardware_id)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();
    
    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;         // 这里写节点的名字
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;       // 问题描述
    }
    s.hardware_id = hardware_id;   // 硬件信息
    log.status.push_back(s);

    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    haikang_sdk_capture hsdkcap;
    ros::Rate rate(30);

    while (ros::ok())
    {
        hsdkcap.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
