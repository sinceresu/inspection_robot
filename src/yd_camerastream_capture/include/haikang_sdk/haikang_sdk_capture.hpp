#include <iostream>
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/image_encodings.h>
#include <diagnostic_msgs/DiagnosticArray.h>

using namespace cv;
using namespace std;

#define __app_name__ "haikang_sdk_node"
#define MAX_PACK_SIZE 5120
#define MAX_FRAME_LENTH 64
#define MAX_DEV 1

typedef struct tagCacheBuffder
{
	BYTE* pCacheBuffer;
	int	  nCacheBufLenth;//最大包长度为5120
	long  lTimeStamp;
	DWORD  dwFrameLenth;

}CacheBuffder;
 
class haikang_sdk_capture
{
public:
    ros::Publisher buffer_pub_;

private:
    std::string visible_topic_str, heartbeat_topic_str, sdkcom_dir_str;
    std::string m_device_ip, m_device_port, m_device_username, m_device_password;
    std::string m_image_width, m_image_height;
    image_transport::Publisher publisher_image_;
    ros::Publisher heartbeat_pub_;

    int m_mFrameCacheLenth[MAX_DEV];
    CacheBuffder m_pFrameCache[MAX_DEV][MAX_FRAME_LENTH];

public:
    haikang_sdk_capture();
    ~haikang_sdk_capture();
    void update();
    void pub_heartbeat(int level, string message, string hardware_id);
    void receive_real_data(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void* dwUser);
    bool get_h246_fromps(BYTE* pBuffer, int nBufLenth, BYTE** pH264, int& nH264Lenth, bool& bVideo);
};
