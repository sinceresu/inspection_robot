#ifndef YD_FRAMWORK_RESCENTER_HPP
#define YD_FRAMWORK_RESCENTER_HPP

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <yd_framework/Settings.h>
#include "yidamsg/UrgencyTask.h"

#include <rapidjson/istreamwrapper.h>
#include <rapidjson/encodedstream.h>
#include <rapidjson/document.h>
#include <thread>
#include <vector>
#include <map>

struct ResMessage
{
    std::string type;
    std::string message;
    ros::Subscriber subscriber;
    double lastStamp, timeout;
};

class ResourceCenter
{
public:
    ros::NodeHandle _node;
    ros::Publisher _heartbeat;
    ResourceCenter()
    {
        _node.param<std::string>("/resource_center/remoteIp", _remote_ip,"");
        _node.param<std::string>("/monitor_center/jsonPath", _jsonPath,"");
        _settings = _node.advertiseService<yd_framework::SettingsRequest, yd_framework::SettingsResponse>(
            "/yd/settings", boost::bind(&ResourceCenter::settingsCallback, this, _1, _2));
        _settingsChanged = _node.advertise<diagnostic_msgs::KeyValue>("/yd/settings_changed", 1);
        _heartbeat = _node.advertise<diagnostic_msgs::DiagnosticArray>("/yd/heartbeat", 10);
        _taskPub = _node.advertise<yidamsg::UrgencyTask>("/ydserver/task_list", 2);
        ///test 
        taskSub = _node.subscribe("/ydserver/task_list", 2, &ResourceCenter::taskCallback, this);
        //_robotStatus = _node.advertise<diagnostic_msgs::DiagnosticArray>("/yd/robot_status", 10);
    }
    
    /** Load into message vector and subscribe to record their data */
    bool loadResource(const std::string& jsonFile);

    bool loadResource(){
        std::cout << "_jsonPath:" << _jsonPath << std::endl;
        loadResource(_jsonPath);
    }
    
    /** Update resource periodly and send them to remote side */
    std::string remoteIP(){
        return _remote_ip;
    }

protected:
    void stringCallback(const std_msgs::StringConstPtr& ptr, const std::string& m);
    void diagnosticCallback(const diagnostic_msgs::DiagnosticArrayConstPtr& ptr, const std::string& m);
    void odometryCallback(const nav_msgs::OdometryConstPtr& ptr, const std::string& m);
    void imageCallback(const sensor_msgs::ImageConstPtr& ptr, const std::string& m);
    void imuCallback(const sensor_msgs::ImuConstPtr& ptr, const std::string& m);
    void navSatFixCallback(const sensor_msgs::NavSatFixConstPtr& ptr, const std::string& m);
    void laserScanCallback(const sensor_msgs::LaserScanConstPtr& ptr, const std::string& m);
    void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& ptr, const std::string& m);
    void transformCallback(const geometry_msgs::TransformStampedConstPtr& ptr, const std::string& m);
    void taskCallback(const yidamsg::UrgencyTask::ConstPtr& msg){
        ROS_INFO("task callback");
        int taskId = msg->task_id;
        ROS_INFO("taskId:%i",taskId);
    }
    
    bool settingsCallback(yd_framework::SettingsRequest& req, yd_framework::SettingsResponse& response);
    bool updateMessage(const std_msgs::Header& header, const std::string& m, ResMessage& msg);
    
    std::mutex _settingsMutex;
    std::map<std::string, std::string> _settingsMap;
    std::map<std::string, ResMessage> _messages;
    ros::ServiceServer _settings;
    ros::Publisher _settingsChanged,_taskPub;//_robotStatus
    ros::Subscriber taskSub;
    std::string _remote_ip,_jsonPath;
};

#endif

