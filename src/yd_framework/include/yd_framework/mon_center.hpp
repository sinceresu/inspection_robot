#ifndef YD_FRAMWORK_MONCENTER_HPP
#define YD_FRAMWORK_MONCENTER_HPP

#include <ros/ros.h>
#include <ros/master.h>
#include <std_msgs/String.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <rosmon_msgs/State.h>
#include <yd_framework/Debug.h>
#include <yd_framework/hmi_ui.hpp>

#include <rapidjson/istreamwrapper.h>
#include <rapidjson/encodedstream.h>
#include <rapidjson/document.h>
#include <vector>
#include <queue>
#include <map>

enum SystemState {
	INIT=1,WAITTING=2,FINISH=3
};

struct NodeState
{
    std::string name;
    bool needHeartbeat, supervising;
    ros::Subscriber supervisingState;
    diagnostic_msgs::DiagnosticArray log;
    std::queue<double> timestamps;
    int restartType; // 0 default 1 restart all 2 restart xavier

    NodeState() : needHeartbeat(false), supervising(false),restartType(0)
    {
    }
};

class MonitoringCenter
{
public:
    MonitoringCenter()
    {
        _node.param<std::string>("/monitor_center/jsonPath", _jsonPath,"");
        _node.param<std::string>("/monitor_center/nvidiaIp", _nvidiaIp,"");
        _debug = _node.advertiseService<yd_framework::DebugRequest, yd_framework::DebugResponse>(
            "/yd/debug", boost::bind(&MonitoringCenter::debugCallback, this, _1, _2));
        _nodeStatus = _node.advertise<diagnostic_msgs::DiagnosticArray>("/yd/node_status", 5);
        _heartbeat = _node.subscribe<diagnostic_msgs::DiagnosticArray>(
            "/yd/heartbeat", 1024, boost::bind(&MonitoringCenter::diagnosticCallback, this, _1));
        _systemStatus = _node.advertise<diagnostic_msgs::DiagnosticStatus>("/yd/system_state", 5);
        //init
        int status = 0;
        if (ros::param::get("/yd/system_state", status)){
            _status = (SystemState)status;
            ROS_INFO("ros param get /yd/system_state success value:%d",status);
        }else{
            _status = SystemState::INIT;
            ros::param::set("/yd/system_state",(int)_status);
        } 
        //localhost
        _glanceTimer = _node.createTimer(ros::Duration(2.0),
                                         boost::bind(&MonitoringCenter::glanceTimerCallback, this, _1));
        // _glanceUriPrefix = "http://127.0.0.1:61208/api/2/";
        // _glanceResults["cpu"] = KeyValueList();
        // _glanceResults["mem"] = KeyValueList();
        // _glanceResults["network"] = KeyValueList();
        // _glanceRequesting = false;

        _glanceResults["yd/cpu"] = KeyValueList();
        _glanceResults["yd/mem"] = KeyValueList();
        _glanceResults["yd/network"] = KeyValueList();
        _glanceIpKey["yd/cpu"]="http://127.0.0.1:61208/api/2/cpu";
        _glanceIpKey["yd/mem"]="http://127.0.0.1:61208/api/2/mem";
        _glanceIpKey["yd/network"]="http://127.0.0.1:61208/api/2/network";

        _glanceResults["nvidia/cpu"] = KeyValueList();
        _glanceResults["nvidia/mem"] = KeyValueList();
        _glanceResults["nvidia/network"] = KeyValueList();
        _glanceIpKey["nvidia/cpu"]="http://"+_nvidiaIp+":61208/api/2/cpu";
        _glanceIpKey["nvidia/mem"]="http://"+_nvidiaIp+":61208/api/2/mem";
        _glanceIpKey["nvidia/network"]="http://"+_nvidiaIp+":61208/api/2/network";
        _glanceRequesting = false;
    }

    /** Load into node registration vector and subscribe to monitor them */
    bool loadResource(const std::string &jsonFile);
    bool loadResource(){
        std::cout << "jsonPath:" << _jsonPath << std::endl;
        loadResource(_jsonPath);
    }

    /** Update node states and check node heartbeats */
    void update();
    /// \brief 更新系统状态
    /// 1.检查心跳      /yd/heartbeat
    /// 2.检查监控节点   mon_XXX
    void statusUpdate();

    /** Get master's' network address */
    std::string masterUri()
    {
        XmlRpc::XmlRpcValue args, result, payload;
        args[0] = ros::this_node::getName();
        if (ros::master::execute("getUri", args, result, payload, true))
            return std::string(payload);
        return "";
    }

    /** Get given node's network address */
    std::string nodeUri(const std::string &node)
    {
        XmlRpc::XmlRpcValue args, result, payload;
        args[0] = ros::this_node::getName();
        args[1] = node;
        if (ros::master::execute("lookupNode", args, result, payload, true))
            return std::string(payload);
        return "";
    }

    /** Get all topics and services and their relationships */
    typedef std::map<std::string, std::vector<std::string>> SupporterMap;
    bool systemState(SupporterMap &publishers, SupporterMap &subscribers, SupporterMap &services)
    {
        XmlRpc::XmlRpcValue args, result, payload;
        args[0] = ros::this_node::getName();
        if (ros::master::execute("getSystemState", args, result, payload, true))
        {
            for (int i = 0; i < payload[0].size(); ++i)
            {
                std::vector<std::string> &l = publishers[std::string(payload[0][i][0])];
                for (int j = 0; j < payload[0][i][1].size(); ++j)
                    l.push_back(payload[0][i][1][j]);
            }

            for (int i = 0; i < payload[1].size(); ++i)
            {
                std::vector<std::string> &l = subscribers[std::string(payload[1][i][0])];
                for (int j = 0; j < payload[1][i][1].size(); ++j)
                    l.push_back(payload[1][i][1][j]);
            }

            for (int i = 0; i < payload[2].size(); ++i)
            {
                std::vector<std::string> &l = services[std::string(payload[2][i][0])];
                for (int j = 0; j < payload[2][i][1].size(); ++j)
                    l.push_back(payload[2][i][1][j]);
            }
            return true;
        }
        return false;
    }

protected:
    void diagnosticCallback(const diagnostic_msgs::DiagnosticArrayConstPtr &ptr);
    void supervisingCallback(const rosmon_msgs::StateConstPtr &ptr, const std::string &m);
    void glanceTimerCallback(const ros::TimerEvent &event);
    bool debugCallback(yd_framework::DebugRequest &req, yd_framework::DebugResponse &response);

    std::map<std::string, NodeState> _regNodes;
    ros::NodeHandle _node;
    ros::Timer _glanceTimer,_glanceTimer2;
    ros::ServiceServer _debug;
    ros::Publisher _nodeStatus,_systemStatus;
    ros::Subscriber _heartbeat;
    SystemState _status;

    typedef std::vector<diagnostic_msgs::KeyValue> KeyValueList;
    //localhost
    std::map<std::string, std::string> _glanceIpKey;
    std::map<std::string, KeyValueList> _glanceResults;
    std::string _glanceUriPrefix, _glanceCurrentKey;
    bool _glanceRequesting;
    
    HmiUI hmi;
    std::string _jsonPath,_nvidiaIp,_serviceName;
    double _last_restart_time;
};

#endif
