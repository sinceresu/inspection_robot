#include <simple_client.hpp>
#include <bridge_message.hpp>
#include <simple_uv.hpp>
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

SimpleUV::SimpleUV(const std::string &addr, int udpPort) : SimpleClient(),m_next_id(0),m_count(0)
{
    url = "ws://"+addr+":"+std::to_string(udpPort);
    _ppClient.set_access_channels(websocketpp::log::alevel::all);
    //_client.clear_access_channels(websocketpp::log::alevel::frame_payload);
    _ppClient.clear_access_channels(websocketpp::log::alevel::all);
    _ppClient.clear_error_channels(websocketpp::log::alevel::all);
    _ppClient.init_asio();
    _ppClient.start_perpetual();
    m_thread.reset(new websocketpp::lib::thread(&ClientType::run, &_ppClient));
}

SimpleUV::~SimpleUV()
{
    LOGI("stream input *** "<< "SimpleUV end"<< " *** ");
}

void SimpleUV::stop(){
    LOGI("[SimpleUV] stop");
    SimpleClient::stop();
    _ppClient.stop_perpetual();
    for (ConList::const_iterator it = m_connection_list.begin(); it != m_connection_list.end(); ++it) {
        LOGI("[SimpleUV] Closing connection " << it->first);
        websocketpp::lib::error_code ec;
        _ppClient.close(it->second, websocketpp::close::status::going_away, "", ec);
        if (ec) {
            LOGI("[SimpleUV] Error closing connection " << it->first << ": "  
                          << ec.message());
        }
    }
    m_thread->join();
    delete m_thread.get();
}

void SimpleUV::runOnce()
{
    if (!_clientStatus){
        LOGI("[SimpleUV] rosbridge websocket("<< _port <<") connect failed");
        if(_serverStatus){
            try
            {
                LOGI("[SimpleUV] websocket client close connection!");
                websocketpp::lib::error_code ec;
                _ppClient.close(_ppConnection, websocketpp::close::status::going_away, "", ec);
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }  
        }
        sleep(1);
        return;
    }
    if(!_serverStatus)
    {
        LOGI("[SimpleUV] bridge server connect failed!");
        connect();
        sleep(5);
        return;
    }
    std::vector<std::string> localSendingList;
    {
        _sendingMutex.lock();
        localSendingList.swap(_sendingList);
        _sendingMutex.unlock();
    }
    for (unsigned int i = 0; i < localSendingList.size(); ++i)
    {
        write(localSendingList[i]);
    }
}

void SimpleUV::pollTimer(){
    if (!_clientStatus || !_serverStatus) return;
    _ppClient.ping(_ppConnection,"");
    //timer delete by 30s
    auto first = _timeout_queue.begin();
    auto end = _timeout_queue.end();
    int64_t current = _timer.elapsed_micro();
    for (first; first < end; first++) {
        int64_t q = *first;
        std::cout << q << " " << std::endl;
        if(current-q<30*1000){
            continue;
        }   
    }
    if(first!=_timeout_queue.begin()){
        //delete
        _timeout_queue.erase(_timeout_queue.begin(),first);
    } 
}

bool SimpleUV::write(std::string& data){
    try
    {
        #if OUTPUT_HEADER
        Document document;
        ParseResult result = document.Parse(data.c_str());
        if (result.IsError())
        {
            LOGE("[BridgeUV] JSON parse error:" << result.Code() << " offset:" << result.Offset());
            return false;
        }
        if (!document.IsObject())
        {
            LOGE("[BridgeUV] error:message is not object,data:" << data);
            return false;
        }
        Value::ConstMemberIterator itr = document.FindMember("op");
        if (itr != document.MemberEnd()){
            if (strcmp(itr->value.GetString(), "publish") == 0){
                std::string topic = document["topic"].GetString();
                if(strcmp(topic.c_str(),"/detect_result")==0){
                    LOGD("[BridgeUV] ready to send " << data);
                    m_count++;
                    LOGD("[BridgeUV] send message time " << m_count);
                }
            }
        }
        #endif
        std::string msg = BridgeMessager::serialized(data,USE_LZ4);
        websocketpp::lib::error_code ec;
        _ppClient.send(_ppConnection, msg, websocketpp::frame::opcode::binary, ec);
        if (ec)
        {
            LOGE("[BridgeUV] send message failed: " << ec.message() << " msg:" << msg);
            return false;
        }
        return true;
    } catch (websocketpp::exception const & e) {
        LOGE("[BridgeUV] websocketpp exception " << e.what());
    }
    return false;
}

void SimpleUV::connect(){
    LOGI("[BridgeUV] read new bridge connect");
    _ppClient.reset();
    websocketpp::lib::error_code ec;
    ClientType::connection_ptr con = _ppClient.get_connection(url, ec);
    if (ec) {
        LOGE("[BridgeUV] could not create connection because: " << ec.message());
        return;
    }
    _ppClient.set_open_handler(websocketpp::lib::bind(&SimpleUV::onOpen, this, ::_1));
    _ppClient.set_fail_handler(websocketpp::lib::bind(&SimpleUV::onFailed, this, ::_1));
    _ppClient.set_close_handler(websocketpp::lib::bind(&SimpleUV::onClose, this, ::_1));
    _ppClient.set_message_handler(websocketpp::lib::bind(&SimpleUV::onMessage, this, ::_1, ::_2));
    //_ppClient.set_pong_timeout_handler(websocketpp::lib::bind(&SimpleUV::onTimeout, this, ::_1));
    //_ppClient.set_ping_handler(websocketpp::lib::bind(&SimpleUV::onPing, this, ::_1, ::_2));

    _ppClient.connect(con);
}

void SimpleUV::handleMessageWS(std::string &msg)
{
    _sendingMutex.lock();
    _sendingList.push_back(msg);
#if OUTPUT_MESSAGE
    LOGD("from ros:"<< msg);
#endif
    _sendingMutex.unlock();
}

void SimpleUV::clear(){
   _sendingMutex.lock();
    _sendingList.clear();
    LOGD("[SimpleUV] clear message");
    _sendingMutex.unlock();
}

void SimpleUV::onFailed(ConnectionType hdl){
    LOGE("[BridgeUV] closing connection:" << hdl.lock().get() << " current connection:" <<_ppConnection.lock().get());
    if(hdl.lock().get()==_ppConnection.lock().get()){
        _ppConnection = hdl;
        _serverStatus = false;
    }
}

void SimpleUV::onTimeout(ConnectionType hdl){
    LOGI("time out "<< _timer.elapsed_micro());
    _timeout_queue.push_back(_timer.elapsed_micro());
}

bool SimpleUV::onPing(ConnectionType hdl,std::string msg){
    std::cout << "ping pong success" << std::endl;
}

void SimpleUV::onOpen(ConnectionType hdl){
    _serverStatus = true;
    _ppConnection = hdl;
    int new_id = m_next_id++;
    m_connection_list[new_id] = hdl;
    LOGI("[BridgeUV] open new bridge connect,id "<< new_id);
    clear();
    _isReceive = true;
    m_count = 0;
}
void SimpleUV::onClose(ConnectionType hdl){
    LOGE("[BridgeUV] closing connection:" << hdl.lock().get() << " current connection:" <<_ppConnection.lock().get());
    if(hdl.lock().get()==_ppConnection.lock().get()){
        _ppConnection = hdl;
        _serverStatus = false;
    }
    try
    {
         for (ConList::const_iterator it = m_connection_list.begin(); it != m_connection_list.end(); ++it) {
            LOGI("[BridgeUV] closing connection " << it->first);
            websocketpp::lib::error_code ec;
            if(it->second.lock()==hdl.lock()){
                _ppClient.close(it->second, websocketpp::close::status::going_away, "", ec);
                if (ec) {
                    LOGE("[BridgeUV] Error closing connection " << it->first << ": "  
                          << ec.message());
                }
            }
        }   
    }
    catch(const std::exception& e)
    {
        LOGE("[BridgeUV] close error:"<< e.what());
    }
}
void SimpleUV::onMessage(ConnectionType hdl, ClientType::message_ptr msg){
    try
    {
        std::string _msg = msg->get_payload();
        std::string data = BridgeMessager::deserialized(_msg,USE_LZ4);
        if (_client == nullptr)
        {
            std::cout << "[SimpleUV] client is null ptr" << std::endl;
        }
        #if OUTPUT_MESSAGE
        LOGD("to ros:"<< data);
        #endif
        send(data);
    }
    catch(const std::exception& e)
    {
        LOGE("[SimpleUV] exception:"<<e.what());
    }
}