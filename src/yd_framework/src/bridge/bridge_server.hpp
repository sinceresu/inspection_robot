#ifndef BRIDGE_SERVER_HPP
#define BRIDGE_SERVER_HPP

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <iostream>
#include <vector>
#include <set>

typedef websocketpp::server<websocketpp::config::asio> ServerType;
typedef websocketpp::connection_hdl ConnectionType;

class BridgeServer
{
public:
    struct ActionData
    {
        enum ActionType { SUBSCRIBE, UNSUBSCRIBE, MESSAGE };
        ActionData(ActionType t, ConnectionType h) : type(t), hdl(h) {}
        ActionData(ActionType t, ConnectionType h, ServerType::message_ptr m) : type(t), hdl(h), msg(m) {}

        ActionType type;
        ConnectionType hdl;
        ServerType::message_ptr msg;
    };
    
    BridgeServer();
    void process();

    virtual void start(uint16_t port);
    virtual void stop();
    virtual void runOnce();

protected:
    void onOpen(ConnectionType hdl);
    void onClose(ConnectionType hdl);
    void onMessage(ConnectionType hdl, ServerType::message_ptr msg);
    virtual void handleMessageWS(ConnectionType& hdl, ServerType::message_ptr& msg) {}
    
    typedef std::set<ConnectionType, std::owner_less<ConnectionType> > ConnectionList;
    ConnectionList _connections;
    std::vector<ActionData> _actions;
    ServerType _server;

    websocketpp::lib::mutex _actionLock, _connectionLock;
    websocketpp::lib::condition_variable _actionCondition;
    bool _isProcessing;
};

#endif
