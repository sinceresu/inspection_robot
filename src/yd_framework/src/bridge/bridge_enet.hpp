#ifndef BRIDGE_ENET_HPP
#define BRIDGE_ENET_HPP

#include "bridge_server.hpp"
#include "bridge_client.hpp"
#include "enet/enet.h"
#include <set>

class BridgeServerEnet : public BridgeServer
{
public:
    BridgeServerEnet(int udpPort);
    virtual ~BridgeServerEnet();
    virtual void runOnce();
    
    bool write(const std::string& data, bool toFulsh = true);
    bool valid() const { return _enetServer != NULL; }
    unsigned int numClients() const { return _peers.size(); }

protected:
    virtual void handleMessageEnet(ENetEvent& event, const std::vector<char>& buf);
    virtual void handleMessageWS(ConnectionType& hdl, ServerType::message_ptr& msg);
    
    std::mutex _sendingMutex;
    std::vector<std::string> _sendingList;
    ENetHost* _enetServer;
    std::set<ENetPeer*> _peers;
};

class BridgeClientEnet : public BridgeClient
{
public:
    BridgeClientEnet(const std::string& addr, int udpPort);
    virtual ~BridgeClientEnet();
    virtual void runOnce();
    
    bool write(const std::string& data, bool toFulsh = true);
    bool valid() const { return _enetClient != NULL; }
    bool isConnected() const { return _serverPeer != NULL; }

protected:
    virtual void handleMessageEnet(ENetEvent& event, const std::vector<char>& buf);
    virtual void handleMessageWS(ConnectionType& hdl, ServerType::message_ptr& msg);
    void reconnect(int confirmTimeout);

    std::mutex _sendingMutex;
    std::vector<std::string> _sendingList;
    ENetAddress _serverAddress;
    ENetHost* _enetClient;
    ENetPeer* _serverPeer;
};

#endif
