#ifndef SIMPLE_ENET_HPP
#define SIMPLE_ENET_HPP

#include "simple_client.hpp"
#include "enet/enet.h"
#include <set>

#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
using namespace rapidjson;

class SimpleEnet : public SimpleClient
{
public:
    SimpleEnet(const std::string& addr, int udpPort);
    ~SimpleEnet();
    virtual void runOnce();
    
    bool write(std::string& data, bool toFulsh = true);
    bool valid() const { return _enetClient != NULL; }
    bool isConnected() const { return _serverPeer != NULL; }

protected:
    virtual void handleMessageEnet(ENetEvent& event, const std::vector<char>& buf);
    virtual void handleMessageWS(std::string& msg);
    void reconnect(int confirmTimeout);
    void clear();

    ENetAddress _serverAddress;
    ENetHost* _enetClient;
    ENetPeer* _serverPeer;
};

#endif
