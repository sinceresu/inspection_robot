#ifndef BRIDGE_MESSAGE_HPP
#define BRIDGE_MESSAGE_HPP

#include <lz4/lz4.h>
#include <websocketpp/message_buffer/alloc.hpp>
#include <websocketpp/message_buffer/message.hpp>
#include <vector>
#include <iostream>
#include <sstream>
#include <log4z.h>

typedef websocketpp::message_buffer::message<websocketpp::message_buffer::alloc::con_msg_manager> MessageType;
typedef websocketpp::message_buffer::alloc::con_msg_manager<MessageType> MsgAllocType;

using namespace zsummer::log4z; 

class BridgeMessager
{
public:
    static std::string serializedData(MessageType::ptr& msg, bool withLZ = false)
    {
        const std::string& header = msg->get_header();
        const std::string& payload = msg->get_payload();
        short opcode = (short)msg->get_opcode();
        short flags = (header.empty() ? 0 : 0x1) | (msg->get_compressed() ? 0 : 0x4)
                    | (msg->get_fin() ? 0 : 0x8);
        int length = (int)payload.length();

        //std::cout << "[BridgeClientEnet] send message (" << length << ") to Server: " << payload.c_str() << std::endl;
        
        std::stringstream ss;
        ss << opcode << "|" << flags << "|" << payload;
        if (!header.empty()) ss << "|" << header;
        
        if (withLZ)
        {
            std::vector<unsigned char> compressedBuf(LZ4_COMPRESSBOUND(ss.str().length()));
            LZ4_resetStream_fast(compressor());
            const int compressedSize = LZ4_compress_fast_continue(
                compressor(), ss.str().data(), (char*)&compressedBuf[0],
                ss.str().length(), compressedBuf.size(), 1);
            return std::string((char*)&compressedBuf[0], compressedSize);
        }
        else return ss.str();
    }

    static std::string serialized(std::string& msg, bool withLZ = false)
    {
        std::stringstream ss;
        ss << msg;
        
        if (withLZ)
        {
            std::vector<unsigned char> compressedBuf(LZ4_COMPRESSBOUND(ss.str().length()));
            LZ4_resetStream_fast(compressor());
            const int compressedSize = LZ4_compress_fast_continue(
                compressor(), ss.str().data(), (char*)&compressedBuf[0],
                ss.str().length(), compressedBuf.size(), 1);
            return std::string((char*)&compressedBuf[0], compressedSize);
        }
        else return ss.str();
    }

    static MessageType::ptr deserializedData(const std::vector<char>& data, bool withLZ = false)
    {
        std::string buf;
        if (withLZ)
        {
            static std::vector<unsigned char> decodedBuf(1024 * 1024);
            const int decodedSize = LZ4_decompress_safe_continue(
                decompressor(), data.data(), (char*)&decodedBuf[0],
                data.size(), decodedBuf.size());
            buf = std::string((char*)&decodedBuf[0], decodedSize);
        }
        else buf = std::string(data.begin(), data.end());
        
        std::string token; int flags = 0;
        std::stringstream ss; ss << buf;

        MessageType::ptr msg = instance()->get_message();
        if (!std::getline(ss, token, '|')) return msg;
        msg->set_opcode((websocketpp::frame::opcode::value)atoi(token.c_str()));
        if (!std::getline(ss, token, '|')) return msg;
        flags = atoi(token.c_str());
        msg->set_compressed(flags & 0x4);
        //msg->set_fin(flags & 0x8);

        if (std::getline(ss, token, '|')) msg->set_payload(token);
        if (flags & 0x1) { std::getline(ss, token, '|'); msg->set_header(token); }
        return msg;
    }

    static std::string deserialized(const std::vector<char>& data, bool withLZ = false)
    {
        std::string buf;
        if (withLZ)
        {
            static std::vector<unsigned char> decodedBuf(1024 * 1024);
            const int decodedSize = LZ4_decompress_safe_continue(
                decompressor(), data.data(), (char*)&decodedBuf[0],
                data.size(), decodedBuf.size());
            if(decodedSize<0){
                return "";
            }
            buf = std::string((char*)&decodedBuf[0], decodedSize);
        }
        else buf = std::string(data.begin(), data.end());
        
       return buf;
    }

    static std::string deserialized(std::string& data, bool withLZ = false)
    {
        std::string buf;
        if (withLZ)
        {
            static std::vector<unsigned char> decodedBuf(1024 * 1024);
            const int decodedSize = LZ4_decompress_safe_continue(
                decompressor(), data.c_str(), (char*)&decodedBuf[0],
                data.size(), decodedBuf.size());
            if(decodedSize<0){
                return "";
            }
            buf = std::string((char*)&decodedBuf[0], decodedSize);
        }
        else buf = data;
        
       return buf;
    }

protected:
    static MsgAllocType::ptr instance()
    {
        static MsgAllocType::ptr _manager = websocketpp::lib::make_shared<MsgAllocType>();
        return _manager;
    }
    
    static LZ4_stream_t* compressor()
    {
        static LZ4_stream_t* _compressor = LZ4_createStream();
        return _compressor;
    }
    
    static LZ4_streamDecode_t* decompressor()
    {
        static LZ4_streamDecode_t* _decompressor = LZ4_createStreamDecode();
        return _decompressor;
    }
};

#endif
