#ifndef LYNMAX4D_COMMANDS_HPP
#define LYNMAX4D_COMMANDS_HPP

#include <map>
#include <set>
#include <vector>
struct IKCPCB;
namespace lynmax4d { class ArlinkThread; }

namespace lynmax4d
{
    struct Commands
    {
        static bool startBinding(ArlinkThread* arlink);
        static bool setBandMode(ArlinkThread* arlink, bool useBand5G, bool useAutoBand);
        static bool setHopping(ArlinkThread* arlink, bool videoHopping, bool rcHopping);
        static bool getDeviceInfo(ArlinkThread* arlink, std::map<std::string, int>& infoMap);
        static bool getDeviceState(ArlinkThread* arlink, std::map<std::string, int>& stateMap);

        static bool sendVideoData(ArlinkThread* arlink, const std::vector<uint8_t>& buffer);
        static bool sendUserData(ArlinkThread* arlink, const std::vector<uint8_t>& buffer, short msgID);
        static bool getUserData(ArlinkThread* arlink, std::vector<uint8_t>& buffer, short& msgID);
    };
}

#endif
