#include <iostream>
#include <sstream>
#include <memory.h>
#include "commands.h"
#include "arlink.h"
using namespace lynmax4d;

#define GET_MSGID(r) (short)(r[2] + (r[3] << 8))
#define GET_NUMPACKTS(r) (short)(r[4])
#define GET_PACKTID(r) (short)(r[5])
#define GET_NUMDATA(r) (short)(r[6] + (r[7] << 8))
#define GET_CRC(r) (short)(r[8] + (r[9] << 8))

static bool requestOnly(ArlinkThread* arlink, unsigned char msgID, const std::vector<uint8_t>& req)
{
    std::vector<uint8_t> command(req.size() + 10);
    command[0] = 0xFF; command[1] = 0x5A; command[2] = msgID; command[3] = 0; command[4] = 0x01; command[5] = 0;
    command[6] = (unsigned char)(req.size() & 0xFF); command[7] = (unsigned char)((req.size() >> 8) & 0xFF);

    short crc = 0;
    for (unsigned int i = 0; i < req.size(); ++i) crc += req[i];
    command[8] = (unsigned char)(crc & 0xFF); command[9] = (unsigned char)((crc >> 8) & 0xFF);

    if (!req.empty()) memcpy(&(command[10]), &(req[0]), req.size());
    return (arlink && arlink->sendRawCommand(&(command[0]), (int)command.size(), 0));
}

static bool getReplyWithNoPayload(std::vector<uint8_t>& reply, ArlinkThread* arlink, unsigned char msgID,
                                  short& replySize, short reqRetries = 10)
{
    unsigned char command[10] = { 0xFF, 0x5A, msgID, 0, 0x01, 0, 0, 0, 0, 0 };
    if (arlink && arlink->sendRawCommand(command, 10, reqRetries))
    {
        reply = arlink->getLastRawReply();
        if (reply.size() > 9 && reply[0] == 0xFF && reply[1] == 0x5A)
        {
            short msgID = GET_MSGID(reply), numData = GET_NUMDATA(reply);
            if (msgID == msgID && numData >= replySize) { replySize = numData; return true; }
        }
    }
    return false;
}

bool Commands::startBinding(ArlinkThread* arlink)
{
    std::vector<uint8_t> cmd(1); cmd[0] = 0x01;
    return requestOnly(arlink, 0x5B, cmd);
}

bool Commands::setBandMode(ArlinkThread* arlink, bool useBand5G, bool useAutoBand)
{
    std::vector<uint8_t> cmd0(1); cmd0[0] = useAutoBand ? 0 : 0x1;
    std::vector<uint8_t> cmd1(1); cmd1[0] = useBand5G ? 0x1 : 0;
    return (requestOnly(arlink, 0x51, cmd0) && requestOnly(arlink, 0x22, cmd1));
}

bool Commands::setHopping(ArlinkThread* arlink, bool videoHopping, bool rcHopping)
{
    std::vector<uint8_t> cmd0(1); cmd0[0] = videoHopping ? 0x1 : 0;
    std::vector<uint8_t> cmd1(1); cmd1[0] = rcHopping ? 0x1 : 0;
    return (requestOnly(arlink, 0x46, cmd0) && requestOnly(arlink, 0x28, cmd1));
}

bool Commands::getDeviceInfo(ArlinkThread* arlink, std::map<std::string, int>& infoMap)
{
    std::vector<uint8_t> reply; short size = 42;
    if (getReplyWithNoPayload(reply, arlink, 0x19, size))
    {
        infoMap["skyGround"] = reply[10];      // 0 = sky, 1 = ground
        infoMap["band"] = reply[11];           // 1 = 2.4G, 2 = 5.8G
        infoMap["bandWidth"] = reply[12];      // 0 = 20M, 2 = 10M
        infoMap["itHopMode"] = reply[13];      // 0 = Video hopping, 1 = fixed
        infoMap["rcHopping"] = reply[14];      // 0 = RC hopping, 1 = fixed
        infoMap["QAM"] = reply[15];            // 0 = Enable adapter bitrate, 1 = Disable
        infoMap["channel"] = reply[16];        // 0 = On, 1 = Off
        infoMap["channel2"] = reply[17];       // 0 = On, 1 = Off
        infoMap["isDebug"] = reply[18];        // 0 = Normal mode, 1 = Debug mode
        infoMap["itQAM"] = reply[19];          // 0 = BPSK, 1 = QPSK, 2 = 16QAM, 3 = 64QAM
        infoMap["itCodeRate"] = reply[20];     // 0 = 1/2, 1 = 2/3, 2 = 3/4, 3 = 5/6
        infoMap["rcQAM"] = reply[21];          // 0 = BPSK, 1 = QPSK
        infoMap["rcCodeRate"] = reply[22];     // 0 = 1/2, 1 = 2/3
        infoMap["ch1Bitrates"] = reply[23];    // Channel1 bitrate
        infoMap["ch2Bitrates"] = reply[24];    // Channel2 bitrate
        infoMap["u8_itRegs0"] = reply[26];     // Video registry0
        infoMap["u8_itRegs1"] = reply[27];     // Video registry1
        infoMap["u8_itRegs2"] = reply[28];     // Video registry2
        infoMap["u8_itRegs3"] = reply[29];     // Video registry3
        infoMap["u8_rcRegs0"] = reply[30];     // RC registry0
        infoMap["u8_rcRegs1"] = reply[31];     // RC registry1
        infoMap["u8_rcRegs2"] = reply[32];     // RC registry2
        infoMap["u8_rcRegs3"] = reply[33];     // RC registry3
        infoMap["rvc"] = reply[34] & 0x3F;
        infoMap["switch_mode_2g_5g"] = (reply[34] >> 6) & 0x1;  // 0 = Auto, 1 = Disabled
        infoMap["pure_vt_valid"] = reply[34] >> 7;
        infoMap["rcid0"] = reply[35];          // RC ID 0-5
        infoMap["rcid1"] = reply[36]; infoMap["rcid2"] = reply[37];
        infoMap["rcid3"] = reply[38]; infoMap["rcid4"] = reply[39];
        infoMap["chipid0"] = reply[40];        // Flash ID 0-5
        infoMap["chipid1"] = reply[41]; infoMap["chipid2"] = reply[42];
        infoMap["chipid3"] = reply[43]; infoMap["chipid4"] = reply[44];
        infoMap["vtid0"] = reply[45];          // Video ID 0-1
        infoMap["vtid1"] = reply[46];
        infoMap["u8_startwrite"] = reply[50];  // CPU2 updating startd
        infoMap["u8_endwrite"] = reply[51];    // CPU2 updating ended
        return true;
    }
    return false;
}

bool Commands::getDeviceState(ArlinkThread* arlink, std::map<std::string, int>& stateMap)
{
    std::vector<uint8_t> reply; short size = 11;
    if (getReplyWithNoPayload(reply, arlink, 0x82, size))
    {
        stateMap["valid"] = reply[10];          // 0 = valid, 1 = invalid
        stateMap["linkState"] = reply[11];      // 0 = idle, 1 = connected, 2 = frequenting, 3 = unmatched
        stateMap["videoState"] = reply[12];     // 0 = no video, 1 = received video, 2 = too many video
        stateMap["groundQuality"] = reply[13];  // 0 - 100
        stateMap["skyQuality"] = reply[14];     // 0 - 100
        stateMap["groundEnergyA"] = reply[15];  // 0 - 255
        stateMap["groundEnergyB"] = reply[16];  // 0 - 255
        stateMap["skyEnergyA"] = reply[17];     // 0 - 255
        stateMap["skyEnergyB"] = reply[18];     // 0 - 255
        stateMap["skyError"] = reply[19];       // 0 - 100
        stateMap["groundError"] = reply[20];    // 0 - 100
        return true;
    }
    return false;
}

struct UserDataCache
{
    std::vector<uint8_t> buffer;
};
static UserDataCache _userCache;

bool Commands::sendVideoData(ArlinkThread* arlink, const std::vector<uint8_t>& buffer)
{
    std::vector<uint8_t> rawBuf(8);
    rawBuf[0] = 0x12; rawBuf[1] = 0x34; rawBuf[2] = 0x56;
    rawBuf[3] = 0x00; rawBuf[4] = 0x00; rawBuf[5] = 0x00;
    
    int realSize = (int)buffer.size();
    rawBuf[6] = (realSize >> 8) & 0xFF; rawBuf[7] = realSize & 0xFF;
    rawBuf.insert(rawBuf.end(), buffer.begin(), buffer.end());
    arlink->sendRawVideoData(&rawBuf[0], (int)rawBuf.size());
}

bool Commands::sendUserData(ArlinkThread* arlink, const std::vector<uint8_t>& buffer, short msgID)
{
    std::vector<uint8_t> realBuffer = buffer;
    realBuffer.push_back((uint8_t)(msgID & 0xFF));
    realBuffer.push_back((uint8_t)(msgID >> 8));  // custom message ID
    realBuffer.push_back(0xA5);
    realBuffer.push_back(0xFF);  // custom tail
    return requestOnly(arlink, 0x84, realBuffer);
}

bool Commands::getUserData(ArlinkThread* arlink, std::vector<uint8_t>& buffer, short& msgID)
{
    std::vector<uint8_t> reply; short size = 0, retries = 1;
    if (getReplyWithNoPayload(reply, arlink, 0x85, size, retries))
    {
        std::vector<uint8_t>::iterator payloadBegin = reply.begin() + 10;
        _userCache.buffer.insert(_userCache.buffer.end(), payloadBegin, payloadBegin + size);

        short rcvMsgID = -1;
        for (int i = (int)_userCache.buffer.size() - 2; i >= 0; --i)
        {
            if (_userCache.buffer[i + 1] == 0xFF && _userCache.buffer[i] == 0xA5 && i > 1)
            {
                std::vector<uint8_t>::iterator bufBegin = _userCache.buffer.begin();
                rcvMsgID = ((short)_userCache.buffer[i - 1] << 8) + (short)_userCache.buffer[i - 2];
                buffer.assign(bufBegin, bufBegin + (i - 2));
                _userCache.buffer.erase(bufBegin, bufBegin + (i + 2));
                break;
            }
        }

        if (_userCache.buffer.size() > 512) _userCache.buffer.clear();  // not allowed by ARlink
        if (rcvMsgID <= msgID && !(msgID == 32767 && rcvMsgID == 0)) return false;
        msgID = rcvMsgID; return !buffer.empty();
    }
    return false;
}
