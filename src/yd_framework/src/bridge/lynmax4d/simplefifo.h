#ifndef LYNMAX4D_SIMPLEFIFO_HPP
#define LYNMAX4D_SIMPLEFIFO_HPP

#include <thread>
#include <mutex>
#include <memory.h>
#define FIFO_MAX_LENGTH 204800

#define LYNMAX_APP_NAME "[Lynmax4D]"
#define LV_DEBUG 0
#define LV_WARNING 1
#define LV_FATAL 2
void printUserLog(int level, const char* format, ...);

namespace lynmax4d
{
    class SimpleFifo
    {
    public:
        SimpleFifo() { _subFifo = NULL; initialize(); }
        virtual ~SimpleFifo() {}

        SimpleFifo* createSubFifo() { _subFifo = new SimpleFifo; return _subFifo; }
        void destroySubFifo(SimpleFifo* f) { if (_subFifo == f) {delete _subFifo; _subFifo = NULL;} }

        virtual void initialize(bool shouldLock = false)
        {
            if (shouldLock)
            {
                std::unique_lock<std::mutex> lock(_mutex);
                _ptrWrite = 0; _ptrRead = 0; _currentLength = 0;
                _isFull = false; _isEmpty = true;
            }
            else
            {
                _ptrWrite = 0; _ptrRead = 0; _currentLength = 0;
                _isFull = false; _isEmpty = true;
            }
        }

        virtual int write(unsigned char* buf, int length)
        {
            int realLength = 0;
            std::unique_lock<std::mutex> lock(_mutex);
            if (length < 1 || _isFull) return 0;

            if ((_currentLength + length) < FIFO_MAX_LENGTH)
            { _isFull = false; _isEmpty = false; realLength = length; }
            else
            { _isFull = true; _isEmpty = false; realLength = FIFO_MAX_LENGTH - _currentLength - 1; }

            if ((_ptrWrite + realLength) > FIFO_MAX_LENGTH)
            {
                int length1 = FIFO_MAX_LENGTH - _ptrWrite;
                int length2 = realLength - length1;
                memcpy(_ram + _ptrWrite, buf, length1);
                memcpy(_ram, &buf[length1], length2);
                _ptrWrite = length2;
            }
            else
            {
                memcpy(_ram + _ptrWrite, buf, realLength);
                _ptrWrite += realLength;
            }
            _currentLength += realLength;
            if (_subFifo) _subFifo->write(buf, realLength);
            return realLength;
        }

        virtual int read(unsigned char* buf, int length)
        {
            int realLength = 0;
            std::unique_lock<std::mutex> lock(_mutex);
            if (length < 1 || _isEmpty) return 0;

            if (length < _currentLength)
            { _isFull = false; _isEmpty = false; realLength = length; }
            else
            { _isFull = false; _isEmpty = true; realLength = _currentLength; }

            if ((_ptrRead + realLength) > FIFO_MAX_LENGTH)
            {
                int length1 = FIFO_MAX_LENGTH - _ptrRead;
                int length2 = realLength - length1;
                memcpy(buf, _ram + _ptrRead, length1);
                memcpy(buf + length1, _ram, length2);
                _ptrRead = length2;
            }
            else
            {
                memcpy(buf, _ram + _ptrRead, realLength);
                _ptrRead += realLength;
            }
            _currentLength -= realLength;
            return realLength;
        }

        bool full() const { return _isFull; }
        bool empty() const { return _isEmpty; }
        int getLength() const { return _currentLength; }

        static bool findNextNAL(unsigned char* buf, int size, int& start, int& end);

    protected:
        unsigned char _ram[FIFO_MAX_LENGTH * 2];
        int _ptrWrite, _ptrRead;
        int _currentLength;
        bool _isFull, _isEmpty;

        SimpleFifo* _subFifo;
        std::mutex _mutex;
    };
}

#endif
