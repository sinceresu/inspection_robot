#ifndef LYNMAX4D_ARLINK_HPP
#define LYNMAX4D_ARLINK_HPP

#include <map>
#include <set>
#include <vector>
#include "threadbase.h"
#define ARLINK_VID 0xAAAA
#define ARLINK_PID 0xAA97

struct libusb_context;
struct libusb_device_handle;
struct libusb_device;
struct libusb_transfer;
namespace lynmax4d { class SimpleFifo; }

namespace lynmax4d
{
    class ArlinkThread : public ThreadBase
    {
    public:
        ArlinkThread(SimpleFifo* fifoV, SimpleFifo* fifoA = NULL, SimpleFifo* fifoU = NULL);
        virtual ~ArlinkThread() {}

        virtual bool initialize();
        virtual void destroy();
        virtual void execute(unsigned long& microsecondsToSleep);

        bool prepare(const char* devPath);
        bool open(int descriptor);
        bool open(int vid, int pid);
        void close();

        bool sendRawVideoData(uint8_t* buffer, int size);
        bool sendRawCommand(uint8_t* cmd, int size, int reqNum);
        const std::vector<uint8_t>& getLastRawReply();

    protected:
        bool openDevice();
        bool receiveControlData(char* buf, int size);
        static void stateActivateCallback(struct libusb_transfer* transfer);

        int hidRead(libusb_device_handle* dev, int ep, char* bytes, int size, int timeout);
        int hidWrite(libusb_device_handle* dev, int ep, char* bytes, int size, int timeout);
        int hidReadWrite(libusb_device_handle* dev, int ep, char* bytes, int size, int timeout);

        enum TransferType { TYPE_CONTROL = 0, TYPE_ISOCHRONOUS, TYPE_BULK, TYPE_INTERRUPT, TYPE_OTHER };
        std::map<int, TransferType> _endpoints;  // BULK = In/Out, INTERRUPT = In (host2device)
        int _epControlIn, _epControlOut, _epVideoIn, _epVideoOut, _epAudioIn, _epAudioOut;
        int _epUserIn, _epUserOut, _replyRequired;

        struct InterfaceData
        {
            int interfaceNumber, devClass;
            std::map<TransferType, int> endpoints;
        };
        std::map<std::string, InterfaceData> _interfaces;

        SimpleFifo *_videoFifo, *_audioFifo, *_userFifo;
        libusb_context* _context;
        libusb_device_handle* _handle;
        libusb_device* _device;
        std::vector<uint8_t> _replyData, _storedReplyData;
        std::mutex _commonMutex;
    };
}

#endif
