#include "bridge_server.hpp"
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;

BridgeServer::BridgeServer() : _isProcessing(false)
{
    _server.set_access_channels(websocketpp::log::alevel::all);
    //_server.clear_access_channels(websocketpp::log::alevel::frame_payload);
    _server.clear_access_channels(websocketpp::log::alevel::all);
    _server.clear_error_channels(websocketpp::log::alevel::all);
    _server.init_asio();
    _server.set_open_handler(websocketpp::lib::bind(&BridgeServer::onOpen, this, ::_1));
    _server.set_close_handler(websocketpp::lib::bind(&BridgeServer::onClose, this, ::_1));
    _server.set_message_handler(websocketpp::lib::bind(&BridgeServer::onMessage, this, ::_1, ::_2));
}

void BridgeServer::process()
{
    _isProcessing = true;
    while (_isProcessing)
    {
        websocketpp::lib::unique_lock<websocketpp::lib::mutex> lock(_actionLock);
        while (_actions.empty() && _isProcessing) _actionCondition.wait(lock);

        std::vector<ActionData> localActions;
        localActions.swap(_actions);
        lock.unlock();

        websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_connectionLock);
        for (std::vector<ActionData>::iterator itr = localActions.begin();
                itr != localActions.end(); ++itr)
        {
            ActionData& action = *itr;
            switch (action.type)
            {
            case ActionData::SUBSCRIBE: _connections.insert(action.hdl); break;
            case ActionData::UNSUBSCRIBE: _connections.erase(action.hdl); break;
            case ActionData::MESSAGE: handleMessageWS(action.hdl, action.msg); break;
            default: break;
            }
        }
    }
}

void BridgeServer::start(uint16_t port)
{
    _server.listen(port);
    _server.start_accept();
}

void BridgeServer::stop()
{
    _isProcessing = false;
    _actionCondition.notify_all();
    _server.stop_listening();
}

void BridgeServer::runOnce()
{
    _server.poll_one();
}

void BridgeServer::onOpen(ConnectionType hdl)
{
    {
        websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_actionLock);
        _actions.push_back(ActionData(ActionData::SUBSCRIBE, hdl));
    }
    _actionCondition.notify_one();
}

void BridgeServer::onClose(ConnectionType hdl)
{
    {
        websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_actionLock);
        _actions.push_back(ActionData(ActionData::UNSUBSCRIBE, hdl));
    }
    _actionCondition.notify_one();
}

void BridgeServer::onMessage(ConnectionType hdl, ServerType::message_ptr msg)
{
    // Queue message up for sending by processing thread
    {
        websocketpp::lib::lock_guard<websocketpp::lib::mutex> guard(_actionLock);
        _actions.push_back(ActionData(ActionData::MESSAGE, hdl, msg));
    }
    _actionCondition.notify_one();
}

#if 0
int main(int argc, char** argv)
{
    BridgeServer server;
    try
    {
        std::thread thread(websocketpp::lib::bind(&BridgeServer::process, &server));
        server.start(9090);
        while (running)
        {
            server.runOnce();
            sleep(20);
        }
        server.stop();
        thread.join();
    }
    catch (websocketpp::exception const & e)
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "other exception" << std::endl;
    }
    return 0;
}
#endif
