#ifndef BRIDGE_CLIENT_HPP
#define BRIDGE_CLIENT_HPP

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>
#include <iostream>

typedef websocketpp::client<websocketpp::config::asio_client> ClientType;
typedef websocketpp::connection_hdl ConnectionType;

class BridgeClient
{
public:
    BridgeClient();
    void process();

    virtual void start(const std::string& addr, uint16_t port);
    virtual void stop();
    virtual void runOnce();

protected:
    void onOpen(ConnectionType hdl);
    void onClose(ConnectionType hdl);
    void onMessage(ConnectionType hdl, ClientType::message_ptr msg);
    virtual void handleMessageWS(ConnectionType& hdl, ClientType::message_ptr& msg) {}

    ClientType _client;
    ConnectionType _connection;
    std::vector<ClientType::message_ptr> _messages;

    websocketpp::lib::mutex _actionLock;
    websocketpp::lib::condition_variable _actionCondition;
    bool _isProcessing;
};

#endif
