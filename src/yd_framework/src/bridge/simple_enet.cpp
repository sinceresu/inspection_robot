#include <simple_client.hpp>
#include <bridge_message.hpp>
#include <simple_enet.hpp>

SimpleEnet::SimpleEnet(const std::string &addr, int udpPort) : SimpleClient(), _enetClient(NULL), _serverPeer(NULL)
{
    if (enet_initialize() == 0)
    {
        // Supports 1 channel only
        _enetClient = enet_host_create(NULL, 1, 1, 0, 0);
        if (_enetClient)
        {
            enet_address_set_host(&_serverAddress, addr.c_str());
            _serverAddress.port = udpPort;
            reconnect(1000);
        }
        else
            enet_deinitialize();
    }

    if (!_enetClient)
        LOGE("[SimpleEnet] Failed to initialize");
}

SimpleEnet::~SimpleEnet()
{
    if (_serverPeer)
        enet_peer_disconnect(_serverPeer, 0);
    enet_deinitialize();
#if OUTPUT_MESSAGE
    LOGI("stream input *** "
         << "SimpleEnet start"
         << " *** ");
#endif
}

void SimpleEnet::runOnce()
{
    if (!_serverPeer)
    {
        reconnect(1000);
        if (!_serverPeer)
            return;
    }
    if (_serverPeer && _enetClient){
        std::vector<std::string> localSendingList;
        {
            _sendingMutex.lock();
            localSendingList.swap(_sendingList);
            _sendingMutex.unlock();
        }
        for (unsigned int i = 0; i < localSendingList.size(); ++i)
        {
            bool isSuccess = write(localSendingList[i], false);
            if(!isSuccess) LOGE("write error,msg "<<localSendingList[i]);
        }
        enet_host_flush(_enetClient);
    }

    ENetEvent event;
    while (enet_host_service(_enetClient, &event, 10) > 0)
    {
        switch (event.type)
        {
        case ENET_EVENT_TYPE_RECEIVE:
        {
            char *ptr = (char *)event.packet->data;
            std::vector<char> buffer(ptr, ptr + event.packet->dataLength);
            handleMessageEnet(event, buffer);
            enet_packet_destroy(event.packet);
        }
        break;
        case ENET_EVENT_TYPE_DISCONNECT:
            LOGI("[SimpleEnet] Lost server");
            _serverPeer = NULL;
            break;
        default:
            break;
        }
    }
}

void SimpleEnet::reconnect(int confirmTimeout)
{
    //waitting for rosbridge server
    if (!_clientStatus){
        LOGI("ws connect failed");
        return;
    }
    if (_enetClient == NULL)
    {
        LOGI("enet connect failed");
        return;
    }
    if (_serverPeer != NULL)
        enet_peer_disconnect(_serverPeer, 0);

    // Connect with 1 channel
    _serverPeer = enet_host_connect(_enetClient, &_serverAddress, 1, 0);
    if (_serverPeer)
    {
        ENetEvent event;
        if (enet_host_service(_enetClient, &event, confirmTimeout) > 0 && event.type == ENET_EVENT_TYPE_CONNECT)
        {
#if OUTPUT_MESSAGE
            LOGI("[SimpleEnet] Connected to server");
#endif
            clear();
        }
        else
        {
#if OUTPUT_MESSAGE
            LOGI("[SimpleEnet] Failed to connect server");
#endif
            enet_peer_reset(_serverPeer);
            _serverPeer = NULL;
        }
    }
#if OUTPUT_MESSAGE
    else
        LOGI("[SimpleEnet] Failed to connect to server");
#endif
}

bool SimpleEnet::write(std::string &data, bool toFulsh)
{
    if (_serverPeer && _enetClient)
    {
        Document document;
        ParseResult result = document.Parse(data.c_str());
        if (result.IsError())
        {
            LOGE("JSON parse error:" << result.Code() << " offset:" << result.Offset() << ",data:"<< data);
            return false;
        }
        if (!document.IsObject())
        {
            LOGE("message is not object,data:" << data);
            return false;
        }
        bool isReliable = false;
        Value::ConstMemberIterator itr = document.FindMember("op");
        if (itr != document.MemberEnd()){
            if (strcmp(itr->value.GetString(), "publish") == 0)
            {
                std::string topic = document["topic"].GetString();
                if(strcmp(topic.c_str(),"/detect_result")==0){
                    LOGD("write message to enet " << data);
                }
                isReliable = false;
            }else{
                isReliable = true;
            }
        }
        std::string msg = BridgeMessager::serialized(data, true);
        ENetPacket *packet = enet_packet_create(msg.c_str(), msg.size(), isReliable ? ENET_PACKET_FLAG_RELIABLE:ENET_PACKET_FLAG_UNSEQUENCED);
        int code = enet_peer_send(_serverPeer, 0, packet);
        if(code < 0){
            LOGD("send failed,data" << data);
        }
        if (toFulsh)
            enet_host_flush(_enetClient);
        return true;
    }
    return false;
}

void SimpleEnet::handleMessageEnet(ENetEvent &event, const std::vector<char> &buf)
{
    std::string msg = BridgeMessager::deserialized(buf, true);
    if (_client == nullptr)
    {
        std::cout << "[SimpleEnet] client is null ptr" << std::endl;
    }
#if OUTPUT_MESSAGE
    LOGD("send message to ros:" << msg);
#endif
    send(msg);
}

void SimpleEnet::handleMessageWS(std::string &msg)
{
    //std::string data = BridgeMessager::serialized(msg, true);
    _sendingMutex.lock();
    _sendingList.push_back(msg);
#if OUTPUT_MESSAGE
    //LOGD("get message:"<< msg);
    LOGD("message size:" << _sendingList.size() << " enet status:"<< (_serverPeer && _enetClient));
#endif
    _sendingMutex.unlock();
}

void SimpleEnet::clear(){
    _sendingMutex.lock();
    _sendingList.clear();
    #if OUTPUT_MESSAGE
        LOGD("clear message size:" << _sendingList.size());
    #endif
    _sendingMutex.unlock();
}