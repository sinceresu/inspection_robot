#define EXPERIMENTAL_ENET_BRIDGE 1

#include <bridge_client.hpp>
#include <simple_client.hpp>
#if EXPERIMENTAL_ENET_BRIDGE
#include <simple_uv.hpp>
#endif

#include <yd_framework/res_center.hpp>
#include <fstream>
#include <iostream>

///////// Backward tracing begin /////////
#define BACKWARD_HAS_DW 1
#include <backward.hpp>
backward::SignalHandling sh;
static void trace()
{
    backward::StackTrace st;
    st.load_here(32);
    backward::Printer p;
    p.print(st);
}
///////// Backward tracing end /////////

void ResourceCenter::stringCallback(const std_msgs::StringConstPtr &ptr, const std::string &m)
{
}

void ResourceCenter::diagnosticCallback(const diagnostic_msgs::DiagnosticArrayConstPtr &ptr,
                                        const std::string &m)
{
}

void ResourceCenter::odometryCallback(const nav_msgs::OdometryConstPtr &ptr, const std::string &m)
{
    ResMessage message;
    if (!ros::ok())
        return;
    if (!updateMessage(ptr->header, m, message))
        return;
}

void ResourceCenter::imageCallback(const sensor_msgs::ImageConstPtr &ptr, const std::string &m)
{
}

void ResourceCenter::imuCallback(const sensor_msgs::ImuConstPtr &ptr, const std::string &m)
{
}

void ResourceCenter::navSatFixCallback(const sensor_msgs::NavSatFixConstPtr &ptr,
                                       const std::string &m)
{
}

void ResourceCenter::laserScanCallback(const sensor_msgs::LaserScanConstPtr &ptr,
                                       const std::string &m)
{
}

void ResourceCenter::pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr &ptr,
                                        const std::string &m)
{
    ResMessage message;
    if (!ros::ok())
        return;
    if (!updateMessage(ptr->header, m, message))
        return;
}

void ResourceCenter::transformCallback(const geometry_msgs::TransformStampedConstPtr &ptr,
                                       const std::string &m)
{
}

bool ResourceCenter::settingsCallback(yd_framework::SettingsRequest &req,
                                      yd_framework::SettingsResponse &response)
{
    std::string value = req.setValue, key = req.key;
    if (key.empty())
        return false;

    _settingsMutex.lock();
    if (value.empty())
        value = _settingsMap[key];
    else
    {
        _settingsMap[key] = value;
        value = "ok";
    }
    _settingsMutex.unlock();

    if (value == "ok")
    {
        diagnostic_msgs::KeyValue kv;
        kv.key = req.key;
        kv.value = req.setValue;
        _settingsChanged.publish(kv);
    }
    response.getValue = value;
    return true;
}

bool ResourceCenter::updateMessage(const std_msgs::Header &header,
                                   const std::string &m, ResMessage &msg)
{
    std::map<std::string, ResMessage>::iterator itr = _messages.find(m);
    if (itr == _messages.end())
    {
        ROS_WARN("Unregistered message %s", m.c_str());
        return false;
    }

    itr->second.lastStamp = header.stamp.toSec();
    msg = itr->second;
    return true;
}

bool ResourceCenter::loadResource(const std::string &jsonFile)
{
    std::ifstream in(jsonFile.c_str());
    if (!in)
    {
        ROS_WARN("Failed to open %s", jsonFile.c_str());
        return false;
    }

    rapidjson::IStreamWrapper isw(in);
    rapidjson::EncodedInputStream<rapidjson::UTF8<>, rapidjson::IStreamWrapper> eis(isw);
    rapidjson::Document doc;
    if (doc.ParseStream(eis).HasParseError())
    {
        ROS_WARN("Failed to parse json in %s:", jsonFile.c_str());
        return false;
    }

    if (!doc.HasMember("resource"))
    {
        ROS_WARN("No 'resource' field in %s:", jsonFile.c_str());
        return false;
    }

    const rapidjson::Value &resource = doc["resource"];
    if (!resource.IsObject())
    {
        ROS_WARN("Invalid 'resource' field in %s:", jsonFile.c_str());
        return false;
    }

    _messages.clear();
    for (rapidjson::Value::ConstMemberIterator itr = resource.MemberBegin();
         itr != resource.MemberEnd(); ++itr)
    {
        ResMessage msgData;
        msgData.message = itr->name.GetString();
        msgData.lastStamp = -1.0;
        msgData.timeout = -1.0;

        const rapidjson::Value &msg = itr->value;
        if (!msg.IsObject())
        {
            ROS_WARN("Invalid message field: %s:", msgData.message.c_str());
            continue;
        }

        int queueSize = 128;
        for (rapidjson::Value::ConstMemberIterator itr2 = msg.MemberBegin();
             itr2 != msg.MemberEnd(); ++itr2)
        {
            std::string key = itr2->name.GetString();
            if (key == "type")
            {
                if (!itr2->value.IsString())
                    continue;
                else
                    msgData.type = itr2->value.GetString();
            }
            else if (key == "timeout")
            {
                if (!itr2->value.IsFloat())
                    continue;
                else
                    msgData.timeout = itr2->value.GetFloat();
            }
            else if (key == "queue_size")
            {
                if (!itr2->value.IsInt())
                    continue;
                else
                    queueSize = itr2->value.GetInt();
            }
            else
            {
            }
        }

        if (msgData.type == "std_msgs/String")
        {
            msgData.subscriber = _node.subscribe<std_msgs::String>(msgData.message,
                                                                   queueSize, boost::bind(&ResourceCenter::stringCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "diagnostic_msgs/DiagnosticArray")
        {
            msgData.subscriber = _node.subscribe<diagnostic_msgs::DiagnosticArray>(msgData.message,
                                                                                   queueSize, boost::bind(&ResourceCenter::diagnosticCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "nav_msgs/Odometry")
        {
            msgData.subscriber = _node.subscribe<nav_msgs::Odometry>(msgData.message,
                                                                     queueSize, boost::bind(&ResourceCenter::odometryCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "sensor_msgs/Image")
        {
            msgData.subscriber = _node.subscribe<sensor_msgs::Image>(msgData.message,
                                                                     queueSize, boost::bind(&ResourceCenter::imageCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "sensor_msgs/Imu")
        {
            msgData.subscriber = _node.subscribe<sensor_msgs::Imu>(msgData.message,
                                                                   queueSize, boost::bind(&ResourceCenter::imuCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "sensor_msgs/NavSatFix")
        {
            msgData.subscriber = _node.subscribe<sensor_msgs::NavSatFix>(msgData.message,
                                                                         queueSize, boost::bind(&ResourceCenter::navSatFixCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "sensor_msgs/LaserScan")
        {
            msgData.subscriber = _node.subscribe<sensor_msgs::LaserScan>(msgData.message,
                                                                         queueSize, boost::bind(&ResourceCenter::laserScanCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "sensor_msgs/PointCloud2")
        {
            msgData.subscriber = _node.subscribe<sensor_msgs::PointCloud2>(msgData.message,
                                                                           queueSize, boost::bind(&ResourceCenter::pointCloudCallback, this, _1, msgData.message));
        }
        else if (msgData.type == "geometry_msgs/TransformStamped")
        {
            msgData.subscriber = _node.subscribe<geometry_msgs::TransformStamped>(msgData.message,
                                                                                  queueSize, boost::bind(&ResourceCenter::transformCallback, this, _1, msgData.message));
        }
        else
        {
            ROS_WARN("Invalid type %s of resource message %s:",
                     msgData.type.c_str(), msgData.message.c_str());
            continue;
        }

        ROS_INFO("Subscribed message %s (%s):", msgData.message.c_str(), msgData.type.c_str());
        _messages[msgData.message] = msgData;
    }

    in.close();
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "resource_center");
    ResourceCenter resCenter;
    //resCenter.loadResource();

    // Initialize bridge to connect ros-bridge-server and remote
    SimpleUV *bridge = NULL;
    std::thread *bridgeThread = NULL;
    std::thread *clientThread = NULL;
    try
    {
#if EXPERIMENTAL_ENET_BRIDGE
        std::cout << "[_remote_ip]: " << resCenter.remoteIP() << std::endl;
        bridge = new SimpleUV(resCenter.remoteIP(), 12001);
#endif
        if (bridge)
        {
            bridge->start("localhost", 9090);
            bridgeThread = new std::thread(std::bind(&SimpleClient::process, bridge));
            clientThread = new std::thread(std::bind(&SimpleClient::poll, bridge));
        }
    }
    catch (websocketpp::exception const &e)
    {
        std::cout << "[BridgeClient] Starting error: " << e.what() << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "[BridgeClient] Starting error:" << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "[BridgeClient] Starting error..." << std::endl;
    }
    //time tick
    ros::Timer timer = resCenter._node.createTimer(ros::Duration(1.0), [&](const ros::TimerEvent &event){
        //ROS_INFO("res center heart");
        ros::Time current = ros::Time::now();
        diagnostic_msgs::DiagnosticArray log;
        log.header.stamp = current;
        {
            diagnostic_msgs::DiagnosticStatus s;
            s.name = "/resource_center";
            s.hardware_id = "1";
            diagnostic_msgs::KeyValue dv;
            if(!bridge->_clientStatus){
                s.level = 2;
                s.message = "local<ws> connection lost";
                dv.key = "error_code";
                dv.value = "10301";
            }else if(!bridge->_serverStatus){
                s.level = 2;
                s.message = "remote<ws> connection lost";
                dv.key = "error_code";
                dv.value = "10302";
            }else{
                s.level = 0;
                s.message = "ok";
                dv.key = "error_code";
                dv.value = "10300";
            }
            s.values.push_back(dv);
            log.status.push_back(s);
        }
        resCenter._heartbeat.publish(log);
    }, false);
    // Start the main loop
    ros::Rate rate(100);
    while (ros::ok())
    {
        ros::spinOnce();

        // Update bridge transferring
        try
        {
            if (bridge)
                bridge->runOnce();
        }
        catch (websocketpp::exception const &e)
        {
            std::cout << "[BridgeClient] Updating error: " << e.what() << std::endl;
        }
        catch (std::exception &e)
        {
            std::cout << "[BridgeClient] Updating error:" << e.what() << std::endl;
        }
        catch (...)
        {
            std::cout << "[BridgeClient] Updating error..." << std::endl;
        }
        rate.sleep();
    }

    if (bridge)
        bridge->stop();
    if (bridgeThread)
        bridgeThread->join();
    if (clientThread)
        clientThread->join();
    delete bridgeThread;
    delete clientThread;
    delete bridge;
    return 0;
}
