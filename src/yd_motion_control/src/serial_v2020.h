﻿/*
 * @Author: your name
 * @Date: 2019-12-10 17:29:06
 * @LastEditTime: 2020-07-13 10:15:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /motor_shenzhen/src/serialcomm_lp_float.h
 */
#ifndef __SERIAL_V2020_H__
#define __SERIAL_V2020_H__
#include "serialcomm.h"

class Serial_v2020 : public SerialComm
{
public:
	Serial_v2020(int _robot_id);
	~Serial_v2020();

public:
	void *comThreadIml();
	void sendSpeedToMcu(char smooth_type, float V, float W);
	void sendSpeedToWCXD(char smooth_type, float V, float W);
	void sendSpeedToDrive(float V, float W);
	void sendLifts(float h);
	void sendFan(bool status);
	void sendTest();
	void sendChargeStatus(int status);
	void sendMotorPower(int status);
	void sendMotorEnable(int status);
};

#endif //__SERIAL_V2020_H__
