/*
 * @Author:lp
 * @Date: 2019-11-19 17:13:01
 * @LastEditTime: 2020-07-10 16:08:57
 * @LastEditors: Please set LastEditors
 */

#include "utilssport.h"

float pidLoc(float SetValue, float ActualValue, PID_LocTypeDef *PID)
{
	float PIDLoc;
	PID->Ek = SetValue - ActualValue;
	PID->LocSum += PID->Ek;
	PIDLoc = PID->Kp * PID->Ek + (PID->Ki * PID->LocSum) + PID->Kd * (PID->Ek1 - PID->Ek);
	PID->Ek1 = PID->Ek;
	return PIDLoc;
}

float pidInc(float SetValue, float ActualValue, PID_IncTypeDef *PID)
{
	float PIDInc;
	PID->Ek = SetValue - ActualValue;
	PIDInc = (PID->Kp * PID->Ek) - (PID->Ki * PID->Ek1) + (PID->Kd * PID->Ek2);
	PID->Ek2 = PID->Ek1;
	PID->Ek1 = PID->Ek;
	return PIDInc;
}

bool isSamePoint(float a_pos_x, float a_pos_z, float b_pos_x, float b_pos_z)
{
	if (a_pos_x == b_pos_x && a_pos_z == b_pos_z)
	{
		return true;
	}
	return false;
}

float calcTurnAngleToLine(float a_pos_x, float a_pos_z, float b_pos_x, float b_pos_z, float anglez)
{
	float vector_road_x = b_pos_x - a_pos_x;
	float vector_road_z = b_pos_z - a_pos_z;
	float vector_position_x = 1;
	float vector_position_z = 0;
	float theao = atan2(vector_road_z, vector_road_x) - atan2(vector_position_z, vector_position_x);

	float road_xyz = theao * 180 / PII;
	float car_xyz = anglez * 180 / PII;
	float result = 0;
	if (car_xyz < 0)
	{
		car_xyz = car_xyz + 360;
	}
	if (road_xyz < 0)
	{
		road_xyz = road_xyz + 360;
	}
	result = car_xyz - road_xyz;
	if (result < -180)
	{
		result = result + 360;
	}
	if (result > 180)
	{
		result = result - 360;
	}
	return result;
}

float calcLineAngle(float a_pos_x, float a_pos_z, float b_pos_x, float b_pos_z)
{
	float vector_road_x = b_pos_x - a_pos_x;
	float vector_road_z = b_pos_z - a_pos_z;
	float vector_position_x = 1;
	float vector_position_z = 0;

	float theao = atan2(vector_road_z, vector_road_x) - atan2(vector_position_z, vector_position_x);
	return theao * 180 / PII;
}

float calcDestDistace(float beginX, float beginZ, float endX, float endZ)
{
	float fDistance = sqrt(fabs(pow(endX - beginX, 2) + pow(endZ - beginZ, 2)));
	return fDistance;
}

float calcToLineDistance(float thisPosX, float thisPosZ, float thisTaskX, float thisTaskZ, float lastTaskX, float lastTaskZ)
{

	float S;
	float x2 = thisTaskX;
	float y2 = thisTaskZ;
	float x1 = lastTaskX;
	float y1 = lastTaskZ;
	float x3 = thisPosX;
	float y3 = thisPosZ;
	if (lastTaskX == thisTaskX)
	{
		lastTaskX = lastTaskX + 0.01;
	}
	float k = (lastTaskZ - thisTaskZ) / (lastTaskX - thisTaskX);
	float b = lastTaskZ - k * lastTaskX;
	float dis = (thisPosZ - k * thisPosX - b) / sqrt(k * k + 1);
	S = (x2 - x3) * (y1 - y3) - (y2 - y3) * (x1 - x3);
	if (S > 0)
	{
		dis = fabs(dis);
	}
	else if (S < 0)
	{
		dis = -fabs(dis);
	}
	return dis;
}

char calcToLimitStatus(float thisPosX, float thisPosZ, float thisTaskX, float thisTaskZ, float lastTaskX, float lastTaskZ, int type, EActionStatus actionStatus, float &over_dis)
{
	char rect_status = 0x00;
	if (type = 2 && actionStatus == EActionStatus::Turn)
	{
		over_dis = calcDestDistace(thisPosX, thisPosZ, thisTaskX, thisTaskZ);
		if (over_dis > DIS_NEAR)
		{
			rect_status = rect_status | (1 << 7);
			return rect_status;
		}
	}
	if (lastTaskX == thisTaskX)
	{
		lastTaskX = lastTaskX + 0.01;
	}
	if (lastTaskZ == thisTaskZ)
	{
		lastTaskZ = lastTaskZ + 0.01;
	}
	float k = (lastTaskZ - thisTaskZ) / (lastTaskX - thisTaskX);
	float k_start, k_end, b_start, b_end;
	k_start = -1 / k;
	k_end = k_start;

	b_start = lastTaskZ - k_start * lastTaskX;
	b_end = thisTaskZ - k_end * thisTaskX;

	float x_end_offset = thisTaskX + 0.1;
	float y_end_offset = k_end * x_end_offset + b_end;
	float x_start_offset = lastTaskX + 0.1;
	float y_start_offset = k_start * x_start_offset + b_start;

	float end_side_dis = calcToLineDistance(x_end_offset, y_end_offset, thisTaskX, thisTaskZ, lastTaskX, lastTaskZ);
	float start_side_dis = calcToLineDistance(x_start_offset, y_start_offset, thisTaskX, thisTaskZ, lastTaskX, lastTaskZ);

	float end_dis = calcToLineDistance(thisPosX, thisPosZ, x_end_offset, y_end_offset, thisTaskX, thisTaskZ);
	// ROS_INFO("end_dis :%f",end_dis);
	if (end_side_dis > 0)
	{
		if (end_dis < -DIS_NEAR)
		{
			rect_status = rect_status | (1 << 0);
			over_dis = end_dis;
		}
	}
	else
	{
		if (end_dis > DIS_NEAR)
		{
			rect_status = rect_status | (1 << 1);
			over_dis = end_dis;
		}
	}
	float start_dis = calcToLineDistance(thisPosX, thisPosZ, x_start_offset, y_start_offset, lastTaskX, lastTaskZ);
	// ROS_INFO("start_dis :%f",start_dis);
	if (start_side_dis > 0)
	{
		if (start_dis > DIS_NEAR)
		{
			rect_status = rect_status | (1 << 2);
			over_dis = start_dis;
		}
	}
	else
	{
		if (start_dis < -DIS_NEAR)
		{
			rect_status = rect_status | (1 << 3);
			over_dis = start_dis;
		}
	}

	float side_to_side = calcToLineDistance(thisPosX, thisPosZ, thisTaskX, thisTaskZ, lastTaskX, lastTaskZ);
	if (fabs(side_to_side) > DIS_NEAR)
	{
		rect_status = rect_status | (1 << 4);
		over_dis = side_to_side;
	}
	return rect_status;
}

float distanceToVelocity(float len, float FStart, float FStop, float flexible, int index)
{
	float deno;
	float melo;
	float num;
	float Fcurrent;

	if (index > len)
	{
		index = len;
	}
	num = len / 2;
	melo = flexible * (index - num) / num;
	deno = 1.0 / (1 + expf(-melo));
	Fcurrent = FStart - (FStart - FStop) * deno;

	return Fcurrent;
}

char forwardGo(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float sensorDistance, float &currentLinearVelocity, float &angularVelocity)
{
	float tempLinearVelocity = 0.0;
	float tempAngularVelocity = 0.0;
	float angular_speed_ref = FIX_ANGULAR_SPEED_MIN;
	float divDistance = allDistance / 2;
	if (distanceToNextPoint >= allDistance)
	{
		distanceToNextPoint = allDistance;
	}
	if (distanceToNextPoint >= divDistance)
	{
		tempLinearVelocity = LINEARK * (fabs(allDistance - distanceToNextPoint)) + LINEARB;
	}
	else
	{
		tempLinearVelocity = LINEARK * distanceToNextPoint + LINEARB;
	}

	float preAngele = fVerticalDis * VERTICLE_RATIO;
	float diffAngele = preAngele - calcTurnAngleToLine;
	tempAngularVelocity = diffAngele * ANGLEK;
	angular_speed_ref = fabs(tempLinearVelocity) > 0.2 ? FIX_ANGULAR_SPEED_MAX : FIX_ANGULAR_SPEED_MIN;
	tempAngularVelocity = tempAngularVelocity > angular_speed_ref ? angular_speed_ref : tempAngularVelocity;
	tempAngularVelocity = tempAngularVelocity < -angular_speed_ref ? -angular_speed_ref : tempAngularVelocity;
	// tempLinearVelocity = tempLinearVelocity>MAX_LINE_SPEED?MAX_LINE_SPEED:tempLinearVelocity;
	tempLinearVelocity = tempLinearVelocity > utilssport::car_speed_max ? utilssport::car_speed_max : tempLinearVelocity;
	// currentLinearVelocity = tempLinearVelocity;
	// angularVelocity = tempAngularVelocity;
	//
	float r_LinearVelocity = 0.0;
	float r_AngularVelocity = 0.0;
	sensorLimitSpeed(sensorDistance, currentLinearVelocity, angularVelocity, tempLinearVelocity, tempAngularVelocity, r_LinearVelocity, r_AngularVelocity);
	currentLinearVelocity = r_LinearVelocity;
	angularVelocity = r_AngularVelocity;
	// --

	return 1;
}

void turnAngle(float orientation, float currentAngel, float destAngle, float &linearVelocity, float &angularVelocity)
{
	// linearVelocity = 0;
	// float diffAngle = fabs(destAngle - currentAngel);
	// diffAngle = diffAngle >= 10?diffAngle:10;
	// float angleSpeed = 0.005*diffAngle;
	// angleSpeed = angleSpeed>0.15?0.15:angleSpeed;
	// angularVelocity = orientation>0?-angleSpeed:angleSpeed;
	// //	angularVelocity = orientation==0?0:angularVelocity;

	//
	linearVelocity = 0.0;
	float lDiff = 0.0; // left diff angle
	float rDiff = 0.0; // right diff angle
	if (destAngle == currentAngel)
	{
		lDiff = 0.0;
		rDiff = 0.0;
	}
	else if (destAngle > currentAngel)
	{
		lDiff = destAngle - currentAngel;
		rDiff = 360.0 - (destAngle - currentAngel);
	}
	else
	{
		lDiff = 360.0 - (currentAngel - destAngle);
		rDiff = currentAngel - destAngle;
	}
	float diff = lDiff >= rDiff ? rDiff : lDiff;
	float angleSpeed = (diff / 2.0) / 180.0 * PII;
	float min_angleSpeed = utilssport::min_angle_speed / 180.0 * PII;
	float max_angleSpeed = utilssport::max_angle_speed / 180.0 * PII;
	angleSpeed = angleSpeed < min_angleSpeed ? min_angleSpeed : angleSpeed; // min
	angleSpeed = angleSpeed > max_angleSpeed ? max_angleSpeed : angleSpeed; // max
	angularVelocity = lDiff >= rDiff ? -angleSpeed : angleSpeed;
}

char backwardGo(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float &currentLinearVelocity, float &angularVelocity)
{
	float angular_speed_ref = FIX_ANGULAR_SPEED_MIN;
	float divDistance = allDistance / 2;
	if (distanceToNextPoint >= allDistance)
	{
		distanceToNextPoint = allDistance;
	}
	if (distanceToNextPoint >= divDistance)
	{
		currentLinearVelocity = LINEARK * (fabs(allDistance - distanceToNextPoint)) + LINEARB;
	}
	else
	{
		currentLinearVelocity = LINEARK * distanceToNextPoint + LINEARB;
	}

	float preAngele = (-1) * fVerticalDis * VERTICLE_RATIO;
	if (calcTurnAngleToLine >= 0)
	{
		calcTurnAngleToLine = 180 - calcTurnAngleToLine;
	}
	else
	{
		calcTurnAngleToLine = (-180) - calcTurnAngleToLine;
	}
	float diffAngele = preAngele - calcTurnAngleToLine;
	angularVelocity = diffAngele * ANGLEK;
	angularVelocity = -angularVelocity;
	angular_speed_ref = fabs(currentLinearVelocity) > 0.2 ? FIX_ANGULAR_SPEED_MAX : FIX_ANGULAR_SPEED_MIN;
	angularVelocity = angularVelocity > angular_speed_ref ? angular_speed_ref : angularVelocity;
	angularVelocity = angularVelocity < -angular_speed_ref ? -angular_speed_ref : angularVelocity;
	currentLinearVelocity = -currentLinearVelocity;
	// currentLinearVelocity = currentLinearVelocity<-MAX_LINE_SPEED?-MAX_LINE_SPEED:currentLinearVelocity;
	currentLinearVelocity = currentLinearVelocity < -utilssport::car_speed_max ? -utilssport::car_speed_max : currentLinearVelocity;

	return 1;
}

char forwardGoHome(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float &currentLinearVelocity, float &angularVelocity)
{
	float tempLinearVelocity = 0.0;
	float tempAngularVelocity = 0.0;
	float angular_speed_ref = FIX_ANGULAR_SPEED_MIN;
	float divDistance = allDistance / 2;
	if (distanceToNextPoint >= allDistance)
	{
		distanceToNextPoint = allDistance;
	}
	if (distanceToNextPoint >= divDistance)
	{
		tempLinearVelocity = LINEARK * (fabs(allDistance - distanceToNextPoint)) + LINEARB;
	}
	else
	{
		tempLinearVelocity = LINEARK * distanceToNextPoint + LINEARB;
	}

	float preAngele = fVerticalDis * VERTICLE_RATIO;
	float diffAngele = preAngele - calcTurnAngleToLine;
	tempAngularVelocity = diffAngele * ANGLEK;
	angular_speed_ref = fabs(tempLinearVelocity) > 0.18 ? FIX_ANGULAR_SPEED_MAX : FIX_ANGULAR_SPEED_MIN;
	tempAngularVelocity = tempAngularVelocity > angular_speed_ref ? angular_speed_ref : tempAngularVelocity;
	tempAngularVelocity = tempAngularVelocity < -angular_speed_ref ? -angular_speed_ref : tempAngularVelocity;
	tempLinearVelocity = tempLinearVelocity > 0.20 ? 0.20 : tempLinearVelocity;
	//
	currentLinearVelocity = tempLinearVelocity;
	angularVelocity = tempAngularVelocity;

	return 1;
}

char backwardGoHome(float fVerticalDis, float calcTurnAngleToLine, float distanceToNextPoint, float allDistance, float &currentLinearVelocity, float &angularVelocity)
{
	float angular_speed_ref = FIX_ANGULAR_SPEED_MIN;
	float divDistance = allDistance / 2;
	if (distanceToNextPoint >= allDistance)
	{
		distanceToNextPoint = allDistance;
	}
	if (distanceToNextPoint >= divDistance)
	{
		currentLinearVelocity = LINEARK * (fabs(allDistance - distanceToNextPoint)) + LINEARB;
	}
	else
	{
		currentLinearVelocity = LINEARK * distanceToNextPoint + LINEARB;
	}

	float preAngele = (-1) * fVerticalDis * VERTICLE_RATIO;
	if (calcTurnAngleToLine >= 0)
	{
		calcTurnAngleToLine = 180 - calcTurnAngleToLine;
	}
	else
	{
		calcTurnAngleToLine = (-180) - calcTurnAngleToLine;
	}
	float diffAngele = preAngele - calcTurnAngleToLine;
	angularVelocity = diffAngele * ANGLEK;
	angularVelocity = -angularVelocity;
	angular_speed_ref = fabs(currentLinearVelocity) > 0.18 ? FIX_ANGULAR_SPEED_MAX : FIX_ANGULAR_SPEED_MIN;
	angularVelocity = angularVelocity > angular_speed_ref ? angular_speed_ref : angularVelocity;
	angularVelocity = angularVelocity < -angular_speed_ref ? -angular_speed_ref : angularVelocity;
	currentLinearVelocity = -currentLinearVelocity;
	currentLinearVelocity = currentLinearVelocity < -0.20 ? -0.20 : currentLinearVelocity;

	return 1;
}

bool isDestination(char destType, float distanceError, float currentAngle, float destAngle, float &linearVelocity, float &angularVelocity)
{
	if (destType == DEST_ANGLE)
	{
		if (fabs(currentAngle - destAngle) <= ANGULAR_ERROR)
		{
			angularVelocity = 0;
			linearVelocity = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		if (distanceError >= 0)
		{
			angularVelocity = 0;
			linearVelocity = 0;

			return true;
		}
		else
		{
			if (distanceError < 0)
			{
				if (fabs(distanceError) < 0.03)
				{
					angularVelocity = 0;
					linearVelocity = 0;

					return true;
				}
			}

			return false;
		}
	}
}

bool SmoothStop(float &linearVelocity, float &angularVelocity)
{
	if (linearVelocity == 0)
	{
		linearVelocity = 0;
	}
	if (linearVelocity > 0)
	{
		float min_lineSpeed = 0.03;
		linearVelocity = linearVelocity >= min_lineSpeed ? (linearVelocity - linearVelocity / utilssport::car_reduction_ratio) : 0;
	}
	if (linearVelocity < 0)
	{
		float min_lineSpeed = -0.03;
		linearVelocity = linearVelocity <= min_lineSpeed ? (linearVelocity - linearVelocity / utilssport::car_reduction_ratio) : 0;
	}
	//
	if (angularVelocity == 0)
	{
		angularVelocity = 0;
	}
	if (angularVelocity > 0)
	{
		float min_angleSpeed = 1.0 / 180.0 * PII;
		angularVelocity = angularVelocity >= min_angleSpeed ? (angularVelocity - angularVelocity / 16) : 0;
	}
	if (angularVelocity < 0)
	{
		float min_angleSpeed = -1.0 / 180.0 * PII;
		angularVelocity = angularVelocity <= min_angleSpeed ? (angularVelocity - angularVelocity / 16) : 0;
	}

	return true;
}

bool sensorLimitSpeed(float sensorDistance, float c_LinearVelocity, float c_AngularVelocity,
					  float i_LinearVelocity, float i_AngularVelocity, float &r_LinearVelocity, float &r_AngularVelocity)
{
	float tempLinearVelocity = 0.0;
	if (sensorDistance != 0 && sensorDistance <= utilssport::sensorLimitSpeed_distance)
	{
		float sensorLinearSpeed = (sensorDistance / 100.0) * 3.0 / 10.0;
		if (c_LinearVelocity > sensorLinearSpeed)
		{
			tempLinearVelocity = c_LinearVelocity - (c_LinearVelocity / utilssport::car_reduction_ratio);
		}
		else
		{
			tempLinearVelocity = sensorLinearSpeed;
		}
		tempLinearVelocity = tempLinearVelocity > i_LinearVelocity ? i_LinearVelocity : tempLinearVelocity;
	}
	else
	{
		tempLinearVelocity = i_LinearVelocity;
	}
	tempLinearVelocity = tempLinearVelocity > utilssport::car_speed_max ? utilssport::car_speed_max : tempLinearVelocity;

	r_LinearVelocity = tempLinearVelocity;
	r_AngularVelocity = i_AngularVelocity;

	return true;
}
