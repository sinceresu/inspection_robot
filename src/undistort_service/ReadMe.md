<!--
 * @Descripttion: 
 * @version: 
 * @Author: li
 * @Date: 2021-05-12 16:14:16
 * @LastEditors: li
 * @LastEditTime: 2021-05-12 18:18:01
-->
# undistort_service
接收红外驱动发出的温度图像以及定位服务提供的红外相机位姿，对两者进行时间同步，打包发送出位姿对齐的温度图像。 

##  订阅话题
###  红外温度图片
Topic: /fixed/infrared/raw 

Type: sensor_msgs::Image

###  云台姿态
Topic: /yida/yuntai/position

nav_msgs::Odometry

##  发布话题
###  位姿对齐温度图片
Topic: /infrared/undistorted 

Type: undistort_service_msgs::PosedImage

undistort_service_msgs::PosedImage定义：
```json
sensor_msgs/Image image   #温度图片 
geometry_msgs/Pose pose   # 当前位姿 
sensor_msgs/CameraInfo camera_info  #相机参数
```

##  服务
###  设置相机参数
Service Name: undistort_service/update_calib_params  
Type: undistort_service_msgs::update_calib_params  

undistort_service_msgs::update_calib_params定义：
```json
string[] calib_params   #相机标定参数
---
undistort_service_msgs/StatusResponse status
```
###  开始对齐任务
Service Name: undistort_service/start_undistort 

Type: undistort_service_msgs::start_undistort

```json
---
undistort_service_msgs/StatusResponse status
```
###  停止对齐任务
Service Name: undistort_service/stop_undistort  

Type: undistort_service_msgs::stop_undistort 

undistort_service_msgs::stop_undistort定义：
```json
---
undistort_service_msgs/StatusResponse status
```
## 参数:

接收从定位模块广播的TF， 源坐标为地图坐标，id值由参数map_frame定义，目的坐标为雷达坐标，id值由参数robot_frame定义。

## 参数:

| 名称                        | 类型        | 描述                        | 缺省值            |
|--------------------- |-----------|----------------------|------------------|
| raw_topic           | string     | 红外图像消息的topic  |/fixed/infrared/raw|
| ptz_topic           | string     | 云台位姿消息的topic  |/yida/yuntai/position|
| undistort_topic| string     | 对齐图片消息的topic  |/infrared/undistorted|
| map_frame| string     | 地图的frame_id  |map|
| robot_frame| string   | 车体位姿Transform的frame_id  |robot_pose|
| lidar_frame| string     | 雷达位姿Transform的frame_id  |lidar_pose|


##  启动服务
###  启动着色对齐服务

```shell
#driver
roslaunch undistort_service undistort_service.launch
```
