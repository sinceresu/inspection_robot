#include <algorithm>
#include <fstream>
#include <iostream>

#include <cv_bridge/cv_bridge.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "undistort_node.h"

using namespace std;

namespace undistort_service
{
  void Run(int argc, char **argv)
  {

    UndistortNode undistort_node;
    cv::Mat raw_image;

    // ::ros::spin();
    // 异步回调
    ros::AsyncSpinner spinner(3);
    spinner.start();
    ros::waitForShutdown();

    LOG(INFO) << "finished  undistort servic.";
  }

}

int main(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  LOG(INFO) << "start undistort service.";

  ::ros::init(argc, argv, " undistort_service_node");

  undistort_service::Run(argc, argv);
  LOG(INFO) << "finished undistort service.";
}
