#include "epoll_client.hpp"

namespace epoll_client
{

    CEpollClient::CEpollClient(const std::string &_server_ip, const uint16_t &_server_port)
        : server_ip_(_server_ip),
          server_port_(_server_port),
          b_open_(false)
    {
    }

    CEpollClient::~CEpollClient()
    {
    }

    int32_t CEpollClient::CreateEpoll()
    {
        int e_fd = epoll_create(1);
        if (e_fd < 0)
        {
            std::cout << "epoll_create failed." << std::endl;
            return -1;
        }
        return e_fd;
    }

    int32_t CEpollClient::UpdateEpoll(int _e_fd, int _op, int _s_fd, int _events)
    {
        struct epoll_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.data.fd = _s_fd;
        ev.events = _events;
        int ec = epoll_ctl(_e_fd, _op, _s_fd, &ev);
        if (ec < 0)
        {
            std::cout << "epoll_ctl failed." << std::endl;
            return -1;
        }
        return ec;
    }

    void CEpollClient::LoopEpoll()
    {
        struct epoll_event *alive_events = static_cast<epoll_event *>(calloc(kMaxEvents, sizeof(epoll_event)));
        if (!alive_events)
        {
            std::cout << "calloc memory failed for epoll_events." << std::endl;
            return;
        }
        while (loop_flag_)
        {
            int num = epoll_wait(epoll_fd_, alive_events, kMaxEvents, kEpollWaitTime);

            for (int i = 0; i < num; ++i)
            {
                int fd = alive_events[i].data.fd;
                int events = alive_events[i].events;

                if ((events & EPOLLERR) || (events & EPOLLHUP))
                {
                    close(fd);
                }
                else if (events & EPOLLRDHUP)
                {
                    close(fd);
                }
                else if (events & EPOLLIN)
                {
                    msg_Packet_t data;
                    OnSocketRead(fd, data);
                }
                else if (events & EPOLLOUT)
                {
                    // msg_Packet_t data;
                    // OnSocketWrite(fd, data);
                }
                else
                {
                    std::cout << "unknown epoll event." << std::endl;
                }
            }
        }
        free(alive_events);
    }

    int32_t CEpollClient::CreateSocket()
    {
        int s_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (s_fd < 0)
        {
            std::cout << "create socket failed." << std::endl;
            return -1;
        }
        return s_fd;
    }

    int32_t CEpollClient::ConnectSocket(int32_t _s_fd)
    {
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = inet_addr(server_ip_.c_str());
        addr.sin_port = htons(server_port_);

        int c = connect(_s_fd, (struct sockaddr *)&addr, sizeof(addr));
        if (c < 0)
        {
            std::cout << "connect failed. errno:" << errno << std::endl;
            return -1;
        }
        return c;
    }

    int32_t CEpollClient::OnSocketRead(int32_t _s_fd, msg_Packet_t &_data)
    {
        read_mtx_.lock();
        char read_buf[1024];
        bzero(read_buf, sizeof(read_buf));
        int n = -1;
        while ((n = read(_s_fd, read_buf, sizeof(read_buf))) > 0)
        {
            std::string msg(read_buf, n);
            _data = msg_Packet_t(_s_fd, msg, n);
            if (recv_callback_)
            {
                recv_callback_(_data);
            }
        }
        if (n < 0)
        {
            if ((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
            {
                // finished
                /* code */
            }
            else
            {
                close(_s_fd);
            }
        }
        if (n == 0)
        {
            close(_s_fd);
        }
        read_mtx_.unlock();

        return n;
    }

    int32_t CEpollClient::OnSocketWrite(int32_t _s_fd, const msg_Packet_t &_data)
    {
        write_mtx_.lock();
        int n = write(_s_fd, _data.msg.data(), _data.msg.size());
        if (n > 0)
        {
            /* code */
        }
        if (n < 0)
        {
            if ((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
            {
                /* code */
            }
            else
            {
                close(_s_fd);
            }
        }
        if (n == 0)
        {
            close(_s_fd);
        }
        write_mtx_.unlock();

        return n;
    }

    bool CEpollClient::Start()
    {
        int s_fd = CreateSocket();
        if (s_fd < 0)
        {
            return false;
        }
        int ret = ConnectSocket(s_fd);
        if (ret < 0)
        {
            Stop();
            return false;
        }
        socket_fd_ = s_fd;

        int e_fd = CreateEpoll();
        if (e_fd < 0)
        {
            Stop();
            return false;
        }
        epoll_fd_ = e_fd;

        int er = UpdateEpoll(epoll_fd_, EPOLL_CTL_ADD, socket_fd_, EPOLLIN | EPOLLET);
        if (er < 0)
        {
            Stop();
            return false;
        }

        assert(!th_loop_);

        b_open_ = true;
        loop_flag_ = true;

        th_loop_ = std::make_shared<std::thread>(&CEpollClient::LoopEpoll, this);
        if (!th_loop_)
        {
            Stop();
            return false;
        }
        th_loop_->detach();

        return true;
    }

    bool CEpollClient::Stop()
    {
        b_open_ = false;
        loop_flag_ = false;
        if (socket_fd_ >= 0)
        {
            close(socket_fd_);
            socket_fd_ = -1;
        }
        if (epoll_fd_ >= 0)
        {
            close(epoll_fd_);
            epoll_fd_ = -1;
        }
        UnRegisterOnRecvCallback();
        return true;
    }

    bool CEpollClient::IsOpen()
    {
        return b_open_;
    }

    int32_t CEpollClient::ReadData(msg_Packet_t &_data)
    {
        return OnSocketRead(socket_fd_, _data);
    }

    int32_t CEpollClient::WriteData(const msg_Packet_t &_data)
    {
        return OnSocketWrite(socket_fd_, _data);
    }

    void CEpollClient::RegisterOnRecvCallback(RecvCallback _callback)
    {
        assert(!recv_callback_);
        recv_callback_ = _callback;
    }

    void CEpollClient::UnRegisterOnRecvCallback()
    {
        assert(recv_callback_);
        recv_callback_ = nullptr;
    }
}