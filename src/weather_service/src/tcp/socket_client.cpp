#include "socket_client.hpp"

namespace socket_client
{

    CSocketClient::CSocketClient(std::string _ip, int _port)
        : socket_fd_(-1), b_open_(false), service_ip_(_ip), service_port_(_port)
    {
        counter_++;
    }

    CSocketClient::~CSocketClient()
    {
        Close();
        counter_--;
    }

    unsigned int CSocketClient::counter_ = 0;

    int CSocketClient::Create()
    {
        int fd = socket(AF_INET, SOCK_STREAM, 0);
        if (fd < 0)
        {
            Close();
            return -1;
        }

        // 设置超时时间
        struct timeval tv_timeout;
        tv_timeout.tv_sec = 3;
        tv_timeout.tv_usec = 0;
        setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &tv_timeout, sizeof(tv_timeout));
        setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv_timeout, sizeof(tv_timeout));

        return fd;
    }

    int CSocketClient::Connect()
    {
        // 建立连接
        struct sockaddr_in addrSrv;
        addrSrv.sin_family = AF_INET;
        addrSrv.sin_addr.s_addr = inet_addr(service_ip_.c_str());
        addrSrv.sin_port = htons(service_port_);
        //
        int ret = connect(socket_fd_, (struct sockaddr *)&addrSrv, sizeof(addrSrv));
        if (ret < 0)
        {
            Close();
            return -1;
        }
        b_open_ = true;

        return ret;
    }

    int CSocketClient::Close()
    {
        b_open_ = false;
        if (socket_fd_ >= 0)
        {
            close(socket_fd_);
            socket_fd_ = -1;
        }

        return 0;
    }

    int CSocketClient::Recv(unsigned char *_buf, int &_len)
    {
        if (socket_fd_ < 0)
        {
            return -1;
        }

        recv_mtx_.lock();
        int size = recv(socket_fd_, (char *)_buf, _len, 0);
        if (size > 0)
        {
            _len = size;
        }
        if (size < 0)
        {
            if ((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
            {
                /* code */
            }
            else
            {
                Close();
            }
        }
        if (size == 0)
        {
            Close();
        }
        recv_mtx_.unlock();

        return size;
    }

    int CSocketClient::Send(const unsigned char *_buf, const unsigned int _len)
    {
        if (socket_fd_ < 0)
        {
            return -1;
        }

        send_mtx_.lock();
        int size = send(socket_fd_, (const char *)_buf, _len, 0);
        if (size > 0)
        {
            /* code */
        }
        if (size < 0)
        {
            if ((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
            {
                /* code */
            }
            else
            {
                Close();
            }
        }
        if (size == 0)
        {
            Close();
        }
        send_mtx_.unlock();

        return size;
    }

    int CSocketClient::start()
    {
        socket_fd_ = Create();
        if (socket_fd_ < 0)
        {
            return -1;
        }
        if (Connect() < 0)
        {
            return -1;
        }

        return 0;
    }
    int CSocketClient::stop()
    {
        return Close();
    }

    bool CSocketClient::is_open()
    {
        return b_open_;
    }

    int CSocketClient::send_data(const unsigned char *_buf, const unsigned int _len)
    {
        if (!is_open())
            return -1;

        return Send(_buf, _len);
    }

    int CSocketClient::recv_data(unsigned char *_buf, int &_len)
    {
        if (!is_open())
            return -1;

        return Recv(_buf, _len);
    }

}