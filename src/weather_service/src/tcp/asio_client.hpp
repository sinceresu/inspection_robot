#ifndef asio_client_HPP
#define asio_client_HPP

#pragma once
#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/write.hpp>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <mutex>

namespace asio_client
{

    using boost::asio::ip::tcp;

    class CAsioClient
    {
    public:
        CAsioClient(const std::string &host, const int &port);
        ~CAsioClient();

    private:
        std::string host_;
        std::string service_;

        boost::asio::io_context io_context_;
        tcp::socket socket_{io_context_};

        bool b_open_;

        std::mutex read_mtx;
        std::mutex write_mtx;

    private:
        void run(std::chrono::steady_clock::duration timeout = std::chrono::seconds(3));

        int start_connect(const std::string &host, const std::string &service,
                          std::chrono::steady_clock::duration timeout = std::chrono::seconds(3));

        int start_read(unsigned char *buf, int &len,
                       std::chrono::steady_clock::duration timeout = std::chrono::seconds(3));

        int start_write(const unsigned char *buf, const unsigned int len,
                        std::chrono::steady_clock::duration timeout = std::chrono::seconds(3));

    public:
        int start();
        int stop();
        int read_data(unsigned char *buf, int &len);
        int write_data(const unsigned char *buf, const unsigned int len);
        bool is_open();
    };
}

#endif