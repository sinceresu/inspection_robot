#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <vector>

using namespace std;

// 按格式返回系统当前时间戳string
string get_system_cur_time();
// 以某字符分割字符串
vector<std::string> split(std::string str, std::string pattern);
// 以指定字符串替换原字符串中的字符串
string replace(string &str, const string &old_value, const string &new_value);

// base64解、编码
std::string base64_encode(unsigned char const *, unsigned int len);
std::string base64_decode(std::string const &s);

#endif
