// 工具类
#include "boost/date_time/posix_time/posix_time.hpp"
#include "../include/yd_status_manager/hpp/sensors_port.hpp"

#include "ros/ros.h"
#include "stdio.h"
#include "time.h"

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <ratio>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

/// Boost
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include "boost/bind.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"

/// OpenCV
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

/// 数据同步相关
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

// 消息类
/// Msg
#include "std_msgs/Bool.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"

#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/CompressedImage.h"

#include <diagnostic_msgs/DiagnosticArray.h>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>

#include "yidamsg/pointcloud_color.h"

/// Srv
#include "yd_framework/Settings.h"

/// 点云着色同步
message_filters::Subscriber<nav_msgs::Odometry>           *synPoseSub;
message_filters::Subscriber<nav_msgs::Odometry>           *platformPoseSub;
//message_filters::Subscriber<sensor_msgs::Image>           *visualImageSub;
message_filters::Subscriber<sensor_msgs::CompressedImage> *visualImageSub;
message_filters::Subscriber<sensor_msgs::CompressedImage> *thermalImageSub;

typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry,
                                                        nav_msgs::Odometry,
                                                        //sensor_msgs::Image,
                                                        sensor_msgs::CompressedImage,
                                                        sensor_msgs::CompressedImage> pointCloudDataSyncPolicy;
message_filters::Synchronizer<pointCloudDataSyncPolicy> *messageSync;

/// 点云着色同步2
message_filters::Subscriber<nav_msgs::Odometry>           *_platformPoseSub;
//message_filters::Subscriber<sensor_msgs::Image>           *_visualImageSub;
message_filters::Subscriber<sensor_msgs::CompressedImage> *_visualImageSub;
message_filters::Subscriber<sensor_msgs::CompressedImage> *_thermalImageSub;

typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry,
                                                        //sensor_msgs::Image,
                                                        sensor_msgs::CompressedImage,
                                                        sensor_msgs::CompressedImage> _pointCloudDataSyncPolicy;
message_filters::Synchronizer<_pointCloudDataSyncPolicy> *_messageSync;

using namespace cv;
using namespace std;
using namespace message_filters;

/// Variables
string LOG_TITLE = ">>>>>:";

char STRING_CHARS[50];
int XAVIER_LEVEL_VALUE;

stringstream THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING;

bool is_stop_receive_laser = false;
nav_msgs::Odometry statusManagerPoseMsg;
nav_msgs::Odometry lastSynPoseMsg;

// 可见光、红外相机状态码
int VISUAL_CAMERA_STATUS_CODE  = -1;
int THERMAL_CAMERA_STATUS_CODE = -1;
// 充电桩、自动门状态码
string CHARGE_STAKE_STATUS_CODE;
string AUTO_DOOR_STATUS_CODE;
// 云台水平、垂直角度
string PLATFORM_HORIZONTAL_ANGLE;
string PLATFORM_VERTICAL_ANGLE;

// 测距超声波
string ULTRASONIC_DIS_VALUE;
// 左(右)防跌落超声波
string ULTRASONIC_FALL_LEFT_VALUE;
string ULTRASONIC_FALL_RIGHT_VALUE;
// 防撞条
string ANTI_COLLISION_BAR_STATUS_CODE;

string BATTERY_VOLTAGE_VALUE;      // 电压
string BATTERY_ELECTRICITY_VALUE;  // 电流
string BATTERY_VOLUME_VALUE;       // 电量
string ROBOT_MOTOR_REV_VALUE;      // 电机转速

// XAvier Buffer Size Value
bool is_robot_stop = false;
//bool is_xavier_sleeping = false;
//int XAVIER_SLEEPING_TIME             = 0;
//int XAVIER_BUFFER_SIZE_MAXIMUM_COUNT = 0;

/// Publisher-Subscriber
// Publisher
ros::Publisher pointCloudSyncDataPub;

ros::Publisher statusManagerHeartbeatPub;
ros::Publisher statusManagerPub;

ros::Publisher robotControlPub;
//ros::Publisher xavierSleepPub;

// Subscriber
ros::Subscriber statusManagerPoseSub;
ros::Subscriber isStopReceiveLaserSub;

ros::Subscriber cameraManagerStatusSub;
ros::Subscriber homeManagerStatusSub;
ros::Subscriber fuzzyControllerStatusSub;
ros::Subscriber platformControllerStatusSub;
ros::Subscriber localizationStatusSub;

/// Client
ros::ServiceClient statusManagerSettingsClient;
ros::ServiceClient statusManagerTasksClient;
// Srv消息
yd_framework::Settings settingsSrv;

/// 串口类
bool is_port_opened = false;
int PORT_READ_NOTHING_COUNT = 0;
float ROBOT_ROLL_ANGLE, ROBOT_PITCH_ANGLE, ROBOT_HEADING_ANGLE;
int OBSTACLE_DIS, LEFT_FALL_DIS, RIGHT_FALL_DIS;
// 构造初始化
SensorsPort port("/dev/ttyUSB0");

/*===================================回调消息代码块===================================*/
// StatusManagerPoseMessagesCallback
void statusManagerPoseMessagesCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    statusManagerPoseMsg = *msg;
}
// IsStopReceiveLaserMessagesCallback
void isStopReceiveLaserMessagesCallback(const std_msgs::Bool::ConstPtr& msg)
{
    is_stop_receive_laser = msg->data;
}

// CameraManagerStatusMessagesCallback
void cameraManagerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    std::cout << "******Camera Manager Status******" << std::endl;
    std::cout << "time: " + boost::posix_time::to_iso_extended_string(msg->header.stamp.toBoost()) << std::endl;
    std::cout << "type: " + msg->header.frame_id << std::endl;

    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it  = msg->status.begin(); it != msg->status.end(); ++it)
    {
        std::cout << "status: "  + it->level << std::endl;
        if(msg->header.frame_id == "visual")
        {
            VISUAL_CAMERA_STATUS_CODE = it->level;
        }
        if(msg->header.frame_id == "thermal")
        {
            THERMAL_CAMERA_STATUS_CODE = it->level;
        }

        std::cout << "ip: "      + it->name << std::endl;
        std::cout << "message: " + it->message << std::endl;
        std::cout << "id: "      + it->hardware_id << std::endl;
    }
    std::cout << "*********************************" << std::endl;
}
// HomeManagerStatusMessagesCallback
void homeManagerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    std::cout << "*****Home Manager Status*****" << std::endl;
    std::cout << "time: " + boost::posix_time::to_iso_extended_string(msg->header.stamp.toBoost()) << std::endl;
    std::cout << "type: " + msg->header.frame_id << std::endl;

    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it = msg->status.begin(); it != msg->status.end(); ++it)
    {
        std::cout << "status: "  + it->level << std::endl;
        if(msg->header.frame_id == "charge_stake")
        {
            CHARGE_STAKE_STATUS_CODE = it->level;
        }
        if(msg->header.frame_id == "door")
        {
            AUTO_DOOR_STATUS_CODE = it->level;
        }

        std::cout << "name: "    + it->name << std::endl;
        std::cout << "message: " + it->message << std::endl;
        std::cout << "id: "      + it->hardware_id << std::endl;
    }
    std::cout << "*****************************" << std::endl;
}
// FuzzyControllerStatusMessagesCallback
void fuzzyControllerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    std::cout << "*****Fuzzy Controller Status*****" << std::endl;
    std::cout << "time: " + boost::posix_time::to_iso_extended_string(msg->header.stamp.toBoost()) << std::endl;

    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it  = msg->status.begin(); it != msg->status.end(); ++it)
    {
        std::cout << "status: "  + it->level << std::endl;
        std::cout << "name: "    + it->name << std::endl;
        std::cout << "id: "      + it->hardware_id << std::endl;

        std::cout << "---------------------------------" << std::endl;
        for(int i = 0; i < it->values.size(); i++)
        {
            std::cout << it->values[i].key + ": " + it->values[i].value << std::endl;
            // 测距超声波
            if(it->values[i].key == "ultrasonic_dis")
            {
                ULTRASONIC_DIS_VALUE = it->values[i].value;
                // TODO.极限判断
            }
            // 左(右)防跌落超声波
            if(it->values[i].key == "ultrasonic_fall_left")
            {
                ULTRASONIC_FALL_LEFT_VALUE = it->values[i].value;
                // TODO.极限判断
            }
            if(it->values[i].key == "ultrasonic_fall_right")
            {
                ULTRASONIC_FALL_RIGHT_VALUE = it->values[i].value;
                // TODO.极限判断
            }
            // 防撞条
            if(it->values[i].key == "anti_collision_bar")
            {
                ANTI_COLLISION_BAR_STATUS_CODE = it->values[i].value;
            }

            // 电压、电流、电量
            if(it->values[i].key == "battery_voltage")
            {
                BATTERY_VOLTAGE_VALUE = it->values[i].value;
            }
            if(it->values[i].key == "battery_electricity")
            {
                BATTERY_ELECTRICITY_VALUE = it->values[i].value;
            }
            if(it->values[i].key == "battery_volume")
            {
                BATTERY_VOLUME_VALUE = it->values[i].value;
            }

            // 电机转速
            if(it->values[i].key == "robot_motor_rev")
            {
                ROBOT_MOTOR_REV_VALUE = it->values[i].value;
            }
        }
        std::cout << "---------------------------------" << std::endl;
    }
    std::cout << "*********************************" << std::endl;
}
// PlatformControllerStatusMessagesCallback
void platformControllerStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    //std::cout << "*****Platform Controller Status*****" << std::endl;
    //std::cout << "time: " + boost::posix_time::to_iso_extended_string(msg->header.stamp.toBoost()) << std::endl;

    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it = msg->status.begin(); it != msg->status.end(); ++it)
    {
        //std::cout << "status: "  + it->level << std::endl;
        //std::cout << "name: "    + it->name << std::endl;
        //std::cout << "message: " + it->message << std::endl;
        //std::cout << "id: "      + it->hardware_id << std::endl;

        string value = it->message;
        std::vector<std::string> v_arr = split(value, " ");
        PLATFORM_HORIZONTAL_ANGLE = v_arr[0];
        PLATFORM_VERTICAL_ANGLE   = v_arr[1];
    }
    //std::cout << "************************************" << std::endl;
}
// 定位节点相关
/// 车体停止、继续移动
void robotControlFun(bool b)
{
    std_msgs::Int32 msg;
    msg.data = b ? 0 : 1;
    robotControlPub.publish(msg);
}
/* Xavier休眠、唤醒
void xavierControlFun(bool b)
{
    std_msgs::Bool msg;
    msg.data = b;
    xavierSleepPub.publish(msg);
}
*/
/// LocalizationStatusMessagesCallback
void localizationStatusMessagesCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr& msg)
{
    vector<diagnostic_msgs::DiagnosticStatus>::const_iterator it;
    for(it = msg->status.begin(); it != msg->status.end(); ++it)
    {
        // 过滤筛选出定位节点状态
        if(it->name == "/Localization_node")
        {
            std::cout << "*****Localization Status*****" << std::endl;
            std::cout << "time: "  + boost::posix_time::to_iso_extended_string(msg->header.stamp.toBoost()) << std::endl;
            std::cout << "level: " + it->level << std::endl;
            std::cout << "name: "  + it->name << std::endl;
            std::cout << "id: "    + it->hardware_id << std::endl;
            std::cout << "************************************" << std::endl;

            // OK
            if(it->level == diagnostic_msgs::DiagnosticStatus::OK)
            {
                XAVIER_LEVEL_VALUE = 0;

                // 车体恢复移动
                if(is_robot_stop)
                {
                    is_robot_stop = false;
                    robotControlFun(is_robot_stop);
                }
            }
            // WARN
            else if(it->level == diagnostic_msgs::DiagnosticStatus::WARN)
            {
                XAVIER_LEVEL_VALUE = 1;
                ROS_INFO("XAvier Bad Matching, Request Initial Pose.");
            }
            // ERROR
            else if(it->level == diagnostic_msgs::DiagnosticStatus::ERROR)
            {
                XAVIER_LEVEL_VALUE = 2;

                // 停止车体
                is_robot_stop = true;
                robotControlFun(is_robot_stop);
            }
            // STALE(TODO: 逻辑基于休眠消息发送后，/heartbeat停止发送)
            else if(it->level == diagnostic_msgs::DiagnosticStatus::STALE)
            {
                XAVIER_LEVEL_VALUE = 3;

                // 停止车体
                is_robot_stop = true;
                robotControlFun(is_robot_stop);

                /*
                XAVIER_BUFFER_SIZE_MAXIMUM_COUNT++;

                // 停止车体
                if(!is_robot_stop)
                {
                    is_robot_stop = true;
                    robotControlFun(is_robot_stop);
                }
                // XAvier休眠1m
                if(XAVIER_BUFFER_SIZE_MAXIMUM_COUNT == 3)
                {
                    XAVIER_BUFFER_SIZE_MAXIMUM_COUNT = 0;

                    is_xavier_sleeping = true;
                    xavierControlFun(is_xavier_sleeping);
                }
                */
            }
        }
    }
}
// MessageSyncPolicyCallback
void messageSyncPolicyCallback(const nav_msgs::Odometry::ConstPtr& poseMsg,
                               const nav_msgs::Odometry::ConstPtr& platformPoseMsg,
                               //const sensor_msgs::ImageConstPtr& visualImageMsg,
                               const sensor_msgs::CompressedImage::ConstPtr& visualImageMsg,
                               const sensor_msgs::CompressedImage::ConstPtr& thermalImageMsg)
{
    nav_msgs::Odometry syn_pose          = *poseMsg;
    nav_msgs::Odometry syn_platform_pose = *platformPoseMsg;

    yidamsg::pointcloud_color pointCloudSyncData;

    // 本地保存，最后一帧定位同步数据
    lastSynPoseMsg.pose.pose.position.x = syn_pose.pose.pose.position.x;
    lastSynPoseMsg.pose.pose.position.y = syn_pose.pose.pose.position.y;
    lastSynPoseMsg.pose.pose.position.z = syn_pose.pose.pose.position.z;

    lastSynPoseMsg.pose.pose.orientation.x = syn_pose.pose.pose.orientation.x;
    lastSynPoseMsg.pose.pose.orientation.y = syn_pose.pose.pose.orientation.y;
    lastSynPoseMsg.pose.pose.orientation.z = syn_pose.pose.pose.orientation.z;
    lastSynPoseMsg.pose.pose.orientation.w = syn_pose.pose.pose.orientation.w;

    /// Image Process
    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);

    // Visual Image Process
    cv_bridge::CvImagePtr visual_cv_image_ptr = cv_bridge::toCvCopy(*visualImageMsg, sensor_msgs::image_encodings::BGR8);
    cv::Mat visual_image_mat                  = visual_cv_image_ptr->image;
    vector<uchar> vec_visual_image;
    imencode(".jpg", visual_image_mat, vec_visual_image, compression_params);

    string visual_image_buffer_base64 = base64_encode(vec_visual_image.data(), vec_visual_image.size());
    pointCloudSyncData.v_format = ".jpg";
    pointCloudSyncData.v_data   = visual_image_buffer_base64;

    /* FLIRONE
    cv_bridge::CvImagePtr visual_cv_image_ptr = cv_bridge::toCvCopy(visualImageMsg, visualImageMsg->encoding);
    pointCloudSyncData.v_format = ".jpg";

    int r = visual_cv_image_ptr->image.rows;
    int c = visual_cv_image_ptr->image.cols;

    // Buffer String Clean
    THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING.str("");

    /// Visual Image Gray Buffer Transform
    for(int i = 0; i < r; ++i)
    {
        const uint16_t *raw = visual_cv_image_ptr->image.ptr<uint16_t>(i);
        for(int j = 0; j < c; ++j)
        {
            uint16_t point = raw[j];
            THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING << point;
            THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING << ",";
        }
    }

    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
    compression_params.push_back(100);

    string thermal_image_gray_buffer_str = THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING.str();
    thermal_image_gray_buffer_str        = thermal_image_gray_buffer_str.substr(0, thermal_image_gray_buffer_str.length() - 1);

    pointCloudSyncData.gray_data = thermal_image_gray_buffer_str;
    */

    // Thermal Image Process
    cv_bridge::CvImagePtr thermal_cv_image_ptr = cv_bridge::toCvCopy(*thermalImageMsg, sensor_msgs::image_encodings::BGR8);
    cv::Mat thermal_image_mat                  = thermal_cv_image_ptr->image;

    // Convert Thermal Image Mat to VECTOR<UCHAR>
    vector<uchar> vec_thermal_image;
    imencode(".jpg", thermal_image_mat, vec_thermal_image, compression_params);

    string thermal_image_buffer_base64 = base64_encode(vec_thermal_image.data(), vec_thermal_image.size());
    pointCloudSyncData.t_format = ".jpg";
    pointCloudSyncData.t_data   = thermal_image_buffer_base64;

    /// Robot Pose Process
    ROS_INFO("%s [ROBOT MOVING] x=%f y=%f z=%f", LOG_TITLE.c_str(), syn_pose.pose.pose.position.x, syn_pose.pose.pose.position.y, syn_pose.pose.pose.position.z);

    pointCloudSyncData.pos_x = syn_pose.pose.pose.position.x;
    pointCloudSyncData.pos_y = syn_pose.pose.pose.position.y;
    pointCloudSyncData.pos_z = syn_pose.pose.pose.position.z;

    pointCloudSyncData.qua_x = syn_pose.pose.pose.orientation.x;
    pointCloudSyncData.qua_y = syn_pose.pose.pose.orientation.y;
    pointCloudSyncData.qua_z = syn_pose.pose.pose.orientation.z;
    pointCloudSyncData.qua_w = syn_pose.pose.pose.orientation.w;

    /// Platform Pose Process
    pointCloudSyncData.horizontal = syn_platform_pose.pose.pose.position.x;
    pointCloudSyncData.vertical   = syn_platform_pose.pose.pose.position.y;

    /// Publish
    pointCloudSyncDataPub.publish(pointCloudSyncData);
}
// _MessageSyncPolicyCallback
void _messageSyncPolicyCallback(const nav_msgs::Odometry::ConstPtr& platformPoseMsg,
                                //const sensor_msgs::ImageConstPtr& visualImageMsg,
                                const sensor_msgs::CompressedImage::ConstPtr& visualImageMsg,
                                const sensor_msgs::CompressedImage::ConstPtr& thermalImageMsg)
{
    if(is_stop_receive_laser)
    {
        nav_msgs::Odometry syn_platform_pose = *platformPoseMsg;

        yidamsg::pointcloud_color pointCloudSyncData;

        /// Image Process
        vector<int> compression_params;
        compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
        compression_params.push_back(100);

        // Visual Image Process
        cv_bridge::CvImagePtr visual_cv_image_ptr = cv_bridge::toCvCopy(*visualImageMsg, sensor_msgs::image_encodings::BGR8);
        cv::Mat visual_image_mat                  = visual_cv_image_ptr->image;
        vector<uchar> vec_visual_image;
        imencode(".jpg", visual_image_mat, vec_visual_image, compression_params);

        string visual_image_buffer_base64 = base64_encode(vec_visual_image.data(), vec_visual_image.size());
        pointCloudSyncData.v_format = ".jpg";
        pointCloudSyncData.v_data   = visual_image_buffer_base64;

        /* FLIRONE
        cv_bridge::CvImagePtr visual_cv_image_ptr = cv_bridge::toCvCopy(visualImageMsg, visualImageMsg->encoding);
        pointCloudSyncData.v_format = ".jpg";

        int r = visual_cv_image_ptr->image.rows;
        int c = visual_cv_image_ptr->image.cols;

        // Buffer String Clean
        THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING.str("");

        /// Visual Image Gray Buffer Transform
        for(int i = 0; i < r; ++i)
        {
            const uint16_t *raw = visual_cv_image_ptr->image.ptr<uint16_t>(i);
            for(int j = 0; j < c; ++j)
            {
                uint16_t point = raw[j];
                THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING << point;
                THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING << ",";
            }
        }

        vector<int> compression_params;
        compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
        compression_params.push_back(100);

        string thermal_image_gray_buffer_str = THERMAL_IMAGE_GRAY_TRANS_BUFFER_STRING.str();
        thermal_image_gray_buffer_str        = thermal_image_gray_buffer_str.substr(0, thermal_image_gray_buffer_str.length() - 1);

        pointCloudSyncData.gray_data = thermal_image_gray_buffer_str;
        */

        // Thermal Image Process
        cv_bridge::CvImagePtr thermal_cv_image_ptr = cv_bridge::toCvCopy(*thermalImageMsg, sensor_msgs::image_encodings::BGR8);
        cv::Mat thermal_image_mat                  = thermal_cv_image_ptr->image;

        // Convert Thermal Image Mat to VECTOR<UCHAR>
        vector<uchar> vec_thermal_image;
        imencode(".jpg", thermal_image_mat, vec_thermal_image, compression_params);

        string thermal_image_buffer_base64 = base64_encode(vec_thermal_image.data(), vec_thermal_image.size());
        pointCloudSyncData.t_format = ".jpg";
        pointCloudSyncData.t_data   = thermal_image_buffer_base64;

        /// Robot Pose Process
        ROS_INFO("%s [ROBOT STOP] x=%f y=%f z=%f", LOG_TITLE.c_str(), lastSynPoseMsg.pose.pose.position.x, lastSynPoseMsg.pose.pose.position.y, lastSynPoseMsg.pose.pose.position.z);

        pointCloudSyncData.pos_x = lastSynPoseMsg.pose.pose.position.x;
        pointCloudSyncData.pos_y = lastSynPoseMsg.pose.pose.position.y;
        pointCloudSyncData.pos_z = lastSynPoseMsg.pose.pose.position.z;

        pointCloudSyncData.qua_x = lastSynPoseMsg.pose.pose.orientation.x;
        pointCloudSyncData.qua_y = lastSynPoseMsg.pose.pose.orientation.y;
        pointCloudSyncData.qua_z = lastSynPoseMsg.pose.pose.orientation.z;
        pointCloudSyncData.qua_w = lastSynPoseMsg.pose.pose.orientation.w;

        /// Platform Pose Process
        pointCloudSyncData.horizontal = syn_platform_pose.pose.pose.position.x;
        pointCloudSyncData.vertical   = syn_platform_pose.pose.pose.position.y;

        /// Publish
        pointCloudSyncDataPub.publish(pointCloudSyncData);
    }
}
/*====================================================================================*/

// 汇总发送各个节点状态
void sendStatusData()
{
    diagnostic_msgs::DiagnosticArray statusMsg;
    statusMsg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus sta;
    sta.name     = "StatusManager";
    sta.message  = "DevicesStatus";

    /// 定位节点
    // 车体位置
    string robotPositionValue = std::to_string(statusManagerPoseMsg.pose.pose.position.x) + "," +
                                std::to_string(statusManagerPoseMsg.pose.pose.position.y) + "," +
                                std::to_string(statusManagerPoseMsg.pose.pose.position.z);

    string robotOrientationValue = std::to_string(statusManagerPoseMsg.pose.pose.orientation.x) + "," +
                                   std::to_string(statusManagerPoseMsg.pose.pose.orientation.y) + "," +
                                   std::to_string(statusManagerPoseMsg.pose.pose.orientation.z) + "," +
                                   std::to_string(statusManagerPoseMsg.pose.pose.orientation.w);

    diagnostic_msgs::KeyValue robotPoseValue;
    robotPoseValue.key   = "Robot Pose Status";
    robotPoseValue.value = robotPositionValue + " " + robotOrientationValue;

    // XAvier Buffer Size
    diagnostic_msgs::KeyValue xavierStatusValue;
    xavierStatusValue.key   = "XAvier Status";
    xavierStatusValue.value = std::to_string(XAVIER_LEVEL_VALUE);

    // 可见光相机
    diagnostic_msgs::KeyValue visualCameraStatusValue;
    visualCameraStatusValue.key   = "Visual Camera Status";
    visualCameraStatusValue.value = std::to_string(VISUAL_CAMERA_STATUS_CODE);
    // 红外相机
    diagnostic_msgs::KeyValue thermalCameraStatusValue;
    thermalCameraStatusValue.key   = "Thermal Camera Status";
    thermalCameraStatusValue.value = std::to_string(THERMAL_CAMERA_STATUS_CODE);

    // 充电桩
    diagnostic_msgs::KeyValue chargeStakeStatusValue;
    chargeStakeStatusValue.key   = "Charge Stake Status";
    chargeStakeStatusValue.value = CHARGE_STAKE_STATUS_CODE;
    // 自动门
    diagnostic_msgs::KeyValue doorStatusValue;
    doorStatusValue.key   = "Door Status";
    doorStatusValue.value = AUTO_DOOR_STATUS_CODE;

    // 云台
    diagnostic_msgs::KeyValue platformStatusValue;
    platformStatusValue.key   = "Platform Status";
    platformStatusValue.value = PLATFORM_HORIZONTAL_ANGLE + " " + PLATFORM_VERTICAL_ANGLE;

    // 测距超声波
    diagnostic_msgs::KeyValue ultrasonicDisStatusValue;
    ultrasonicDisStatusValue.key   = "Ultrasonic Dis Status";
    ultrasonicDisStatusValue.value = ULTRASONIC_DIS_VALUE;
    // 左(右)防跌落超声波
    diagnostic_msgs::KeyValue ultrasonicFallLeftStatusValue;
    ultrasonicFallLeftStatusValue.key   = "Ultrasonic Fall Left Status";
    ultrasonicFallLeftStatusValue.value = ULTRASONIC_FALL_LEFT_VALUE;

    diagnostic_msgs::KeyValue ultrasonicFallRightStatusValue;
    ultrasonicFallRightStatusValue.key   = "Ultrasonic Fall Right Status";
    ultrasonicFallRightStatusValue.value = ULTRASONIC_FALL_RIGHT_VALUE;
    // 防撞条
    diagnostic_msgs::KeyValue antiCollisionBarStatusValue;
    antiCollisionBarStatusValue.key   = "Anti Collision Bar Status";
    antiCollisionBarStatusValue.value = ANTI_COLLISION_BAR_STATUS_CODE;

    // 电压、电流、电量、电机转速
    diagnostic_msgs::KeyValue batteryVoltageStatusValue;
    batteryVoltageStatusValue.key   = "Battery Voltage";
    batteryVoltageStatusValue.value = BATTERY_VOLTAGE_VALUE;

    diagnostic_msgs::KeyValue batteryElectricityStatusValue;
    batteryElectricityStatusValue.key   = "Battery Electricity";
    batteryElectricityStatusValue.value = BATTERY_ELECTRICITY_VALUE;

    diagnostic_msgs::KeyValue batteryVolumeStatusValue;
    batteryVolumeStatusValue.key   = "Battery Volume";
    batteryVolumeStatusValue.value = BATTERY_VOLUME_VALUE;

    diagnostic_msgs::KeyValue robotMotorRevStatusValue;
    robotMotorRevStatusValue.key   = "Robot Motor Rev";
    robotMotorRevStatusValue.value = ROBOT_MOTOR_REV_VALUE;

    // Publish
    sta.values.push_back(robotPoseValue);
    sta.values.push_back(xavierStatusValue);

    sta.values.push_back(visualCameraStatusValue);
    sta.values.push_back(thermalCameraStatusValue);

    sta.values.push_back(chargeStakeStatusValue);
    sta.values.push_back(doorStatusValue);

    sta.values.push_back(platformStatusValue);

    sta.values.push_back(ultrasonicDisStatusValue);
    sta.values.push_back(ultrasonicFallLeftStatusValue);
    sta.values.push_back(ultrasonicFallRightStatusValue);
    sta.values.push_back(antiCollisionBarStatusValue);

    sta.values.push_back(batteryVoltageStatusValue);
    sta.values.push_back(batteryElectricityStatusValue);
    sta.values.push_back(batteryVolumeStatusValue);
    sta.values.push_back(robotMotorRevStatusValue);

    statusMsg.status.push_back(sta);
    statusManagerPub.publish(statusMsg);
}

// 节点心跳
void nodeHeartBeat()
{
    diagnostic_msgs::DiagnosticArray heartbeatMsg;
    heartbeatMsg.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus heartbeatStatus;
    heartbeatStatus.level = 0;
    heartbeatStatus.name  = "/StatusManager";
    heartbeatMsg.status.push_back(heartbeatStatus);
    statusManagerHeartbeatPub.publish(heartbeatMsg);
}

// StatusManager主线程
void statusManager()
{
		ROS_INFO("%s statusManager()", LOG_TITLE.c_str());

    time_t last_t = time(NULL);
    time_t cur_t  = time(NULL);

    while(true)
		{
        // 读取串口数据
        if(is_port_opened)
        {
            port.read_port(&PORT_READ_NOTHING_COUNT,
                           &ROBOT_ROLL_ANGLE, &ROBOT_PITCH_ANGLE, &ROBOT_HEADING_ANGLE,
                           &OBSTACLE_DIS,  &LEFT_FALL_DIS, &RIGHT_FALL_DIS);
            /*
            if(PORT_READ_NOTHING_COUNT == )
            {
                ROS_INFO("%s PORT READ NOTHING [%d] TIMES!", LOG_TITLE.c_str(), PORT_READ_NOTHING_COUNT);
                PORT_READ_NOTHING_COUNT = 0;

                // SENSORS ALARM
            }
            */
        }

        if(cur_t - last_t == 1)
				{
            last_t = time(NULL);

            // time note
            string cur_time = get_system_cur_time();
            ROS_INFO("%s TIME[%s]", LOG_TITLE.c_str(), cur_time.c_str());

            sendStatusData();  // 每秒发送设备状态节点信息
            nodeHeartBeat();   // 向 监控中心 发送 节点心跳

            // TODO: test
            if(is_port_opened)
            {
                ROS_INFO("%s *****SENSORS STATE*****", LOG_TITLE.c_str());
                ROS_INFO("%s RPH: %f %f %f", LOG_TITLE.c_str(), ROBOT_ROLL_ANGLE, ROBOT_PITCH_ANGLE, ROBOT_HEADING_ANGLE);
                ROS_INFO("%s DY7: %i", LOG_TITLE.c_str(), OBSTACLE_DIS);
                ROS_INFO("%s TF6: %i", LOG_TITLE.c_str(), LEFT_FALL_DIS);
                ROS_INFO("%s TF8: %i", LOG_TITLE.c_str(), RIGHT_FALL_DIS);
                ROS_INFO("%s ***********************", LOG_TITLE.c_str());
            }

            /* XAvier休眠提示
            if(is_xavier_sleeping)
            {
                XAVIER_SLEEPING_TIME++;
                ROS_INFO("XAvier Bad Situation, Sleeping 1m(%ds)", XAVIER_SLEEPING_TIME);

                // XAvier休眠结束
                if(XAVIER_SLEEPING_TIME == 60)
                {
                    XAVIER_SLEEPING_TIME = 0;
                    is_xavier_sleeping   = false;
                    xavierControlFun(is_xavier_sleeping);

                    // 车子恢复移动
                    is_robot_stop = false;
                    robotControlFun(is_robot_stop);
                }
            }
            */
				}
				else
				{
						cur_t = time(NULL);
				}
		}
}

// ros初始化线程
void *rosInit(void *arg)
{
    ROS_INFO("%s rosInit()", LOG_TITLE.c_str());

    ros::NodeHandle n = *((ros::NodeHandle*) arg);

		// Subscriber
    statusManagerPoseSub  = n.subscribe<nav_msgs::Odometry>("/robot_pose", 1, statusManagerPoseMessagesCallback);
    isStopReceiveLaserSub = n.subscribe<std_msgs::Bool>("/stop_receive_laser", 1, isStopReceiveLaserMessagesCallback);

		cameraManagerStatusSub = n.subscribe<diagnostic_msgs::DiagnosticArray>("/yida/internal/camera_status", 1, cameraManagerStatusMessagesCallback);
		homeManagerStatusSub   = n.subscribe<diagnostic_msgs::DiagnosticArray>("/yida/internal/charge_status", 1, homeManagerStatusMessagesCallback);

		fuzzyControllerStatusSub    = n.subscribe<diagnostic_msgs::DiagnosticArray>("/yida/internal/fuzzy_status", 1, fuzzyControllerStatusMessagesCallback);
		platformControllerStatusSub = n.subscribe<diagnostic_msgs::DiagnosticArray>("/yida/internal/platform_status", 1, platformControllerStatusMessagesCallback);

    localizationStatusSub = n.subscribe<diagnostic_msgs::DiagnosticArray>("/yida/heartbeat", 1, localizationStatusMessagesCallback);

		// Client
    // TODO: 订阅/yida/settings、/yida/tasks 服务
    statusManagerSettingsClient = n.serviceClient<yd_framework::Settings>("/yida/settings");
    //statusManagerTasksClient    = n.serviceClient<yidamsg::Tasks>("/yida/tasks");

    /// 点云着色同步
    // 车体位置消息、云台位置消息 以及 可见光图片消息 与 红外图片消息
    synPoseSub      = new message_filters::Subscriber<nav_msgs::Odometry>(n, "/robot_pose", 1);
    platformPoseSub = new message_filters::Subscriber<nav_msgs::Odometry>(n, "/yida/yuntai/position", 1);

    // TODO: Dali
    visualImageSub    = new message_filters::Subscriber<sensor_msgs::CompressedImage>(n, "/yida/internal/visible/image_proc/compressed", 1);
    thermalImageSub   = new message_filters::Subscriber<sensor_msgs::CompressedImage>(n, "/yida/internal/infrared/image_proc/compressed", 1);
    // TODO: flirone
    //visualImageSub  = new message_filters::Subscriber<sensor_msgs::Image>(n, "/thermal/image_raw", 1);
    //thermalImageSub = new message_filters::Subscriber<sensor_msgs::CompressedImage>(n, "/thermal/image_proc/compressed", 1);

    // 注册
    messageSync = new message_filters::Synchronizer<pointCloudDataSyncPolicy>(pointCloudDataSyncPolicy(20),
                                                                              *synPoseSub,
                                                                              *platformPoseSub,
                                                                              *visualImageSub,
                                                                              *thermalImageSub);
    messageSync->registerCallback(boost::bind(&messageSyncPolicyCallback, _1, _2, _3, _4));

    /// 点云着色同步2
    // 云台位置消息 以及 可见光图片消息 与 红外图片消息
    _platformPoseSub = new message_filters::Subscriber<nav_msgs::Odometry>(n, "/yida/yuntai/position", 1);

    // TODO: Dali
    _visualImageSub    = new message_filters::Subscriber<sensor_msgs::CompressedImage>(n, "/yida/internal/visible/image_proc/compressed", 1);
    _thermalImageSub   = new message_filters::Subscriber<sensor_msgs::CompressedImage>(n, "/yida/internal/infrared/image_proc/compressed", 1);
    // TODO: flirone
    //_visualImageSub  = new message_filters::Subscriber<sensor_msgs::Image>(n, "/thermal/image_raw", 1);
    //_thermalImageSub = new message_filters::Subscriber<sensor_msgs::CompressedImage>(n, "/thermal/image_proc/compressed", 1);

    _messageSync = new message_filters::Synchronizer<_pointCloudDataSyncPolicy>(_pointCloudDataSyncPolicy(20),
                                                                                *_platformPoseSub,
                                                                                *_visualImageSub,
                                                                                *_thermalImageSub);
    _messageSync->registerCallback(boost::bind(&_messageSyncPolicyCallback, _1, _2, _3));

    /* TODO: Client-Test
    settingsSrv.request.key      = "test_key";
    settingsSrv.request.setValue = "test_value";
    if(statusManagerTasksClient.call(settingsSrv))
    {
        ROS_INFO("XXX set success!");
    }
    else
    {
        ROS_INFO("Can't connect to Settings Server, or XXX set failed!");
    }
    */

    ros::spin();
}

// 主函数
int main(int argc, char** argv)
{
    // TODO: 打开超声波串口
    if(port.open_port())
    {
        is_port_opened = true;
    }
    ros::init(argc, argv, "StatusManager");
	  ros::NodeHandle n;

    // Pub
		statusManagerHeartbeatPub = n.advertise<diagnostic_msgs::DiagnosticArray>("/yida/heartbeat", 1);
    statusManagerPub          = n.advertise<diagnostic_msgs::DiagnosticArray>("/yida/status_updates", 1);

    robotControlPub = n.advertise<std_msgs::Int32>("/robot_position_status", 1);
    //xavierSleepPub  = n.advertise<std_msgs::Bool>("/stop_receive_laser", 1);

    pointCloudSyncDataPub = n.advertise<yidamsg::pointcloud_color>("/yd/pointcloud/vt", 1);

    // Ros Init Thread
    pthread_t rosInitThread = 0;
    pthread_create(&rosInitThread, NULL, rosInit, &n);

		// StatusManager Thread
    thread statusManagerThread(statusManager);
    statusManagerThread.join();

    return 0;
}
