/*
 * @Descripttion: 
 * @version: 
 * @Author: li
 * @Date: 2021-01-10 13:02:30
 * @LastEditors: li
 * @LastEditTime: 2021-02-26 10:19:23
 */
#include "ros/ros.h"
#include "messagesync2.hpp"

#define __app_name__ "sync_node"

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    ROS_INFO("%s node start ...",__app_name__);
    MessageSync2 node("yd_status_manager");
    
    ros::Rate rate(30);

    while (ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}