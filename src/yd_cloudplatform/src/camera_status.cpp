#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/image_encodings.h>
#include <diagnostic_msgs/DiagnosticArray.h>

#include "yd_cloudplatform/camera_status.hpp"
#include "yd_cloudplatform/common.hpp"


std::string g_visible_ip_str = "";
std::string g_infrared_ip_str = "";
float g_visible_time;
float g_infrared_time;

void *get_camera_status(void* args)
{
    FILE *fp_visible = NULL;
    FILE *fp_infrared = NULL;
    while(true)
    {
        char *cmd_visible = SplicingString("ping -c 1 ", g_visible_ip_str.c_str());
        //sprintf(cmd_visible, "pwd 2>/dev/null; echo $?");
        g_visible_time = 0.0;

        if ((fp_visible = popen(cmd_visible , "r")) != NULL)
        {
            char buf[512];
            /*读出管道的数据*/
            string str_result;
            while(fgets(buf,sizeof(buf), fp_visible))
            {
                str_result += buf;
                if((buf[0] == '6') && (buf[1] == '4'))
                {
                    //printf ( "%s", buf);
                    vector<string> temp;
                    SplitString(buf, temp, " ");
                    vector<string> time;
                    SplitString(temp[6], time, "=");
                    //std::cout << temp[6] << std::endl;
                    //std::cout << time[0] << ',' << time[1] << std::endl;
                    g_visible_time = atof(time[1].c_str());
                    //std::cout << "g_visible_time" << g_visible_time << std::endl;
                } 
            }
            /*关闭管道*/
            if(fp_visible != NULL)
            {
                pclose (fp_visible);
            }
        }
        sleep(1);

        char *cmd_infrared = SplicingString("ping -c 1 ", g_infrared_ip_str.c_str());
        //sprintf(cmd_infrared, "pwd 2>/dev/null; echo $?");
        g_infrared_time = 0.0;

        if ((fp_infrared = popen(cmd_infrared , "r")) != NULL)
        {
            char buf[512];
            /*读出管道的数据*/
            string str_result;
            while(fgets(buf,sizeof(buf), fp_infrared))
            {
                str_result += buf;
                if((buf[0] == '6') && (buf[1] == '4'))
                {
                    //printf ( "%s", buf);
                    vector<string> temp;
                    SplitString(buf, temp, " ");
                    vector<string> time;
                    SplitString(temp[6], time, "=");
                    //std::cout << temp[6] << std::endl;
                    //std::cout << time[0] << ',' << time[1] << std::endl;
                    g_infrared_time = atof(time[1].c_str());
                    //std::cout << "g_visible_time" << g_visible_time << std::endl;
                } 
            }
            /*关闭管道*/
            if(fp_infrared != NULL)
            {
                pclose (fp_infrared);
            }
        }
        sleep(1);
    }
}

camera_status::camera_status()
{ 
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;

    private_node_handle.param<std::string>("camera_status_topic", camera_status_topic_str, "/yida/internal/camera_status");
    private_node_handle.param<std::string>("visible_ip", visible_ip_str, "192.168.1.67");
    private_node_handle.param<std::string>("infrared_ip", infrared_ip_str, "192.168.1.67");
    private_node_handle.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");
    g_visible_ip_str = visible_ip_str;
    g_infrared_ip_str = infrared_ip_str;

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    camera_status_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(camera_status_topic_str, 1);

    create_getstatus_thread();
}

camera_status::~camera_status()
{

}

void camera_status::create_getstatus_thread()
{
    pthread_t ros_thread_visible = 0; 
    pthread_create(&ros_thread_visible,NULL,get_camera_status,NULL);
}

void camera_status::update()
{
    //std::cout << "g_infrared_time: " << g_infrared_time << std::endl;
    //std::cout << "g_visible_time: " << g_visible_time << std::endl;

    int level = 0;
    std::string message = "ok";
    std::string hardware_id = "1";
    std::string error_code = "";

    float delay_time = g_visible_time;
    std::string frame_id = "visual";
    std::string name = g_visible_ip_str;
    hardware_id = g_visible_ip_str;
    if(g_visible_time == 0)
    {
        level = 2;
        message = "visible camera is not connect";
        error_code = "10202";
    }
    else
    {
        if(g_visible_time > 100)
        {
            level = 1;
            message = "visible camera latency is high";
            error_code = "10201";
        }
        else
        {
            level = 0;
            message = "visible camera is ok";
            error_code = "10200";
        }
    }
    pub_camerastatus(frame_id, name, level, message, hardware_id, delay_time);
    sleep(0.05);
    pub_heartbeat(level, message, hardware_id,error_code);

    delay_time = g_infrared_time;
    frame_id = "thermal";
    name = g_infrared_ip_str;
    hardware_id = g_infrared_ip_str;
    if(g_infrared_time == 0)
    {
        level = 2;
        message = "infrared camera is not connect";
        error_code = "10204";
    }
    else
    {
        if(g_infrared_time > 100)
        {
            level = 1;
            message = "infrared camera latency is high";
            error_code = "10203";
        }
        else
        {
            level = 0;
            message = "infrared camera is ok";
            error_code = "10200";
        }
    }
    pub_camerastatus(frame_id, name, level, message, hardware_id, delay_time);
    sleep(0.05);
    pub_heartbeat(level, message, hardware_id,error_code);
}

void camera_status::pub_camerastatus(string frame_id, string name, int level, string message, string hardware_id, float delay_time)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();
    log.header.frame_id = frame_id;

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;         // 这里写节点的名字
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    s.message = message;           // 问题描述
    s.hardware_id = hardware_id;   // 硬件信息

    diagnostic_msgs::KeyValue kv;
    kv.key = "delay_time";
    char chFloat[20];	          //用来存放转换后的字符串
    sprintf(chFloat,"%f",delay_time);
    kv.value = chFloat;
    s.values.push_back(kv);

    log.status.push_back(s);
    camera_status_pub_.publish(log);
}

void camera_status::pub_heartbeat(int level, string message, string hardware_id,string error_code)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    //s.name = __app_name__;         // 这里写节点的名字
    s.name = ros::this_node::getName();
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    s.hardware_id = "1";   // 硬件信息
    if (!message.empty())
    {
        s.message = message;       // 问题描述
    }else{
        s.message = "ok";
    }
    diagnostic_msgs::KeyValue kv;
    kv.key = "error_code";
    kv.value = error_code;
    s.values.push_back(kv);
    log.status.push_back(s);

    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    camera_status cam_status;
    ros::Rate rate(1);

    while (ros::ok())
    {
        cam_status.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}

