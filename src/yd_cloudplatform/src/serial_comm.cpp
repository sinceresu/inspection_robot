#include <stdio.h>	    /* Standard input/output definitions */
#include <string.h>	    /* String function definitions */
#include <unistd.h>	    /* UNIX standard function definitions */
#include <fcntl.h>	    /* File control definitions */
#include <errno.h>	    /* Error number definitions */
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <iostream>
#include "yd_cloudplatform/serial_comm.hpp"

using namespace std;

serial_comm::serial_comm()
{

}

serial_comm::~serial_comm()
{

}

int serial_comm::connect(const std::string& deviceName, unsigned int baudRate)
{
	m_fd = open(deviceName.c_str(), O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
	if (m_fd < 0)
	{
		std::cout << "serial_comm: unable to open serial port " << deviceName << "\n";
		return -1;
	}
	set_flags_baudrate(baudRate);
	// tcflush(m_fd, TCIOFLUSH);
	// usleep(1000);
	// tcflush(m_fd, TCIFLUSH);
	// fcntl(m_fd, F_SETFL, FNDELAY);
	return 0;
}

void serial_comm::set_flags_baudrate(unsigned int baudRate)
{
	unsigned int nSpeed = baudRate;
	int nBits = 8;
	char nEvent = 'N';
	int nStop = 1;
	struct termios newtio,oldtio;
	if (tcgetattr( m_fd,&oldtio) != 0)
	{
		perror("SetupSerial 1");
		//return -1;
	}
	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag  |=  CLOCAL | CREAD;
	newtio.c_cflag &= ~CSIZE;

	switch(nBits)
	{
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
		newtio.c_cflag |= CS8;
		break;
	}

	switch(nEvent)
	{
	case 'O':                     //奇校验
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':                     //偶校验
		newtio.c_iflag |= (INPCK | ISTRIP);
		newtio.c_cflag |= PARENB;
		newtio.c_cflag &= ~PARODD;
		break;
	case 'N':                     //无校验
		newtio.c_cflag &= ~PARENB;
		break;
	}

	switch(nSpeed)
	{
	case 2400:
		cfsetispeed(&newtio, B2400);
		cfsetospeed(&newtio, B2400);
		break;
	case 4800:
		cfsetispeed(&newtio, B4800);
		cfsetospeed(&newtio, B4800);
		break;
	case 9600:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	case 115200:
		cfsetispeed(&newtio, B115200);
		cfsetospeed(&newtio, B115200);
		break;
	default:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	}
	if(nStop == 1)
	{
		newtio.c_cflag &=  ~CSTOPB;
	}
	else if(nStop == 2)
	{
		newtio.c_cflag |=  CSTOPB;
	}
	newtio.c_cc[VTIME]  = 0;
	newtio.c_cc[VMIN] = 0;
	tcflush(m_fd,TCIFLUSH);
	if((tcsetattr(m_fd,TCSANOW,&newtio))!=0)
	{
		perror("com set error");
		//return -1;
	}
	printf("set done!\n");
	// return 0;
}

int serial_comm::disconnect()
{
	fcntl(m_fd, F_SETFL, 0);
	tcflush(m_fd, TCIOFLUSH);
	tcsetattr(m_fd, TCSANOW, &m_oldopt);
	::close(m_fd);
	return 0;
}

string serial_comm::deal_cmd(string cmd)
{
	int res ,read_len=0;
	char sCmd[256];
	memset(sCmd, 0, sizeof(sCmd));
	int cmd_size = cmd.size();
	for (int i=0;i<cmd_size;i++)
	{
		sCmd[i]=cmd[i];
	}
	sCmd[cmd_size]=0x0d;
	res = write(m_fd,sCmd,cmd_size+1);
	if(res<0)
	{
		printf("%s() 's write  error!\n",__FUNCTION__);
		// return res;
	}
//	ROS_INFO("I send %s",cmd.c_str());
	// printf("\ni send ");
	// for (int i=0;i<cmd_size+1;i++)
	// {
	// 	printf ("[%02x],",sCmd[i]);
	// }
	char rece[256];
	memset(rece, 0, sizeof(rece));
	res =0;

	usleep(50000);
	do{
		read_len =  read(m_fd,rece,sizeof(rece));
		res+= read_len;
	}while(read_len>0);

	// printf("\ni rece ");
	string out="";

	if (strcmp(rece, sCmd)>0)
	{
		for (int i=0;i<res;i++)
		{
			// printf ("[%02x],",rece[i]);
			if (i>cmd_size)
			{
				out+=rece[i];
			}
		}
	}
	
	// else
	// {
	// 	cout<<"返回值错误"<<endl;
	// 	out="";
	// }
	// printf("\n");
//	ROS_INFO("I receive %s",out.c_str());
	return out;
}

int serial_comm::serial_write(unsigned char *arr_buff, int len)
{
	unsigned char cmd[len] = {0};
	//for(int i = 0; i < len; i++)
	//	printf("[%02x]", arr_buff[i]);
	//std::cout << std::endl;
	memcpy(cmd, arr_buff, len);
	//for(int i = 0; i < len; i++)
	//	printf("[%02x]", cmd[i]);
	//std::cout << std::endl;
	int n_res = write(m_fd, arr_buff, len);
	if(n_res != 7)
		std::cout << "serial write return n_res: " << n_res << std::endl;
	//std::cout << "size is " << sizeof(arr_buff) << " " << sizeof(arr_buff[0])  << " " << sizeof(arr_buff) / sizeof(arr_buff[0]) << " "  << std::endl;
	if(n_res < 0)
	{
		std::cout << "Write Error!" << std::endl;
		return -1;
	}
	memset(cmd, 0, len);
	return 0;
}

unsigned char* serial_comm::serial_read(int n_arrlen)
{
	/*unsigned char buf[10];
    memset(buf, 0, sizeof(buf));
    int i = 0;
    int r = 0;
    unsigned int value = 0;
	usleep(1000);
    while((r=read (m_fd, &buf[i], 1))>0 && i<sizeof(buf)){
        printf("%d,%d\n",i,r);
        i=i+r;
    }*/
	int res ,read_len=0;
	static unsigned char buf[256];
	memset(buf, 0, sizeof(buf));
	res =0;

	usleep(50000);
	do{
		read_len =  read(m_fd,buf,sizeof(buf));
		res += read_len;
	}while(read_len>0);

    //printf ("res=%d,返回值=%02x,%02x,%02x,%02x,%02x,%02x,%02x\n", res,buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6]);
    //printf ("res=%d,返回值=%d,%d,%d,%d,%d,%d,%d\n", res,buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6]);

	return buf;
}