#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <string>
#include <sstream>
#include <vector>

#include "haikang_sdk/HCNetSDK.h"
#include "haikang_sdk/LinuxPlayM4.h"
#include "haikang_sdk/convert.h"
#include "haikang_sdk/cloudplatform_sdk_control.hpp"
#include "yd_cloudplatform/common.hpp"


using namespace std;

std::string g_error_info_str;
unsigned int g_get_info_flag = 1;
unsigned int g_now_xyposition = 0;
unsigned int g_now_zposition = 0;
unsigned int g_action = 0;
unsigned int g_control_type = 0;
unsigned int g_xy_goal = 0;
unsigned int g_z_goal = 0;
unsigned int g_reach_flag = 0;
unsigned int g_xy_reach_flag = 0;
unsigned int g_z_reach_flag = 0;
float g_temperature_c = 0.0;

long lUserID;
long lRealPlayHandle;

WORD DECToHEX_Doc(int x)
{
    if(x > MAX_DEGREE_DEC)
    {
        return 0;
    }
    return (x/1000)*4096 + ((x%1000)/100)*256 + ((x%100)/10)*16 + x%10;
}

WORD HEXToDEC_Doc(int x)
{
    if(x > MAX_DEGREE_HEX)
    {
        return 0;
    }
    return (x/4096*1000) + (x%4096/256*100) + (x%256/16*10) + x%16;
}

//回调透传数据函数的外部实现
void CALLBACK g_fSerialDataCallBack(LONG lSerialHandle, char *pRecvDataBuffer, DWORD dwBufSize, DWORD dwUser)
{
    //…… 处理接收到的透传数据，pRecvDataBuffer中存放接收到的数据
}

void CALLBACK g_cbStateCallback(DWORD dwType, void *lpBuffer, DWORD dwBufLen, void *pUserData)
{
    //…… 处理接收到的透传数据，pRecvDataBuffer中存放接收到的数据
    if (dwType == NET_SDK_CALLBACK_TYPE_DATA)
    {
        LPNET_DVR_THERMOMETRY_UPLOAD lpThermometry = new NET_DVR_THERMOMETRY_UPLOAD;
        memcpy(lpThermometry, lpBuffer, sizeof(*lpThermometry));

        /*NET_DVR_TIME struAbsTime = {0};
        struAbsTime.dwYear = GET_YEAR(lpThermometry->dwAbsTime);
        struAbsTime.dwMonth = GET_MONTH(lpThermometry->dwAbsTime);
        struAbsTime.dwDay = GET_DAY(lpThermometry->dwAbsTime);
        struAbsTime.dwHour = GET_HOUR(lpThermometry->dwAbsTime);
        struAbsTime.dwMinute = GET_MINUTE(lpThermometry->dwAbsTime);
        struAbsTime.dwSecond = GET_SECOND(lpThermometry->dwAbsTime);

        printf("实时测温结果:byRuleID[%d]wPresetNo[%d]byRuleCalibType[%d]byThermometryUnit[%d]byDataType[%d]"
            "dwAbsTime[%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d]\n", lpThermometry->byRuleID, lpThermometry->wPresetNo, 
            lpThermometry->byRuleCalibType,lpThermometry->byThermometryUnit, lpThermometry->byDataType,
            struAbsTime.dwYear, struAbsTime.dwMonth, struAbsTime.dwDay, 
            struAbsTime.dwHour, struAbsTime.dwMinute, struAbsTime.dwSecond); */
        printf("实时测温结果:byRuleID[%d]wPresetNo[%d]byRuleCalibType[%d]byThermometryUnit[%d]byDataType[%d]\n",
             lpThermometry->byRuleID, lpThermometry->wPresetNo, 
            lpThermometry->byRuleCalibType,lpThermometry->byThermometryUnit, lpThermometry->byDataType); 

        if(lpThermometry->byRuleCalibType==0) //点测温
        {
            printf("点测温信息:fTemperature[%f]\n", lpThermometry->struPointThermCfg.fTemperature);
            g_temperature_c = lpThermometry->struPointThermCfg.fTemperature;
        } 

        if((lpThermometry->byRuleCalibType==1)||(lpThermometry->byRuleCalibType==2)) //框/线测温
        {
            printf("框/线测温信息:fMaxTemperature[%f]fMinTemperature[%f]fAverageTemperature[%f]fTemperatureDiff[%f]\n", 
                lpThermometry->struLinePolygonThermCfg.fMaxTemperature,lpThermometry->struLinePolygonThermCfg.fMinTemperature,
                lpThermometry->struLinePolygonThermCfg.fAverageTemperature,lpThermometry->struLinePolygonThermCfg.fTemperatureDiff);
            g_temperature_c = lpThermometry->struLinePolygonThermCfg.fMaxTemperature;
        }       

        if (lpThermometry != NULL)
        {
            delete lpThermometry;
            lpThermometry = NULL;
        }
    }
}

void *get_degree(void* args)
{
    int z_diff_val = 0, xy_diff_val = 0;
    cloudplatform_sdk_control * _this = (cloudplatform_sdk_control *)args;

    while(true)
    {
        sleep(0.1);
        if(_this->_cmd_read_queue.size() < 10)
        {
            pthread_mutex_lock(&_this->_mutex_read);
            std::string get_xydegree = _this->device_id + "/0/0/0";
            _this->_cmd_read_queue.push_back(get_xydegree);
            sleep(0.2);
            std::string get_zdegree = _this->device_id + "/0/1/0";
            _this->_cmd_read_queue.push_back(get_zdegree);
            sleep(0.2);
            pthread_mutex_unlock(&_this->_mutex_read);
        }

        if((g_action == 1) &&   ((g_control_type == 0) || (g_control_type == 1) || (g_control_type == 3) || (g_control_type == 4)))
        {
            if((g_xy_goal != 0) || (g_z_goal != 0)){
                xy_diff_val = g_xy_goal - g_now_xyposition;
                //std::cout << "g_xy_goal: " << g_xy_goal << std::endl;
                //std::cout << "g_now_xyposition: " << g_now_xyposition << std::endl;
                //std::cout << "xy_val: " << xy_diff_val << std::endl;
                if((xy_diff_val > 35900) || (xy_diff_val < -35900))
                    xy_diff_val = 0;
                z_diff_val = g_z_goal - g_now_zposition;
                //std::cout << "g_z_goal: " << g_z_goal << std::endl;
                //std::cout << "g_now_zposition: " << g_now_zposition << std::endl;
                //std::cout << "z_val: " << z_diff_val << std::endl;
                if((z_diff_val > 35900) || (z_diff_val < -35900))
                    z_diff_val = 0;
                if(((z_diff_val < 100) && (z_diff_val > -100)) && ((xy_diff_val < 100) && (xy_diff_val > -100)))
                {
                    g_action = -1;
                    g_xy_reach_flag = 1;
                    g_z_reach_flag = 1;
                }
            }
        }
    }
}

void *cmd_work(void* args)
{
    while(true)
    {
        sleep(0.1);
        std::string work_cmd = "";
        cloudplatform_sdk_control * _this = (cloudplatform_sdk_control *)args;
        if(_this->_cmd_control_queue.size() > 0)
        {
            pthread_mutex_lock(&_this->_mutex_control);
            std::cout << "before execute, _cmd_control_queue size is : " << _this->_cmd_control_queue.size() << std::endl;
            work_cmd = _this->_cmd_control_queue[0];
            _this->_cmd_control_queue.pop_front();
            std::cout << "after execute, _cmd_control_queue size is : " << _this->_cmd_control_queue.size() << std::endl;
            pthread_mutex_unlock(&_this->_mutex_control);

            if(work_cmd != "")
            {
                vector<std::string> cmd_value_strv;
                SplitString(work_cmd, cmd_value_strv, "/");

                int id = atoi(cmd_value_strv[0].c_str());
                int action = atoi(cmd_value_strv[1].c_str());
                int type = atoi(cmd_value_strv[2].c_str());
                int value = atoi(cmd_value_strv[3].c_str());
                std::cout << "id: " << id << ", action: " << action << ", type: " << type << ", value: " << value << std::endl;

                int xy_value = 0;
                int z_value = 0;
                int zoom_value = 0;
                if(type == 3)
                {
                    xy_value = atoi(cmd_value_strv[4].c_str());
                    z_value = atoi(cmd_value_strv[5].c_str());
                }
                if(type == 4)
                {
                    std::cout << "in type 4!" << std::endl;
                    xy_value = atoi(cmd_value_strv[4].c_str());
                    z_value = atoi(cmd_value_strv[5].c_str());
                    zoom_value = atoi(cmd_value_strv[6].c_str());
                }

                g_action = action;
                if((type == 0) || (type == 1))
                {
                    g_control_type = type;
                    if(g_control_type == 0)
                        g_xy_goal = value;
                    else if(g_control_type == 1)
                        g_z_goal = value;
                }
                else if((type == 3) || (type == 4))
                {
                    g_control_type = type;
                    g_xy_goal = xy_value;
                    g_z_goal = z_value;
                }
            }
        }
        else if(_this->_cmd_read_queue.size() > 0)
        {
            pthread_mutex_lock(&_this->_mutex_read);
            //std::cout << "before execute, _cmd_read_queue size is : " << _this->_cmd_read_queue.size() << std::endl;
            work_cmd = _this->_cmd_read_queue[0];
            _this->_cmd_read_queue.pop_front();
            //std::cout << "after execute, _cmd_read_queue size is : " << _this->_cmd_read_queue.size() << std::endl;
            pthread_mutex_unlock(&_this->_mutex_read);
        }
        if(work_cmd != "")
        {
            unsigned int result_value = 0;
            vector<std::string> cmd_value_strv;
            SplitString(work_cmd, cmd_value_strv, "/");

            //action:0 获取  action:1 设置
            //type:0 水平  type:1 垂直  type:2 变倍
            int id = atoi(cmd_value_strv[0].c_str());
            int action = atoi(cmd_value_strv[1].c_str());
            int type = atoi(cmd_value_strv[2].c_str());
            int value = atoi(cmd_value_strv[3].c_str());
            int xy_value = 0;
            int z_value = 0;
            int zoom_value = 0;
            if((action==1) && (type == 3))
            {
                xy_value = atoi(cmd_value_strv[4].c_str());
                z_value = atoi(cmd_value_strv[5].c_str());
            }
            if((action==1) && (type == 4))
            {
                xy_value = atoi(cmd_value_strv[4].c_str());
                z_value = atoi(cmd_value_strv[5].c_str());
                zoom_value = atoi(cmd_value_strv[6].c_str());
            }
            //此处分开设置水平、垂直及变倍值是为兼容通过485遵循pelco-d协议设置的方式
            if(action == 1){
                _this->set_action(id, type, value, xy_value, z_value, zoom_value);
            }
            else if(action == 0){
                result_value = _this->get_action(id, type);
            }
            else if(action == 2){
                if(type == 0){
                    NET_DVR_PTZControl_Other(lUserID, 1, PAN_RIGHT, 0);
                }
                else if(type == 1){
                    NET_DVR_PTZControl_Other(lUserID, 1, PAN_LEFT, 0);
                }
                else if(type == 2){
                    NET_DVR_PTZControl_Other(lUserID, 1, TILT_UP, 0);
                }
                else if(type == 3){
                    NET_DVR_PTZControl_Other(lUserID, 1, TILT_DOWN, 0);
                }

            }
            else if(action == 3){
                NET_DVR_PTZControl_Other(lUserID, 1, PAN_RIGHT, 1);
                NET_DVR_PTZControl_Other(lUserID, 1, PAN_LEFT, 1);
                NET_DVR_PTZControl_Other(lUserID, 1, TILT_UP, 1);
                NET_DVR_PTZControl_Other(lUserID, 1, TILT_DOWN, 1);
            }

            if((action == 0) && (type == 0))
            {
                g_now_xyposition = result_value;
            }
            else if((action == 0) && (type == 1))
            {
                g_now_zposition = result_value;
            }
        }
    }
}

void *get_temperature(void* args)
{
    cloudplatform_sdk_control *_this = (cloudplatform_sdk_control*)args;
    //获取全图温度
    NET_DVR_JPEGPICTURE_WITH_APPENDDATA struJpegWithAppendData = { 0 };
    int i = sizeof(NET_DVR_JPEGPICTURE_WITH_APPENDDATA);
    const int ciPictureBufSize = 2 * 1024 * 1024;//2M
    const int ciVisPictureBufSize = 4 * 1024 * 1024;//2M
    char* ucJpegBuf = new char[ciPictureBufSize];
    char* ucAppendDataBuf = new char[ciPictureBufSize];
    char* ucvisJpegBuf = new char[ciVisPictureBufSize];

    memset(ucJpegBuf, 0, ciPictureBufSize);
    memset(ucAppendDataBuf, 0, ciPictureBufSize);
    memset(ucvisJpegBuf, 0, ciVisPictureBufSize);
    
    struJpegWithAppendData.pJpegPicBuff = ucJpegBuf;
    struJpegWithAppendData.pP2PDataBuff = ucAppendDataBuf;

    while(true)
    {
        //获取热图的SDK接口
        if (!NET_DVR_CaptureJPEGPicture_WithAppendData(lUserID, 2, &struJpegWithAppendData))
        {
            std::cout << "NET_DVR_CaptureJPEGPicture_WithAppendData get hot picture failed!" << std::endl;
            continue;
        }
        //判断抓图数据是否正确
        if (struJpegWithAppendData.dwP2PDataLen != 4 * struJpegWithAppendData.dwJpegPicWidth * struJpegWithAppendData.dwJpegPicHeight)
        {
            std::cout << "NET_DVR_CaptureJPEGPicture_WithAppendData get data error!" << std::endl;
            continue;
        }

        char* g_Buffer = struJpegWithAppendData.pP2PDataBuff;
        unsigned int g_len = struJpegWithAppendData.dwP2PDataLen;

        vector<unsigned char> vc(struJpegWithAppendData.pP2PDataBuff, struJpegWithAppendData.pP2PDataBuff+struJpegWithAppendData.dwP2PDataLen);
        sensor_msgs::Image msg;
        msg.header.stamp = ros::Time::now();
        msg.height = struJpegWithAppendData.dwJpegPicHeight;
        msg.width = struJpegWithAppendData.dwJpegPicWidth;
        msg.data = vc;
        msg.step = struJpegWithAppendData.dwP2PDataLen;
        _this->temperature_buffer_pub_.publish(msg);

        /*int iIndex = 0;
        FILE* fp = NULL;

        float minTemp = 10000.0;
        float maxTemp = -10000.0;

        //std::cout << "struJpegWithAppendData.dwJpegPicHeight:" << struJpegWithAppendData.dwJpegPicHeight << std::endl;
        //std::cout << "struJpegWithAppendData.dwJpegPicWidth: " << struJpegWithAppendData.dwJpegPicWidth << std::endl;

        for (int iWriteHeight = 0; iWriteHeight < struJpegWithAppendData.dwJpegPicHeight; ++iWriteHeight)
        {
            for (int iWriteWidth = 0; iWriteWidth < struJpegWithAppendData.dwJpegPicWidth; ++iWriteWidth)
            {
                float fTemp = *((float*)(struJpegWithAppendData.pP2PDataBuff + iIndex));

                //判断fTemp是否是一个正常值，不是则赋值最大或最小，防止设备崩溃
                fTemp = (9999 < fTemp) ? 9999 : ((-9999 > fTemp) ? -9999 : fTemp);

                minTemp = (minTemp > fTemp) ? fTemp : minTemp;
                maxTemp = (maxTemp > fTemp) ? maxTemp : fTemp;

                printf("iWriteHeight,iWriteWidth:[%d, %d], [%.2f] \n", iWriteHeight, iWriteWidth, fTemp);

                iIndex += 4;
            }
        }*/
    }
}

void *set_infrared_focusmode_thread(void* args)
{
    cloudplatform_sdk_control *_this = (cloudplatform_sdk_control*)args;
    NET_DVR_FOCUSMODE_CFG m_setfocusmode = {0};
    DWORD dwOutBufferSize = 1000;
    DWORD lpBytesReturn = 0;
    if(NET_DVR_GetDVRConfig(lUserID, NET_DVR_GET_FOCUSMODECFG, 2, &m_setfocusmode, dwOutBufferSize, &lpBytesReturn))
    {
        std::cout << "m_setfocusmode.byFocusMode: " << (int)m_setfocusmode.byFocusMode << std::endl;
    }

    m_setfocusmode.byFocusMode = 2;
    //设置红外聚焦模式为半自动
    if (!NET_DVR_SetDVRConfig(lUserID, NET_DVR_SET_FOCUSMODECFG, 2, &m_setfocusmode, sizeof(NET_DVR_FOCUSMODE_CFG))){
        std::cout << "set focus mode error" << std::endl;
    }
    else{
        std::cout << "set focus mode success!" << std::endl;
    }
    sleep(_this->_set_infrared_focus_waittime);
     m_setfocusmode.byFocusMode = 1;
    //设置红外聚焦模式为手动
    if (!NET_DVR_SetDVRConfig(lUserID, NET_DVR_SET_FOCUSMODECFG, 2, &m_setfocusmode, sizeof(NET_DVR_FOCUSMODE_CFG))){
        std::cout << "set focus mode success!" << std::endl;
    }
    else{
        std::cout << "set focus mode success!" << std::endl;
    }
}

void cloudplatform_sdk_control::detect_rect_callback(const yidamsg::Detect_Result& msg)
{
    //设置要进行ptz区域
    NET_DVR_POINT_FRAME  posdata;  
    posdata.xTop = (int)(msg.xmin * 255 / 1920 );   
    // (int)((target_tmp.x + target_tmp.width) / std_cols * 255); 
    int diff_x_value = (int)(msg.xmax - msg.xmin) * 255 / 1920;     
    if(diff_x_value > 2){
        posdata.xBottom = posdata.xTop + diff_x_value;
    }
    else{
        posdata.xBottom = posdata.xTop + 2;
    }       
    posdata.yTop = (int)(msg.ymin * 255 / 1080);
    // (int)((target_tmp.y + target_tmp.height) / std_rows * 255); 
    int diff_y_value = (int)(msg.ymax - msg.ymin) * 255 / 1080;
    if(diff_y_value > 2){
        posdata.yBottom = posdata.yTop + diff_y_value;
    }
    else{
        posdata.yBottom = posdata.yTop + 2;
    }   
    posdata.bCounter = 1;    
    if (!NET_DVR_PTZSelZoomIn_EX(0, 1, &posdata)){
        std::cout << "set pzt zoom failed！" << std::endl;
    }
    else{
        std::cout << "set ptz zoom success!" << std::endl;
    }

}

cloudplatform_sdk_control::cloudplatform_sdk_control()
{ 
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;
    std::string attitude_topic_str,ptz_server_str;
    private_node_handle.param<std::string>("haikang_topic_name", visible_topic_str, "/haikangsdk/image_proc");
    private_node_handle.param<std::string>("sdkcom_dir", sdkcom_dir_str, "/home/yd/workspace/src/yd_camerastream_capture/libs");
    private_node_handle.param<std::string>("heartbeat_topic_string", heartbeat_topic_str, "/yida/heartbeat");
    private_node_handle.param<std::string>("device_ip", m_device_ip, "192.168.1.64");
    private_node_handle.param<std::string>("device_port", m_device_port, "8000");
    private_node_handle.param<std::string>("device_username", m_device_username, "admin");
    private_node_handle.param<std::string>("device_password", m_device_password, "abcd1234");
    private_node_handle.param<std::string>("image_width", m_image_width, "1920");
    private_node_handle.param<std::string>("image_height", m_image_height, "1080");
    private_node_handle.param<bool>("set_infrared_focus", m_set_infrared_focus, true);
    private_node_handle.param<int>("set_infrared_focus_waittime", _set_infrared_focus_waittime, 30);
    private_node_handle.param<std::string>("attitude_topic_str", attitude_topic_str, "/yida/yuntai/position");
    private_node_handle.param<std::string>("ptz_server_str", ptz_server_str, "/yida/internal/platform_cmd");

    char* device_ip = new char[100];
    strcpy(device_ip, m_device_ip.c_str());
    char* device_username = new char[100];
    strcpy(device_username, m_device_username.c_str());
    char* device_password = new char[100];
    strcpy(device_password, m_device_password.c_str());

    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(heartbeat_topic_str, 1);
    cloudplatform_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>("/yida/internal/platform_status", 1);
    isreach_pub_ = node_handle_.advertise<std_msgs::Int32>("/yida/platform_isreach", 1);
    yuntaiposition_pub_ = node_handle_.advertise<nav_msgs::Odometry>(attitude_topic_str, 1);
    temperature_buffer_pub_ = node_handle_.advertise<sensor_msgs::Image>("/yida/internal/temperature", 1);
    
    detectresult_sub_ = node_handle_.subscribe("/detect_rect", 1, &cloudplatform_sdk_control::detect_rect_callback, this);

    cloudplatform_server = node_handle_.advertiseService(ptz_server_str, &cloudplatform_sdk_control::handle_cloudplatform, this);
    gettemperature_server = node_handle_.advertiseService("/yida/internal/get_temperature", &cloudplatform_sdk_control::handle_gettemperature, this);

    level = 0;
    message = "cloutplatform is ok!";
    pthread_mutex_init(&_mutex_read, NULL);
    pthread_mutex_init(&_mutex_control, NULL);

    lUserID = -1;
    lRealPlayHandle = -1;

    NET_DVR_LOCAL_SDK_PATH struComPath = {0};
    std::string comDir = sdkcom_dir_str; // HCNetSDKCom
    sprintf(struComPath.sPath, "%s", comDir.c_str());
    NET_DVR_SetSDKInitCfg(NET_SDK_INIT_CFG_SDK_PATH, &struComPath);

    NET_DVR_Init();
    //登录参数，包括设备地址、登录用户、密码等
    NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
    struLoginInfo.bUseAsynLogin = 0;                    //同步登录方式
    strcpy(struLoginInfo.sDeviceAddress, device_ip);    //设备IP地址
    struLoginInfo.wPort = atoi(m_device_port.c_str());  //设备服务端口
    strcpy(struLoginInfo.sUserName, device_username);   //设备登录用户名
    strcpy(struLoginInfo.sPassword, device_password);   //设备登录密码
    NET_DVR_DEVICEINFO_V40 struDeviceInfo = {0};

    lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfo);
    if (lUserID < 0)
    {
        printf("ip: %s---Login error,  %d\n", m_device_ip.c_str(), NET_DVR_GetLastError());
        g_error_info_str = "login error!";
        NET_DVR_Cleanup();
    }
    else
    {
        /*NET_DVR_PREVIEWINFO struPlayInfo = {0};
        struPlayInfo.hPlayWnd     = 0;       //需要SDK解码时句柄设为有效值，仅取流不解码时可设为空
        struPlayInfo.lChannel     = 1;       //预览通道号
        struPlayInfo.dwStreamType = 0;       //0-主码流，1-子码流，2-码流3，3-码流4，以此类推
        struPlayInfo.dwLinkMode   = 0;       //0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP
        struPlayInfo.bBlocked     = 1;       //0- 非阻塞取流，1- 阻塞取流
        lRealPlayHandle = NET_DVR_RealPlay_V40(lUserID, &struPlayInfo, RealDataCallBack_V30, this);
        if(lRealPlayHandle < 0)
        {
            printf("ip: %s---Login error,  %d\n", m_device_ip.c_str(), NET_DVR_GetLastError());
            g_error_info_str = "get data error!";
            NET_DVR_Logout_V30(lUserID);
            NET_DVR_Cleanup();
        }*/

        /*NET_DVR_PREVIEWINFO struPlayInfo_infrared = {0};
        struPlayInfo_infrared.hPlayWnd     = 0;       //需要SDK解码时句柄设为有效值，仅取流不解码时可设为空
        struPlayInfo_infrared.lChannel     = 2;       //预览通道号
        struPlayInfo_infrared.dwStreamType = 0;       //0-主码流，1-子码流，2-码流3，3-码流4，以此类推
        struPlayInfo_infrared.dwLinkMode   = 0;       //0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP
        struPlayInfo_infrared.bBlocked     = 1;       //0- 非阻塞取流，1- 阻塞取流
        lRealPlayHandle = NET_DVR_RealPlay_V40(lUserID, &struPlayInfo_infrared, RealDataCallBack_Infrared_V30, this);
        if(lRealPlayHandle < 0)
        {
            printf("ip: %s---Login error,  %d\n", m_device_ip.c_str(), NET_DVR_GetLastError());
            g_error_info_str = "get data error!";
            NET_DVR_Logout_V30(lUserID);
            NET_DVR_Cleanup();
        }*/

        NET_DVR_PTZCFG m_ptzcfg;
        NET_DVR_GetPTZProtocol(lUserID, &m_ptzcfg);
        std::cout << "支持协议的个数：" << m_ptzcfg.dwPtzNum << std::endl;
        for(int i = 0; i < m_ptzcfg.dwPtzNum; i++)
        {
            printf("info is %s:", m_ptzcfg.struPtz[i].byDescribe);
            std::cout << "decoder type is :" << m_ptzcfg.struPtz[i].dwType << std::endl;
        }

        //获取水平、垂直及变倍值范围值
        NET_DVR_PTZSCOPE m_ptzScope;
        DWORD dwOutBufferSize = 1000;
        DWORD lpBytesReturn = 0;
        if(NET_DVR_GetDVRConfig(0, NET_DVR_GET_PTZSCOPE, 0, &m_ptzScope, dwOutBufferSize, &lpBytesReturn))
        {
            std::cout << "wPanPosMin:" << m_ptzScope.wPanPosMin << std::endl;
            std::cout << "wPanPosMax:" << m_ptzScope.wPanPosMax << std::endl;
            std::cout << "wTiltPosMin:" << m_ptzScope.wTiltPosMin << std::endl;
            std::cout << "wTiltPosMax:" << m_ptzScope.wTiltPosMax << std::endl;
            std::cout << "wZoomPosMin:" << m_ptzScope.wZoomPosMin << std::endl;
            std::cout << "wZoomPosMax:" << m_ptzScope.wZoomPosMax << std::endl;
            std::cout << "lpBytesReturn:" << lpBytesReturn << std::endl;
        }
        
        //获取当前水平、垂直及变倍值
        NET_DVR_PTZPOS m_ptzPosNow;
        if(NET_DVR_GetDVRConfig(0, NET_DVR_GET_PTZPOS, 0, &m_ptzPosNow, dwOutBufferSize, &lpBytesReturn))
        {
            std::cout << "wPanPos:" << m_ptzPosNow.wPanPos << std::endl;
            std::cout << "wTiltPos:" << m_ptzPosNow.wTiltPos << std::endl;
            std::cout << "wZoomPos:" << m_ptzPosNow.wZoomPos << std::endl;
            std::cout << "lpBytesReturn:" << lpBytesReturn << std::endl;
        }

        DWORD dwChannel = 2;  //热成像通道
        char *m_pOutBuf = new char[ISAPI_OUT_LEN];
        memset(m_pOutBuf, 0, ISAPI_OUT_LEN);
        char *m_pStatusBuf = new char[ISAPI_STATUS_LEN];
        memset(m_pStatusBuf, 0, ISAPI_STATUS_LEN);

        //测温能力集
        NET_DVR_STD_ABILITY struStdAbility = {0};
        struStdAbility.lpCondBuffer = &dwChannel;
        struStdAbility.dwCondSize = sizeof(DWORD);
        struStdAbility.lpOutBuffer    = m_pOutBuf;
        struStdAbility.dwOutSize      = ISAPI_OUT_LEN;
        struStdAbility.lpStatusBuffer = m_pStatusBuf;
        struStdAbility.dwStatusSize   = ISAPI_STATUS_LEN;

        if(!NET_DVR_GetSTDAbility(lUserID,NET_DVR_GET_THERMAL_CAPABILITIES,&struStdAbility)){
            printf("net_dvr_get_thermal_capabilities failed, error code: %d\n", NET_DVR_GetLastError());
        }
        else{
            printf("net_dvr_get_thermal_capabilities is successful!\n");
        }

        if(!NET_DVR_GetSTDAbility(lUserID,NET_DVR_GET_THERMOMETRY_BASICPARAM_CAPABILITIES,&struStdAbility)){
            printf("net_dvr_thermometry_basicparam_capabilities failed, error code: %d\n", NET_DVR_GetLastError());
        }
        else{
            printf("net_dvr_thermometry_basicparam_capabilities is successful!\n");
        }

        if(!NET_DVR_GetSTDAbility(lUserID,NET_DVR_GET_THERMOMETRY_SCENE_CAPABILITIES,&struStdAbility)){
            printf("net_dvr_get_thermometry_scene_capabilities failed, error code: %d\n", NET_DVR_GetLastError());
        }
        else{
            printf("net_dvr_get_thermometry_scene_capabilities is successful!\n");
        }
        
        if(!NET_DVR_GetSTDAbility(lUserID,NET_DVR_GET_THERMOMETRY_MODE_CAPABILITIES,&struStdAbility)){
            printf("net_dvr_get_thermometry_mode_capabilities failed, error code: %d\n", NET_DVR_GetLastError());
        }
        else{
            printf("net_dvr_get_thermometry_mode_capabilities is successful!\n");
        }

        //设置测温模式
        NET_DVR_STD_CONFIG m_stdset_config = {0};
        m_stdset_config.lpCondBuffer = &dwChannel;
        m_stdset_config.dwCondSize = sizeof(DWORD);
        NET_DVR_THERMOMETRY_MODE m_mode_info;
        m_mode_info.dwSize = sizeof(m_mode_info);
        m_mode_info.byMode = 1;
        m_stdset_config.lpInBuffer = &m_mode_info;
        m_stdset_config.dwInSize = sizeof(m_mode_info);
        if(!NET_DVR_SetSTDConfig(lUserID, NET_DVR_SET_THERMOMETRY_MODE, &m_stdset_config)){
            printf("NET_DVR_SET_MODE failed, error code: %d\n", NET_DVR_GetLastError());
        }
        else{
        }

        create_work_thread();
        create_getdegree_thread();
        create_gettemperature_thread();
        if(m_set_infrared_focus == true){
            create_set_infrares_focusmode_thread();
        }
    }
}

cloudplatform_sdk_control::~cloudplatform_sdk_control()
{

}

bool cloudplatform_sdk_control::set_action(int id, int type, int value, int xy_value, int z_value, int zoom_value)
{
    //设置水平、垂直、变倍
    float z_degree = 0.0;
    float xy_degree = 0.0;
    float focus_adjust = 0.0;//焦距
    NET_DVR_PTZPOS m_ptzPos;
    if(type == 0){
        m_ptzPos.wAction = 2;
        xy_degree = value;
    }
    else if(type == 1){
        m_ptzPos.wAction = 3;
        z_degree = 36000 - value;
    }
    else if(type == 2){
        m_ptzPos.wAction = 4;
        focus_adjust = value;   
    }
    else if(type == 3){
        m_ptzPos.wAction = 5;
        xy_degree = xy_value;
        z_degree = 36000 - z_value;
    }
    else if(type == 4){
        m_ptzPos.wAction = 1;
        xy_degree = xy_value;
        z_degree = 36000 - z_value;
        focus_adjust = zoom_value;
    }
    int xy_idegree = xy_degree / 10;
    int z_idegree = z_degree / 10;
    int focus_iadjust = focus_adjust / 100;
    std::cout << "wPanPos:" << xy_idegree << std::endl;
    std::cout << "wTiltPos:" << z_idegree << std::endl;
    std::cout << "wZoomPos:" << focus_iadjust << std::endl;
    //进制转换
    m_ptzPos.wPanPos = DECToHEX_Doc(xy_idegree);
    m_ptzPos.wTiltPos = DECToHEX_Doc(z_idegree);
    m_ptzPos.wZoomPos = DECToHEX_Doc(focus_iadjust);
    //设置PTZ参数
    if (!NET_DVR_SetDVRConfig(0, NET_DVR_SET_PTZPOS, 0, &m_ptzPos, sizeof(NET_DVR_PTZPOS))){
        std::cout << "set error!" << std::endl;
    }
}

int cloudplatform_sdk_control::get_action(int id, int type)
{
    //获取当前水平、垂直及变倍值
    NET_DVR_PTZPOS m_ptzPosNow;
    DWORD dwOutBufferSize = 1000;
    DWORD lpBytesReturn = 0;
    if(NET_DVR_GetDVRConfig(0, NET_DVR_GET_PTZPOS, 0, &m_ptzPosNow, dwOutBufferSize, &lpBytesReturn))
    {
        //std::cout << "wPanPos:" << HEXToDEC_Doc(m_ptzPosNow.wPanPos)*10 << std::endl;
        //std::cout << "wTiltPos:" << (3600-HEXToDEC_Doc(m_ptzPosNow.wTiltPos))*10 << std::endl;
        //std::cout << "wZoomPos:" << HEXToDEC_Doc(m_ptzPosNow.wZoomPos) << std::endl;
        //std::cout << "lpBytesReturn:" << lpBytesReturn << std::endl;
    }

    if(type == 0){
        return HEXToDEC_Doc(m_ptzPosNow.wPanPos)*10;
    }
    else if(type == 1){
        return (3600-HEXToDEC_Doc(m_ptzPosNow.wTiltPos))*10;
    }
    else if(type == 2){
        return HEXToDEC_Doc(m_ptzPosNow.wZoomPos);   
    }

    return 0;
}

bool cloudplatform_sdk_control::handle_cloudplatform(yd_cloudplatform::CloudPlatControl::Request &req, yd_cloudplatform::CloudPlatControl::Response &res)
{
    pthread_mutex_lock(&_mutex_control);
    std::stringstream ss;
	ss.clear();
	ss.str("");
	ss << req.id << "/" << req.action << "/" << req.type << "/" << req.value;
    if(req.type == 3){
        ss << "/" << req.allvalue[0] << "/" << req.allvalue[1];
    }
    if(req.type == 4){
        std::cout << "1111111111111" << std::endl;
        ss << "/" << req.allvalue[0] << "/" << req.allvalue[1] << "/" << req.allvalue[2];
    }
    std::string s_cmd = ss.str();
    std::cout << s_cmd << std::endl;
    _cmd_control_queue.push_back(s_cmd);
    pthread_mutex_unlock(&_mutex_control);
	return true;
}

bool cloudplatform_sdk_control::handle_gettemperature(yd_cloudplatform::GetTemperature::Request &req, yd_cloudplatform::GetTemperature::Response &res)
{
    //设置预置位
    NET_DVR_PTZPreset_Other(lUserID, 2, SET_PRESET, 93);
    NET_DVR_THERMOMETRY_PRESETINFO m_set_therpreset_info = {0};
    m_set_therpreset_info.dwSize = sizeof(m_set_therpreset_info);
    m_set_therpreset_info.wPresetNo = 93;

    if(req.spot_x.size() == 1){
        /*
		int row = req.spot_x[0];
		int col = req.spot_y[0];
		res.temperature_c = temperature_[row][col];*/
        NET_DVR_THERMOMETRY_PRESETINFO_PARAM set_point_param = {0};
        NET_VCA_POINT point = {0};
        point.fX = 0.4;
        point.fY = 0.6;
        set_point_param.byEnabled = 1;
        set_point_param.byRuleID = 2;
        set_point_param.wDistance = 6;
        set_point_param.fEmissivity = 0.97;
        set_point_param.byDistanceUnit = 0;
        set_point_param.byRuleCalibType = 0;
        strcpy(set_point_param.szRuleName, "set by code");
        set_point_param.struPoint = point;
        m_set_therpreset_info.struPresetInfo[0] = set_point_param;
	}
	else if(req.spot_x.size() == 2){
		/*float temperature_max = temperature_[row_begin][col_begin];
		for(unsigned short i = row_begin; i <= row_end; i++){
			for(unsigned short j = col_begin; j <= col_end; j++){
				if(temperature_max < temperature_[i][j])
				{
					temperature_max = temperature_[i][j];
				}
			}
		}
		res.temperature_c = temperature_max;*/
        //设置预置点关联测温信息配置
        NET_DVR_THERMOMETRY_PRESETINFO_PARAM set_polygon_param = {0};
        NET_VCA_POLYGON set_polygon = {0};
        NET_VCA_POINT   set_struPos = {0};
        set_struPos.fX = 0.4;
        set_struPos.fY = 0.4;
        set_polygon.struPos[0] = set_struPos;
        set_struPos.fX = 0.4;
        set_struPos.fY = 0.6;
        set_polygon.struPos[1] = set_struPos;
        set_struPos.fX = 0.6;
        set_struPos.fY = 0.6;
        set_polygon.struPos[2] = set_struPos;
        set_struPos.fX = 0.6;
        set_struPos.fY = 0.4;
        set_polygon.struPos[3] = set_struPos;
        set_polygon.dwPointNum = 4;

        set_polygon_param.byEnabled = 1;
        set_polygon_param.byRuleID = 1;
        set_polygon_param.wDistance = 6;
        set_polygon_param.fEmissivity = 0.97;
        set_polygon_param.byDistanceUnit = 0;
        set_polygon_param.byRuleCalibType = 1;
        strcpy(set_polygon_param.szRuleName, "set by code");
        set_polygon_param.struRegion = set_polygon;
        m_set_therpreset_info.struPresetInfo[1] = set_polygon_param;
	}
    NET_DVR_THERMOMETRY_COND m_ther_set_cond;
    m_ther_set_cond.dwSize = sizeof(m_ther_set_cond);
    m_ther_set_cond.dwChannel = 2;
    m_ther_set_cond.wPresetNo = 93;

    NET_DVR_STD_CONFIG m_settype_config = {0};
    m_settype_config.lpCondBuffer = &m_ther_set_cond;
    m_settype_config.dwCondSize = sizeof(m_ther_set_cond);
    m_settype_config.lpInBuffer = &m_set_therpreset_info;
    m_settype_config.dwInSize = sizeof(m_set_therpreset_info);
    m_settype_config.byDataType = 0;

    if(!NET_DVR_SetSTDConfig(lUserID, NET_DVR_SET_THERMOMETRY_PRESETINFO, &m_settype_config)){
        printf("NET_DVR_SET_POLYGON failed, error code: %d\n", NET_DVR_GetLastError());
    }
    else{
    }

    //获取预置点关联测温信息配置
    NET_DVR_STD_CONFIG m_std_config = {0};
    NET_DVR_THERMOMETRY_COND m_ther_cond;
    m_ther_cond.dwSize = sizeof(m_ther_cond);
    m_ther_cond.dwChannel = 2;
    m_ther_cond.wPresetNo = 1;
    m_std_config.lpCondBuffer = &m_ther_cond;
    m_std_config.dwCondSize = sizeof(m_ther_cond);
    NET_DVR_THERMOMETRY_PRESETINFO m_therpreset_info;
    m_therpreset_info.dwSize = sizeof(m_therpreset_info);
    m_therpreset_info.wPresetNo = 1;
    m_std_config.lpOutBuffer = &m_therpreset_info;
    m_std_config.dwOutSize = sizeof(m_therpreset_info);
    NET_DVR_THERMOMETRY_PRESETINFO_PARAM m_info = {0};
    if(!NET_DVR_GetSTDConfig(lUserID, NET_DVR_GET_THERMOMETRY_PRESETINFO, &m_std_config)){
        printf("NET_DVR_GET_THERMOMETRY_MODE_CAPABILITIES failed, error code: %d\n", NET_DVR_GetLastError());
    }
    else{
        std::cout << "lpOutBuffer size:" << sizeof(m_std_config.lpOutBuffer) << std::endl;
        std::cout << "m_info size:" << sizeof(m_info) << std::endl;
        //for(int i; i < strlen((char*)m_std_config->lpOutBuffer)/sizeof(m_info); i++)
        //{
        //    std::cout << "1111" << std::endl;
        //}
        printf("m_std_config info value is %d.\n", m_therpreset_info.struPresetInfo[0].byRuleCalibType);
        printf("m_std_config info value is %d.\n", m_therpreset_info.struPresetInfo[1].byRuleCalibType);
        printf("m_std_config info value is %d.\n", m_therpreset_info.struPresetInfo[2].byRuleCalibType);
        printf("m_std_config info value is %d.\n", m_therpreset_info.struPresetInfo[3].byRuleCalibType);
    }
    //调用预置点位
    NET_DVR_PTZPreset_Other(lUserID, 2, GOTO_PRESET, 93);

    NET_DVR_REALTIME_THERMOMETRY_COND m_realtime_thermometry = {0};
    m_realtime_thermometry.dwSize = sizeof(m_realtime_thermometry);
    m_realtime_thermometry.dwChan = 2;
    m_realtime_thermometry.byRuleID = 0;
    LONG lHandle = NET_DVR_StartRemoteConfig(lUserID, NET_DVR_GET_REALTIME_THERMOMETRY , &m_realtime_thermometry,
    sizeof(m_realtime_thermometry), g_cbStateCallback, NULL);

    sleep(3);  //等待一段时间，接收实时测温结果
    res.temperature_c = g_temperature_c;

    //关闭长连接配置接口所创建的句柄，释放资源
    if(!NET_DVR_StopRemoteConfig(lHandle))
    {
        printf("NET_DVR_StopRemoteConfig failed, error code: %d\n", NET_DVR_GetLastError());
    }

	/*if(req.spot_x.size() == 1){
		int row = req.spot_x[0];
		int col = req.spot_y[0];
		res.temperature_c = temperature_[row][col];
	}
	else if(req.spot_x.size() == 2){
		float temperature_max = temperature_[row_begin][col_begin];
		for(unsigned short i = row_begin; i <= row_end; i++){
			for(unsigned short j = col_begin; j <= col_end; j++){
				if(temperature_max < temperature_[i][j])
				{
					temperature_max = temperature_[i][j];
				}
			}
		}
		res.temperature_c = temperature_max;
	}
	std::cout << "res.temperature_c = " << res.temperature_c << std::endl;
    res.temperature_c = 22.6;*/
	return true;
}

void cloudplatform_sdk_control::create_getdegree_thread()
{
    pthread_t ros_thread = 0; 
    pthread_create(&ros_thread,NULL,get_degree,(void*)this);
}

void cloudplatform_sdk_control::create_work_thread()
{
    pthread_t ros_thread = 1; 
    pthread_create(&ros_thread,NULL,cmd_work,(void*)this);
}

void cloudplatform_sdk_control::create_gettemperature_thread()
{
    pthread_t ros_thread = 2;
    pthread_create(&ros_thread,NULL,get_temperature,(void*)this);
}

void cloudplatform_sdk_control::create_set_infrares_focusmode_thread()
{
    pthread_t ros_thread = 3;
    pthread_create(&ros_thread,NULL,set_infrared_focusmode_thread,(void*)this);
}

void cloudplatform_sdk_control::update()
{
    pub_heartbeat(level, message);

    nav_msgs::Odometry msg_pos;
	msg_pos.header.stamp = ros::Time::now();
	msg_pos.pose.pose.position.x = g_now_xyposition;
	msg_pos.pose.pose.position.z = g_now_zposition;
	yuntaiposition_pub_.publish(msg_pos);

    if((g_xy_reach_flag == 1) && (g_z_reach_flag == 1))
    {
        g_xy_reach_flag = 0;
        g_z_reach_flag = 0;
        g_reach_flag = 1;
        std_msgs::Int32 res_msg;
		res_msg.data = 1;
		isreach_pub_.publish(res_msg);
    }

    diagnostic_msgs::DiagnosticArray status_msg;
    status_msg.header.stamp = ros::Time::now();
    diagnostic_msgs::DiagnosticStatus status_diagnostic;
    status_diagnostic.name = "platform_angle";
    status_diagnostic.message = std::to_string(g_now_xyposition) + " " + std::to_string(g_now_zposition);
    status_msg.status.push_back(status_diagnostic);
    cloudplatform_pub_.publish(status_msg);
}

void cloudplatform_sdk_control::pub_heartbeat(int level, string message)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;             // 这里写节点的名字
    s.level = level;                   // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;           // 问题描述
    }
    s.hardware_id = "cloutplatform";   // 硬件信息
    log.status.push_back(s);
    sleep(0.1);
    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    cloudplatform_sdk_control hsdkcap;
    ros::Rate rate(25);

    while (ros::ok())
    {
        hsdkcap.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
