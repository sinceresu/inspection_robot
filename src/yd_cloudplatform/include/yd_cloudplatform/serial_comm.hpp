#ifndef __SERIAL_COMM_H__
#define __SERIAL_COMM_H__

#include <string>
#include <cstddef>
#include <unistd.h>
#include <termios.h>
#include "ros/ros.h"

#define OPEN_SUCCESS      0
#define OPEN_FAILED       -1
#define RX_BUFFER_SIZE    4096
#define DEFAULT_BAUD_RATE 9600
using namespace std;

class serial_comm
{
	public:
		 serial_comm();
		 ~serial_comm();
		int connect(const std::string& deviceName, unsigned int baudRate = DEFAULT_BAUD_RATE);
		int disconnect();
		int serial_write(unsigned char *arr_buff, int len);
		unsigned char* serial_read(int n_arrlen);

	private:
		void set_flags_baudrate(unsigned int baudRate);
		string deal_cmd(string cmd);

	protected:
		unsigned char m_rxBuffer[RX_BUFFER_SIZE];
		int m_fd;
		int m_rxCount;
		float* m_ranges;
		unsigned int m_rangesCount;
		struct termios m_oldopt;
};

#endif //__SERIAL_COMM_H__
