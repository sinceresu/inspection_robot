#ifndef __CLOUDPLATFORM_CONTROL_H__
#define __CLOUDPLATFORM_CONTROL_H__

#include <iostream>
#include <stdio.h>
#include <deque>

#include <diagnostic_msgs/DiagnosticArray.h>
#include <std_msgs/Int32.h>
#include <nav_msgs/Odometry.h>
#include <yd_cloudplatform/CloudPlatControl.h>
#include "serial_comm.hpp"

#define __app_name__ "cloudplat_control"
#define LENGTH 7

using namespace std;

class cloudplatform_control
{
private:
    ros::NodeHandle node_handle_, private_node_handle_;
    serial_comm m_obj_cloudplat;
    string platform1_str, platform2_str, platform3_str, heartbeat_topic_str, band_rate;
    int level;
    string message;

    ros::Publisher heartbeat_pub_;
    ros::Publisher cloudplatform_pub_;
    ros::Publisher isreach_pub_;
    ros::Publisher yuntaiposition_pub_;
    ros::ServiceServer cloudplatform_server;

public:
    string device_id;
    deque<string> _cmd_control_queue, _cmd_read_queue;
    pthread_mutex_t _mutex_read, _mutex_control;

private:
    void create_getdegree_thread();
    void create_work_thread();
    
public:
    cloudplatform_control(const ros::NodeHandle &node_handle, const ros::NodeHandle &private_node_handle);
    ~cloudplatform_control();
    bool handle_cloudplatform(yd_cloudplatform::CloudPlatControl::Request &req, yd_cloudplatform::CloudPlatControl::Response &res);
    void update();
    void pub_heartbeat(int level, string message);
    unsigned int get_sumcs(unsigned char cmdin[]);
    unsigned int cmd_serialcontrol(unsigned char cmd_cloudplat[], unsigned int size, unsigned int action, unsigned int type);
};

#endif