#ifndef __CAMERA_STATUS_H__
#define __CAMERA_STATUS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <setjmp.h>
#include <errno.h>
#include <vector>
#include <iostream>

#include <ros/ros.h>
#include <diagnostic_msgs/DiagnosticArray.h>

using namespace cv;
using namespace std;

#define __app_name__ "camera_status_node"
 
class camera_status
{
private:
    std::string camera_status_topic_str, visible_ip_str, infrared_ip_str, heartbeat_topic_str;
    ros::Publisher camera_status_pub_;
    ros::Publisher heartbeat_pub_;
private:
    void create_getstatus_thread();

public:
    camera_status();
    ~camera_status();
    void update();
    void pub_camerastatus(string frame_id, string name, int level, string message, string hardware_id, float delay_time);
    void pub_heartbeat(int level, string message, string hardware_id,string error_code);
};

#endif