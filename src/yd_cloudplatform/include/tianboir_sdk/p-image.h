#ifndef _P_IMAGE_HEADER_
#define _P_IMAGE_HEADER_

#include "common-def.h"
#include <string>

#if defined(_WIN32) || defined(_WIN64)

#ifdef PIMAGE_EXPORTS
#define P_IMAGE_API __declspec(dllexport) bool __stdcall
#else
#define P_IMAGE_API __declspec(dllimport) bool __stdcall
#endif

#else
#define P_IMAGE_API bool
#endif

#ifdef __cplusplus
extern "C"
{
#endif

P_IMAGE_API p_image_is_valid_file(const std::string &str_file_name);
P_IMAGE_API p_image_is_valid_data(unsigned char *pc_data, unsigned int &ui_data_len);
P_IMAGE_API p_image_delete_temperature_data(const std::string &str_file_name);

P_IMAGE_API p_image_init(THANDLE &h);
P_IMAGE_API p_image_uninit(THANDLE &h);
P_IMAGE_API p_image_load_from_file(THANDLE h, const std::string &str_file_name);
P_IMAGE_API p_image_load_from_buffer(THANDLE h, unsigned char *pc_data, unsigned int &ui_data_len);

P_IMAGE_API p_image_get_version(THANDLE h, unsigned short &w_version);
P_IMAGE_API p_image_get_storage_time(THANDLE h, std::string &str_time);
P_IMAGE_API p_image_get_user_data(THANDLE h, unsigned char *pby_data, unsigned int &dw_data_size);
P_IMAGE_API p_image_get_sound_data(THANDLE h, unsigned char *pby_data, unsigned int &dw_data_size);

#ifdef __cplusplus
}
#endif

#endif //< _P_IMAGE_HEADER_
