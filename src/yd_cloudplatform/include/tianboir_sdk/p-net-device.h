#ifndef _P_NET_DEVICE_HEADER_
#define _P_NET_DEVICE_HEADER_

#include "common-def.h"
#include <string>

#if defined(_WIN32) || defined(_WIN64)

#ifdef PNETDEVICE_EXPORTS
#define P_ND_API __declspec(dllexport) bool __stdcall
#else
#define P_ND_API __declspec(dllimport) bool __stdcall
#endif

#else
#define P_ND_API bool
#endif

#ifdef __cplusplus
extern "C"
{
#endif

P_ND_API p_nd_init(THANDLE &h);
P_ND_API p_nd_uninit(THANDLE &h);

P_ND_API p_nd_connect(THANDLE h, const std::string &str_ip);
P_ND_API p_nd_disconnect(THANDLE h);
P_ND_API p_nd_capture_one_frame(THANDLE h);

P_ND_API p_nd_temperature_correct(THANDLE h);
P_ND_API p_nd_set_len_and_meas_range(THANDLE h, LenType type, MeasRange range);
P_ND_API p_nd_is_correcting(THANDLE h, bool &b_is_correcting);

typedef void(*p_cb_one_frame_received)(void *p_user_data, unsigned short *pw_ad, unsigned short w_width, unsigned short w_height);
P_ND_API p_nd_set_cb_one_frame_received(THANDLE h, p_cb_one_frame_received p_cb, void *p_user_data);

typedef void(*p_cb_on_disconnected)(void *p_user_data);
P_ND_API p_nd_set_cb_on_disconnected(THANDLE h, p_cb_on_disconnected p_cb, void *p_user_data);

typedef void(*p_cb_on_detect_sun)(void *p_user_data, bool b_sun);
P_ND_API p_nd_set_cb_on_detect_sun(THANDLE h, p_cb_on_detect_sun p_cb, void *p_user_data);

typedef void(*p_cb_enable_detect_sun_status_changed)(void *p_user_data, bool b_enable_detect_sun_status_changed);
P_ND_API p_nd_set_cb_enable_detect_sun_status_changed(THANDLE h, p_cb_enable_detect_sun_status_changed p_cb, void *p_user_data);

P_ND_API p_nd_get_ip_access_mode(THANDLE h, bool &b_static_ip);
P_ND_API p_nd_get_len_adjust_method(THANDLE h, LenAdjustMethod &method);
P_ND_API p_nd_motor_forward(THANDLE h);
P_ND_API p_nd_motor_reverse(THANDLE h);
P_ND_API p_nd_motor_stop(THANDLE h);
P_ND_API p_nd_zoom_motor_forward(THANDLE h);
P_ND_API p_nd_zoom_motor_backword(THANDLE h);
P_ND_API p_nd_zoom_motor_stop(THANDLE h);
P_ND_API p_nd_shutter_down(THANDLE h);
P_ND_API p_nd_shutter_up(THANDLE h);
P_ND_API p_nd_get_battery_level(THANDLE h, unsigned int &ui_battery_level);
P_ND_API p_nd_laser_on(THANDLE h);
P_ND_API p_nd_laser_off(THANDLE h);
P_ND_API p_nd_is_support_len_pos(THANDLE h, bool &b_support);
P_ND_API p_nd_set_len_pos(THANDLE h, unsigned short w_pos);
P_ND_API p_nd_get_len_pos(THANDLE h, unsigned short &w_pos);
P_ND_API p_nd_reboot(THANDLE h);
P_ND_API p_nd_get_machine_sys_version(THANDLE h, std::string &str);
P_ND_API p_nd_get_machine_hardware_version(THANDLE h, std::string &str);
P_ND_API p_nd_get_machine_software_version(THANDLE h, std::string &str);
P_ND_API p_nd_get_machine_sn(THANDLE h, std::string &str);
P_ND_API p_nd_get_temperature_snap(THANDLE h, unsigned char *pc_data, unsigned int &ui_data_len);
P_ND_API p_nd_enable_detect_sun(THANDLE h, bool b_enable);
P_ND_API p_nd_is_enabled_detect_sun(THANDLE h, bool &b_enable);
P_ND_API p_nd_auto_focus(THANDLE h, unsigned short w_left_top_x, unsigned short w_left_top_y, unsigned short w_right_bottom_x, unsigned short w_right_bottom_y );
/* by_frep ȡֵ 1 2 3 4 5 8 16 */
P_ND_API p_nd_set_frequency(THANDLE h, unsigned char by_freq);

#ifdef __cplusplus
}
#endif

#endif //< _P_NET_DEVICE_HEADER_
