#ifndef _P_COMMON_DEF_HEADER_
#define _P_COMMON_DEF_HEADER_

#ifndef _DEFINE_THANDLE_
typedef void *THANDLE;
#define _DEFINE_THANDLE_
#endif


//< 绑定x的值不超出mn和mx的范围
#define PBOUND( x, mn, mx )	( ( x) < (mn) ? (mn) : ( ( x) > (mx) ? (mx) : (x ) ) )
#define PMIN( x, y ) ( ( x ) < ( y ) ? ( x ) : ( y ) )	//< 找出x和y中的较小值
#define PMAX( x, y ) ( ( x ) > ( y ) ? ( x ) : ( y ) )	//< 找出x和y中的较大值

#ifndef _P_UINT64_
#define _P_UINT64_
#if ( defined( linux ) || defined( unix ) || defined( __linux__ ) || defined( __unix__ ) )
#define TUINT64 unsigned long long
#else
#define TUINT64 unsigned __int64
#endif
#endif //< _P_UINT64_


#ifndef _ENUM_DETECTOR_RESOLUTION_
#define _ENUM_DETECTOR_RESOLUTION_
enum DetectorResolution{
    DR80x60 = 0,
    DR160x120 = 1,
    DR320x240 = 2,
    DR640x480 = 3,
    DR1024x768 = 4,
    DRMax = 5,

    DRError = 0xFFFFFFFF,
};
#endif


//#ifndef _ENUM_FPGA_OUTPUT_TYPE_
//#define  _ENUM_FPGA_OUTPUT_TYPE_
//enum FPGAOutputType{
//	FOTOriginal = 0,
//    FOTCorrected = 1,
//    FOTColorBar = 2,
//    FOTMax = 3,

//    FOTError = 0xFFFFFFFF,
//};
//#endif

#ifndef _ENUM_FPGA_OUTPUT_TYPE_
#define  _ENUM_FPGA_OUTPUT_TYPE_
enum FPGAOutputType{
    FOTK = 0,
    FOTB = 1,
    FOTOriginal = 2,
    FOTColorBar = 3,
    FOTCorrectedWithBadPts = 4,
    FOTCorrected = 5,
    FOTLowAD = 6,
    FOTHighAD = 7,
    FOTBadPts = 8,
    FOTMax,

    FOTError = 0xFFFFFFFF,
};
#endif

#ifndef _ENUM_REVERSE_TYPE
#define _ENUM_REVERSE_TYPE
enum DataReverse {
    LR_BT = 0,
    LR_TB = 1,
    RL_BT = 2,
    RL_TB = 3,
    DRTMax = 4,

    DRTError = 0xFFFFFFFF,
};
#endif
		
#ifndef _ENUM_DATA_STATUS_
#define _ENUM_DATA_STATUS_
enum DataStatus {
    DSOriginal = 0,	//< 原始数据
    DSCorrected = 1,	//< 正常校正后的数据
    DSOnePtCorrect = 2,	//< 正在进行单点校正
    DSTwoPtsCorrectLow = 3,	//< 正在进行低温时的两点校正
    DSTwoPtsCorrectHigh = 4,	//< 正在进行高温时的两点校正
    DSMax = 5,

    DSError = 0xFFFFFFFF,
};
#endif

#ifndef _ENUM_LEN_TYPE_
#define _ENUM_LEN_TYPE_
enum LenType{
    Len3 = 0,
    Len6 = 1,
    Len12 = 2,
    Len24 = 3,
    Len48 = 4,
	LenMacro = 5,
    LenMax = 6,

	LenError = 0xFFFFFFFF,
};

enum LenAdjustMethod {
    LAMManual = 0,
    LAMElectric = 1,
    LAMContinuousMagnification = 2,
    LAMMax = 3,

    LAMError = 0xFF,
};
#endif

#ifndef _ENUM_BPDPTS_OP_FPGA
#define _ENUM_BPDPTS_OP_FPGA
enum BadPtsOpFPGA {
    BPOFCalc = 0x01,
    BPOFClear = 0x02,
    BPOFAddRectMax = 0x04,
    BPOFAddRectMin = 0x08,
    BPOFSave = 0x10,
    BPOFLoad = 0x20,
    BPOFError = 0xFFFF,
};
#endif

#ifndef _ENUM_MEASUREMENT_RANGE_
#define _ENUM_MEASUREMENT_RANGE_
#define TOTAL_MEAS_RANGE 4
enum MeasRange{
    MR0= 0,
    MR1= 1,
    MR2 = 2,
    MR3 = 3,
    MR4 = 4,
    MR5 = 5,
    MR6 = 6,
    MR7 = 7,
    MRMax = 8,

    MRError = 0xFFFFFFFF,
};
#endif

#ifndef _ENUM_TEMPERATURE_UNIT_
#define _ENUM_TEMPERATURE_UNIT_
enum TemperatureInit {
	Celsius = 0,
	Fahrenheit = 1,
};
#endif

#ifndef _ENUM_ANA_TYPE_LOCATION
#define _ENUM_ANA_TYPE_LOCATION
enum AnaTextLocation{
    ATLLeftTop = 0,
    ATLLeftBottom = 1,
    ATLLeft = 2,
    ATLRight = 3,
    ATLMax = 4,
    ATLError = 0xF,
};
#endif

#ifndef _ENUM_SHOW_TEXT_TYPE
#define _ENUM_SHOW_TEXT_TYPE
enum ShowTextType {
    STTNone = 0,		    //< 000
    STTMax = 1,			//< 001
    STTMaxMin = 3,  	//< 011
    STTMaxAvg = 5,	    //< 101
    STTMin = 2,			//< 010
    STTMinAvg = 6,		//< 110
    STTAvg = 4,			//< 100
    STTAll = 7,		    	//< 111

    STTError = 0xF,
};
#endif

#ifndef _ENUM_PALETTE_INDEX_
#define _ENUM_PALETTE_INDEX_
//#define COLOR_DEPTH 256
enum PALETTE {
    PINone = -1,
    PIIron = 1,					//< 铁红
    PIRainbow = 2,				//< 彩虹
    PIFeather = 3,				//< 医疗
    PIGray = 4,					//< 灰度
    PICWFeather = 5,             //< 测温医疗
    PIronReverse = 6,		//< 反转铁红
    PIRainbowReverse = 7,//< 反转彩虹
    PIFeatherReverse = 8,	//< 反转医疗
    PIGrayReverse = 9,		//< 反转灰度
    PICWFeatherReverse = 10,  //< 测温反转医疗
    PIMax = 11,

    PIError = 0xFFFFFFFF,
};
#endif

#ifndef _ENUM_ROTATE_ANGLE_
#define _ENUM_ROTATE_ANGLE_
enum RotationAngle{
    RANormal = 0,       //< 正常
    RARight90 = 1,	    //< 右转90度
    RALeft90 = 2,		//< 左转90度
    RARight180 = 3, 	//< 右转180度
    RALeft180 = 4,	    //< 左转180度
    RAMax = 5,

    RAError = 0xFFFFFFFF,
};
#endif

#ifndef _ENUM_IAMGING_TYPE_
#define _ENUM_IAMGING_TYPE_
enum ImagingType {
	ITAuto = 0,
	ITManual = 1,
};
#endif

#ifndef _ENUM_IMAGE_SACLE_FACTOR
#define _ENUM_IMAGE_SACLE_FACTOR
enum ImageZoomFactor {
    IZFOne = 1,
    IZFTwo = 2,
    IZFFour = 4,
    IZFEight = 8,
    IZFFifteen = 16,
    IZFMax = 17,

    IZFError = 0xFFFFFFFF,
};
#endif

//< 统一各分析的名称和缩写首字母为，点-P，直线-L，圆-E，框-R，多边形-A，折线-S
#ifndef _ENUM_P_ANA_TYPE_
#define _ENUM_P_ANA_TYPE_
enum AnaType {	
    PPoint = 0,	

    PLine = 10,
    PLineStart = 11,
    PLineEnd = 12,
    PLineCursorMoveX = 13,
    PLineCursorMoveY = 14,

    PEllipse = 20,	
    PEllipseBorderNW = 21,
    PEllipseBorderNE = 22,

    PRect = 30,
    PRectLT = 31,
    PRectT = 32,
    PRectRT = 33,
    PRectR = 34,
    PRectRB = 35,
    PRectB = 36,
    PRectLB = 37,
    PRectL = 38,

    PPoly = 40,
    PPolyBorder = 41,
    PPolyPoint = 42,

    PPolyline = 50,
    PPolylineBorder = 51,
    PPolylinePoint = 52,

    PWholeImage = 100,
    PMax = 101,
};
#endif  //< _ENUM_P_ANA_TYPE_

#ifndef _P_TIME_VAL_
#define _P_TIME_VAL_
typedef struct tagTimeVal{
    unsigned int tv_sec;         /* seconds */
    unsigned int tv_usec;        /* and microseconds */
}TimeVal_S, *PTimeVal_S;
#endif //< _P_TIME_VAL_

#ifndef _S_POINT_DEF_
#define _S_POINT_DEF_
typedef struct tagPPoint {
    unsigned short w_x, w_y;
    tagPPoint(){
        w_x = w_y = 0;
    }
    tagPPoint(unsigned short x, unsigned short y):w_x(x),w_y(y){}
}S_POINT;
#endif

#ifndef _S_AD_DEF_
#define _S_AD_DEF_
typedef struct tagPAD {
    unsigned short	w_x, w_y;
    unsigned short	w_ad;
    tagPAD() {
        w_x = w_y = w_ad = 0;
    }
}S_MAX_AD, S_MIN_AD;
#endif  //< _S_AD_DEF_

#ifndef _S_LINE_TBL_DEF_
#define _S_LINE_TBL_DEF_

typedef struct tagLineTbl {
    unsigned short	w_height;
    unsigned short	w_start, w_end;
    tagLineTbl() : w_height(0), w_start(0), w_end(0) {
    }
}S_LINE_TABLE, *P_S_LINE_TABLE;

#endif

#endif //< _P_COMMON_DEF_HEADER_
