#ifndef _P_NET_HEADER_
#define _P_NET_HEADER_

#include <string>
#include <list>
using namespace std;

#ifdef WIN32

#if (defined PNET_LIBRARY)
#define P_NET_API __declspec(dllexport) bool __stdcall
#else
#define P_NET_API __declspec(dllimport) bool __stdcall
#endif

#else
#define P_NET_API bool
#endif

#ifndef _DEFINE_NHANDLE_
typedef void *NHANDLE;
#define _DEFINE_NHANDLE_
#endif

#ifndef _P_NET_TCP_EVT_DEF_
#define _P_NET_TCP_EVT_DEF_
#define EVT_TCP_CONNECTED       0x0001
#define EVT_TCP_DISCONNECTED    0x0002
#define EVT_TCP_EXCEPTION       0x0003
#define EVT_TCP_RECV_DATA       0x0004
#endif

#ifndef _P_NET_DISCOVERY_EVT_DEF_
#define _P_NET_DISCOVERY_EVT_DEF_
#define EVT_DISCOVERY_CREATE_SUCCEED       0x0001
#define EVT_DISCOVERY_CREATE_FAILED        0x0002
#define EVT_DISCOVERY_EXCEPTION            0x0003
#define EVT_DISCOVERY_NORMAL_FOUND         0x0004
#define EVT_DISCOVERY_DIRECT_LINK_FOUND    0x0005
#endif

#ifndef _P_NET_TCP_RECV_DATA_DEF_
#define _P_NET_TCP_RECV_DATA_DEF_
struct TcpRecvDataParam_S{
    unsigned int client_id;     //For server: allocated > 0 when client connected. For client: 0.
    unsigned int recv_type;     //received events, EVT_TCP*...
    char        *p_recv_data;   //real data buffer when recv_type is EVT_TCP_RECV_DATA, otherwise NULL.
    unsigned int ui_size;       //real data buffer size or 0.
};
#endif

#ifndef _P_NET_DISCOVERY_RECV_DATA_DEF_
#define _P_NET_DISCOVERY_RECV_DATA_DEF_
struct DiscoveryRecvDataParam_S{
    unsigned int recv_type;
    std::string str_server_ip;     // When recv_type is EVT_DISCOVERY_FOUND, stores server start passed str_server_ip, otherwise empty.
    std::list<unsigned int> server_connect_ports_list;   // When recv_type is EVT_DISCOVERY_FOUND, stores server start passed connect ports, otherwise empty.
    unsigned int ui_msg_len;    // len for p_msg_data
    char *p_msg_data;    // When recv_type is EVT_DISCOVERY_FOUND, stores server start passed str_server_message, such as sn. otherwise other information or NULL.
};
#endif


/**
 * @brief callback function for complete net data received.
 * @param recv_data: received data struct.
 * @param p_user_data: user defined object.
 * @return bool: tmp unknown.
 */

#ifndef _P_NET_READ_CB_FUNC_DEF_
#define _P_NET_READ_CB_FUNC_DEF_
typedef bool ( *READ_CB_FUNC )( TcpRecvDataParam_S* recv_data, void *p_user_data );
#endif

#ifndef _P_NET_DISCOVERY_CB_FUNC_DEF_
#define _P_NET_DISCOVERY_CB_FUNC_DEF_
typedef bool ( *DISCOVERY_CB_FUNC )( DiscoveryRecvDataParam_S* recv_data, void *p_user_data );
#endif

/************************************************tcp server below***************************************************/

/**
 * @brief p_net_tcp_server_init
 * @param h
 * @return
 */
P_NET_API p_net_tcp_server_init( NHANDLE& h );

/**
 * @brief p_net_tcp_server_uninit
 * @param h
 * @return
 */
P_NET_API p_net_tcp_server_uninit( NHANDLE& h );

/**
 * @brief p_net_start_tcp_server
 * @param h
 * @param s_ip: server ip address.
 * @param i_port: server listened port.
 * @param p_read_cb_func: callback function when server received complete data from client.
 * @param p_user_data: user defined object.
 * @return
 */
P_NET_API p_net_start_tcp_server( NHANDLE h,
                                      const std::string str_local_ip,
                                      unsigned int ui_local_port,
                                      READ_CB_FUNC p_read_cb_func,
                                      void *p_user_data);

/**
 * @brief p_net_tcp_server_get_addr
 * @param h
 * @param server_ip
 * @param server_port
 * @return
 */
P_NET_API p_net_tcp_server_get_addr(NHANDLE h, std::string& str_server_ip, unsigned int& ui_server_port);

/**
 * @brief p_net_stop_server
 * @param h
 * @return
 */
P_NET_API p_net_stop_server( NHANDLE h );

/**
 * @brief p_net_send_data_to_client
 * @param h
 * @param h_client: destination client handle.
 * @param h_data_buf: data pointer to send.
 * @param u_buf_len: data length to send.
 * @return
 */
P_NET_API p_net_send_data_to_client_by_id( NHANDLE h, unsigned int ui_client_id, char* p_data, unsigned int ui_data_len );

/**
 * @brief p_net_get_tcp_server_addr
 * @param h
 * @param server_ip
 * @param server_port
 * @return
 */
P_NET_API p_net_server_get_client_addr_by_id(NHANDLE h, unsigned int ui_client_id, std::string &str_client_ip, unsigned int &ui_client_port);


/**
 * @brief p_net_server_disconnect_client_by_id
 * @param h
 * @param client_id
 * @return
 */
P_NET_API p_net_server_disconnect_client_by_id(NHANDLE h, unsigned int ui_client_id);

/**
 * @brief p_net_get_server_last_error
 * @param handle
 * @param error
 * @return
 */
P_NET_API p_net_get_server_last_error( NHANDLE handle, std::string &str_error );

/************************************************tcp client below***************************************************/

/**
 * @brief p_net_tcp_client_init
 * @param h
 * @return
 */
P_NET_API p_net_tcp_client_init( NHANDLE& h );

/**
 * @brief p_net_tcp_client_uninit
 * @param h
 * @return
 */
P_NET_API p_net_tcp_client_uninit( NHANDLE& h );


/**
 * @brief p_net_connect_server
 * @param h
 * @param s_server_ip: target server ip.
 * @param i_port: target server port.
 * @param p_read_cb: callback funtion when client received complete data from target server.
 * @param p_user_data: user defined object.
 * @return
 */
P_NET_API p_net_connect_server( NHANDLE h,
                                        const std::string str_server_ip,
                                        unsigned int ui_server_port,
                                        READ_CB_FUNC p_read_cb_func,
                                        void *p_user_data);

/**
 * @brief p_net_disconnect_server
 * @param h
 * @return
 */
P_NET_API p_net_disconnect_server( NHANDLE h );

/**
 * @brief p_net_send_data_to_server
 * @param h
 * @param h_data_buf
 * @param u_buf_len
 * @return
 */
P_NET_API p_net_send_data_to_server( NHANDLE h, char* p_data, unsigned int ui_data_len );

/**
 * @brief p_net_get_client_last_error
 * @param handle
 * @param error
 * @return
 */
P_NET_API p_net_get_client_last_error( NHANDLE handle, std::string &str_error );

/************************************************discovery server below***************************************************/

P_NET_API p_net_discovery_server_start( NHANDLE &h,
                                        const std::string str_server_ip,  // pass server connect ip.
                                        std::list<unsigned int> server_connect_port_list,  // pass server connect ports, appoint sequence for c-s use.
                                        unsigned int ui_discovery_port,
                                        const std::string str_server_msg, // pass some message you wished, such as sn.
                                        DISCOVERY_CB_FUNC p_discovery_cb_func,
                                        void* p_user_data);

P_NET_API p_net_discovery_server_finish( NHANDLE &h );

/************************************************discovery client below***************************************************/

P_NET_API p_net_discovery_client_start( NHANDLE &h, unsigned int ui_discovery_port, DISCOVERY_CB_FUNC p_discovery_cb_func, void* p_user_data );

P_NET_API p_net_discovery_broadcast( NHANDLE h, std::list<std::string> net_card_mac_list );

P_NET_API p_net_direct_link_discovery( NHANDLE h, std::list<std::string> net_card_mac_list );

P_NET_API p_net_discovery_client_finish( NHANDLE &h );

#endif
