#!/usr/bin/env python
#coding=utf-8
import rospy
from time import sleep
import threading
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from std_msgs.msg import Int32
from yd_cloudplatform.srv import *
import requests

if __name__ == '__main__':
    try:
        rospy.init_node('collection_data_node')
        # init
        rtsp_url = "rtsp://admin:123qweasd@192.168.1.67:554/h264/ch1/main/av_stream"
        snap_url = "http://127.0.0.1/index/api/getSnap"
        rospy.wait_for_service('/yida/internal/platform_cmd')
        client = rospy.ServiceProxy('/yida/internal/platform_cmd', CloudPlatControl)       
        # request = client(0,1,0,3000,[])
        # r = request.result
        # print("result:",r)
        #
        index = 0
        angle = 360
        rate = rospy.Rate(1) # 20hz
        while not rospy.is_shutdown():
            print("time tick...")
            # 1 call service
            call_result = 0
            while not call_result:
                request = client(0,1,0,angle * 100,[])
                call_result = request.result
                print("call result:",call_result)
            sleep(10)
            # 2 ffmpeg
            url_params = {}
            url_params["url"] = rtsp_url
            url_params["timeout_sec"] = 10
            url_params["expire_sec"] = 1
            url_params["secret"] = "035c73f7-bb6b-4889-a715-d9eb2d1925cc"
            r = requests.get(snap_url,params = url_params)
            img_data = r.content
            #print(r.headers)
            # print(html)
            with open("/home/li/bag/base/"+str(angle)+".jpg",'wb') as fp:
                fp.write(img_data)
            angle = angle - 1
            rate.sleep()
    except rospy.ROSInterruptException:
        pass