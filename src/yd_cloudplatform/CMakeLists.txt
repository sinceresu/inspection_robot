cmake_minimum_required(VERSION 2.8.3)
project(yd_cloudplatform)

## Compile as C++11, supported in ROS Kinetic and newer
 add_compile_options(-std=c++11)

option(ARM_BUILD_YD_C "option for arm build" ON)
option(HK_CAMERA_YD_C "option for camera type:hk/tb" ON)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
  tf
  cv_bridge
  param_server
)

find_package(OpenCV REQUIRED)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a exec_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a exec_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
 add_message_files(
   FILES
#   Message1.msg
#   Message2.msg
    hostcp_control.msg
 )

## Generate services in the 'srv' folder
 add_service_files(
   FILES
   CloudPlatControl.srv
   GetTemperature.srv
#   Service2.srv
 )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   std_msgs
 )

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a exec_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES yd_cloudplatform
  CATKIN_DEPENDS roscpp rospy std_msgs
#  DEPENDS system_lib
)

###########
## Build ##
###########
set(CMAKE_BUILD_TYPE DEBUG)

if (ARM_BUILD_YD_C)
  Message("ARM BUILD")
else()
  Message("X86 BUILD")
  if(HK_CAMERA_YD_C)
    set(CORE_LIBRARY ${PROJECT_SOURCE_DIR}/libs/libHCCore.so)
    set(hpr_LIBRARY ${PROJECT_SOURCE_DIR}/libs/libhpr.so)
    set(hcnetsdk_LIBRARY ${PROJECT_SOURCE_DIR}/libs/libhcnetsdk.so)
    set(PlayCtrl_LIBRARY ${PROJECT_SOURCE_DIR}/libs/libPlayCtrl.so)
    set(AudioRender_LIBRARY ${PROJECT_SOURCE_DIR}/libs/libAudioRender.so)
    set(SuperRender_LIBRARY ${PROJECT_SOURCE_DIR}/libs/libSuperRender.so)
  else()
    set(lua_5_1_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/liblua-5.1.so)
    set(p_ad_data_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp-ad-data.so)
    set(p_ad_temp_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp-ad-temp.so)
    set(p_compress_algorithm_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp_compress_algorithm.so)
    set(p_image_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp-image.so)
    set(p_imaging_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp-imaging.so)
    set(p_ir_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp-ir.so)
    set(p_log_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp_log.so)
    set(p_net_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp_net.so)
    set(p_net_device_LIBRARY ${PROJECT_SOURCE_DIR}/libs/tianboir/libp-net-device.so)
    link_directories(${OpenCV_LIBRARY_DIRS} libs/tianboir)
  endif()
endif()
## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
# add_library(${PROJECT_NAME}
#   src/${PROJECT_NAME}/yd_cloudplatform.cpp
# )

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
## With catkin_make all packages are built within a single CMake context
## The recommended prefix ensures that target names across packages don't collide
# add_executable(${PROJECT_NAME}_node src/yd_cloudplatform_node.cpp)


 add_executable(visible_cloudplat_node src/visible_ptz.cpp src/common.cpp)
 add_executable(infrared_cloudplat_node src/infrared_ptz.cpp src/common.cpp)
 add_executable(hostpc_cloudplat_node src/hostpc_ptz.cpp src/common.cpp)
 add_executable(camera_status_node src/camera_status.cpp src/common.cpp)
 target_link_libraries(visible_cloudplat_node
   ${catkin_LIBRARIES}
   ${OpenCV_LIBRARIES}
 )
 target_link_libraries(infrared_cloudplat_node
   ${catkin_LIBRARIES}
   ${OpenCV_LIBRARIES}
 )
 target_link_libraries(hostpc_cloudplat_node
   ${catkin_LIBRARIES}
   ${OpenCV_LIBRARIES}
 )
 target_link_libraries(camera_status_node
   ${catkin_LIBRARIES}
   ${OpenCV_LIBS}
 )
 add_dependencies(visible_cloudplat_node yd_cloudplatform_generate_messages_cpp)
 add_dependencies(infrared_cloudplat_node yd_cloudplatform_generate_messages_cpp)
 add_dependencies(hostpc_cloudplat_node yd_cloudplatform_generate_messages_cpp)
 add_dependencies(camera_status_node yd_cloudplatform_generate_messages_cpp)

if (ARM_BUILD_YD_C)
 Message("ARM BUILD")
else()
 Message("X86 BUILD")
 if(HK_CAMERA_YD_C)
  add_executable(cloudplatform_control_node src/cloudplatform_control.cpp src/serial_comm.cpp src/common.cpp)
  add_executable(cloudplatform_sdk_control_node src/cloudplatform_sdk_control.cpp src/common.cpp)
  target_link_libraries(cloudplatform_control_node
    ${catkin_LIBRARIES}
    ${OpenCV_LIBRARIES}
  )
 
  target_link_libraries(cloudplatform_sdk_control_node
    ${OpenCV_LIBS}
    ${CORE_LIBRARY} 
    ${hcnetsdk_LIBRARY}
    ${catkin_LIBRARIES}
    ${PlayCtrl_LIBRARY}
    ${AudioRender_LIBRARY}
    ${SuperRender_LIBRARY}
    ${Hpr_LIBRARY}
 )
 add_dependencies(cloudplatform_control_node yd_cloudplatform_generate_messages_cpp)
 add_dependencies(cloudplatform_sdk_control_node yd_cloudplatform_generate_messages_cpp)
 else()
 # tianboir  
 add_executable(tianboir_sdk_control_node src/tianboir_sdk_control.cpp)
 add_executable(tianboir_sdk_control_node_test src/tianboir_sdk_control_test.cpp src/common.cpp)
 target_link_libraries(tianboir_sdk_control_node
 ${catkin_LIBRARIES}
 ${OpenCV_LIBRARIES}
 ${hcnetsdk_LIBRARY}
 ${PlayCtrl_LIBRARY}
 ${AudioRender_LIBRARY}
 ${SuperRender_LIBRARY}
 ${Hpr_LIBRARY}
 ${lua_5_1_LIBRARY}
 ${p_ad_data_LIBRARY}
 ${p_ad_temp_LIBRARY}
 ${p_compress_algorithm_LIBRARY}
 ${p_image_LIBRARY}
 ${p_imaging_LIBRARY}
 ${p_ir_LIBRARY}
 ${p_log_LIBRARY}
 ${p_net_LIBRARY}
 ${p_net_device_LIBRARY}
#  lua-5.1
#  p-ad-data
#  p-ad-temp
#  p_compress_algorithm
#  p-image
#  p-imaging
#  p-ir
#  p_log
#  p_net
#  p-net-device
 lz4
)

target_link_libraries(tianboir_sdk_control_node_test
${catkin_LIBRARIES}
${OpenCV_LIBRARIES}
${hcnetsdk_LIBRARY}
${PlayCtrl_LIBRARY}
${AudioRender_LIBRARY}
${SuperRender_LIBRARY}
${Hpr_LIBRARY}
${lua_5_1_LIBRARY}
${p_ad_data_LIBRARY}
${p_ad_temp_LIBRARY}
${p_compress_algorithm_LIBRARY}
${p_image_LIBRARY}
${p_imaging_LIBRARY}
${p_ir_LIBRARY}
${p_log_LIBRARY}
${p_net_LIBRARY}
${p_net_device_LIBRARY}
# lua-5.1
# p-ad-data
# p-ad-temp
# p_compress_algorithm
# p-image
# p-imaging
# p-ir
# p_log
# p_net
# p-net-device
lz4
)
  add_dependencies(tianboir_sdk_control_node yd_cloudplatform_generate_messages_cpp)
  add_dependencies(tianboir_sdk_control_node_test yd_cloudplatform_generate_messages_cpp)
endif()
endif()

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables for installation
## See http://docs.ros.org/melodic/api/catkin/html/howto/format1/building_executables.html
# install(TARGETS ${PROJECT_NAME}_node
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark libraries for installation
## See http://docs.ros.org/melodic/api/catkin/html/howto/format1/building_libraries.html
install(TARGETS visible_cloudplat_node infrared_cloudplat_node hostpc_cloudplat_node camera_status_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  # RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

catkin_install_python(PROGRAMS
  scripts/isapi_ptz_node.py
  scripts/isapi.py
  scripts/rtsp_client.py
  scripts/rtsp_node.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark other files for installation (e.g. launch and bag files, etc.)
install(FILES
  launch/cloudplatform_correlation.launch
  launch/hk_nosdk.launch
  launch/robot_camera.urdf
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_yd_cloudplatform.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
