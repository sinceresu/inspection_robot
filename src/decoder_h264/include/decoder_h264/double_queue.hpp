#ifndef _DOUBLE_QUEUE_HPP_
#define _DOUBLE_QUEUE_HPP_

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <pthread.h>
#include <string>

#define QUEUE_SIZE 50

typedef enum
{
    Free_Queue,
    Work_Queue
}QueueType;

typedef struct CommonQueueNode
{
    unsigned int index;
    unsigned char *data;
    unsigned int size;
    struct CommonQueueNode *next;
}CommonQueueNode;

typedef struct CommonQueue
{
    int size;
    QueueType  type;
    CommonQueueNode *front;
    CommonQueueNode *rear;
}CommonQueue;

class double_queue
{
public:
    CommonQueue *m_free_queue;
    CommonQueue *m_work_queue;

private:
    pthread_mutex_t m_free_queue_mutex;
    pthread_mutex_t m_work_queue_mutex;

public:
    double_queue(/* args */);
    ~double_queue();

    void init_queue(CommonQueue *queue, QueueType type);
    void push_queue(CommonQueue *queue, CommonQueueNode *node);
    CommonQueueNode *pop_queue(CommonQueue *queue);

    void clear_queue(CommonQueue *queue);
    void free_node(CommonQueueNode *node);
    void reset_free_queue(CommonQueue *work_queue, CommonQueue *free_queue);
};

#endif