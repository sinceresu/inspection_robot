# yd_frontend

yd_frontend is a laser odometry, which uses Eigen and Ceres Solver to simplify code structure. This code is modified from A-LOAM. This code is clean and simple without complicated mathematical derivation and redundant operations.

**Modifier:** [Zhang Songpeng](429260570@qq.com)


## 1. Prerequisites
### 1.1 **Ubuntu** and **ROS**
Ubuntu 64-bit 16.04 or 18.04.
ROS Kinetic or Melodic. [ROS Installation](http://wiki.ros.org/ROS/Installation)


### 1.2. **Ceres Solver**
Follow [Ceres Installation](http://ceres-solver.org/installation.html).

### 1.3. **PCL**
Follow [PCL Installation](http://www.pointclouds.org/downloads/linux.html).


## 2. Velodyne VLP-16 Launch
roslaunch yd_frontend yd_laser_odometry.launch

## 3. Subscribed Topics
name:                                       type:                               frequnecy:          publish node:
/velodyne_points                            sensor_msgs::PointCloud2            10Hz                velodyne_nodelet
/AuxLocator/velodyne_cloud_2                sensor_msgs::PointCloud2            10Hz                AuxLocator
/AuxLocator/laser_cloud_sharp               sensor_msgs::PointCloud2            10Hz                AuxLocator
/AuxLocator/laser_cloud_less_sharp          sensor_msgs::PointCloud2            10Hz                AuxLocator
/AuxLocator/laser_cloud_flat                sensor_msgs::PointCloud2            10Hz                AuxLocator
/AuxLocator/laser_cloud_less_flat           sensor_msgs::PointCloud2            10Hz                AuxLocator
/AuxLocator/laser_remove_points             sensor_msgs::PointCloud2            10Hz                AuxLocator
## 4. Published Topics
name:                                       type:                                   frequnecy:
/AuxLocator/laser_odom_to_init              nav_msgs::Odometry                      10Hz
/AuxLocator/heartbeat                       diagnostic_msgs::DiagnosticArray        10Hz
/AuxLocator/velodyne_cloud_2                sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_cloud_sharp               sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_cloud_less_sharp          sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_cloud_flat                sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_cloud_less_flat           sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_cloud_corner_last         sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_cloud_surf_last           sensor_msgs::PointCloud2                10Hz
/AuxLocator/velodyne_cloud_3                sensor_msgs::PointCloud2                10Hz
/AuxLocator/laser_odom_path                 nav_msgs::Path                          10Hz
