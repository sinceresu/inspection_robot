#pragma once

#include <string>
#include <memory>
#include <vector>
#include <mutex>
#include <thread>


#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "ros/ros.h"
#include <sensor_msgs/Image.h>

#include "streaming_service_msgs/start_transcode.h"
#include "streaming_service_msgs/stop_transcode.h"



namespace  streaming_service{
class Streamer;

class TranscodeNode {
 public:
    enum TranscodeResult {
        SUCCESS = 0,
        IO_ERROR = -2,
        INVALID_PARAM = -4,
        ERROR = -5
    };
  enum TranscodeAction {
        START = 0,
        STOP = 1,
  };
  TranscodeNode();
  ~TranscodeNode();

  TranscodeNode(const TranscodeNode&) = delete;
  TranscodeNode& operator=(const TranscodeNode&) = delete;

  private:
    enum TranscodeState {
        IDLE = 0,
        TRANSCODING = 1,
    };

    void GetParameters();
    bool HandleStartTranscode(
        streaming_service_msgs::start_transcode::Request& request,
        streaming_service_msgs::start_transcode::Response& response);
        
    bool HandleStopTranscode(
        streaming_service_msgs::stop_transcode::Request& request,
        streaming_service_msgs::stop_transcode::Response& response);
    
    void SendImage(uint64_t timestamp_ms);
    
    void InitializeImages(const cv::Size& image_size);
    int Initialize(const cv::Size& image_size, uint64_t start_time_ms);   

    void Run();


    ::ros::NodeHandle node_handle_;

    std::vector<::ros::ServiceServer> service_servers_;


    ::ros::Subscriber infrared_temp_subscriber_;


    int state_ = TranscodeState::IDLE;

    std::string temperature_topic__;

    std::string streaming_topic_;
  
    std::string camera_id_;
    int streaming_port_;

    std::unique_ptr<Streamer> streamer_;
   cv::Mat bgr_img_;
    uint64_t start_timestamp_;
    int key_frame_interval_;
    int constant_rate_factor_;

    bool initialized_;
    bool stop_flag_;

    cv::Mat input_frame_;

    std::thread work_thread_;

    float colorize_min_celcius_;
    float colorize_max_celcius_;
  };
  
}
