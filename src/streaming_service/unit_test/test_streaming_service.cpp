
#include <stdio.h>
#include <thread> 

#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "tf2_eigen/tf2_eigen.h"
#include <tf_conversions/tf_eigen.h>
#include "tf2_ros/transform_broadcaster.h"

#include "gflags/gflags.h"
#include "glog/logging.h"



DEFINE_string(topics, "",

              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");



#include <memory>
using namespace std;
namespace streaming_service{
namespace {

void Streaming(int argc, char** argv) {
    ::ros::NodeHandle node_handle_;


  std::this_thread::sleep_for(std::chrono::milliseconds(100));

  }


  


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test_streaming_service.";

  ros::init(argc, argv, "test_streaming_service");
   ros::start();

  ::ros::spin();
  getchar();
 
}

