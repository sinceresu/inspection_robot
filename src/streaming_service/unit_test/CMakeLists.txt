cmake_minimum_required(VERSION 3.0.2)
project(test_streaming_service)


set(srcs
  test_streaming_service.cpp
)
add_executable(${PROJECT_NAME}
  ${srcs}
)
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
  glog
  gflags
  drm
  ${OpenCV_LIBRARIES}
  ${AVCODEC_LIBRARY}
  ${AVUTIL_LIBRARY}
  ${AVFORMAT_LIBRARY}
  ${SWSCALE_LIBRARY}
  ${AVFILTER_LIBRARY}
)
target_include_directories(${PROJECT_NAME}
  SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include
  ${catkin_INCLUDE_DIRS}
)
install(TARGETS ${PROJECT_NAME}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

set(srcs_test_streamer
  test_streamer.cpp
  ../src/streamer.cpp
)
add_executable(test_streamer
  ${srcs_test_streamer}
)
target_link_libraries(test_streamer
  ${catkin_LIBRARIES}
  glog
  gflags
  drm
  ${OpenCV_LIBRARIES}
  ${AVCODEC_LIBRARY}
  ${AVUTIL_LIBRARY}
  ${AVFORMAT_LIBRARY}
  ${SWSCALE_LIBRARY}
  ${AVFILTER_LIBRARY}
)
target_include_directories(test_streamer
  SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include
  ${catkin_INCLUDE_DIRS}
)
install(TARGETS test_streamer
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

set(srcs_test_streaming
  test_streaming.cpp
  ../src/streamer.cpp
)
add_executable(test_streaming
  ${srcs_test_streaming}
)
target_link_libraries(test_streaming
  ${catkin_LIBRARIES}
  glog
  gflags
  drm
  ${OpenCV_LIBRARIES}
  ${AVCODEC_LIBRARY}
  ${AVUTIL_LIBRARY}
  ${AVFORMAT_LIBRARY}
  ${SWSCALE_LIBRARY}
  ${AVFILTER_LIBRARY}
)
target_include_directories(test_streaming
  SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include
  ${catkin_INCLUDE_DIRS}
)
install(TARGETS test_streaming
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

set(srcs_test_codec
  test_codec.cpp
)
add_executable(test_codec
  ${srcs_test_codec}
)
target_link_libraries(test_codec
  ${catkin_LIBRARIES}
  glog
  gflags
  drm
  ${OpenCV_LIBRARIES}
)
target_include_directories(test_codec
  SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include
  ${catkin_INCLUDE_DIRS}
)
install(TARGETS test_codec
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

set(srcs_test_transcode
  test_transcode.cpp
)
add_executable(test_transcode
  ${srcs_test_transcode}
)
target_link_libraries(test_transcode
  ${catkin_LIBRARIES}
  glog
  gflags
  ${OpenCV_LIBRARIES}
)
target_include_directories(test_transcode
  SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include
  ${catkin_INCLUDE_DIRS}
)
install(TARGETS test_transcode
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
