#include "charging_room_service_node.hpp"

namespace charging_room_service
{

#define charging_pile_data_pub_msg "/charging/pile/data"
#define status_value_array_pub_msg "/charging/pile/data/array"
#define door_control_service_msg "/charging/room/door/service"
#define door_control_action_msg "/charging/room/door"

    CChargingRoomServiceNode::CChargingRoomServiceNode()
        : door_action(nh_, door_control_action_msg, boost::bind(&CChargingRoomServiceNode::door_executeCB, this, _1), false)
    {
        LaunchParam();
        LaunchPublishers();
        LaunchService();
        LaunchActions();
    }

    CChargingRoomServiceNode::~CChargingRoomServiceNode()
    {
    }

    void CChargingRoomServiceNode::LaunchParam()
    {
        ros::param::get("~robot_id", node_options_.robot_id);

        ros::param::get("~service_ip", charging_room_param.service_ip);
        ros::param::get("~service_port", charging_room_param.service_port);
        ros::param::get("~device_slave", charging_room_param.device_slave);
        ros::param::get("~door_timeout", charging_room_param.door_timeout);
    }

    void CChargingRoomServiceNode::LaunchPublishers()
    {
        charging_pile_data_publisher_ = nh_.advertise<charging_room_service_msgs::ChargingPileDataStamped>(charging_pile_data_pub_msg, 1);
        status_value_array_publisher_ = nh_.advertise<charging_room_service_msgs::StatusValueArray>(status_value_array_pub_msg, 1);
    }

    void CChargingRoomServiceNode::LaunchService()
    {
        door_control_service = nh_.advertiseService(door_control_service_msg,
                                                    &CChargingRoomServiceNode::door_control_service_Callback, this);
    }

    void CChargingRoomServiceNode::LaunchActions()
    {
        door_action.start();
    }

    bool CChargingRoomServiceNode::door_control_service_Callback(charging_room_service_msgs::DoorControl::Request &req,
                                                                 charging_room_service_msgs::DoorControl::Response &res)
    {
        bool control_ret = false;
        if (req.status == 0)
        {
            control_ret = charging_room_service.CloseDoor();
        }
        else if (req.status == 1)
        {
            control_ret = charging_room_service.OpenDoor();
        }
        else
        {
            ROS_ERROR("unknown cmd");
            res.result = -1;
            res.message = "unknown cmd";
            return false;
        }

        if (control_ret)
        {
            res.result = 0;
            res.message = "";
            return true;
        }

        res.result = -1;
        res.message = "control failed";

        return false;
    }

    void CChargingRoomServiceNode::door_executeCB(const charging_room_service_msgs::DoorGoalConstPtr &goal)
    {
        ROS_INFO_STREAM("door_executeCB goal:\n"
                        << *goal);

        // execution
        bool control_ret = false;
        if (goal->status == 0)
        {
            control_ret = charging_room_service.CloseDoor();
        }
        else if (goal->status == 1)
        {
            control_ret = charging_room_service.OpenDoor();
        }
        else
        {
            charging_room_service_msgs::DoorResult result;
            result.result = -1;
            result.text = "unknown cmd";
            // set the action state to aborted
            door_action.setAborted(result);
            ROS_ERROR("unknown cmd");
            return;
        }

        if (!control_ret)
        {
            charging_room_service_msgs::DoorResult result;
            result.result = -1;
            result.text = goal->status == 0 ? "close failed" : "open failed";
            // set the action state to aborted
            door_action.setAborted(result);
            ROS_ERROR("control failed");
            return;
        }

        ros::Time start_time = ros::Time::now();
        ros::Time attemp_end = start_time + ros::Duration(charging_room_param.door_timeout);
        // helper variables
        ros::Rate r(1);
        while (ros::ok())
        {
            // check that preempt has not been requested by the client
            if (door_action.isPreemptRequested() || !ros::ok())
            {
                charging_room_service_msgs::DoorResult result;
                result.result = 2;
                result.text = "preempted canceled";
                // set the action state to preempted
                door_action.setPreempted(result);
                break;
            }

            // check the action timed out
            if (ros::Time::now() > attemp_end)
            {
                charging_room_service_msgs::DoorResult result;
                result.result = -1;
                result.text = goal->status == 0 ? "close timeout" : "open timeout";
                // set the action state to aborted
                door_action.setAborted(result);
                ROS_ERROR("action timeout");
                break;
            }

            // check the action result
            {
                bool goal_result = false;
                if (goal->status == 0)
                {
                    if (charging_room_service.GetDoorStatus() == EDoorStatus::Close)
                    {
                        goal_result = true;
                    }
                }
                if (goal->status == 1)
                {
                    if (charging_room_service.GetDoorStatus() == EDoorStatus::Open)
                    {
                        goal_result = true;
                    }
                }
                if (goal_result)
                {
                    charging_room_service_msgs::DoorResult result;
                    result.result = 0;
                    result.text = goal->status == 0 ? "close done" : "open done";
                    // set the action state to succeeded
                    door_action.setSucceeded(result);
                    ROS_INFO("action done");
                    break;
                }
            }

            // feedback
            charging_room_service_msgs::DoorFeedback feedback;
            feedback.status = 1;
            feedback.text = goal->status == 0 ? "closing" : "opening";
            // publish the feedback
            door_action.publishFeedback(feedback);
            ROS_INFO("actioning");

            r.sleep();
        }
    }

    void CChargingRoomServiceNode::Run()
    {
        charging_room_service.Connect(charging_room_param.service_ip, charging_room_param.service_port, charging_room_param.device_slave);
        std::thread data_Thread = std::thread(std::bind(&CChargingRoomServiceNode::data_handle, this, nullptr));
        data_Thread.detach();
    }

    void CChargingRoomServiceNode::data_handle(void *p)
    {
        ros::Rate r(3);
        while (ros::ok())
        {
            std::vector<Key_Value_t> charging_pile_data;
            if (charging_room_service.QueryData(charging_pile_data))
            {
                charging_room_service_msgs::ChargingPileDataStamped data_msg;
                data_msg.header.frame_id = "charging_pile_" + std::to_string(node_options_.robot_id);
                data_msg.header.stamp = ros::Time::now();
                data_msg.charging_pile_data.door_status = (int)charging_room_service.GetDoorStatus();
                data_msg.charging_pile_data.touch_status = (int)charging_room_service.GetTouchStatus();
                charging_pile_data_publisher_.publish(data_msg);

                charging_room_service_msgs::StatusValueArray status_msg;
                status_msg.header.frame_id = "charging_pile_" + std::to_string(node_options_.robot_id);
                status_msg.header.stamp = ros::Time::now();
                for (std::vector<Key_Value_t>::iterator data = charging_pile_data.begin(); data != charging_pile_data.end(); data++)
                {
                    charging_room_service_msgs::KeyValue item;
                    item.key = data->key;
                    item.value = data->value;
                    status_msg.values.push_back(item);
                }
                status_value_array_publisher_.publish(status_msg);
            }

            ros::spinOnce();
            r.sleep();
        }
    }

}
