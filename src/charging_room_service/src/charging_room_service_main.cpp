#include <stdio.h>
#include <csignal>
#include "glog/logging.h"
#include "charging_room_service_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace charging_room_service
{
    void run()
    {
        CChargingRoomServiceNode charging_room_service_node;
        charging_room_service_node.Run();

        ROS_INFO("charging_room_service_node node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    utils_common_libs::init();

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("charging_room_service");

    ros::init(argc, argv, "charging_room_service");
    ros::Time::init();

    charging_room_service::run();

    ros::shutdown();

    return 0;
}