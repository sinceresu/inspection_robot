#include "Common.hpp"

namespace utils_common_libs
{
    static bool b_init = false;
    static bool b_keep_running = false;

    void sig_handler(int sig)
    {
        if (sig == SIGINT)
        {
            shutdown();
            printf("Interrupt signal (\"%d\") received. \r\n", sig);
        }
    }

    void init()
    {
        if (!b_init)
        {
            b_init = true;
            b_keep_running = true;
            signal(SIGINT, sig_handler);
        }
    }

    void shutdown()
    {
        b_init = false;
        b_keep_running = false;
    }

    bool is_ok()
    {
        return b_keep_running;
    }
}