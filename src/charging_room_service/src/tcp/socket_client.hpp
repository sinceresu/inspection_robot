#ifndef socket_client_HPP
#define socket_client_HPP

#pragma once
#include <thread>
#include <mutex>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include <unistd.h>
#include <string.h>

#define DATA_GET_HIGH_BYTE(data) (((data) >> 8) & 0xFF)
#define DATA_GET_LOW_BYTE(data) ((data)&0xFF)

namespace socket_client
{

    class CSocketClient
    {
    public:
        CSocketClient(std::string _ip, int _port);
        ~CSocketClient();

    private:
        static unsigned int counter_;

        std::string service_ip_;
        int service_port_;

        int socket_fd_;
        bool b_open_;

        std::mutex send_mtx_;
        std::mutex recv_mtx_;

    private:
        int Create();
        int Connect();
        int Close();
        int Recv(unsigned char *_buf, int &_len);
        int Send(const unsigned char *_buf, const unsigned int _len);

    public:
        int start();
        int stop();
        bool is_open();
        int send_data(const unsigned char *_buf, const unsigned int _len);
        int recv_data(unsigned char *_buf, int &_len);
    };

}

#endif