#include "asio_client.hpp"

/**
 * @brief
 *
 * Example
 * try
 * {
 *     CAsioClient c("127.0.0.1", 9898);
 *     c.start();
 *
 *     auto time_sent = std::chrono::steady_clock::now();
 *
 *     unsigned char data[5] = "test";
 *     c.write_data(data, 5);
 *
 *     for (;;)
 *     {
 *         if (!c.is_open())
 *         {
 *             sleep(3);
 *             c.start();
 *         }
 *
 *         unsigned char buf[128] = {0};
 *         int len = 128;
 *         c.read_data(buf, len);
 *         std::cout << buf << "\n";
 *     }
 *
 *     auto time_received = std::chrono::steady_clock::now();
 *
 *     std::cout << "Round trip time: ";
 *     std::cout << std::chrono::duration_cast<
 *                      std::chrono::microseconds>(
 *                      time_received - time_sent)
 *                      .count();
 *     std::cout << " microseconds\n";
 * }
 * catch (std::exception &e)
 * {
 *     std::cerr << "Exception: " << e.what() << "\n";
 * }
 *
 */

namespace asio_client
{

    CAsioClient::CAsioClient(const std::string &host, const int &port)
        : host_(host), service_(std::to_string(port)), b_open_(false)
    {
    }

    CAsioClient::~CAsioClient()
    {
    }

    void CAsioClient::run(std::chrono::steady_clock::duration timeout)
    {
        // Restart the io_context, as it may have been left in the "stopped" state
        // by a previous operation.
        io_context_.restart();

        // Block until the asynchronous operation has completed, or timed out. If
        // the pending asynchronous operation is a composed operation, the deadline
        // applies to the entire operation, rather than individual operations on
        // the socket.
        std::size_t n = io_context_.run_for(timeout);

        // If the asynchronous operation completed successfully then the io_context
        // would have been stopped due to running out of work. If it was not
        // stopped, then the io_context::run_for call must have timed out.
        if (!io_context_.stopped())
        {
            // Close the socket to cancel the outstanding asynchronous operation.
            socket_.close();

            // Run the io_context again until the operation completes.
            io_context_.run();
        }
    }

    int CAsioClient::start_connect(const std::string &host, const std::string &service,
                                   std::chrono::steady_clock::duration timeout)
    {
        // Resolve the host name and service to a list of endpoints.
        auto endpoints = tcp::resolver(io_context_).resolve(host, service);
        boost::asio::ip::tcp::endpoint m_ep(boost::asio::ip::address::from_string(host), atoi(service.c_str()));

        // Start the asynchronous operation itself. The lambda that is used as a
        // callback will update the error variable when the operation completes.
        boost::system::error_code error;
        // socket_.connect(m_ep, error);
        socket_.async_connect(m_ep,
                              [&](const boost::system::error_code &result_error)
                              {
                                  error = result_error;
                              });

        // Run the operation until it completes, or until the timeout.
        run(timeout);

        // Determine whether a connection was successfully established.
        // if (error)
        //     throw std::system_error(error);
        if (error)
        {
            stop();
            return -1;
        }
        b_open_ = true;

        return 0;
    }

    int CAsioClient::start_read(unsigned char *buf, int &len, std::chrono::steady_clock::duration timeout)
    {
        // Start the asynchronous operation. The lambda that is used as a callback
        // will update the error and n variables when the operation completes.
        boost::system::error_code error;
        std::size_t n = 0;
        // n = socket_.read_some(boost::asio::buffer(buf, len), error);
        socket_.async_read_some(boost::asio::buffer(buf, len),
                                [&](const boost::system::error_code &result_error,
                                    std::size_t result_n)
                                {
                                    error = result_error;
                                    n = result_n;
                                    len = result_n;
                                });

        // Run the operation until it completes, or until the timeout.
        run(timeout);

        // Determine whether the read completed successfully.
        // if (error)
        //     throw std::system_error(error);
        if (error)
        {
            stop();
            return -1;
        }

        return n;
    }

    int CAsioClient::start_write(const unsigned char *buf, const unsigned int len,
                                 std::chrono::steady_clock::duration timeout)
    {
        // Start the asynchronous operation itself. The lambda that is used as a
        // callback will update the error variable when the operation completes.
        boost::system::error_code error;
        std::size_t n = 0;
        // n = socket_.write_some(boost::asio::buffer(buf, len), error);
        socket_.async_write_some(boost::asio::buffer(buf, len),
                                 [&](const boost::system::error_code &result_error,
                                     std::size_t result_n)
                                 {
                                     error = result_error;
                                     n = result_n;
                                 });

        // Run the operation until it completes, or until the timeout.
        run(timeout);

        // Determine whether the read completed successfully.
        // if (error)
        //     throw std::system_error(error);
        if (error)
        {
            stop();
            return -1;
        }

        return n;
    }

    int CAsioClient::start()
    {
        return start_connect(host_, service_);
    }

    int CAsioClient::stop()
    {
        b_open_ = false;
        try
        {
            socket_.close();
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
        }

        return 0;
    }

    int CAsioClient::read_data(unsigned char *buf, int &len)
    {
        if (!is_open())
            return -1;

        read_mtx.lock();
        int n = start_read(buf, len);
        read_mtx.unlock();

        return n;
    }

    int CAsioClient::write_data(const unsigned char *buf, const unsigned int len)
    {
        if (!is_open())
            return -1;

        write_mtx.lock();
        int n = start_write(buf, len);
        write_mtx.unlock();

        return n;
    }

    bool CAsioClient::is_open()
    {
        return b_open_;
    }
}